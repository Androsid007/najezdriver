package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by X-cellent Technology on 21-11-2017.
 */

public class CollectCashModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("ride_breakdown")
    @Expose
    private RideBreakdown rideBreakdown;
    @SerializedName("fare_details")
    @Expose
    private ArrayList<FareDetail> fareDetails;

    public ArrayList<FareDetail> getFareDetails() {
        return fareDetails;
    }

    public void setFareDetails(ArrayList<FareDetail> fareDetails) {
        this.fareDetails = fareDetails;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public RideBreakdown getRideBreakdown() {
        return rideBreakdown;
    }

    public void setRideBreakdown(RideBreakdown rideBreakdown) {
        this.rideBreakdown = rideBreakdown;
    }



}
