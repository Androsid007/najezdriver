package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hp1 on 9/22/2017.
 */

public class UserRideData implements Serializable{


        @SerializedName("fname")
        @Expose
        private String fname;
        @SerializedName("chatid")
        @Expose
        private String chatid;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("show_phone")
        @Expose
        private String showPhone;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @SerializedName("rating")
        @Expose
        private String rating;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getChatid() {
        return chatid;
    }

    public void setChatid(String chatid) {
        this.chatid = chatid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getShowPhone() {
        return showPhone;
    }

    public void setShowPhone(String showPhone) {
        this.showPhone = showPhone;
    }



}



