package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by X-cellent Technology on 05-09-2017.
 */

public class OptionType {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("option_en")
    @Expose
    private String optionEn;
    @SerializedName("option_ar")
    @Expose
    private String optionAr;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOptionEn() {
        return optionEn;
    }

    public void setOptionEn(String optionEn) {
        this.optionEn = optionEn;
    }

    public String getOptionAr() {
        return optionAr;
    }

    public void setOptionAr(String optionAr) {
        this.optionAr = optionAr;
    }

}