package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by X-cellent Technology on 02-11-2017.
 */

public class LongCityList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("city_name_en")
    @Expose
    private String cityNameEn;
    @SerializedName("city_name_ar")
    @Expose
    private String cityNameAr;
    @SerializedName("location")
    @Expose
    private LongCityLocation longCityLocation;

    public boolean isSelceted() {
        return Selceted;
    }

    public void setSelceted(boolean selceted) {
        this.Selceted = selceted;
    }

    private boolean Selceted;

    public LongCityLocation getLongCityLocation() {
        return longCityLocation;
    }

    public void setLongCityLocation(LongCityLocation longCityLocation) {
        this.longCityLocation = longCityLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityNameEn() {
        return cityNameEn;
    }

    public void setCityNameEn(String cityNameEn) {
        this.cityNameEn = cityNameEn;
    }

    public String getCityNameAr() {
        return cityNameAr;
    }

    public void setCityNameAr(String cityNameAr) {
        this.cityNameAr = cityNameAr;
    }



}

