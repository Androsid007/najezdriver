package com.techconlabs.najezdriver.model;

/**
 * Created by hp on 6/23/2017.
 */

public class TodayTrip {

    private String name,time,amount,modeofpay;

    public TodayTrip(String name, String time, String amount, String modeofpay) {
        this.name = name;
        this.time = time;
        this.amount = amount;
        this.modeofpay = modeofpay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getModeofpay() {
        return modeofpay;
    }

    public void setModeofpay(String modeofpay) {
        this.modeofpay = modeofpay;
    }
}
