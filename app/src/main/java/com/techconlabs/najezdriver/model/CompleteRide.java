package com.techconlabs.najezdriver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 9/21/2017.
 */

public class CompleteRide implements Parcelable {
    @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("ride_id")
        @Expose
        private String rideId;
        @SerializedName("total_amount")
        @Expose
        private String totalAmount;
        @SerializedName("kilometers")
        @Expose
        private String kilometers;


    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    @SerializedName("minutes")
        @Expose
        private String minutes;

    public Double getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(Double remainAmount) {
        this.remainAmount = remainAmount;
    }

    @SerializedName("remain_amount")
        @Expose
        private Double remainAmount;
        @SerializedName("payment_method")
        @Expose
        private String paymentMethod;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getKilometers() {
        return kilometers;
    }

    public void setKilometers(String kilometers) {
        this.kilometers = kilometers;
    }




    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}