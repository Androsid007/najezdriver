package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hp1 on 9/18/2017.
 */

public class RideNotification implements Serializable {

    @SerializedName("notify_type")
    @Expose
    private String notifyType;
    @SerializedName("ride_data")
    @Expose
    private String rideData;
    @SerializedName("sound")
    @Expose
    private String sound;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("vibrate")
    @Expose
    private String vibrate;
    @SerializedName("message")
    @Expose
    private String message;



    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public String getRideData() {
        return rideData;
    }

    public void setRideData(String rideData) {
        this.rideData = rideData;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVibrate() {
        return vibrate;
    }

    public void setVibrate(String vibrate) {
        this.vibrate = vibrate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
