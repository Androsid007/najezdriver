package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 8/30/2017.
 */

public class VerifyOtp   {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("driverdata")
    @Expose
    private Driverdata driverdata;
    @SerializedName("vehicle_count")
    @Expose
    private Integer vehicleCount;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;


    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @SerializedName("balance")
    @Expose
    private String balance;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Driverdata getDriverdata() {
        return driverdata;
    }

    public void setDriverdata(Driverdata driverdata) {
        this.driverdata = driverdata;
    }

    public Integer getVehicleCount() {
        return vehicleCount;
    }

    public void setVehicleCount(Integer vehicleCount) {
        this.vehicleCount = vehicleCount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}