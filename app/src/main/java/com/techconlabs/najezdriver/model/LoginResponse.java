package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp on 6/29/2017.
 */

public class LoginResponse {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;

    public LoginResponse(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
