package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

@SerializedName("car_model")
@Expose
private String carModel;
@SerializedName("car_brand")
@Expose
private String carBrand;

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

public String getCarModel() {
return carModel;
}

public void setCarModel(String carModel) {
this.carModel = carModel;
}

public String getCarBrand() {
return carBrand;
}

public void setCarBrand(String carBrand) {
this.carBrand = carBrand;
}

}