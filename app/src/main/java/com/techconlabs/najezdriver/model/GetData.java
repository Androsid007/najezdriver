package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hp1 on 9/22/2017.
 */

public class GetData implements Serializable{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ride_data")
    @Expose
    private RideValue rideData;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RideValue getRideData() {
        return rideData;
    }

    public void setRideData(RideValue rideData) {
        this.rideData = rideData;
    }

}
