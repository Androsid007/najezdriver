
package com.techconlabs.najezdriver.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarType {

    @SerializedName("vehicle_count")
    @Expose
    private Integer vehicleCount;
    @SerializedName("register_vehicle_list")
    @Expose
    private List<RegisterVehicleList> registerVehicleList = null;
    @SerializedName("vehicle_list")
    @Expose
    private List<VehicleList> vehicleList;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getVehicleCount() {
        return vehicleCount;
    }

    public void setVehicleCount(Integer vehicleCount) {
        this.vehicleCount = vehicleCount;
    }

    public List<RegisterVehicleList> getRegisterVehicleList() {
        return registerVehicleList;
    }

    public void setRegisterVehicleList(List<RegisterVehicleList> registerVehicleList) {
        this.registerVehicleList = registerVehicleList;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<VehicleList> getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(List<VehicleList> vehicleList) {
        this.vehicleList = vehicleList;
    }
}
