package com.techconlabs.najezdriver.model;

/**
 * Created by X-cellent Technology on 03-10-2017.
 */

public class AllWallet {
    String Wallettype;
    String OrderNumber;
    String OrderDate;
    String TransferMoney;

    public String getWallettype() {
        return Wallettype;
    }

    public void setWallettype(String wallettype) {
        Wallettype = wallettype;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getTransferMoney() {
        return TransferMoney;
    }

    public void setTransferMoney(String transferMoney) {
        TransferMoney = transferMoney;
    }

    public AllWallet(String wallettype, String orderNumber, String orderDate, String transferMoney) {
        Wallettype = wallettype;
        OrderNumber = orderNumber;
        OrderDate = orderDate;
        TransferMoney = transferMoney;
    }
}
