package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 9/18/2017.
 */

public class RideData {

    @SerializedName("ride_id")
    @Expose
    private String rideId;
    @SerializedName("ride_data")
    @Expose
    private RideData rideData;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("message")
    @Expose
    private String message;

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public RideData getRideData() {
        return rideData;
    }

    public void setRideData(RideData rideData) {
        this.rideData = rideData;
    }
    @SerializedName("pickup_log")
    @Expose
    private String pickupLog;
    @SerializedName("drop_lat")
    @Expose
    private String dropLat;
    @SerializedName("drop_log")
    @Expose
    private String dropLog;
    @SerializedName("pickup_lat")
    @Expose
    private String pickupLat;

    public String getPickupLog() {
        return pickupLog;
    }

    public void setPickupLog(String pickupLog) {
        this.pickupLog = pickupLog;
    }

    public String getDropLat() {
        return dropLat;
    }

    public void setDropLat(String dropLat) {
        this.dropLat = dropLat;
    }

    public String getDropLog() {
        return dropLog;
    }

    public void setDropLog(String dropLog) {
        this.dropLog = dropLog;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    @SerializedName("notify_type")
    @Expose
    private String notifyType;

    public String getNotifyData() {
        return notifyData;
    }

    public void setNotifyData(String notifyData) {
        this.notifyData = notifyData;
    }

    @SerializedName("notify_data")
    @Expose
    private String notifyData;

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    @SerializedName("vehicle_id")
    @Expose
    private String vehicleId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}