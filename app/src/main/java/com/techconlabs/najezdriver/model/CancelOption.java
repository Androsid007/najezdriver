package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by X-cellent Technology on 05-09-2017.
 */

public class CancelOption {



    @SerializedName("options")
    @Expose
    private java.util.List<OptionType> options = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public List<OptionType> getOptions() {
        return options;
    }

    public void setOptions(List<OptionType> options) {
        this.options = options;
    }

}