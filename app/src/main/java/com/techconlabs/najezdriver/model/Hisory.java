package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by hp1 on 9/5/2017.
 */

public class Hisory {



        @SerializedName("history")
        @Expose
        private List<HistoryData> history = null;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("message")
        @Expose
        private String message;

        public List<HistoryData> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryData> history) {
        this.history = history;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
