package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by X-cellent Technology on 13-10-2017.
 */

public class UpdateDriverData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("emailid")
    @Expose
    private Object emailid;
    @SerializedName("fname")
    @Expose
    private Object fname;
    @SerializedName("mname")
    @Expose
    private String mname;
    @SerializedName("lname")
    @Expose
    private Object lname;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("mobile")
    @Expose
    private Object mobile;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("total_rides")
    @Expose
    private String totalRides;
    @SerializedName("can_driver_deliver")
    @Expose
    private String canDriverDeliver;
    @SerializedName("webblock")
    @Expose
    private String webblock;
    @SerializedName("referralcode")
    @Expose
    private String referralcode;
    @SerializedName("verified")
    @Expose
    private String verified;
    @SerializedName("rating")
    @Expose
    private String rating;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getEmailid() {
        return emailid;
    }

    public void setEmailid(Object emailid) {
        this.emailid = emailid;
    }

    public Object getFname() {
        return fname;
    }

    public void setFname(Object fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public Object getLname() {
        return lname;
    }

    public void setLname(Object lname) {
        this.lname = lname;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Object getMobile() {
        return mobile;
    }

    public void setMobile(Object mobile) {
        this.mobile = mobile;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTotalRides() {
        return totalRides;
    }

    public void setTotalRides(String totalRides) {
        this.totalRides = totalRides;
    }

    public String getCanDriverDeliver() {
        return canDriverDeliver;
    }

    public void setCanDriverDeliver(String canDriverDeliver) {
        this.canDriverDeliver = canDriverDeliver;
    }

    public String getWebblock() {
        return webblock;
    }

    public void setWebblock(String webblock) {
        this.webblock = webblock;
    }

    public String getReferralcode() {
        return referralcode;
    }

    public void setReferralcode(String referralcode) {
        this.referralcode = referralcode;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

}