package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DriverRegister {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("driverdata")
    @Expose
    private Driverdata driverdata;
    @SerializedName("vehicle_count")
    @Expose
    private Integer vehicleCount;
    @SerializedName("vehicle_list")
    @Expose
    private java.util.List<VehicleList> vehicleList;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Driverdata getDriverdata() {
        return driverdata;
    }

    public void setDriverdata(Driverdata driverdata) {
        this.driverdata = driverdata;
    }

    public Integer getVehicleCount() {
        return vehicleCount;
    }

    public void setVehicleCount(Integer vehicleCount) {
        this.vehicleCount = vehicleCount;
    }



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public java.util.List<VehicleList> getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(List<VehicleList> vehicleList) {
        this.vehicleList = vehicleList;
    }
}