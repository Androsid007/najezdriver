package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VehicleRegister  implements Serializable{

    @SerializedName("vehicle_sno")
    @Expose
    private String vehicleSno;
    @SerializedName("vehicle_plate_char")
    @Expose
    private String vehiclePlateChar;
    @SerializedName("vehicle_plate_num")
    @Expose
    private String vehiclePlateNum;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("car_model")
    @Expose
    private String carModel;
    @SerializedName("vehicle_year")
    @Expose
    private String vehicleYear;
    @SerializedName("vehicle_color")
    @Expose
    private String vehicleColor;
    @SerializedName("photograph")
    @Expose
    private String photograph;
    @SerializedName("residence")
    @Expose
    private String residence;
    @SerializedName("driver_license")
    @Expose
    private String driverLicense;
    @SerializedName("driver_license_expire")
    @Expose
    private String driverLicenseExpire;
    @SerializedName("vehicle_reg")
    @Expose
    private String vehicleReg;
    @SerializedName("vehicle_reg_expire")
    @Expose
    private String vehicleRegExpire;

    @SerializedName("vehicle_insur")
    @Expose
    private String vehicleInsur;
    @SerializedName("vehicle_insur_expire")
    @Expose
    private String vehicleInsurExpire;
    @SerializedName("auth_img")
    @Expose
    private String authImg;
    @SerializedName("tafweeth_img")
    @Expose
    private String tafweethImg;
    @SerializedName("tafweeth_expire")
    @Expose
    private String tafweethExpire;
    @SerializedName("iban")
    @Expose
    private String iban;
    @SerializedName("iban_bank")
    @Expose
    private String ibanBank;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVehicleSno() {
        return vehicleSno;
    }

    public void setVehicleSno(String vehicleSno) {
        this.vehicleSno = vehicleSno;
    }

    public String getVehiclePlateChar() {
        return vehiclePlateChar;
    }

    public void setVehiclePlateChar(String vehiclePlateChar) {
        this.vehiclePlateChar = vehiclePlateChar;
    }

    public String getVehiclePlateNum() {
        return vehiclePlateNum;
    }

    public void setVehiclePlateNum(String vehiclePlateNum) {
        this.vehiclePlateNum = vehiclePlateNum;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(String vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public String getPhotograph() {
        return photograph;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getDriverLicenseExpire() {
        return driverLicenseExpire;
    }

    public void setDriverLicenseExpire(String driverLicenseExpire) {
        this.driverLicenseExpire = driverLicenseExpire;
    }

    public String getVehicleReg() {
        return vehicleReg;
    }

    public void setVehicleReg(String vehicleReg) {
        this.vehicleReg = vehicleReg;
    }

    public String getVehicleRegExpire() {
        return vehicleRegExpire;
    }

    public void setVehicleRegExpire(String vehicleRegExpire) {
        this.vehicleRegExpire = vehicleRegExpire;
    }

    public String getVehicleInsur() {
        return vehicleInsur;
    }

    public void setVehicleInsur(String vehicleInsur) {
        this.vehicleInsur = vehicleInsur;
    }

    public String getVehicleInsurExpire() {
        return vehicleInsurExpire;
    }

    public void setVehicleInsurExpire(String vehicleInsurExpire) {
        this.vehicleInsurExpire = vehicleInsurExpire;
    }

    public String getAuthImg() {
        return authImg;
    }

    public void setAuthImg(String authImg) {
        this.authImg = authImg;
    }

    public String getTafweethImg() {
        return tafweethImg;
    }

    public void setTafweethImg(String tafweethImg) {
        this.tafweethImg = tafweethImg;
    }

    public String getTafweethExpire() {
        return tafweethExpire;
    }

    public void setTafweethExpire(String tafweethExpire) {
        this.tafweethExpire = tafweethExpire;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getIbanBank() {
        return ibanBank;
    }

    public void setIbanBank(String ibanBank) {
        this.ibanBank = ibanBank;
    }



}