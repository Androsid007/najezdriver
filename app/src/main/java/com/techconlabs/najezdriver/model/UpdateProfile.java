package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by X-cellent Technology on 13-10-2017.
 */

public class UpdateProfile {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("driverdata")
    @Expose
    private Driverdata driverdata;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Driverdata getDriverdata() {
        return driverdata;
    }

    public void setDriverdata(Driverdata driverdata) {
        this.driverdata = driverdata;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
