package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 10/11/2017.
 */

public class FamilyOption {

    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}