package com.techconlabs.najezdriver.model;

/**
 * Created by hp on 6/20/2017.
 */

public class CountryCode {

    String text;
    Integer imageId;
    public CountryCode(String text, Integer imageId){
        this.text=text;
        this.imageId=imageId;
    }

    public String getText(){
        return text;
    }

    public Integer getImageId(){
        return imageId;
    }
}
