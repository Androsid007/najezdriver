package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 9/5/2017.
 */

public class HistoryData  {

    @SerializedName("GPS_Destination_Point")
    @Expose
    private GPSDestinationPoint gPSDestinationPoint;
    @SerializedName("GPS_Starting_Point")
    @Expose
    private GPSStartingPoint gPSStartingPoint;
    @SerializedName("booking_time")
    @Expose
    private String bookingTime;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("final_price")
    @Expose
    private String finalPrice;
    @SerializedName("pickup_location")
    @Expose
    private String pickupLocation;
    @SerializedName("drop_location")
    @Expose
    private String dropLocation;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("userdata")
    @Expose
    private Userdata userdata;
    @SerializedName("trip_type")
    @Expose
    private String tripType;
    @SerializedName("ride_status")
    @Expose
    private String rideStatus;

    public GPSDestinationPoint getGPSDestinationPoint() {
        return gPSDestinationPoint;
    }

    public void setGPSDestinationPoint(GPSDestinationPoint gPSDestinationPoint) {
        this.gPSDestinationPoint = gPSDestinationPoint;
    }

    public GPSStartingPoint getGPSStartingPoint() {
        return gPSStartingPoint;
    }

    public void setGPSStartingPoint(GPSStartingPoint gPSStartingPoint) {
        this.gPSStartingPoint = gPSStartingPoint;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(String dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Userdata getUserdata() {
        return userdata;
    }

    public void setUserdata(Userdata userdata) {
        this.userdata = userdata;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getRideStatus() {
        return rideStatus;
    }

    public void setRideStatus(String rideStatus) {
        this.rideStatus = rideStatus;
    }

}