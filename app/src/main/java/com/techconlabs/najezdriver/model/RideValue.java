package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hp1 on 9/22/2017.
 */

public class RideValue  implements Serializable{

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("driverid")
    @Expose
    private String driverid;
    @SerializedName("pickup_location")
    @Expose
    private String pickupLocation;
    @SerializedName("drop_location")
    @Expose
    private String dropLocation;
    @SerializedName("ride_flag")
    @Expose
    private String rideFlag;
    @SerializedName("userdata")
    @Expose
    private UserRideData userdata;
    @SerializedName("pickup_lat")
    @Expose
    private String pickupLat;
    @SerializedName("pickup_lng")
    @Expose
    private String pickupLng;
    @SerializedName("drop_lat")
    @Expose
    private String dropLat;
    @SerializedName("drop_lng")
    @Expose
    private String dropLng;

    public String getDeliveryImg() {
        return deliveryImg;
    }

    public void setDeliveryImg(String deliveryImg) {
        this.deliveryImg = deliveryImg;
    }

    @SerializedName("delivery_img")
    @Expose
    private String deliveryImg;

    public String getDeliveryComment() {
        return deliveryComment;
    }

    public void setDeliveryComment(String deliveryComment) {
        this.deliveryComment = deliveryComment;
    }

    @SerializedName("delivery_comment")
    @Expose
    private String deliveryComment;

    public Integer getIsDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(Integer isDelivery) {
        this.isDelivery = isDelivery;
    }

    @SerializedName("isDelivery")
    @Expose
    private Integer isDelivery;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(String dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getRideFlag() {
        return rideFlag;
    }

    public void setRideFlag(String rideFlag) {
        this.rideFlag = rideFlag;
    }

    public UserRideData getUserdata() {
        return userdata;
    }

    public void setUserdata(UserRideData userdata) {
        this.userdata = userdata;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getPickupLng() {
        return pickupLng;
    }

    public void setPickupLng(String pickupLng) {
        this.pickupLng = pickupLng;
    }

    public String getDropLat() {
        return dropLat;
    }

    public void setDropLat(String dropLat) {
        this.dropLat = dropLat;
    }

    public String getDropLng() {
        return dropLng;
    }

    public void setDropLng(String dropLng) {
        this.dropLng = dropLng;
    }

}