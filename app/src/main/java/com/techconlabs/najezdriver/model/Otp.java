package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 8/29/2017.
 */

public class Otp {


           @SerializedName("Code")
            @Expose
           private Integer code;


        @SerializedName("MessageIs")
        @Expose
        private String messageIs;
        @SerializedName("valid")
        @Expose
        private String valid;
        @SerializedName("nvalid")
        @Expose
        private String nvalid;
        @SerializedName("Blocked")
        @Expose
        private String blocked;
        @SerializedName("Repeated")
        @Expose
        private String repeated;
        @SerializedName("lastuserpoints")
        @Expose
        private Integer lastuserpoints;
        @SerializedName("SMSNUmber")
        @Expose
        private Integer sMSNUmber;
        @SerializedName("totalcout")
        @Expose
        private Integer totalcout;
        @SerializedName("currentuserpoints")
        @Expose
        private Integer currentuserpoints;
        @SerializedName("totalsentnumbers")
        @Expose
        private Integer totalsentnumbers;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessageIs() {
        return messageIs;
    }

    public void setMessageIs(String messageIs) {
        this.messageIs = messageIs;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getNvalid() {
        return nvalid;
    }

    public void setNvalid(String nvalid) {
        this.nvalid = nvalid;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public String getRepeated() {
        return repeated;
    }

    public void setRepeated(String repeated) {
        this.repeated = repeated;
    }

    public Integer getLastuserpoints() {
        return lastuserpoints;
    }

    public void setLastuserpoints(Integer lastuserpoints) {
        this.lastuserpoints = lastuserpoints;
    }

    public Integer getSMSNUmber() {
        return sMSNUmber;
    }

    public void setSMSNUmber(Integer sMSNUmber) {
        this.sMSNUmber = sMSNUmber;
    }

    public Integer getTotalcout() {
        return totalcout;
    }

    public void setTotalcout(Integer totalcout) {
        this.totalcout = totalcout;
    }

    public Integer getCurrentuserpoints() {
        return currentuserpoints;
    }

    public void setCurrentuserpoints(Integer currentuserpoints) {
        this.currentuserpoints = currentuserpoints;
    }

    public Integer getTotalsentnumbers() {
        return totalsentnumbers;
    }

    public void setTotalsentnumbers(Integer totalsentnumbers) {
        this.totalsentnumbers = totalsentnumbers;
    }

}


