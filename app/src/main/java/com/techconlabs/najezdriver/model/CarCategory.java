package com.techconlabs.najezdriver.model;

/**
 * Created by hp on 6/22/2017.
 */

public class CarCategory {

    private String category,capacity;

    public CarCategory(String category, String capacity) {
        this.category = category;
        this.capacity = capacity;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }
}
