package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by X-cellent Technology on 21-11-2017.
 */

public class RideBreakdown {

    @SerializedName("amount_collected")
    @Expose
    private String amountCollected;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("total_fare")
    @Expose
    private String totalFare;
    @SerializedName("received_from_wallet")
    @Expose
    private String receivedFromWallet;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("cashcollected")
    @Expose
    private String cashcollected;
    @SerializedName("outstanding_balance")
    @Expose
    private String outstandingBalance;

    public String getAmountCollected() {
        return amountCollected;
    }

    public void setAmountCollected(String amountCollected) {
        this.amountCollected = amountCollected;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(String totalFare) {
        this.totalFare = totalFare;
    }

    public String getReceivedFromWallet() {
        return receivedFromWallet;
    }

    public void setReceivedFromWallet(String receivedFromWallet) {
        this.receivedFromWallet = receivedFromWallet;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCashcollected() {
        return cashcollected;
    }

    public void setCashcollected(String cashcollected) {
        this.cashcollected = cashcollected;
    }

    public String getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(String outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

}