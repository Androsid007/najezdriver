package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Driverdata {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("emailid")
    @Expose
    private String emailid;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("mname")
    @Expose
    private String mname;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("webstatus")
    @Expose
    private String webstatus;
    @SerializedName("referralcode")
    @Expose
    private String referralcode;

    public String getTotalRides() {
        return totalRides;
    }

    public void setTotalRides(String totalRides) {
        this.totalRides = totalRides;
    }

    @SerializedName("total_rides")
    @Expose
    private String totalRides;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @SerializedName("rating")
    @Expose
    private String rating;

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    @SerializedName("verified")
    @Expose
    private String verified;

    public String getWebblock() {
        return webblock;
    }

    public void setWebblock(String webblock) {
        this.webblock = webblock;
    }

    @SerializedName("webblock")
    @Expose
    private String webblock;

    public String getCanDriverDeliver() {
        return canDriverDeliver;
    }

    public void setCanDriverDeliver(String canDriverDeliver) {
        this.canDriverDeliver = canDriverDeliver;
    }

    @SerializedName("can_driver_deliver")
    @Expose
    private String canDriverDeliver;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getWebstatus() {
        return webstatus;
    }

    public void setWebstatus(String webstatus) {
        this.webstatus = webstatus;
    }

    public String getReferralcode() {
        return referralcode;
    }

    public void setReferralcode(String referralcode) {
        this.referralcode = referralcode;
    }

}