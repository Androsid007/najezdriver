package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.*;
import java.util.List;

/**
 * Created by X-cellent Technology on 02-11-2017.
 */

public class LongCityModel {

    @SerializedName("city_list")
    @Expose
    private ArrayList<LongCityList> cityList;

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;

    public List<SelectedCityList> getSelectedCityList() {
        return selectedCityList;
    }

    public void setSelectedCityList(List<SelectedCityList> selectedCityList) {
        this.selectedCityList = selectedCityList;
    }

    @SerializedName("selected_city_list")
    @Expose
    private java.util.List<SelectedCityList> selectedCityList = null;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public ArrayList<LongCityList> getCityList() {
        return cityList;
    }

    public void setCityList(ArrayList<LongCityList> cityList) {
        this.cityList = cityList;
    }

}
