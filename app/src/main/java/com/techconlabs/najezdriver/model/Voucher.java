package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 9/1/2017.
 */

public class Voucher {

    public VoucherDetail getData() {
        return data;
    }

    public void setData(VoucherDetail data) {
        this.data = data;
    }

    @SerializedName("data")
    @Expose
    private VoucherDetail data;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Integer status;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}