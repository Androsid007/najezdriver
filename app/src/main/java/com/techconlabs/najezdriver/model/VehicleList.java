package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VehicleList implements Serializable{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("list")
    @Expose
    private java.util.List<com.techconlabs.najezdriver.model.List> list = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public java.util.List<com.techconlabs.najezdriver.model.List> getList() {
        return list;
    }

    public void setList(java.util.List<com.techconlabs.najezdriver.model.List> list) {
        this.list = list;
    }

}