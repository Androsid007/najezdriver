package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hp1 on 8/13/2017.
 */

public class Data   {

    @SerializedName("description_en")
    @Expose
    private String descriptionEn;

    @SerializedName("city_name")
    @Expose
    private String cityNameEn;
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("bank_name")
    @Expose
    private String bankName;


    @SerializedName("card_number")
    @Expose
    private String cardNumber;
    @SerializedName("expire_month")
    @Expose
    private String expireMonth;
    @SerializedName("expire_year")
    @Expose
    private String expireYear;
    @SerializedName("card_type")
    @Expose
    private String cardType;
    @SerializedName("card_username")
    @Expose
    private String cardUsername;
    @SerializedName("card_cvv")
    @Expose
    private String cardCvv;
    @SerializedName("card_id")
    @Expose
    private String cardId;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpireMonth() {
        return expireMonth;
    }

    public void setExpireMonth(String expireMonth) {
        this.expireMonth = expireMonth;
    }

    public String getExpireYear() {
        return expireYear;
    }

    public void setExpireYear(String expireYear) {
        this.expireYear = expireYear;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardUsername() {
        return cardUsername;
    }

    public void setCardUsername(String cardUsername) {
        this.cardUsername = cardUsername;
    }

    public String getCardCvv() {
        return cardCvv;
    }

    public void setCardCvv(String cardCvv) {
        this.cardCvv = cardCvv;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCityNameEn() {
        return cityNameEn;
    }

    public void setCityNameEn(String cityNameEn) {
        this.cityNameEn = cityNameEn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

}