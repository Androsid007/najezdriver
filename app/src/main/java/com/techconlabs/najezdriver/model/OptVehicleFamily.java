package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by X-cellent Technology on 11-10-2017.
 */

public class OptVehicleFamily  implements Serializable {

    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("id")
    @Expose
    private String id;

    public OptVehicleFamily(String family, String id) {
        this.family = family;
        this.id = id;
    }

    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public boolean setSelected(boolean selected) {
        this.selected = selected;
        return selected;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}


