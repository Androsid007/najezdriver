package com.techconlabs.najezdriver.model;

/**
 * Created by X-cellent Technology on 05-09-2017.
 */

public class CancelRide {
    String popText;

    public String getPopText() {
        return popText;
    }

    public void setPopText(String popText) {
        this.popText = popText;
    }

    public CancelRide(String popText) {
        this.popText = popText;
    }
}
