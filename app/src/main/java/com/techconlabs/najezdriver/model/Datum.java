package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hp1 on 8/12/2017.
 */

public class Datum implements Serializable {

    @SerializedName("vehicle_id")
    @Expose
    private String vehicleId;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_color")
    @Expose
    private String vehicleColor;
    @SerializedName("vehicle_sno")
    @Expose
    private String vehicleSno;
    @SerializedName("vehicle_family_id")
    @Expose
    private String vehicleFamilyId;
    @SerializedName("vehicle_number")
    @Expose
    private String vehicleNumber;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("vehicle_manufacture_year")
    @Expose
    private String vehicleManufactureYear;

    @SerializedName("doc_val")
    @Expose
    private String docVal;
    @SerializedName("vehicle_logo")
    @Expose
    private String vehicleLogo;

    @SerializedName("opt_vehicle_family")
    @Expose

    private ArrayList<OptVehicleFamily> optVehicleFamily = null;
    @SerializedName("active")
    @Expose
    private String active;

    public Integer getOnlineFlag() {
        return onlineFlag;
    }

    public void setOnlineFlag(Integer onlineFlag) {
        this.onlineFlag = onlineFlag;
    }

    @SerializedName("online_flag")
    @Expose
    private Integer onlineFlag;

    public ArrayList<OptVehicleFamily> getOptVehicleFamily() {
        return optVehicleFamily;
    }

    public void setOptVehicleFamily(ArrayList<OptVehicleFamily> optVehicleFamily) {
        this.optVehicleFamily = optVehicleFamily;
    }
/* public List<FamilyOption> getOptVehicleFamily() {
        return optVehicleFamily;
    }

    public void setOptVehicleFamily(List<FamilyOption> optVehicleFamily) {
        this.optVehicleFamily = optVehicleFamily;
    }

    @SerializedName("opt_vehicle_family")
    @Expose
    private List<FamilyOption> optVehicleFamily = null;*/

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public String getVehicleSno() {
        return vehicleSno;
    }

    public void setVehicleSno(String vehicleSno) {
        this.vehicleSno = vehicleSno;
    }

    public String getVehicleFamilyId() {
        return vehicleFamilyId;
    }

    public void setVehicleFamilyId(String vehicleFamilyId) {
        this.vehicleFamilyId = vehicleFamilyId;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleManufactureYear() {
        return vehicleManufactureYear;
    }

    public void setVehicleManufactureYear(String vehicleManufactureYear) {
        this.vehicleManufactureYear = vehicleManufactureYear;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDocVal() {
        return docVal;
    }

    public void setDocVal(String docVal) {
        this.docVal = docVal;
    }

    public String getVehicleLogo() {
        return vehicleLogo;
    }

    public void setVehicleLogo(String vehicleLogo) {
        this.vehicleLogo = vehicleLogo;
    }
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @SerializedName("price")
    @Expose
    private String price;
    public String getRides() {
        return rides;
    }

    public void setRides(String rides) {
        this.rides = rides;
    }

    @SerializedName("rides")
    @Expose
    private String rides;





}