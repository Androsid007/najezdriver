package com.techconlabs.najezdriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by X-cellent Technology on 07-11-2017.
 */

public class ColorList {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("colorlist")
    @Expose
    private ArrayList<String> colorlist = null;

    public ArrayList<String> getColorlist() {
        return colorlist;
    }

    public void setColorlist(ArrayList<String> colorlist) {
        this.colorlist = colorlist;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }



}
