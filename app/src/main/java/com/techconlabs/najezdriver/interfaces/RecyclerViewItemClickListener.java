package com.techconlabs.najezdriver.interfaces;

/**
 * Created by Dell on 8/9/2017.
 */

public interface RecyclerViewItemClickListener {
    public void onItemClickListener(int position);
}
