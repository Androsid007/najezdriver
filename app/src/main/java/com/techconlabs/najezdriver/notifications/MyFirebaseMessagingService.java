package com.techconlabs.najezdriver.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.activity.AddVechileInformationActivity;
import com.techconlabs.najezdriver.activity.CarCategoryActivity;
import com.techconlabs.najezdriver.activity.HomeActivity;
import com.techconlabs.najezdriver.activity.SignInActivity;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.RideData;
import com.techconlabs.najezdriver.model.RideNotification;
import com.techconlabs.najezdriver.utils.NotificationUtility;

import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private RideNotification rideNotification;
    private Notification notification;
    private RideData rideIdData;
    NotificationUtility notificationUtils;


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> params = remoteMessage.getData();
            JSONObject object = new JSONObject(params);
            Log.e("JSON_OBJECT", object.toString());
            final RideNotification rideNote = new Gson().fromJson(object.toString(), RideNotification.class);
            rideIdData = new Gson().fromJson(rideNote.getRideData(), RideData.class);
            rideIdData.setMessage(rideNote.getMessage());
            rideIdData.setTitle(rideNote.getTitle());
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(getApplicationContext(), message +" received", Toast.LENGTH_LONG).show();
                    if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("user_cancel_ride")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {
                            sendNotification(getApplicationContext(),rideIdData.getMessage(),rideIdData.getTitle());
                            Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("cancelRide", true);
                         //   ((AppCompatActivity)getApplicationContext()).getSupportActionBar().show();
                            notificationIntent.putExtras(bundle);
                            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(notificationIntent);

                        }
                        else{


                        }
                    } else if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("vehicle_block")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {
                            if (SharedPreferenceManager.getVechileId(getApplicationContext()).equals(rideIdData.getVehicleId()))
                            {  Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("vechileBlock", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);
                            } else {
                                Intent myIntent = new Intent(getApplicationContext(), CarCategoryActivity.class);
                                myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(myIntent);

                            }
                        }
                         else {
                            sendNotification(getApplicationContext(),rideIdData.getMessage(),rideIdData.getTitle());
                              SharedPreferenceManager.setOnLineStatus(getApplicationContext(), false);  // showNotificationMessage(MyFirebaseMessagingService.this,rideIdData.);


                        }

                    } else if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("vehical_unblock")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {
                            Intent notificationIntent = new Intent(getApplicationContext(), CarCategoryActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("vechileUnblock", true);
                            notificationIntent.putExtras(bundle);
                            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(notificationIntent);
                        }
                        else{
                            sendNotification(getApplicationContext(),rideIdData.getMessage(),rideIdData.getTitle());

                        }
                    } else if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("deact_user")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {

                            if (SharedPreferenceManager.getLoggin(getApplicationContext())) {

                                Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("deactUser", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);
                            } else {
                                Intent notificationIntent = new Intent(getApplicationContext(), SignInActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("deactUser", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);
                            }
                        }else{
                            sendNotification(getApplicationContext(),rideIdData.getMessage(),rideIdData.getTitle());

                        }
                    } else if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("block_user")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {

                            int listSize = SharedPreferenceManager.getCarListSize(getApplicationContext());

                            if (SharedPreferenceManager.getLoggin(getApplicationContext()) && listSize > 0) {
                                Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("blockUser", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);
                            } else if (SharedPreferenceManager.getLoggin(getApplicationContext()) && listSize == 0) {
                                Intent notificationIntent = new Intent(getApplicationContext(), AddVechileInformationActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("unblockUser", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);

                            } else {
                                Intent notificationIntent = new Intent(getApplicationContext(), SignInActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("blockUser", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);
                            }
                        }else{
                            sendNotification(getApplicationContext(),rideIdData.getMessage(),rideIdData.getTitle());

                        }
                    } else if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("unblock_user")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {

                            int listSize = SharedPreferenceManager.getCarListSize(getApplicationContext());
                            if (SharedPreferenceManager.getLoggin(getApplicationContext()) && listSize != 0) {
                                sendLocationBroadcast();


                            } else if (SharedPreferenceManager.getLoggin(getApplicationContext()) && listSize == 0) {
                                Intent notificationIntent = new Intent(getApplicationContext(), AddVechileInformationActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("unblockUser", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);

                            } else {
                                Intent notificationIntent = new Intent(getApplicationContext(), SignInActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("unblockUser", true);
                                notificationIntent.putExtras(bundle);
                                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(notificationIntent);
                            }
                        }
                        else{
                            sendNotification(getApplicationContext(),rideIdData.getMessage(),rideIdData.getTitle());


                        }

                    } else if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("activate_user")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {

                            Intent notificationIntent = new Intent(getApplicationContext(), SignInActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("activeUser", true);
                            notificationIntent.putExtras(bundle);
                            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(notificationIntent);
                        }else{
                            sendNotification(getApplicationContext(),rideIdData.getMessage(),rideIdData.getTitle());


                        }
                    } else if (rideIdData.getNotifyType() != null && rideIdData.getNotifyType().equals("transfer_money")) {
                        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {
                            sendNotification(getApplicationContext(), rideIdData.getMessage(), rideIdData.getTitle());
                        }
                        else{
                            sendNotification(getApplicationContext(), rideIdData.getMessage(), rideIdData.getTitle());
                        }

                    } else {
                        Intent notificationIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("value", rideNote);
                        notificationIntent.putExtras(bundle);
                        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(notificationIntent);
                    }

                }
            });

            Log.d(TAG, "Message data payload: " + rideNote.getRideData());
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }
        }

    }

    private void sendNotificationwithRide(String messageBody, String title, RideData rideData) {

       /* //Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Bundle bundle = new Bundle();
       // bundle.putSerializable(RideData"", rideData);
        intent.putExtras(bundle);*/
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationManager.IMPORTANCE_HIGH);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void sendLocationBroadcast() {

            Intent intent = new Intent("NazejRideDriver");
            Bundle bundle = new Bundle();
            bundle.putBoolean("unblockUser", true);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent.putExtras(bundle));

    }


    private void scheduleJob() {

    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String title) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_home_white_24dp)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void sendNotification(Context context,String messageBody, String title) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationManager.IMPORTANCE_HIGH);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

}