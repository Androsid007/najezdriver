package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.model.CancelOption;

/**
 * Created by X-cellent Technology on 05-09-2017.
 */

public class CancelledRideAdapter extends RecyclerView.Adapter<CancelledRideAdapter.DataViewHolder> {
    CancelOption cancelRides;
    Context context;
    RidePositionCallback ridePosition;

    public CancelledRideAdapter(CancelOption cancelRides, Context context,RidePositionCallback ridePosition) {
        this.cancelRides = cancelRides;
        this.context = context;
     //   this.ridePosition = (RidePositionCallback) context;
        this.ridePosition=ridePosition;

    }

    LayoutInflater inflater;
    private int lastCheckedPosition = -1;


    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.radio_btn_layout, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CancelledRideAdapter.DataViewHolder holder, int position) {
        int cancel_ride = cancelRides.getOptions().size();
        holder.radioText.setText(cancelRides.getOptions().get(position).getOptionEn());
        holder.raionBtn.setChecked(position == lastCheckedPosition);


    }

    @Override
    public int getItemCount() {
        return cancelRides.getOptions().size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        private RadioButton raionBtn;
        private TextView radioText;

        public DataViewHolder(View itemView) {
            super(itemView);

            radioText = (TextView) itemView.findViewById(R.id.pop_text);
            raionBtn = (RadioButton) itemView.findViewById(R.id.pop_radio);
            raionBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  radioButton.setBackgroundColor(context.getResources().getColor(R.color.checkout));
                    lastCheckedPosition = getAdapterPosition();
                    notifyItemRangeChanged(0, cancelRides.getOptions().size());
                    ridePosition.RidePos(lastCheckedPosition);

                }
            });
        }
    }

    public interface RidePositionCallback {
        public void RidePos(int pos);
    }
}
