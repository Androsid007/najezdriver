package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.model.Hisory;

/**
 * Created by hp on 12/7/2016.
 */

public class TripTwoAdapter extends RecyclerView.Adapter<TripTwoAdapter.DataObjectHolder> {
    private static String LOG_TAG = "TopFiveCustomer";
    private Hisory todayTripArrayList;
    private static MyClickListener myClickListener;
    private static Context context;
    private  String  unixTimeStamp;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView userName, time, amt, paymode;
        private ImageView userImage;


        public DataObjectHolder(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.nameTxt);
            time = (TextView) itemView.findViewById(R.id.timeTxt);
            userImage = (ImageView) itemView.findViewById(R.id.userImg);
            amt = (TextView) itemView.findViewById(R.id.amountTxt);
            paymode = (TextView) itemView.findViewById(R.id.paymode);

            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
           // Toast.makeText(v.getContext(), "Clicked" + position, Toast.LENGTH_LONG).show();
         /*   Intent intent = new Intent(context, VechileInformationActivity.class);
            context.startActivity(intent);*/
        }
    }


    public TripTwoAdapter(Hisory myDataset, Context con) {
        context = con;
        todayTripArrayList = myDataset;
    }


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trip_two_recyclerview_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        if(todayTripArrayList.getHistory().get(position).getUserdata()!= null)
        {
            holder.userName.setText(todayTripArrayList.getHistory().get(position).getUserdata().getFname());
            Picasso.with(context).load(todayTripArrayList.getHistory().get(position).getUserdata().getPhoto()).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).resize(1080, 720).centerInside().into(holder.userImage);

        }
        unixTimeStamp = todayTripArrayList.getHistory().get(position).getBookingTime();
     holder.time.setText(CommonUtility.getFormattedDate(unixTimeStamp));
         holder.amt.setText(todayTripArrayList.getHistory().get(position).getFinalPrice());
        holder.paymode.setText(todayTripArrayList.getHistory().get(position).getPaymentMethod());
    }


    @Override
    public int getItemCount() {
        return todayTripArrayList.getHistory().size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
