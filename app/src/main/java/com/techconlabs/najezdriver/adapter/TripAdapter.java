package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.model.TodayTrip;

import java.util.ArrayList;

/**
 * Created by hp on 12/7/2016.
 */

public class TripAdapter extends RecyclerView
        .Adapter<TripAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "TopFiveCustomer";
    private ArrayList<TodayTrip> todayTripArrayList;
    private static MyClickListener myClickListener;
    private static Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        private TextView userName, time, amt, paymode;


        public DataObjectHolder(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.nameTxt);
            time = (TextView) itemView.findViewById(R.id.timeTxt);
            amt = (TextView) itemView.findViewById(R.id.amountTxt);
            paymode = (TextView) itemView.findViewById(R.id.paymode);

            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
           /* int position = getLayoutPosition();
            Toast.makeText(v.getContext(), context.getString(R.string.clicke) + position, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, VechileInformationActivity.class);
            context.startActivity(intent);*/
        }
    }


    public TripAdapter(ArrayList<TodayTrip> myDataset, Context con) {
        context = con;
        todayTripArrayList = myDataset;
    }


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trip_recyclerview_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        TodayTrip todayTrip = todayTripArrayList.get(position);
        holder.userName.setText(todayTrip.getName());
        holder.time.setText(todayTrip.getTime());
        holder.amt.setText(todayTrip.getAmount());
        holder.paymode.setText(todayTrip.getModeofpay());
    }

    public void addItem(TodayTrip dataObj, int index) {
        todayTripArrayList.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        todayTripArrayList.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return todayTripArrayList.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
