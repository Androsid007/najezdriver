package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.model.Transaction;

import java.util.List;

/**
 * Created by X-cellent Technology on 03-10-2017.
 */

public class CreditWalletAdapter extends RecyclerView.Adapter<CreditWalletAdapter.DataViewHolder> {

    Context mcontext;
    List<Transaction> transactions;
    String unixTimeStamp;

    public CreditWalletAdapter(Context mcontext, List<Transaction> transactions) {
        this.mcontext = mcontext;
        this.transactions = transactions;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mcontext).inflate(R.layout.all_wallet_cardview, parent, false);
        return new DataViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        if (transactions.get(position).getTransactionType().equals("CREDIT")) {
            holder.walletType.setTextColor(mcontext.getResources().getColor(R.color.green));
            holder.walletType.setText(transactions.get(position).getTransactionType());
            holder.walletImage.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.credit_new));
        }
        holder.orderType.setText(transactions.get(position).getDescription());
        holder.transferMoney.setText(transactions.get(position).getAmount());
        unixTimeStamp = transactions.get(position).getTransactionDatetime();

        long timestamp = Long.parseLong(unixTimeStamp) * 1000L;
        holder.orderdateTime.setText(CommonUtility.getDate(timestamp));

    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        private TextView walletType, orderType, orderdateTime, transferMoney;
        private ImageView walletImage;

        public DataViewHolder(View itemView) {
            super(itemView);
            walletType = (TextView) itemView.findViewById(R.id.walletTypeTv);
            orderType = (TextView) itemView.findViewById(R.id.ordertypeTv);
            orderdateTime = (TextView) itemView.findViewById(R.id.dateTimeTv);
            transferMoney = (TextView) itemView.findViewById(R.id.moneytransferTv);
            walletImage = (ImageView) itemView.findViewById(R.id.walletimg);
        }
    }
}
