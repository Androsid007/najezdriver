package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.interfaces.OnCheckedListner;
import com.techconlabs.najezdriver.model.OptVehicleFamily;

import java.util.List;

/**
 * Created by X-cellent Technology on 11-10-2017.
 */

public class VehcileTypeAdapter extends RecyclerView.Adapter<VehcileTypeAdapter.DataViewHolder> {

    Context mContext;
    List<OptVehicleFamily> vehicleFamilies;
    OnCheckedListner onCheckedListner;

    public VehcileTypeAdapter(Context mContext, List<OptVehicleFamily> vehicleFamilies, OnCheckedListner onCheckedListner) {
        this.mContext = mContext;
        this.vehicleFamilies = vehicleFamilies;
        this.onCheckedListner = onCheckedListner;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_type_cardview, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.cartypeTv.setText(vehicleFamilies.get(position).getFamily());
        holder.cartypeCb.setChecked(vehicleFamilies.get(position).isSelected());
        holder.cartypeCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onCheckedListner.OnChage(position, isChecked);
            }
        });

    }

    @Override
    public int getItemCount() {
        return vehicleFamilies.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        private TextView cartypeTv;
        private CheckBox cartypeCb;

        public DataViewHolder(View itemView) {
            super(itemView);
            cartypeTv = (TextView) itemView.findViewById(R.id.cartypeTv);
            cartypeCb = (CheckBox) itemView.findViewById(R.id.cartypeCb);
        }
    }
}
