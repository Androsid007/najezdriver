package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.model.Datum;

import java.util.ArrayList;

/**
 * Created by hp1 on 8/13/2017.
 */

public class RateCardAdapter extends RecyclerView
        .Adapter<RateCardAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "CarCategory";
    private ArrayList<Datum> rateCardList;
    private static Context context;
    private static RateCardAdapter.MyClickListener myClickListener;


    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        private TextView remainRide, purchaseRide;


        public DataObjectHolder(View itemView) {
            super(itemView);
            remainRide = (TextView) itemView.findViewById(R.id.remainRide);
            purchaseRide = (TextView) itemView.findViewById(R.id.priceRide);


            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
             /* Intent intent = new Intent(context, HomeActivity.class);
            context.startActivity(intent);*/


        }

    }


    public RateCardAdapter(Context con, ArrayList<Datum> myDataset) {
        this.context = con;
        this.rateCardList = myDataset;
    }


    @Override
    public RateCardAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.purchase_ride, parent, false);

        RateCardAdapter.DataObjectHolder dataObjectHolder = new RateCardAdapter.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(RateCardAdapter.DataObjectHolder holder, int position) {
        holder.purchaseRide.setText(rateCardList.get(position).getPrice() + " " + context.getString(R.string.sar));
        holder.remainRide.setText(context.getString(R.string.get) + " " + rateCardList.get(position).getRides() + " " + context.getString(R.string.rides));

    }


    @Override
    public int getItemCount() {
        return rateCardList.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);


    }


}
