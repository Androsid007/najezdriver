package com.techconlabs.najezdriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.model.List;

import java.util.ArrayList;

/**
 * Created by hp on 6/20/2017.
 */

public class CustomModelSpinnerAdapter extends ArrayAdapter<com.techconlabs.najezdriver.model.List> {
    int groupid;
    Activity context;
    ArrayList<com.techconlabs.najezdriver.model.List> list;
    LayoutInflater inflater;

    public CustomModelSpinnerAdapter(Activity context, int groupid, int id, java.util.List<List>
            list) {
        super(context, id, list);
        this.context = context;
        this.list = (ArrayList<List>) list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid = groupid;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupid, parent, false);
        TextView textView = (TextView) itemView.findViewById(R.id.txt);
        textView.setTextColor(context.getResources().getColor(R.color.grey));
        textView.setTextSize(15);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "font/roboto_light.ttf");
        textView.setTypeface(tf);

        textView.setText(list.get(position).getCarBrand() + " " + list.get(position).getCarModel());
        if (position != 0) {
            textView.setTextColor(context.getResources().getColor(R.color.grey));
        }
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup
            parent) {
        return getView(position, convertView, parent);
    }
}
