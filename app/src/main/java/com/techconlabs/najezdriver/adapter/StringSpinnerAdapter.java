package com.techconlabs.najezdriver.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;

import java.util.ArrayList;

/**
 * Created by hp on 6/20/2017.
 */

public class StringSpinnerAdapter extends ArrayAdapter<String> {
    int groupid;
    Activity context;
    ArrayList<String> list;
    LayoutInflater inflater;

    public StringSpinnerAdapter(Activity context, int groupid, int id, ArrayList<String> list) {
        super(context, id, list);
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid = groupid;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupid, parent, false);
        TextView textView = (TextView) itemView.findViewById(R.id.txt);
        textView.setText(list.get(position));
        textView.setTextSize(15);
        textView.setTextColor(context.getResources().getColor(R.color.grey));
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "font/roboto_light.ttf");
        textView.setTypeface(tf);


        if (position != 0) {
            textView.setTextColor(context.getResources().getColor(R.color.grey));
        }
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
       View v =  getView(position, convertView, parent);

        return v;
    }
}
