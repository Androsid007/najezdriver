package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.model.FareDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by X-cellent Technology on 21-11-2017.
 */

public class CollectCashAdapter extends RecyclerView.Adapter<CollectCashAdapter.DataViewHolder> {
    Context context;
    List<FareDetail> fareDetails;

    public CollectCashAdapter(Context context, List<FareDetail> fareDetails) {
        this.context = context;
        this.fareDetails = fareDetails;
    }




    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collect_cash_card_view,parent,false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        holder.cashType.setText(fareDetails.get(position).getTitle());
        holder.amount.setText(fareDetails.get(position).getValue() +" "+"SAR");

    }

    @Override
    public int getItemCount() {
        return fareDetails.size();
    }

    public  class  DataViewHolder extends RecyclerView.ViewHolder{
        TextView amount,cashType;
        public DataViewHolder(View itemView) {
            super(itemView);
            cashType  = (TextView) itemView.findViewById(R.id.cashType);
            amount = (TextView) itemView.findViewById(R.id.cashVal);
        }
    }
}
