package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;


import com.techconlabs.najezdriver.R;

import java.util.ArrayList;

public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<String> asr;

    public CustomSpinnerAdapter(Context context, ArrayList<String> asr) {
        this.asr = asr;
        activity = context;
    }


    public int getCount() {
        return asr.size();
    }

    public Object getItem(int i) {
        return asr.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(asr.get(position));
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        View spinnerRow = LayoutInflater.from(activity).inflate(R.layout.spinner_bg, viewgroup, false);
        TextView txt = (TextView) spinnerRow.findViewById(R.id.text);
        ImageView drop_image = (ImageView) spinnerRow.findViewById(R.id.drop_image);
        drop_image.setColorFilter(drop_image.getContext().getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
            /*txt.setGravity(Gravity.CENTER);
            txt.setPadding(16, 16, 16, 16);
            txt.setTextSize(16);
            txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.radio_on, 0);*/
        txt.setText(asr.get(i));
        //txt.setTextColor(Color.parseColor("#FFFFFF"));
        return spinnerRow;
    }

}