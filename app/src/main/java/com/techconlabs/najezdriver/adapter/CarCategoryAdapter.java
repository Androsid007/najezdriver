package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.interfaces.RecyclerViewItemClickListener;
import com.techconlabs.najezdriver.model.Datum;

import java.util.ArrayList;

/**
 * Created by hp on 12/7/2016.
 */

public class CarCategoryAdapter extends RecyclerView
        .Adapter<CarCategoryAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "CarCategory";
    private ArrayList<Datum> carCategoryList;
    private Context context;
    private RecyclerViewItemClickListener myClickListener;


    public class DataObjectHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private TextView carType, capacity, capacityIcon;
        private ImageView circulartaxi, greenStatus,carStatus;


        public DataObjectHolder(View itemView) {
            super(itemView);
            carType = (TextView) itemView.findViewById(R.id.carTypeTv);
            capacity = (TextView) itemView.findViewById(R.id.capCity);
            capacityIcon = (TextView) itemView.findViewById(R.id.capacityIcon);
            circulartaxi = (ImageView) itemView.findViewById(R.id.taxi_icon);
            greenStatus = (ImageView) itemView.findViewById(R.id.greenStatus);
            carStatus  = (ImageView) itemView.findViewById(R.id.carStatus);

            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            if(carCategoryList.get(position).getActive().equals("1")&&carCategoryList.get(position).getActive()!=null) {
                myClickListener.onItemClickListener(position);
            }
            else{
                Toast.makeText(context, context.getResources().getString(R.string.block_admin), Toast.LENGTH_LONG).show();

            }


        }

    }


    public CarCategoryAdapter(Context context, ArrayList<Datum> myDataset, RecyclerViewItemClickListener myClickListener) {
        this.context = context;
        this.carCategoryList = myDataset;
        this.myClickListener = myClickListener;
    }


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.car_cat_recyclerview_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(CarCategoryAdapter.DataObjectHolder holder, int position) {
          holder.carType.setText(carCategoryList.get(position).getVehicleModel());
        holder.capacityIcon.setText(carCategoryList.get(position).getVehicleNumber());
        holder.capacity.setText(carCategoryList.get(position).getVehicleType());
        String imagePath = carCategoryList.get(position).getDocVal();
        if (carCategoryList.get(position).getDocVal().equals("")) {
            holder.circulartaxi.setImageResource(R.drawable.car_info_car);
        } else {
            Picasso.with(context).load(imagePath).resize(1080, 720).centerInside().into(holder.circulartaxi);
        }
        if (SharedPreferenceManager.getOnLineStatus(context)) {
            if (SharedPreferenceManager.getVechileIdNumber(context).equals(carCategoryList.get(position).getVehicleId()))

            {    holder.greenStatus.setVisibility(View.VISIBLE);
                holder.greenStatus.setImageResource(R.drawable.ic_status_online);
            }
            else{
                holder.greenStatus.setVisibility(View.GONE);

            }
        }
        if(carCategoryList.get(position).getActive().equals("1") && carCategoryList.get(position).getActive()!=null)   {
            holder.carStatus.setVisibility(View.GONE);


        }
        else{

            holder.carStatus.setVisibility(View.VISIBLE);

        }
    }

    public void addItem(Datum dataObj, int index) {
        carCategoryList.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        carCategoryList.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return carCategoryList.size();
    }
}
