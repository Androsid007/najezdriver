package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.interfaces.RecyclerViewItemClickListener;
import com.techconlabs.najezdriver.model.Data;

import java.util.List;

/**
 * Created by hp on 12/7/2016.
 */

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.DataObjectHolder> {
    private static String LOG_TAG = "CarCategory";
    private List<Data> cardList;
    private Context context;
    private RecyclerViewItemClickListener myClickListener;

    public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView card_noTv, cardTypeTv;
        private ImageView ivDeleteCard;
        private ImageButton ibUseCard;
        private EditText etCVV;

        public DataObjectHolder(View itemView) {
            super(itemView);
            card_noTv = (TextView) itemView.findViewById(R.id.card_noTv);
            cardTypeTv = (TextView) itemView.findViewById(R.id.cardTypeTv);
            ibUseCard = (ImageButton) itemView.findViewById(R.id.ibUseCard);
            ivDeleteCard = (ImageView) itemView.findViewById(R.id.ivDeleteCard);
            etCVV = (EditText) itemView.findViewById(R.id.etCVV);

            ivDeleteCard.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            myClickListener.onItemClickListener(position);
        }

    }

    public CardListAdapter(Context context, List<Data> myDataset, RecyclerViewItemClickListener myClickListener) {
        this.context = context;
        this.cardList = myDataset;
        this.myClickListener = myClickListener;
    }


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.save_card_layout, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(CardListAdapter.DataObjectHolder holder, int position) {
        holder.card_noTv.setText(cardList.get(position).getCardNumber());
        holder.cardTypeTv.setText(cardList.get(position).getCardUsername());
        holder.ibUseCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "Under Development", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void addItem(Data dataObj, int index) {
        cardList.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        cardList.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }
}
