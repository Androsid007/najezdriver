package com.techconlabs.najezdriver.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.fragment.SettingsFragment;
import com.techconlabs.najezdriver.interfaces.OnCheckedListner;
import com.techconlabs.najezdriver.model.LongCityList;

import java.util.ArrayList;

/**
 * Created by X-cellent Technology on 02-11-2017.
 */

public class LongCityListAdapter extends RecyclerView.Adapter<LongCityListAdapter.DataViewHolder> {
    ArrayList<LongCityList> longCityList;
    Context context;
    private  OnCheckedListner onCheckedListner;

    public LongCityListAdapter(ArrayList<LongCityList> longCityList, Context context, OnCheckedListner onCheckedListner) {
        this.longCityList = longCityList;
        this.context = context;
        this.onCheckedListner = onCheckedListner;
    }



    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.long_city_card,parent,false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {
        holder.tvText.setText(longCityList.get(position).getCityNameEn());
        holder.checkbox.setChecked(longCityList.get(position).isSelceted());
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                onCheckedListner.OnChage(position,b);
            }
        });

    }

    @Override
    public int getItemCount() {
        return longCityList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder{
        CheckBox checkbox;
        TextView tvText;
        public DataViewHolder(View itemView) {
            super(itemView);
            tvText = (TextView) itemView.findViewById(R.id.tvText);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }

}
