package com.techconlabs.najezdriver.location;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;

import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.LocationModel;
import com.techconlabs.najezdriver.networkservice.RestApiMaker;
import com.techconlabs.najezdriver.networkservice.RetrofitApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hp1 on 9/20/2017.
 */

public class MyLocationRideUpdate extends Service {
    LocationListener locationListener;
    LocationManager locationManager;
    private static String TAG = MyLocationRideUpdate.class.getSimpleName();
    private MyLocationThread myLocationThread;
    public boolean isRunning = false;
    private double latitude, longitude;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myLocationThread = new MyLocationThread();
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
               // Toast.makeText(getApplicationContext(), "latitude = " + location.getLatitude() + ",\nlongitude = " + location.getLongitude(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent("location_update");
                intent.putExtra("lat", String.valueOf(location.getLatitude()));
                intent.putExtra("lng", String.valueOf(location.getLongitude()));
                sendBroadcast(intent);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        getProvider();
    }


    private void getProvider() {
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            getUpdatedLocation(LocationManager.NETWORK_PROVIDER);
        } else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getUpdatedLocation(LocationManager.NETWORK_PROVIDER);
        }
    }

    private void getUpdatedLocation(String provider) {
        //noinspection MissingPermission
        locationManager.requestLocationUpdates(provider, LocationUtils.getLocationUtils(getApplicationContext()).getInterval(), 0, locationListener);
    }

    @Override
    public synchronized void onDestroy() {
        super.onDestroy();
        if (!isRunning) {
            myLocationThread.interrupt();
            myLocationThread.stop();
        }
        if (locationManager != null) {
            locationManager.removeUpdates(locationListener);
        }
    }

    @Override
    public synchronized void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (!isRunning) {
            myLocationThread.start();
            isRunning = true;
        }
    }

    public class MyLocationThread extends Thread {
        static final long DELAY = 3000;

        @Override
        public void run() {
            while (isRunning) {
                Log.d(TAG, "Running");
                try {
                    sendLocationToServer();
                    Thread.sleep(DELAY);
                } catch (InterruptedException e) {
                    isRunning = false;
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendLocationToServer() {
      String url = "http://13.126.37.98:3000/api/rest_api/add_driver_ride_location";
//Dev
         //String url = "http://35.196.171.153:3003/api/rest_api/add_driver_ride_location";
        Call<LocationModel> call = RestApiMaker
                .getRetroInstance().create(RetrofitApiInterface.class)
                .sendLocationWithRide(SharedPreferenceManager.getToken(getApplicationContext()), SharedPreferenceManager.getRideId(getApplicationContext()), SharedPreferenceManager.getDriverId(getApplicationContext()), String.valueOf(latitude), String.valueOf(longitude), url);
        call.enqueue(new Callback<LocationModel>() {
            @Override
            public void onResponse(Call<LocationModel> call, Response<LocationModel> response) {
                if (response != null && response.body() != null) {

                    /*if(response.body().getCode()==1) {
                        Log.e(TAG,"Running location send successfully");
                    }
                    if(response.body().getCode()==2) {
                        Log.e(TAG,"Running location send fail due to server error");
                    }
                    else{
                        Log.e(TAG,"Running location send fail");
                    }*/
                } else {
                    Log.e(TAG, "Running location send fail due to server error");
                }
            }

            @Override
            public void onFailure(Call<LocationModel> call, Throwable t) {
                Log.e(TAG, "Running location send fail");
            }
        });
    }
}
