package com.techconlabs.najezdriver.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.techconlabs.najezdriver.common.CommonUtility;

/**
 * Created by Dell on 8/21/2017.
 */

public class LoactionHelper implements LocationListener {

    private Context context;
    private LocationManager locationManager;
    private SourceType sourceType;
    private UpdateLoactionListener callback;
    private static final int PERMISSIONS_LOCATION_CODE = 124;
    static private final int TIME_INTERVAL = 100; // minimum time between updates in milliseconds
    static private final int DISTANCE_INTERVAL = 1; // minimum distance between updates in meters

    public LoactionHelper(Context context) {
        super();
        this.context = context;
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }


    static public enum SourceType {
        NETWORK,
        GPS,
        NETWORK_THEN_GPS
    }


    public void getLocation(LoactionHelper.SourceType sourceType, LoactionHelper.UpdateLoactionListener callback) {
        this.sourceType = sourceType;
        this.callback = callback;
        switch (this.sourceType) {
            case NETWORK:
            case NETWORK_THEN_GPS:
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location networkLocation = this.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (networkLocation != null) {
                    Log.d("", "Last known location found for network provider : " + networkLocation.toString());
                    this.callback.onLocationUpdate(networkLocation);
                } else {
                    Log.d("", "Request updates from network provider.");
                    this.requestUpdates(LocationManager.NETWORK_PROVIDER);
                }
                break;
            case GPS:
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location gpsLocation = this.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (gpsLocation != null) {
                    Log.d("", "Last known location found for GPS provider : " + gpsLocation.toString());
                    this.callback.onLocationUpdate(gpsLocation);
                } else {
                    Log.d("", "Request updates from GPS provider.");
                    this.requestUpdates(LocationManager.GPS_PROVIDER);
                }
                break;
        }
    }


    private void requestUpdates(String provider) {
        if (this.locationManager.isProviderEnabled(provider)) {
            if (provider.contentEquals(LocationManager.NETWORK_PROVIDER)
                    && CommonUtility.isNetworkAvailable(this.context)) {
                Log.d("", "Network connected, start listening : " + provider);
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                this.locationManager.requestLocationUpdates(provider, TIME_INTERVAL, DISTANCE_INTERVAL, this);
            } else if (provider.contentEquals(LocationManager.GPS_PROVIDER)
                    && CommonUtility.isNetworkAvailable(this.context)) {
                Log.d("", "Mobile network connected, start listening : " + provider);
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                this.locationManager.requestLocationUpdates(provider, TIME_INTERVAL, DISTANCE_INTERVAL, this);
            } else {
                Log.d("", "Proper network not connected for provider : " + provider);
                this.onProviderDisabled(provider);
            }
        } else {
            this.onProviderDisabled(provider);
        }
    }

    /*public void cancel() {
        Log.d("", "Locating canceled.");
        this.locationManager.removeUpdates(this);
    }*/

    @Override
    public void onLocationChanged(Location location) {
        Log.d("", "Location found : " + location.getLatitude() + ", " + location.getLongitude() + (location.hasAccuracy() ? " : +- " + location.getAccuracy() + " meters" : ""));
        this.locationManager.removeUpdates(this);
        this.callback.onLocationUpdate(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("", "Provided status changed : " + provider + " : status : " + status);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("", "Provider enabled : " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("", "Provider disabled : " + provider);
        if (this.sourceType == LoactionHelper.SourceType.NETWORK_THEN_GPS
                && provider.contentEquals(LocationManager.NETWORK_PROVIDER)) {
            // Network provider disabled, try GPS
            Log.d("", "Requesst updates from GPS provider, network provider disabled.");
            this.requestUpdates(LocationManager.GPS_PROVIDER);
        } else {
            this.locationManager.removeUpdates(this);
            this.callback.onLocationFailure();
        }
    }

    public interface UpdateLoactionListener {
        public void onLocationUpdate(Location networkLocation);

        public void onLocationFailure();
    }
}
