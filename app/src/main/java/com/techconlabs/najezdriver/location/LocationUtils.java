package com.techconlabs.najezdriver.location;

import android.content.Context;


/**
 * Created by Dell on 9/4/2017.
 */

public class LocationUtils {
    private long interval;
    private String sourceType;
    private static LocationUtils locationUtils;

    private LocationUtils() {

    }

    public static synchronized LocationUtils getLocationUtils(Context context) {
        if (locationUtils == null) {
            locationUtils = new LocationUtils();
        }
        return locationUtils;
    }

    public void setLocationParameter(Context context) {
       /*DriverSessionManager driverSessionManager = new DriverSessionManager(context);
       LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
       Details details = driverSessionManager.getLoginDetail().getDetails();
       if(details.getDutyStatus() == DUTY_STATUS_ON) {
           if(details.getOnTripStatus() == 1) {
               if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                   SharedPreferenceManager.setLocationSourceType(context, LocationManager.NETWORK_PROVIDER);
                   SharedPreferenceManager.setInterval(context, ON_TRIP_INTERVAL);
               } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                   SharedPreferenceManager.setLocationSourceType(context, LocationManager.NETWORK_PROVIDER);
                   SharedPreferenceManager.setInterval(context, ON_TRIP_INTERVAL);
               }
           }
           else if(details.getOnTripStatus() == 2) {
               if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                   SharedPreferenceManager.setLocationSourceType(context, LocationManager.NETWORK_PROVIDER);
                   SharedPreferenceManager.setInterval(context, OFF_TRIP_ON_DUTY_INTERVAL);
               } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                   SharedPreferenceManager.setLocationSourceType(context, LocationManager.NETWORK_PROVIDER);
                   SharedPreferenceManager.setInterval(context, OFF_TRIP_ON_DUTY_INTERVAL);
               }
           }
       }
       else{
           if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
               SharedPreferenceManager.setLocationSourceType(context, LocationManager.NETWORK_PROVIDER);
               SharedPreferenceManager.setInterval(context, OFF_TRIP_OFF_DUTY_INTERVAL);
           } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
               SharedPreferenceManager.setLocationSourceType(context, LocationManager.NETWORK_PROVIDER);
               SharedPreferenceManager.setInterval(context, OFF_TRIP_OFF_DUTY_INTERVAL);
           }
       }
       setInterval(SharedPreferenceManager.getInterval(context));
       setSourceType(SharedPreferenceManager.getLocationSourceType(context));*/
    }

    /*public LocationUtils getLocationParameter(Context context){
        *//*locationUtils.setInterval(SharedPreferenceManager.getInterval(context));
        locationUtils.setSourceType(SharedPreferenceManager.getLocationSourceType(context));
        return locationUtils;*//*
    }*/
    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }
}
