package com.techconlabs.najezdriver.networkservice;


import com.techconlabs.najezdriver.model.About;
import com.techconlabs.najezdriver.model.Accept;
import com.techconlabs.najezdriver.model.Balance;
import com.techconlabs.najezdriver.model.Cancel;
import com.techconlabs.najezdriver.model.CancelOption;
import com.techconlabs.najezdriver.model.CarListing;
import com.techconlabs.najezdriver.model.CarType;
import com.techconlabs.najezdriver.model.CardModel;
import com.techconlabs.najezdriver.model.ChangePass;
import com.techconlabs.najezdriver.model.CityModel;
import com.techconlabs.najezdriver.model.CollectCashModel;
import com.techconlabs.najezdriver.model.ColorList;
import com.techconlabs.najezdriver.model.CompleteRide;
import com.techconlabs.najezdriver.model.ConfirmArrival;
import com.techconlabs.najezdriver.model.DriverRegister;
import com.techconlabs.najezdriver.model.Forget;
import com.techconlabs.najezdriver.model.GetData;
import com.techconlabs.najezdriver.model.Hisory;
import com.techconlabs.najezdriver.model.LocationModel;
import com.techconlabs.najezdriver.model.Login;
import com.techconlabs.najezdriver.model.LongCityModel;
import com.techconlabs.najezdriver.model.Offline;
import com.techconlabs.najezdriver.model.Otp;
import com.techconlabs.najezdriver.model.PartialPaymentModel;
import com.techconlabs.najezdriver.model.QBModel;
import com.techconlabs.najezdriver.model.Rate;
import com.techconlabs.najezdriver.model.Reset;
import com.techconlabs.najezdriver.model.Review;
import com.techconlabs.najezdriver.model.Suport;
import com.techconlabs.najezdriver.model.Token;
import com.techconlabs.najezdriver.model.TransactionDetail;
import com.techconlabs.najezdriver.model.TransationDetails;
import com.techconlabs.najezdriver.model.Trip;
import com.techconlabs.najezdriver.model.UpdateProfile;
import com.techconlabs.najezdriver.model.VehicleRegister;
import com.techconlabs.najezdriver.model.VerifyOtp;
import com.techconlabs.najezdriver.model.Voucher;
import com.techconlabs.najezdriver.model.VoucherNew;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface RetrofitApiInterface<M> {

     String cartID = null;


    @FormUrlEncoded
    @POST
   Call<DriverRegister> registerDriver(
                                       @Field("emailid") String emailid,
                                       @Field("fname") String first_name,
                                       @Field("mname") String middleName,
                                       @Field("lname") String last_name,
                                       @Field("password") String password,
                                       @Field("photo") String photo,
                                       @Field("gender") String gender,
                                       @Field("mobile") String mobile,
                                       @Field("referralcode") String referralcode,
                                       @Field("can_deliver") String canDeliver,
                                       @Field("city") String cityName,
                                       @Field("lat") String lattitude,
                                       @Field("lng") String lottitude,
                                       @Field("iban") String iban,
                                       @Field("bank") String bank,
                                       @Field("dob") String dob,
                                       @Field("resident") String resident,
                                       @Field("driving_lic") String driving_lic,
                                       @Field("iqama") String iqama,
                                       @Field("lang") String lang,
                                       @Field("device_model") String modelNum,
                                       @Field("device_os") String deviceOs,
                                       @Url String url);

    /*@FormUrlEncoded
    @POST
    Call<Login> loginDriver(@Field("username") String emailid,
                            @Field("password") String password, @Url String url);*/

    /*@Multipart
    @POST
    Call<Login> loginDriver(@Part("username") RequestBody emailid , @Part("password") RequestBody password,@Url String url);*/


    @FormUrlEncoded
    @POST
    Call<Login> loginDriver(@Field("mobile") String mobuleNo,
                            @Field("password") String password,@Field("device") String device,@Field("device_token") String deviceToken,@Field("lang") String lang,@Field("device_model") String modelNum,@Field("device_os") String deviceOs, @Url String url);

    @FormUrlEncoded
    @POST
    Call<Forget> getForgetPassword(@Field("mobile") String mobile, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Reset> getUpdatePassword(@Field("mobile") String email, @Field("new_password") String password, @Field("otp") String code,@Field("lang") String lang, @Url String url);
    @FormUrlEncoded
    @POST
    Call<CarListing> getCarListing(@Header("Authorization") String str,@Field("driver_id") String driverId, @Url String url);
    @FormUrlEncoded
    @POST
    Call<About> getAboutUs(@Header("Authorization") String str,@Field("page") String page, @Field("lang") String lang, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Suport> getSupport(@Header("Authorization") String str,@Field("emailid") String email, @Field("name") String name, @Field("comment") String comment,@Field("user_type") String driver,@Field("photo") String photo,@Url String url);

    @GET
    Call<Rate> getRateCard(@Header("Authorization") String str, @Url String url);

   @FormUrlEncoded
   @POST
   Call<VehicleRegister> doVehicleRegistration(@Header("Authorization") String str,
                                               @Field("auth_img") String authImg,
                                               @Field("car_model") String carModel,
                                                @Field("photograph") String photograph,
                                                @Field("tafweeth_expire") String tafweethExpire,
                                               @Field("tafweeth_img") String tafweethImg,
                                               @Field("vehicle_color") String vehicleColor,
                                               @Field("vehicle_insur") String vehicleInsur,
                                               @Field("vehicle_insur_expire") String vehicleInsurExpire,
                                               @Field("vehicle__plate_char") String vehiclePlateChar,
                                               @Field("vehicle_plate_num") String vehiclePlateNum,
                                               @Field("vehicle_reg") String vehicle_reg,
                                               @Field("vehicle_reg_expire") String fName,
                                               @Field("vehicle_sno") String vehicle_sno,
                                               @Field("vehicle_type") String vehicle_type,
                                               @Field("vehicle_year") String vehicle_year,
                                               @Field("vehicle_plate_char") String plateChar,

                                               @Url String url);

     @GET
     Call<CityModel> getCity(@Url String url);

    @GET
    Call<CarType> getCartType(@Header("Authorization") String token, @Url String url);

    @GET
    Call<CityModel> getBankList(@Header("Authorization") String token, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Otp> getSendOtp(@Field("mobile") String mob,@Field("type") String userType,@Field("lang") String lang,
                          @Url String url);
    @FormUrlEncoded
    @POST
    Call<VerifyOtp> getSendOtpToServer(@Field("mobile") String mobile, @Field("otp") String otpNum, @Field("device") String device, @Field("device_token") String deviceToken, @Field("lang") String languageType,
                                       @Url String url);

    @FormUrlEncoded
    @POST
    Call<Accept> getAcceptRide(@Header("authorization") String str,@Field("driver_id") String driverId, @Field("vehicle_family_id") String familyId,@Field("ride_id") String rideId,@Field("vehicle_id") String vecId,@Field("status") String status,
                               @Url String url);
   @FormUrlEncoded
    @POST
    Call<Offline> getOnOffRide(@Header("Authorization") String str, @Field("vehicle_id") String ride, @Field("status") String status,@Field("parent_vehicle_family") String vehicle_family_id,@Field("opt_vehicle_family") String subFamilyId,
                               @Url String url);
    @FormUrlEncoded
    @POST
    Call<Voucher> getApplyVoucher(@Header("Authorization") String str, @Field("code") String code,
                                  @Url String url);

 @FormUrlEncoded
 @POST
 Call<Token> getDeviceToken( @Field("token") String token,@Field("device") String deviceName,
                            @Url String url);




    @POST
    @FormUrlEncoded
    Call<Suport> getSupportType(@Field("type") String user_support_option, @Field("lang") String en, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Trip> getDeviceRideStart(@Header("Authorization") String str, @Field("ride_id") String ride,
                                  @Url String url);
    @FormUrlEncoded
    @POST
    Call<Hisory> getHistoryTrip(@Header("Authorization") String str, @Field("type") String type, @Field("status") String status,
                                @Url String url);



    @GET
    Call<CancelOption> getCangelRide(@Header("Authorization") String str,@Url String url);
    @FormUrlEncoded
    @POST
    Call<Cancel> getCancelRide(@Header("Authorization") String str, @Field("ride_id") String ride,@Field("user_type") String userType, @Field("cancel_option") String cancelOption,
                               @Url String url);
    @FormUrlEncoded
    @POST
    Call<LocationModel> sendLocation(@Header("authorization") String str, @Field("driver_id") String driver_id, @Field("latitude") String latitude, @Field("longitude") String longitude,@Field("vehicle_id") String vehicle_type,@Field("vehicle_family_id") String vehicle_family_id,
                                      @Url String url);
    @FormUrlEncoded
    @POST
    Call<CardModel> saveNewCard(@Header("Authorization") String str,@Field("card_number") String cardNumber,@Field("card_username") String cardUserName,@Field("card_type") String cardType,@Field("card_cvv") String cardCvv,@Field("expire_month") String expireMonth,@Field("expire_year") String expireYear,@Field("type") String type,@Url String url);
    @FormUrlEncoded
    @POST
     Call<CardModel> getSavedCard(@Header("Authorization") String token, @Field("type") String driver, @Url String url);

    @FormUrlEncoded
    @POST
    Call<CardModel> deleteCard(@Header("Authorization") String token,@Field("type") String type,@Field("card_id") String cardId,@Url String url);

    @FormUrlEncoded
    @POST
    Call<LocationModel> sendLocationWithRide(@Header("authorization") String str, @Field("ride_id") String rideId, @Field("driver_id") String driverId, @Field("latitude") String lattude,@Field("longitude") String longitude,
                                     @Url String url);
    @FormUrlEncoded
    @POST
    Call<ConfirmArrival> confirmArrival(@Header("Authorization") String token, @Field("ride_id") String rideId,  @Url String url);

    @FormUrlEncoded
    @POST
    Call<CompleteRide> driverCompleteRide(@Header("authorization") String token, @Field("ride_id") String rideId, @Url String url);

//Get Ride Data
    @FormUrlEncoded
    @POST
    Call<GetData> driverRideData(@Header("Authorization") String token, @Field("ride_id") String rideId, @Field("type") String type, @Url String url);

    @FormUrlEncoded
    @POST
    Call<ChangePass> getChangePassword(@Header("Authorization") String token,@Field("old_password") String oldPass,@Field("new_password") String newPass, @Url String url);


    @FormUrlEncoded
    @POST
    Call<Review> getDriverRating(@Header("Authorization") String token, @Field("ride_id") String rideId, @Field("star") String star, @Field("review") String review, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Balance> getDriverBalance(@Header("authorization") String token, @Field("driver_id") String driverId, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Balance> getDriverAddBalance(@Header("authorization") String token, @Field("driver_id") String driverId, @Field("amount") String amount,@Field("transaction_id") String transaction_id, @Url String url);

    @FormUrlEncoded
    @POST
    Call<Balance> getDriverSendBalance(@Header("authorization") String token, @Field("driver_id") String driverId, @Field("amount") String amount,@Field("desc") String desc,@Field("mobile_no") String mobileNo, @Url String url);


    @FormUrlEncoded
    @POST
    Call<TransactionDetail> getTransaction(@Header("Authorization") String auth, @Field("driver_id") String driver_id, @Field("transaction_type") String transaction_type,@Url String url);
    @FormUrlEncoded
    @POST
    Call<VoucherNew> getDriverOfflineVoucher(@Header("authorization") String token, @Field("voucher_code") String voucherCode, @Field("id") String driverId, @Field("membertype") String type, @Url String url);

    @FormUrlEncoded
    @POST
    Call<Offline> getDriverOfflineDelivery(@Header("Authorization") String token, @Field("opt") String voucherCode, @Url String url);
    @FormUrlEncoded
    @POST
    Call<UpdateProfile> getDriverProfileUpdate(@Header("Authorization") String token, @Field("emailid") String emailid, @Field("fname") String fname, @Field("mname") String mname, @Field("lname") String lname, @Field("gender") String gender, @Field("mobile") String mobile, @Field("photo") String photo, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Offline> getOptionLanguage(@Header("Authorization") String token, @Field("lang") String lang,@Field("type") String userType, @Url String url);

    @FormUrlEncoded
    @POST
    Call<Offline> getLastLocation(@Header("Authorization") String token, @Field("city") String city, @Url String url);
    @FormUrlEncoded
    @POST
    Call<TransationDetails> getTransactionDetails(@Header("Authorization") String token, @Field("mobile_no") String mobile_no, @Field("amount") String amount, @Url String url);


    @POST
    Call<LongCityModel> getLongCityList(@Header("Authorization") String str, @Url String url);
    @FormUrlEncoded
    @POST
    Call<Offline> getAddLongCity(@Header("Authorization") String token, @Field("cityid") String city, @Url String url);

    @GET
    Call<ColorList> getColorList(@Url String url);

    @FormUrlEncoded
    @POST
    Call<CollectCashModel> getCollectCashDeatil(@Header("Authorization") String token, @Field("ride_id") String ride_id, @Url String url);

    @FormUrlEncoded
    @POST
    Call<PartialPaymentModel> getPartialPayment(@Header("Authorization") String token, @Field("ride_id") String ride_id,@Field("amount")String amount , @Url String url);

    @FormUrlEncoded
    @POST
    Call<QBModel> sendQbId(@Header("Authorization") String token, @Field("chatid") String chatId, @Field("chatemail")String ChatEmail , @Field("type")String userType , @Url String url);
    @FormUrlEncoded
    @POST
    Call<Offline> logoutSession(@Header("Authorization") String token, @Field("user_type") String userType, @Url String url);

}

