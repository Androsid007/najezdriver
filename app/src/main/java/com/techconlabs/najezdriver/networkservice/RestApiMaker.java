package com.techconlabs.najezdriver.networkservice;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.TimeUnit;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestApiMaker {
    private static Retrofit retrofit;

    private RestApiMaker() {

    }

    public static Retrofit getRetroInstance() {


        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://najez-online.com/api/driver/")
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create())

                    .build();

        }
        return retrofit;
    }




    private static OkHttpClient getClient(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // init cookie manager
        OkHttpClient okHttpClient;
        CookieHandler cookieHandler = new CookieManager();

        okHttpClient = new OkHttpClient.Builder().addNetworkInterceptor(interceptor)
                .cookieJar(new JavaNetCookieJar(cookieHandler))
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        return okHttpClient;


    }

}