package com.techconlabs.najezdriver.networkservice;

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.techconlabs.najezdriver.BuildConfig;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.CardModel;
import com.techconlabs.najezdriver.model.VehicleRegister;

import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import static com.techconlabs.najezdriver.common.CommonUtility.CallerFunction.VEHICLE_REGISTRATION;


public class RetrofitTask<M> implements Callback<M> {


    private RetrofitTaskListener<M> _callback;
    private Context _context;
    private CommonUtility.HTTP_REQUEST_TYPE _methodType;
    private CommonUtility.CallerFunction _callerFunction;
    private SharedPreferences sharedPreferences;
    private DownloadManager.Request request;
    private Response response;
    private  String rideId,vechileId,deviceName,token,rideStartTime,currentTimeStamp,star,review,transaction_type;
    private String status,type,cancelOption,familyId,subFamilyId;
    private Integer statusValue;


    private Object Fast;
    String  oldPass, newPass;
    private String productId, qty, flavour, weight, cartId;
    String email, pwd, Firstname, Lastname,desc, default_mobile_number, session,checkBalance, address_id, coupon_code_string, search_string;
    private String fName, lName, company, street, city, region, cntryId, tele, fax, regId, AddId, shipAddId, shipMethod, payMethod;
    private String billing;
    private Map<String, String> address_data = new HashMap<>();
    M genricList;
    private Retrofit retrofit;
    private String URL;
    private Object postParamsEntity;
    long preExecutionTime, totalExecutionTime;
    boolean shouldAllowRetry = true;
    int timeOut;
    private String headerSession;
    private  String newPassword,amount,transactionId,modelNum,modelumber,languageType,chatEmail,chatId,userType,driverId,about,lang,comment,driver,coupanCode,deviceToken,deviceType,vehicle_family_id,ibanNum,bankName,dobDriver,residentAddress,drivingLic,iqamaNo;
    private String username,photo,gender,latlng,referralcode,firstName,lastName,emailId,number,otp,password,can_deliver,middleName,cityName,lat,lng,otpCode,supportType;
    private  CardModel cardModel;

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context) {
        this._callback = callback;
        this._methodType = methodType;



        this.URL = url;
        _context = context;
        this._callerFunction = callerFunction;
        timeOut = CommonUtility.RETROFIT_TIMEOUT;
    }

    public RetrofitTask(RetrofitTaskListener<M> callback,
                        CommonUtility.HTTP_REQUEST_TYPE methodType, CommonUtility.CallerFunction callerFunction,
                        String url, Context context, Object postParamsEntity) {
        this._callback = callback;
        this._methodType = methodType;
        this.URL = url;
        if(callerFunction == VEHICLE_REGISTRATION){
            timeOut = CommonUtility.RETROFIT_LONG_TIMEOUT;
        }
        else {
            timeOut = CommonUtility.RETROFIT_TIMEOUT;


        }
        _context = context;
        this._callerFunction = callerFunction;
        this.postParamsEntity = postParamsEntity;
    }

    public void execute() {
        executeTask(timeOut);
    }


    private void executeTask(int timeOut) {


        retrofit = RestApiMaker.getRetroInstance();
        RetrofitApiInterface service = retrofit.create(RetrofitApiInterface.class);



        Call<M> call = null;
        switch (_callerFunction) {
            case DRIVER_REGISTER:
                call = service.registerDriver(emailId,firstName,middleName,lastName,password,photo,gender,number,referralcode,can_deliver,cityName,lat,lng,ibanNum,bankName,dobDriver,residentAddress,drivingLic,iqamaNo,languageType,modelumber,"android",URL);
                      break;
            case DRIVER_LOGIN:

                call = service.loginDriver(default_mobile_number,password,deviceType,deviceToken,languageType,modelNum,"android",URL);
                         break;
            case GET_FORGET_PASSWORD:
                call = service.getForgetPassword(default_mobile_number, URL);
                break;
            case GET_UPDATE_PASSWORD:
                call = service.getUpdatePassword(default_mobile_number,newPassword,otp,languageType, URL);
                break;
            case CAR_LISTING:
                call = service.getCarListing(session,driverId, URL);
                break;
            case ABOUT_US:
                call = service.getAboutUs(session,about,lang, URL);
                break;
            case HELP_SUPPORT:
                call = service.getSupport(session,email,fName,comment,driver,photo, URL);
                break;
            case RATE_CARD:
                call = service.getRateCard(session, URL);
                break;

            case VEHICLE_REGISTRATION:
                VehicleRegister vehicleRegister = (VehicleRegister)postParamsEntity;
                call = service.doVehicleRegistration(SharedPreferenceManager.getToken(_context),vehicleRegister.getAuthImg(),vehicleRegister.getCarModel(),vehicleRegister.getPhotograph(),vehicleRegister.getTafweethExpire(),vehicleRegister.getTafweethImg(),vehicleRegister.getVehicleColor(),vehicleRegister.getVehicleInsur(),vehicleRegister.getVehicleInsurExpire(),vehicleRegister.getVehiclePlateChar(),vehicleRegister.getVehiclePlateNum(),vehicleRegister.getVehicleReg(),vehicleRegister.getVehicleRegExpire(),vehicleRegister.getVehicleSno(),vehicleRegister.getVehicleType(),vehicleRegister.getVehicleYear(),vehicleRegister.getVehiclePlateChar(),URL);
                break;
            case CITY_LIST:
                call = service.getCity(URL);
                break;
            case GET_CAR_TYPE_LIST:
                call = service.getCartType(SharedPreferenceManager.getToken(_context),URL);
                break;
            case GET_BANK_LIST:
                call = service.getBankList(SharedPreferenceManager.getToken(_context),URL);
                break;
            case GET_OTP_SEND:
                call = service.getSendOtp(default_mobile_number,userType,languageType,URL);
                break;
            case  OTP_VERIFY_TO_SERVER:
                call = service.getSendOtpToServer(default_mobile_number,otpCode,deviceType,deviceToken,languageType,URL);
                break;
            case ACCEPT_RIDE:
                call = service.getAcceptRide(session,driverId,vehicle_family_id,vechileId,rideId,status,URL);
                break;
            case DECLINE_RIDE:
                call = service.getAcceptRide(session,driverId,vehicle_family_id,vechileId,rideId,status,URL);
                break;

            case OFF_ONLINE:
                call = service.getOnOffRide(session,vechileId,status,familyId,subFamilyId,URL);
                break;
            case VOUCHER_APPLY:
                call = service.getApplyVoucher(session,coupanCode,URL);
                break;
            case DEVICE_TOKEN:
                call = service.getDeviceToken(session,coupanCode,URL);
                break;
            case DRIVER_START_TRIP:
                call = service.getDeviceRideStart(session,rideId,URL);
                break;
            case CANCEL_OPTION:
                call = service.getCangelRide(session,URL);
                break;
             case SUPPORT_TYPE:
                call = service.getSupportType("user_support_option","en",URL);
                break;
            case HISTORY_TRIP_API:
                call = service.getHistoryTrip(session,userType,status,URL);
                break;
            case CANCEL_RIDE:
                call = service.getCancelRide(session,rideId,userType,cancelOption,URL);
                break;
            case ADD_NEW_CARD:
                 cardModel = (CardModel) postParamsEntity;
                call = service.saveNewCard(SharedPreferenceManager.getToken(_context),cardModel.getCardNumber(),cardModel.getCardUserName(),cardModel.getCardType(),cardModel.getCardCvv(),cardModel.getExpireMonth(),cardModel.getExpireYear(),cardModel.getType(),URL);
                break;
            case GET_SAVED_CARD:
                call = service.getSavedCard(SharedPreferenceManager.getToken(_context),"driver",URL);
                break;
            case REMOVE_SAVED_CARD:
                 cardModel = (CardModel) postParamsEntity;
                call = service.deleteCard(SharedPreferenceManager.getToken(_context),cardModel.getType(),cardModel.getCardId(),URL);
                break;
            case CONFIRM_ARRIVAL:
                call = service.confirmArrival(session,rideId,URL);
                break;
            case DRIVER_END_TRIP:
                call = service.driverCompleteRide(session,rideId,URL);
                break;
            case GET_RIDE_DATA:
                call = service.driverRideData(session,rideId,type,URL);
                break;
            case GET_CHANGE_PASSWORD:
                call = service.getChangePassword(session,oldPass,newPass, URL);
                break;
            case DRIVER_RATING_REVIEW:
                call = service.getDriverRating(session,rideId,star, review,URL);
                break;
            case CHECK_BALANCE:
                call = service.getDriverBalance(session,checkBalance,URL);
                break;
            case ADD_BALANCE:
                call = service.getDriverAddBalance(session,driverId,amount,transactionId,URL);
                break;
            case SEND_BALANCE:
                call = service.getDriverSendBalance(session,driverId,amount,desc,default_mobile_number,URL);
                break;
            case TRASACTION_DETAIL:
                call = service.getTransaction(session,driverId,type,URL);
                break;
            case OFF_VOUCHER:
                call = service.getDriverOfflineVoucher(session,coupanCode,driverId,userType,URL);
                break;
            case DELIVERY_MEN_ONLINE:
                call = service.getDriverOfflineDelivery(session,status,URL);
                break;
            case DRIVER_PROFILE_UPDATE:
                call = service.getDriverProfileUpdate(session,emailId,firstName,lastName,middleName,gender,number,photo,URL);
                break;
            case LANG_TYPE:
                call = service.getOptionLanguage(session,languageType,userType,URL);
                break;
            case LAST_LOCATION:
                call = service.getLastLocation(session,city,URL);
                break;
            case TRANSACTION_DETAIL:
                call = service.getTransactionDetails(session,default_mobile_number,amount,URL);
                break;
            case GET_LONG_CITY_LIST:
                call = service.getLongCityList(session,URL);
                break;
            case ADD_lONG_CITY:
                call = service.getAddLongCity(session,city,URL);
                break;
            case GET_COLOR_LIST:
                call = service.getColorList(URL);
                break;
            case DRIVER_BREAK_DOWN:
                call = service.getCollectCashDeatil(session,rideId,URL);
                break;
            case PARTIAL_PAYMENT:
                call = service.getPartialPayment(session,rideId,amount,URL);
                break;
            case QBID_SEND_TOSERVER:
                call = service.sendQbId(session,chatId,chatEmail,userType,URL);
                break;
            case LOGOUT_USER:
                call = service.logoutSession(session,userType,URL);
                break;




                }

        try {
            if (BuildConfig.DEBUG) {
                Log.e("called url",call.request().url().toString());
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        call.enqueue(this);
    }




    public void executeDriverRegister(String email,String fName,String lName,String mname,String pwd,String uidimage,String gen,String mNumber,String refecode,String deliver_type,String cityname,String latitude,String longitude,String iban,String bank,String dob,String resident,String driving_lic,String iqama,String langType,String modelNo) {
        emailId = email;
        firstName = fName;
        lastName = lName;
        password = pwd;
        photo = uidimage;
        gender = gen;
        number = mNumber;
        referralcode = refecode;
        can_deliver = deliver_type;
        middleName = mname;
        cityName = cityname;
        lat = latitude;
        lng = longitude;
        ibanNum = iban;
        bankName = bank;
        dobDriver = dob;
        residentAddress = resident;
        drivingLic = driving_lic;
        iqamaNo =iqama;
        languageType =langType;
       modelumber =modelNo;


     executeTask(timeOut);
    }
    public void executeDriverUpdate(String token,String emailid,String fname,String mname,String lname,String ugender,String umobile,String uphoto) {
        session = token;
        emailId = emailid;
        firstName = fname;
        lastName = lname;
        middleName = mname;
        gender =ugender;
        number= umobile;
        photo  = uphoto;


        executeTask(timeOut);
    }

    public void executeDriverLogin(String param1,String param2,String param3,String param4,String param5,String param6) {
        default_mobile_number = param1;
        password = param2;
        deviceType = param3;
        deviceToken = param4;
        languageType = param5;
        modelNum=param6;

        executeTask(timeOut);
    }
    public void executeUpdatePassword(String mobileNumber, String param1, String param2,String param3) {
       default_mobile_number = mobileNumber;
        newPassword = param1;
        otp = param2;
        languageType =param3;
        executeTask(timeOut);
    }
    public void executeCarListing( String param1,String param2) {
        session =param1;
       driverId =param2;
     executeTask(timeOut);
    }
    public void executeAboutUs(String param1,String param2,String param3) {
        session = param1;
        about = param2;
        lang = param3;
        executeTask(timeOut);
    }
    public void executeQbIdServer(String param1,String param2,String param3,String param4) {
        session = param1;
        chatId = param2;
        chatEmail = param3;
        userType = param4;
        executeTask(timeOut);
    }
    public void executeHelpSupport(String token, String param1, String param2, String param3, String param4, String param5) {
        this.session =token;
        email = param1;
        fName = param2;
        comment = param3;
        driver=param4;
        photo = param5;
           executeTask(timeOut);
    }
    public void executeRateCard(String param1) {
         session = param1;
         executeTask(timeOut);
    }
    public void executeVoucher(String param1,String param2) {
        session = param1;
        coupanCode = param2;
        executeTask(timeOut);
    }
    public void executeOtpSend(String param1,String param2,String param3) {
        default_mobile_number = param1;
        userType = param2;
        languageType = param3;
        executeTask(timeOut);}
    public void executeOtpSendToServer(String param1,String param2,String param3,String param4,String param5) {
        default_mobile_number = param1;
        otpCode = param2;
        deviceType= param3;
        deviceToken = param4;
        languageType = param5;


        executeTask(timeOut);
    }
    public void executeOffVoucher(String param1,String param2,String param3,String param4) {
        session = param1;
        coupanCode = param2;
        driverId = param3;
        userType = param4;
        executeTask(timeOut);
    }



    @Override
    public void onResponse(Call<M> call, retrofit2.Response<M> response) {
        try {
            if (BuildConfig.DEBUG) {
                Log.e("response", new Gson().toJson(response.body()));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        _callback.onRetrofitTaskComplete(response, _context, _callerFunction);
    }

    public void executeForgetPassword(String param1) {
        default_mobile_number = param1;
        executeTask(timeOut);
    }
    public void executeChangePassword(String param1,String param2, String param3 ) {
         session = param1;
        oldPass = param2;
        newPass = param3;
     executeTask(timeOut);
    }
    public void executeDriverRideAccept(String param1,String param2,String param3,String param4,String param5,String param6) {
        session = param1;
        driverId =param2;
        vehicle_family_id = param3;
        vechileId=param4;
        rideId = param5;
        status=param6;

        executeTask(timeOut);
    }
    public void executeCancelRide(String param1) {
        session = param1;
        executeTask(timeOut);
    }
    public void executeDriverOnOff(String param1,String param2,String param3,String param4,String param5) {
        session = param1;
        vechileId = param2;
        status=param3;
        familyId=param4;
        subFamilyId=param5;
       executeTask(timeOut);
    }
    public void executeDriveryMenOnOff(String param1,String param2) {
        session = param1;
        status = param2;
        executeTask(timeOut);
    }
    public void executeLogout(String param1,String param2) {
        session = param1;
        userType = param2;
        executeTask(timeOut);
    }

   public void executeDeviceTokenToServer(String param1,String param2) {
        token = param1;
        deviceName = param2;
        executeTask(timeOut);
    }

    public void executeDriverStartTrip(String param1,String param2) {
        session = param1;
        rideId = param2;
         executeTask(timeOut);
    }
    public void executeDriverRideComplete(String param1,String param2) {
        session = param1;
        rideId = param2;
        executeTask(timeOut);
    }
    public void executeHistoryToken(String param1,String param2,String param3) {
        session = param1;
        userType = param2;
        status = param3;
        executeTask(timeOut);
    }
    public void executeDriverCancelRide(String param1,String param2,String param3,String param4) {
        session = param1;
        rideId = param2;
        userType = param3;
        cancelOption=param4;
        executeTask(timeOut);
    }
    public void executeDriverConfirmArrival(String param1,String param2) {
        session = param1;
        rideId =param2;

        executeTask(timeOut);
    }
    public void executeDriverRideData(String param1,String param2,String param3) {
        session = param1;
        rideId =param2;
        type = param3;

        executeTask(timeOut);
    }
    public void executeDriverReview(String param1,String param2,String param3,String param4) {
        session = param1;
        rideId = param2;
        star = param1;
        review = param2;
        executeTask(timeOut);
    }
    public void executeTransactionDetail(String driversession,String driver_id,String transactiontype) {
        session = driversession;
        rideId =driver_id;
        type = transactiontype;

        executeTask(timeOut);
    }
    public void executeDriverBalance(String param1,String param2) {
        session = param1;
        checkBalance = param2;
        executeTask(timeOut);
    }
    public void executeDriverAddBalance(String param1,String param2,String param3, String param4) {
        session = param1;
        driverId = param2;
        amount = param3;
        transactionId = param4;
        executeTask(timeOut);
    }
    public void executeDriverLangType(String param1,String param2,String param3) {
        session = param1;
        languageType = param2;
        userType = param3;
        executeTask(timeOut);
    }

    public void executeSendBalance(String param1,String param2,String param3, String param4,String param5) {
        session = param1;
        driverId = param2;
        amount = param3;
        desc = param4;
        default_mobile_number =param5;
        executeTask(timeOut);
    }
    public void executeTransactionInfo(String auth,String mobile_no,String ammount_add) {
        session = auth;
        default_mobile_number = mobile_no;
        amount = ammount_add;
        executeTask(timeOut);
    }
    public void executeLastLoc(String auth,String newcity){
        session =auth;
        city = newcity;
        executeTask(timeOut);
    }
    public void executeDriverBreakDown(String auth,String ride_id){
        session =auth;
        rideId = ride_id;
        executeTask(timeOut);
    }
    public void executePartialPayment(String auth,String ride_id,String pay_ment){
        session =auth;
        rideId = ride_id;
        amount =pay_ment;
        executeTask(timeOut);
    }
    public void executeAddLongCity(String auth,String cityid){
        session =auth;
        city = cityid;
        executeTask(timeOut);
    }
    public void executeLongCity(String param1) {
        session = param1;
        executeTask(timeOut);
    }
    @Override
    public void onFailure(Call<M> call, Throwable t) {

        _callback.onRetrofitTaskFailure(t);
    }


}