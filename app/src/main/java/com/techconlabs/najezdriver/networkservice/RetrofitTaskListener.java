package com.techconlabs.najezdriver.networkservice;

import android.content.Context;

import com.techconlabs.najezdriver.common.CommonUtility;

import retrofit2.Response;


public interface RetrofitTaskListener<M> {
    public void onRetrofitTaskComplete(Response<M> response, Context context, CommonUtility.CallerFunction _callerFunction);
    public void onRetrofitTaskFailure(Throwable t);
}
