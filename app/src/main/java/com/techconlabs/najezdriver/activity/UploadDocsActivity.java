package com.techconlabs.najezdriver.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.techconlabs.najezdriver.R;

public class UploadDocsActivity extends AppCompatActivity {

    private Button btnGetStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_docs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(R.string.vechile_info_);

        init();
        onClick();
    }

    public void init() {
        btnGetStart = (Button) findViewById(R.id.buttonGetStart);
    }

    public void onClick() {
        btnGetStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UploadDocsActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
