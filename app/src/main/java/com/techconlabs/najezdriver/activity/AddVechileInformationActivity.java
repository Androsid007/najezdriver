package com.techconlabs.najezdriver.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.fragment.FragmentVehicleRegistration;
import com.theartofdev.edmodo.cropper.CropImage;


public class AddVechileInformationActivity extends AppCompatActivity {

    private Button btnCntinu;
    private Spinner carName, carModel, noOfYears;
    private int STORAGE_REQUEST_CODE = 125;
    private FragmentVehicleRegistration fragmentVehicleRegistration;
    private Uri mCropImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_registration);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_up_appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(R.string.vehcile_title);

        fragmentVehicleRegistration = new FragmentVehicleRegistration();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.contentFrame, fragmentVehicleRegistration).commit();
    }
    @Override
    protected void onNewIntent(Intent intent) {
        if (intent != null) {
            Bundle bundle1 = intent.getExtras();
          if (bundle1 != null && bundle1.getBoolean("unblockUser")) {
                //Toast.makeText(this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();
                if(SharedPreferenceManager.getLoggin(this)) {
                    Toast.makeText(this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();
                    SharedPreferenceManager.setWrblockOnline(this, "1");

                }
            }
             if (bundle1 != null && bundle1.getBoolean("blockUser")) {
                Toast.makeText(this, getString(R.string.block_admin), Toast.LENGTH_LONG).show();
                logoutUser();
            }


        }
    }

    private void logoutUser() {
        SharedPreferenceManager.setReferalCode(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setEmailId(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setFirstName(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setvechileCount(AddVechileInformationActivity.this, 0);
        SharedPreferenceManager.setDriverId(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setMobileNumber(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setCity(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setPassword(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setDriverRating(AddVechileInformationActivity.this,0);
        SharedPreferenceManager.setBalance(AddVechileInformationActivity.this,0);
        SharedPreferenceManager.settotalRide(AddVechileInformationActivity.this,0);
        SharedPreferenceManager.setToken(AddVechileInformationActivity.this,"");
        SharedPreferenceManager.setBalance(AddVechileInformationActivity.this,0);
        SharedPreferenceManager.setCarListSize(AddVechileInformationActivity.this,0);
        SharedPreferenceManager.setLoggin(AddVechileInformationActivity.this,false);
        SharedPreferenceManager.setWalletBalance(AddVechileInformationActivity.this,"");
        SharedPreferenceManager.setOnLineStatus(AddVechileInformationActivity.this, false);
        SharedPreferenceManager.setVechileId(AddVechileInformationActivity.this,"");
        SharedPreferenceManager.setWalletBalance(AddVechileInformationActivity.this, "");
        SharedPreferenceManager.setChatId(AddVechileInformationActivity.this,"");
        Intent intent = new Intent(AddVechileInformationActivity.this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (CropImage.isReadExternalStoragePermissionsRequired(this, result.getUri())) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = result.getUri();
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
            fragmentVehicleRegistration.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.activity(mCropImageUri)
                        .start(this);
            } else {
                Toast.makeText(this, R.string.cancel_toast, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}