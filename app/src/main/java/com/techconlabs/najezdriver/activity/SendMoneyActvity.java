package com.techconlabs.najezdriver.activity;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Balance;
import com.techconlabs.najezdriver.model.TransationDetails;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import retrofit2.Response;

public class SendMoneyActvity extends AppCompatActivity implements RetrofitTaskListener {
    private TextInputLayout phone_inp, money_inp;
    private EditText et_phone, et_money, et_comment;
    private String phone, money, review;
    private Button sendMonetEt;
    private ImageView phonebook;
    private static final int RESULT_PICK_CONTACT = 2015;
    private ProgressDialog progressDialog;
    String phoneNo = null;
    String name = null;
    String walletBal;
    TextView balanceTv;
    TextView tv_hint,tv_yes,tv_no;
    private android.support.v4.app.FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(R.string.tarnsfer_money);
        balanceTv = (TextView) findViewById(R.id.balanceTv);

        walletBal = SharedPreferenceManager.getWalletBalance(SendMoneyActvity.this);
        if (!walletBal.equals("0")) {
            balanceTv.setText(getString(R.string.wallet_bal) + " " + walletBal);
        }

        init();
        sendMonetEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validate()) {
                    phone_inp.setErrorEnabled(false);
                    money_inp.setErrorEnabled(false);
                    calltransationDetail();


                } else {

                }

            }
        });
        phonebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
            }
        });


    }

    private void sendMoneyToAccount() {
        showProgreass();
         String url = String.format(ServerConfigStage.USER_SEND_BALANCE());
        RetrofitTask task = new RetrofitTask<Balance>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.SEND_BALANCE, url, this);
        String session = SharedPreferenceManager.getToken(this);
        //String newPhoene = "91" + phone;
        String newPhoene = "966" + phone;

        task.executeSendBalance(session, SharedPreferenceManager.getDriverId(this), money, review, newPhoene);

    }
    private void  calltransationDetail() {
        showProgreass();
        String url = String.format(ServerConfigStage.DRIVER_TRANSCATION_DETAILS());
        RetrofitTask task = new RetrofitTask<TransationDetails>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.TRANSACTION_DETAIL, url, this);
        String session = SharedPreferenceManager.getToken(this);
       // String newPhoene = "91" + phone;
        String newPhoene = "966" + phone;
        task.executeTransactionInfo(session,newPhoene,money);

    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    private boolean Validate() {
        stringVal();

        if (phone.isEmpty() && phone.equals("")) {
            money_inp.setErrorEnabled(false);
            phone_inp.setErrorEnabled(true);
            phone_inp.setError(getString(R.string.enter_mobile_to_tran));
            return false;

        } else if (phone.length() < 9) {
            money_inp.setErrorEnabled(false);
            phone_inp.setErrorEnabled(true);
            phone_inp.setError(getString(R.string.valid_no_to_ttransfer));
            return false;
        } else if (money.isEmpty() && money.equals("")) {
            phone_inp.setErrorEnabled(false);
            money_inp.setErrorEnabled(true);
            money_inp.setError(getString(R.string.enter_amt_transfer));
            return false;
        }

        return true;

    }

    private void init() {
        et_phone = (EditText) findViewById(R.id.mobileNumberEt);
        et_money = (EditText) findViewById(R.id.amountAddEt);
        et_comment = (EditText) findViewById(R.id.optioanlDescText);
        sendMonetEt = (Button) findViewById(R.id.sendMonetEt);
        /***********TextInputLayout***************/
        phone_inp = (TextInputLayout) findViewById(R.id.mobileLayout);
        money_inp = (TextInputLayout) findViewById(R.id.amountLayout);

        /****************Image View*********************/
        phonebook = (ImageView) findViewById(R.id.phonebook);


    }

    private void stringVal() {
        phone = et_phone.getText().toString().trim();
        money = et_money.getText().toString().trim();
        review = et_comment.getText().toString().trim();
    }


    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check whether the result is ok
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {

            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int nameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
        if (phoneNo.length() > 9) {
                if (phoneNo.startsWith("+966")) {
                    String number = phoneNo.substring(4);
                    et_phone.setText(number);
                } else if (phoneNo.startsWith("966")) {
                    String number = phoneNo.substring(3);
                    et_phone.setText(number);
                } else if (phoneNo.startsWith("0") || phoneNo.startsWith("+")) {
                    String number = phoneNo.substring(1);
                    et_phone.setText(number);
                }
            }

      /*      if (phoneNo.length() > 10) {
                if (phoneNo.startsWith("+91")) {
                    String number = phoneNo.substring(3);
                    et_phone.setText(number);
                } else if (phoneNo.startsWith("91")) {
                    String number = phoneNo.substring(2);
                    et_phone.setText(number);
                } else if (phoneNo.startsWith("0") || phoneNo.startsWith("+")) {
                    String number = phoneNo.substring(1);
                    et_phone.setText(number);
                     }
                }*/
            else {
                et_phone.setText(phoneNo);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (_callerFunction == CommonUtility.CallerFunction.TRANSACTION_DETAIL){
            TransationDetails transationDetails = (TransationDetails) response.body();

            if (response.isSuccessful()){
                  if (transationDetails.getStatus()>0){
                    String userInfo = transationDetails.getMessage();
                    popup(userInfo);
                }else {
                    Toast.makeText(SendMoneyActvity.this, transationDetails.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        } else if (_callerFunction == CommonUtility.CallerFunction.SEND_BALANCE && response != null) {
            Balance balance = (Balance) response.body();
            if (response.isSuccessful()) {
                if (balance.getStatus() > 0) {
                    String totalbal = balance.getBalance();
                    SharedPreferenceManager.setWalletBalance(this, totalbal);
                    finish();

                } else {
                    Toast.makeText(context, balance.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    private void popup(String userInfo) {
        final BottomSheetDialog bottomSheetDialog =  new BottomSheetDialog(SendMoneyActvity.this);
        View popupView = getLayoutInflater().inflate(R.layout.are_you_sure_pop_layout, null);
        bottomSheetDialog.setContentView(popupView);
        bottomSheetDialog.show();
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }

        });
        tv_hint = (TextView) popupView.findViewById(R.id.hinttext);
        tv_no = (TextView) popupView.findViewById(R.id.textNo);
        tv_yes = (TextView) popupView.findViewById(R.id.textYes);
        tv_hint.setText(userInfo);
        tv_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                sendMoneyToAccount();
            }
        });
        tv_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });



    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }
}
