package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.model.Forget;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;


public class ForgetPassword extends AppCompatActivity implements RetrofitTaskListener<Forget> {
    EditText emailfeild;
    Button resetbtn;
    private ProgressDialog progressDialog;
    TextInputLayout emaillayout;
    ImageView iv_sad;
    String mobileNo;
    private String email;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_up_appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(getString(R.string.frgt_pwd));


        emailfeild = (EditText) findViewById(R.id.inputemail);
        resetbtn = (Button) findViewById(R.id.resetbutton);
        emaillayout = (TextInputLayout) findViewById(R.id.emailLayput);
        iv_sad = (ImageView) findViewById(R.id.img_reset_pwd);
        //  iv_sad.setColorFilter(iv_sad.getContext().getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
        resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailfeild.getText().toString().trim();
               // mobileNo = "91" + email;
                mobileNo = "966" + email;

                if (email!=null && email.length() > 8){
                    emaillayout.setErrorEnabled(false);
                    emaillayout.clearFocus();
                    showProgreass();
                    String url = String.format(ServerConfigStage.USER_FORGET_PASSWORD());
                    RetrofitTask task = new RetrofitTask<Forget>(ForgetPassword.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_FORGET_PASSWORD, url, ForgetPassword.this);
                    task.executeForgetPassword(mobileNo);

                } else {
                    emaillayout.setError(getString(R.string.enter_mobile_msg));
                    emaillayout.setErrorEnabled(true);
                    emaillayout.requestFocus();

                }

            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(ForgetPassword.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onRetrofitTaskComplete(retrofit2.Response<Forget> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {

            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                    Intent main = new Intent(ForgetPassword.this, ResetPassword.class);
                    main.putExtra("mobile", mobileNo);
                    main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(main);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                } else {
                    Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();

                }


            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(ForgetPassword.this, getString(R.string.internet_connecton), Toast.LENGTH_LONG).show();


    }

    @Override
    public void onVisibleBehindCanceled() {
        super.onVisibleBehindCanceled();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
