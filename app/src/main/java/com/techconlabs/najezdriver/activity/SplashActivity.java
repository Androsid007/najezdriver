package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.GetData;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import retrofit2.Response;

/**
 * Created by hp on 6/19/2017.
 */

public class SplashActivity extends AppCompatActivity implements RetrofitTaskListener<GetData> {
    private int count;
    private String driverId;
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private ProgressDialog progressDialog;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.SplashTheme);
       super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

       /*  setContentView(R.layout.splash);
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    count = SharedPreferenceManager.getvechileCount(SplashActivity.this);
                } catch (Exception e) {

                }
                if( SharedPreferenceManager.getLanguage(SplashActivity.this).equals("ar")){
                    Locale locale = new Locale("ar");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                }else{
                    Locale locale = new Locale("en");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                }

                if (SharedPreferenceManager.getEmailId(SplashActivity.this) != null && !SharedPreferenceManager.getEmailId(SplashActivity.this).equals("")) {
                    int rideStatus = SharedPreferenceManager.getrideStatus(SplashActivity.this);
                    if (rideStatus==1 ||rideStatus==2||rideStatus==3||rideStatus==4) {
                        GetRideDataApi();
                    }
                  else {
                        if (count > 0) {
                            driverId = SharedPreferenceManager.getDriverId(SplashActivity.this);
                            Intent intent = new Intent(SplashActivity.this, CarCategoryActivity.class);
                            intent.putExtra("driverId", driverId);
                            startActivity(intent);
                            finish();


                        } else {
                            Intent intent = new Intent(SplashActivity.this, AddVechileInformationActivity.class);
                            startActivity(intent);
                            finish();

                        }

                    }
                }else {
                    startActivity(new Intent(SplashActivity.this, ChooseLanguageActivity.class));
                    finish();
                }


            }
        }, SPLASH_DISPLAY_LENGTH);

    }
    public void showProgreass() {
        progressDialog = new ProgressDialog(SplashActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {

        if (progressDialog != null && progressDialog.isShowing())
            try {
                progressDialog.dismiss();
            }
            catch (Exception e)
            {

            }
    }

    private void GetRideDataApi() {

        showProgreass();
        String url = String.format(ServerConfigStage.GET_RIDE());
        RetrofitTask task = new RetrofitTask<GetData>(SplashActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_RIDE_DATA, url, SplashActivity.this);
        String session = SharedPreferenceManager.getToken(SplashActivity.this);
        task.executeDriverRideData(session, SharedPreferenceManager.getRideId(SplashActivity.this), "user");

    }


    @Override
    public void onRetrofitTaskComplete(Response<GetData> response, Context context, CommonUtility.CallerFunction _callerFunction) {

        stopProgress();
        if (response.isSuccessful()) {

             GetData rideInformation = (GetData) response.body();
                if (rideInformation.getStatus() > 0) {
                    if(SharedPreferenceManager.getrideStatus(SplashActivity.this)==1)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("rideInformationLost", rideInformation);
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent.putExtras(bundle));
                        finish();


                    }
                    else if(SharedPreferenceManager.getrideStatus(SplashActivity.this)==2) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("rideInformation", rideInformation);
                        Intent intent = new Intent(SplashActivity.this, MapsActivity.class);
                        startActivity(intent.putExtras(bundle));
                        finish();
                    }
                    else if(SharedPreferenceManager.getrideStatus(SplashActivity.this)==3) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("rideInformation", rideInformation);
                        Intent intent = new Intent(SplashActivity.this, MapsActivity.class);
                        startActivity(intent.putExtras(bundle));
                        finish();
                    }
                    else if(SharedPreferenceManager.getrideStatus(SplashActivity.this)==4) {
                        Intent intent =  new Intent(this, CollectCashActvity.class);
                        intent.putExtra("username",rideInformation.getRideData().getUserdata().getFname());
                        this.startActivity(intent);
                        this.overridePendingTransition(R.anim.enter,R.anim.exit);
                        this.finish();
                    }




                } else {


            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
    }
}

