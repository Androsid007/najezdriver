package com.techconlabs.najezdriver.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.fragment.HomeFragment;
import com.techconlabs.najezdriver.fragment.VehicleInfoFragment;
import com.techconlabs.najezdriver.model.Datum;

public class VechileInformationActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, NavigationView.OnNavigationItemSelectedListener// implements AdapterView.OnItemSelectedListener
{
    private Button btnCntinu;
    private Spinner carName, carModel, noOfYears;
    private SwitchCompat switchCompat;
    private TextView tvTitle;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private TextView tvFrom, tvRating, tvUserName;
    private ImageView profileImg;
    private FragmentManager fragmentManager;
    private Datum carDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vechile_information);
        if (getIntent().getExtras() != null && getIntent().getExtras().getSerializable("carDetail") != null) {
            carDetail = (Datum) getIntent().getExtras().getSerializable("carDetail");
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        switchCompat = (SwitchCompat) findViewById(R.id.switchCompat);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitle(R.string.vechile_info_);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.container, new VehicleInfoFragment(), "off").commit();
        switchCompat.setOnCheckedChangeListener(this);
        setUpNavigation();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //CommonUtil.addFragment(this,R.id.container,new VehicleInfoFragment());


    }

    private void setUpNavigation() {
        View navHeader = navigationView.getHeaderView(0);
        tvFrom = (TextView) navHeader.findViewById(R.id.tvFrom);
        tvRating = (TextView) navHeader.findViewById(R.id.tvRating);
        tvUserName = (TextView) navHeader.findViewById(R.id.tvUserName);
        profileImg = (ImageView) navHeader.findViewById(R.id.profileImg);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        navigationView.setNavigationItemSelectedListener(this);
         /*findViewById(R.id.imgHumburger).setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 drawer.openDrawer(GravityCompat.END);
             }
         });*/
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switchCompat:
                if (isChecked) {
                    tvTitle.setText("ONLINE");
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment(), "on").commit();
                } else {
                    tvTitle.setText("OFFLINE");
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.container, new VehicleInfoFragment(), "off").commit();
                }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END, true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
          /*   case R.id.nav_way_bill:
                 fragmentManager.beginTransaction().replace(R.id.container,new HomeFragment()).commit();
                 break;*/
           /*  case R.id.nav_purchase_ride:
                 fragmentManager.beginTransaction().replace(R.id.container,new HomeFragment()).commit();
                 break;*/
            /* case R.id.nav_document:
                 fragmentManager.beginTransaction().replace(R.id.container,new HomeFragment()).commit();
            */
            case R.id.nav_get_free_ride:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.nav_my_cards:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.nav_my_car:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.nav_about:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.nav_faq:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.nav_settings:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.nav_hepl_support:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
            case R.id.nav_logout:
                fragmentManager.beginTransaction().replace(R.id.container, new HomeFragment()).commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
