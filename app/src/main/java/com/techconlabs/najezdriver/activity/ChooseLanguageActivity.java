package com.techconlabs.najezdriver.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Offline;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.Locale;

import retrofit2.Response;

public class ChooseLanguageActivity extends AppCompatActivity implements RetrofitTaskListener<Offline> {

    RadioButton rb_english, rb_arabic;
    Button next_btn;
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    String[] permissionsRequired = new String[]{Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_CONTACTS,Manifest.permission.CALL_PHONE};
    private TextView txtPermissions;
    private Button btnCheckPermissions;
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;
    private ProgressDialog progressDialog;
    private String languageType;




    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_choose_language);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        init();
        onClick();
    }

    private void init() {
        rb_arabic = (RadioButton) findViewById(R.id.arabic_rb_btn);
        rb_english = (RadioButton) findViewById(R.id.english_rb_btn);
        next_btn = (Button) findViewById(R.id.next_btn);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void onClick() {
        rb_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_english.isChecked()) {
                    rb_arabic.setChecked(false);
                    Locale locale = new Locale("en");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

                }

            }
        });
        rb_arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_arabic.isChecked()) {
                    rb_english.setChecked(false);
                  Locale locale = new Locale("ar");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
              }
            }
        });
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_arabic.isChecked()) {
                   languageType = "ar";
                   SharedPreferenceManager.setLanguage(ChooseLanguageActivity.this,languageType);
                   //setLanguageType(languageType);

                }
                else{
                    languageType = "en";
                    SharedPreferenceManager.setLanguage(ChooseLanguageActivity.this,languageType);

                    //setLanguageType(languageType);
                }
                startActivity(new Intent(ChooseLanguageActivity.this, SignInActivity.class));
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });
        storagePermission();
    }

    private void setLanguageType(String langType) {
        showProgreass();
        String url = String.format(ServerConfigStage.GET_OPTION_LANGUAGE());
        RetrofitTask task = new RetrofitTask<Offline>(ChooseLanguageActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LANG_TYPE, url, this);
        String session = SharedPreferenceManager.getToken(this);
        task.executeDriverLangType("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjMzIn0.rj5h2fI7lwP_JZwZpcxa3n87-FXxQjhmA_RWX_wdIHQ", langType, "driver");
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void storagePermission() {
        if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, permissionsRequired[2]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, permissionsRequired[4]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, permissionsRequired[5]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])
                    || ActivityCompat.checkSelfPermission(this, permissionsRequired[3]) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[4])
                    ||ActivityCompat.checkSelfPermission(this, permissionsRequired[5]) != PackageManager.PERMISSION_GRANTED) {
                //Show Information about why you need the permission
                ActivityCompat.requestPermissions(ChooseLanguageActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);

            } else if (permissionStatus.getBoolean(permissionsRequired[0], false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                sentToSettings = true;
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);

            } else {
                //just request the permission
                ActivityCompat.requestPermissions(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {

            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[2])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[3])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[4])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[5])) {
                ActivityCompat.requestPermissions(ChooseLanguageActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);

            } else {
                Toast.makeText(getBaseContext(), R.string.no_permision, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRetrofitTaskComplete(Response<Offline> response, Context context, CommonUtility.CallerFunction _callerFunction)
    {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    startActivity(new Intent(ChooseLanguageActivity.this, SignInActivity.class));
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();


    }
}




