package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.CarCategoryAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.interfaces.RecyclerViewItemClickListener;
import com.techconlabs.najezdriver.model.CarListing;
import com.techconlabs.najezdriver.model.Datum;
import com.techconlabs.najezdriver.model.OptVehicleFamily;
import com.techconlabs.najezdriver.model.RideNotification;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import retrofit2.Response;

public class CarCategoryActivity extends AppCompatActivity implements Serializable, RecyclerViewItemClickListener, RetrofitTaskListener<CarListing> {
    private ArrayList<Datum> carCategoryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CarCategoryAdapter mAdapter;
    private ProgressDialog progressDialog;
    private String driverId;
    private ImageView fab_icon,fabHome;
    private Button add_car_btn;
    List<OptVehicleFamily> familyList;
    RideNotification rideNotification;
    LocalBroadcastManager localBroadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_category);
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        String token = SharedPreferenceManager.getDeviceToken(CarCategoryActivity.this);
        Fabric.with(this, new Crashlytics());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
        driverId = SharedPreferenceManager.getDriverId(CarCategoryActivity.this);
        //  add_car_btn = (Button) findViewById(R.id.car_add_btn);
        fab_icon = (ImageView) findViewById(R.id.fab_add_icon);
        fabHome = (ImageView) findViewById(R.id.fabHome);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.tool_bar_text);
        mTitle.setText(R.string.car_cat_title);
        if (driverId != null && CommonUtility.isNetworkAvailable(CarCategoryActivity.this)) {
            apiCallService(driverId);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.internet_connecton)
                    .setCancelable(false)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

        }
        recyclerView = (RecyclerView) findViewById(R.id.car_cat_recycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        fabHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CarCategoryActivity.this, HomeActivity.class);
                intent.putExtra("v_block_home","v_block_home");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


         fab_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CarCategoryActivity.this, AddVechileInformationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        localBroadcastManager.registerReceiver(mMessageReceiver, new IntentFilter("NazejRideDriver"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        localBroadcastManager.unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {
                Bundle bundle1 = intent.getExtras();
                if (bundle1 != null && bundle1.getBoolean("unblockUser")) {
                   SharedPreferenceManager.setWrblockOnline(CarCategoryActivity.this, "1");
                    Toast.makeText(CarCategoryActivity.this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();


                }


            }



        }
    };
    @Override
    protected void onNewIntent(Intent intent) {
        if (intent != null) {
            Bundle bundle1 = intent.getExtras();
                     if (bundle1 != null && bundle1.getBoolean("vechileUnblock")) {
                    Toast.makeText(this, R.string.unblock_user, Toast.LENGTH_LONG).show();
                    finish();
                    startActivity(new Intent(this, CarCategoryActivity.class));
         }


            }
        }



    public void apiCallService(String driverId) {
        showProgreass();
        String url = String.format(ServerConfigStage.CAR_LISTING());
        RetrofitTask task = new RetrofitTask<CarListing>(CarCategoryActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CAR_LISTING, url, CarCategoryActivity.this);
        String session = SharedPreferenceManager.getToken(this);
        task.executeCarListing(session,driverId);
 }

    public void showProgreass() {
        progressDialog = new ProgressDialog(CarCategoryActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }


    @Override
    public void onItemClickListener(int position) {
             SharedPreferenceManager.setfamilyId(CarCategoryActivity.this, carCategoryList.get(position).getVehicleFamilyId());
        SharedPreferenceManager.setModelName(CarCategoryActivity.this, carCategoryList.get(position).getVehicleModel());
        SharedPreferenceManager.setModelNum(CarCategoryActivity.this, carCategoryList.get(position).getVehicleNumber());
        SharedPreferenceManager.setModelYear(CarCategoryActivity.this, carCategoryList.get(position).getVehicleManufactureYear());
        SharedPreferenceManager.setSerialNum(CarCategoryActivity.this, carCategoryList.get(position).getVehicleSno());
        SharedPreferenceManager.setColorCode(CarCategoryActivity.this, carCategoryList.get(position).getVehicleColor());
        SharedPreferenceManager.setVechileId(CarCategoryActivity.this, carCategoryList.get(position).getVehicleId());

        familyList = carCategoryList.get(position).getOptVehicleFamily();
        if (carCategoryList.get(position).getOptVehicleFamily() != null && !carCategoryList.get(position).getOptVehicleFamily().isEmpty()) {
            SharedPreferenceManager.setVechileList(CarCategoryActivity.this, familyList);
        } else {
            SharedPreferenceManager.setVechileList(CarCategoryActivity.this, familyList);
        }
        if (carCategoryList.get(position).getDocVal().equals("") || carCategoryList.get(position).getDocVal() == null) {
            SharedPreferenceManager.setCarImage(CarCategoryActivity.this, "");
        } else {
            SharedPreferenceManager.setCarImage(CarCategoryActivity.this, carCategoryList.get(position).getVehicleLogo());
        }
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



    @Override
    public void onRetrofitTaskComplete(Response<CarListing> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    carCategoryList = response.body().getData();
                    int size  =carCategoryList.size();
                    SharedPreferenceManager.setCarListSize(CarCategoryActivity.this,size);
                    mAdapter = new CarCategoryAdapter(this, carCategoryList, this);
                    recyclerView.setAdapter(mAdapter);
                    if(carCategoryList.size()>=3)
                    {
                        fab_icon.setVisibility(View.GONE);
                    }
                    else{
                        fab_icon.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.add));
                   }
                   for(int i=0;i<carCategoryList.size();i++) {
                       if (carCategoryList.get(i).getActive().equals("1")) {
                           fabHome.setVisibility(View.INVISIBLE);
                       }
                   }

                 }
             }
         }
     }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(CarCategoryActivity.this, getString(R.string.internet_connecton), Toast.LENGTH_LONG).show();


    }

   /* @Override
    public void onBackPressed() {
    public void onBackPressed()on {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }*/
}
