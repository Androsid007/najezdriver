package com.techconlabs.najezdriver.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.CancelledRideAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.fragment.CompleteTripFragment;
import com.techconlabs.najezdriver.fragment.StartTripMainFragment;
import com.techconlabs.najezdriver.location.MyLocationRideUpdate;
import com.techconlabs.najezdriver.location.MyLocationService;
import com.techconlabs.najezdriver.model.Cancel;
import com.techconlabs.najezdriver.model.CancelOption;
import com.techconlabs.najezdriver.model.CompleteRide;
import com.techconlabs.najezdriver.model.ConfirmArrival;
import com.techconlabs.najezdriver.model.DirectionsJSONParser;
import com.techconlabs.najezdriver.model.GetData;
import com.techconlabs.najezdriver.model.Review;
import com.techconlabs.najezdriver.model.RideValue;
import com.techconlabs.najezdriver.model.Trip;
import com.techconlabs.najezdriver.model.UserRideData;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Response;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, RetrofitTaskListener, CancelledRideAdapter.RidePositionCallback {

    private GoogleMap mMap;
    BroadcastReceiver receiver;
    private Marker marker1, marker;
    ProgressDialog progressDialog;
    double dest_lat = 28.4593;
    double dest_lng = 77.0724;
    private LatLng pickupLatlng;
    private boolean isFirstTime = true;
    private LatLng latLng;
    //private Button btn_piked, btn_nav;
    private boolean isMarkerRotating = false;
    private RelativeLayout rlContent;
    private GetData rideInformation;
    PopupWindow pw, cancel_Pop_up;
    private CancelOption caneloption;
    private LinearLayout parent_pop_layout;
    RecyclerView pop_recyclerView;
    private Button next_btn;
    private int radioPos = -1;
    String option_id;
    private CancelledRideAdapter adapter;
    BottomSheetDialog start_popup, arrivalpopup, completetrip;
    private Button submitButton;
    private TextView tv_totalfare, tv_clientname, tv_totaldistance, tv_timetaken;
    RatingBar ratingBar;
    Button ratingSubmit;
    String ratingbyuser;
    Dialog fareDialog, ratingDialog;
    private EditText et_review;
    String driverReview;
    RideValue rideData;
    UserRideData userRideData;
    String userName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getSupportFragmentManager().popBackStackImmediate();
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("rideInformation")) {
            rideInformation = (GetData) getIntent().getExtras().getSerializable("rideInformation");
            if (SharedPreferenceManager.getrideStatus(MapsActivity.this) == 3) {
                rideData = rideInformation.getRideData();
                userRideData = rideData.getUserdata();
                userName = userRideData.getFname();
                FragmentTransaction transection = getSupportFragmentManager().beginTransaction();
                CompleteTripFragment mfragment = new CompleteTripFragment();
                transection.setCustomAnimations(R.anim.popanim, R.anim.popanim);
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", rideInformation);
                mfragment.setArguments(bundle);
                transection.replace(R.id.popUpLayout, mfragment);
                transection.commit();
            } else {
                rideData = rideInformation.getRideData();
                userRideData = rideData.getUserdata();
                userName = userRideData.getFname();
                confirmArrivalApi();
            }
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            if (!runTimePermission()) {
                startLocationService();
            }

        }
    }

    @Override
    public void RidePos(int pos) {
        radioPos = pos;
    }

    private void confirmArrivalApi() {
        String url = String.format(ServerConfigStage.CONFIRM_DRIVER_ARRIVAL());
        RetrofitTask task = new RetrofitTask<ConfirmArrival>(MapsActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CONFIRM_ARRIVAL, url, MapsActivity.this);
        String session = SharedPreferenceManager.getToken(MapsActivity.this);
        task.executeDriverConfirmArrival(session, SharedPreferenceManager.getRideId(MapsActivity.this));

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(MapsActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void startTrippop() {

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        StartTripMainFragment startTripFragment= new StartTripMainFragment();
        fragmentTransaction.setCustomAnimations(R.anim.slide_up_dialog,R.anim.slide_out_down);
        Bundle bundle= new Bundle();
        bundle.putSerializable("data",rideInformation);
        startTripFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.popUpLayout,startTripFragment);
        fragmentTransaction.commit();

//        start_popup = new BottomSheetDialog(MapsActivity.this);
//        View popupView = getLayoutInflater().inflate(R.layout.start_trip_popup, null);
//        TextView textView = (TextView) popupView.findViewById(R.id.nameTextView);
//        textView.setText(userName);
//        start_popup.setContentView(popupView);
//        start_popup.show();
//        start_popup.setCanceledOnTouchOutside(false);
//
//        final Button startTrip_layout = (Button) start_popup.findViewById(R.id.btnStartTrip);
//        final RelativeLayout cancleTrip_layout = (RelativeLayout) start_popup.findViewById(R.id.cancleTrip_layput);
//
//        start_popup.setOnKeyListener(new DialogInterface.OnKeyListener() {
//            @Override
//            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
//                return true;
//            }
//        });
//        cancleTrip_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                start_popup.dismiss();
//                cancelRide();
//                cancelridepopup();
//            }
//        });
//        startTrip_layout.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                start_popup.dismiss();
//                StartTrip();
//            }
//        });
//        if (start_popup.isShowing() && start_popup != null) {
//            new CountDownTimer(10000, 1000) {
//                public void onTick(long millisUntilFinished) {
//
//                }
//
//                public void onFinish() {
//                    cancleTrip_layout.setVisibility(View.VISIBLE);
//                }
//            }.start();
//        } else {
//
//        }
    }

    private void cancelRide() {
        showProgreass();
        String url = String.format(ServerConfigStage.CANCEL_RIDE());
        RetrofitTask task = new RetrofitTask<CancelOption>(MapsActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.CANCEL_OPTION, url, MapsActivity.this);
        String session = SharedPreferenceManager.getToken(this);
        task.executeCancelRide(session);
    }

    private void cancelridepopup() {
        //  mp.stop();
        LayoutInflater inflater = (LayoutInflater) MapsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.cancel_ride_pop_up, null);
        layout.setAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up_dialog));
        cancel_Pop_up = new PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        cancel_Pop_up.showAtLocation(layout, Gravity.CENTER, 0, 0);
        cancel_Pop_up.setOutsideTouchable(true);
        cancel_Pop_up.setFocusable(false);
        cancel_Pop_up.update();
        pop_recyclerView = (RecyclerView) layout.findViewById(R.id.radio_recyclerView);
        parent_pop_layout = (LinearLayout) layout.findViewById(R.id.parent_pop_layout);
        pop_recyclerView.setHasFixedSize(false);
        LinearLayoutManager verticalmanager = new LinearLayoutManager(MapsActivity.this);
        pop_recyclerView.setLayoutManager(verticalmanager);
        next_btn = (Button) layout.findViewById(R.id.pop_up_btn);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_Pop_up.dismiss();
                if (radioPos != -1) {
                    option_id = caneloption.getOptions().get(radioPos).getId();
                    afterOptionCancelRide();
                    start_popup.dismiss();
                } else {

                }
            }
        });


    }

    private void afterOptionCancelRide() {
        showProgreass();
        String url = String.format(ServerConfigStage.CANCEL_RRIDE());
        RetrofitTask task = new RetrofitTask<Trip>(MapsActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CANCEL_RIDE, url, MapsActivity.this);
        String session = SharedPreferenceManager.getToken(MapsActivity.this);
        task.executeDriverCancelRide(session, SharedPreferenceManager.getRideId(MapsActivity.this), "driver", option_id);
    }

    private void StartTrip() {
        showProgreass();
        String url = String.format(ServerConfigStage.START_TRIP());
        RetrofitTask task = new RetrofitTask<Trip>(MapsActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_START_TRIP, url, MapsActivity.this);
        String session = SharedPreferenceManager.getToken(MapsActivity.this);
        task.executeDriverStartTrip(session, SharedPreferenceManager.getRideId(MapsActivity.this));
    }

    private void DriverReview(String review) {
        showProgreass();
        String url = String.format(ServerConfigStage.Driver_Review());
        RetrofitTask task = new RetrofitTask<Review>(MapsActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_RATING_REVIEW, url, MapsActivity.this);
        String session = SharedPreferenceManager.getToken(MapsActivity.this);
        task.executeDriverReview(session, SharedPreferenceManager.getRideId(MapsActivity.this), ratingbyuser, review);
    }

    private void completeTrippopup() {
        completetrip = new BottomSheetDialog(MapsActivity.this);
        View popupView = getLayoutInflater().inflate(R.layout.complete_trip_popup, null);
        TextView textViewName = (TextView) popupView.findViewById(R.id.textViewName);
        textViewName.setText(userName);
        completetrip.setContentView(popupView);
        completetrip.show();
        completetrip.setCanceledOnTouchOutside(false);
        completetrip.setCancelable(false);


        RelativeLayout complete_trip_layout = (RelativeLayout) completetrip.findViewById(R.id.complete_trip_layout);
        complete_trip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rideCompleteApi();
            }
        });

    }

    private void totalfare(CompleteRide tripComplete) {
        fareDialog = new Dialog(MapsActivity.this);
        fareDialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        fareDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        fareDialog.getWindow().setGravity(Gravity.CENTER);
        fareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fareDialog.setContentView(R.layout.fragment_collect_cash);
        fareDialog.setCanceledOnTouchOutside(false);
        fareDialog.setCancelable(true);
        fareDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
               /* if (keyCode == KeyEvent.KEYCODE_BACK){
                    return false;
                }*/
                return true;
            }
        });
      /***************INIT*****************/
        tv_totalfare = (TextView)  fareDialog.findViewById(R.id.totalcashTv);
        tv_clientname = (TextView) fareDialog.findViewById(R.id.clientnameTv);
        tv_clientname.setText(userName);
        tv_totaldistance = (TextView) fareDialog.findViewById(R.id.distanceTv);
        tv_timetaken = (TextView) fareDialog.findViewById(R.id.timeTv);
        tv_timetaken.setText(tripComplete.getMinutes() + " Minutes");
        submitButton = (Button) fareDialog.findViewById(R.id.done_btn);
        tv_totalfare.setText(tripComplete.getTotalAmount());
        tv_totaldistance.setText(tripComplete.getKilometers());


        /***************Button Click*******************/
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fareDialog.dismiss();
                ratingDialog();

            }
        });

        fareDialog.show();
    }

    private void ratingDialog() {
        final BottomSheetDialog ratingDialog = new BottomSheetDialog(MapsActivity.this);
        ratingDialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;

        ratingDialog.getWindow().setGravity(Gravity.CENTER_VERTICAL);

        ratingDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
         ratingDialog.setContentView(R.layout.rating_screen);
        ratingDialog.setCanceledOnTouchOutside(false);
        ratingDialog.setCancelable(true);
        ratingDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        ratingDialog.setCancelable(false);
        TextView textView = (TextView) ratingDialog.findViewById(R.id.userRatingTextView);
        textView.setText("How Was Your Trip With " + userName + "?");

         ratingBar = (RatingBar) ratingDialog.findViewById(R.id.ratingBar);
        ratingSubmit = (Button) ratingDialog.findViewById(R.id.ratingSubmit);
        et_review = (EditText) ratingDialog.findViewById(R.id.rdivereeviewEt);
        /***************Button Click*******************/
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingbyuser = String.valueOf(rating);
            }
        });
        ratingSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driverReview = et_review.getText().toString().trim();
                DriverReview(driverReview);
                //ratingDialog();
                ratingDialog.dismiss();
                finish();
            }
        });
        ratingDialog.show();

    }


    private void rideCompleteApi() {
        showProgreass();
        String url = String.format(ServerConfigStage.RIDE_COMPLETE());
        RetrofitTask task = new RetrofitTask<Trip>(MapsActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_END_TRIP, url, MapsActivity.this);
        String session = SharedPreferenceManager.getToken(MapsActivity.this);
        task.executeDriverRideComplete(session, SharedPreferenceManager.getRideId(MapsActivity.this));
    }

    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_END_TRIP && response != null) {
            stopProgress();
            CompleteRide tripComplete = (CompleteRide) response.body();
            if (response.isSuccessful()) {
                if (tripComplete.getStatus() > 0) {
                    completetrip.dismiss();
                    //Toast.makeText(context, tripComplete.getTotalAmount(), Toast.LENGTH_LONG).show();
                    if (CommonUtility.isMyServiceRunning(MyLocationRideUpdate.class, this)) {
                        this.stopService(new Intent(this, MyLocationRideUpdate.class));
                    }
                    totalfare(tripComplete);
                    SharedPreferenceManager.setOnRdieStatus(MapsActivity.this, false);
                  /*  if(tripComplete.getRemainAmount().equals("0")) {
                        if(tripComplete.getPaymentMethod().equals("wallet")) {
                            totalfare(tripComplete);
                            SharedPreferenceManager.setOnRdieStatus(MapsActivity.this, false);
                        }
                        else if(tripComplete.getPaymentMethod().equals("cash")) {
                            totalfare(tripComplete);
                            SharedPreferenceManager.setOnRdieStatus(MapsActivity.this, false);
                        }
     }
                        else{
                        totalfare(tripComplete);
                        SharedPreferenceManager.setOnRdieStatus(MapsActivity.this, false);

                    }*/


                }

            } else {
                //Toast.makeText(context, tripComplete.getMessage(), Toast.LENGTH_LONG).show();

            }
        }

        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_START_TRIP && response != null) {
            stopProgress();
            Trip off = (Trip) response.body();

            if (response.isSuccessful()) {
                if (off.getStatus() > 0) {
                    pickupLatlng = new LatLng(Double.parseDouble(rideInformation.getRideData().getDropLat()), Double.parseDouble(rideInformation.getRideData().getDropLng()));
                   // Toast.makeText(context, off.getMessage(), Toast.LENGTH_LONG).show();

                    completeTrippopup();
                }

            } else {
               // Toast.makeText(context, off.getMessage(), Toast.LENGTH_LONG).show();
                completeTrippopup();


            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.CONFIRM_ARRIVAL && response != null) {
            stopProgress();
            ConfirmArrival arrival = (ConfirmArrival) response.body();

            if (response.isSuccessful()) {
                if (arrival.getStatus() > 0) {
                    pickupLatlng = new LatLng(Double.parseDouble(rideInformation.getRideData().getPickupLat()), Double.parseDouble(rideInformation.getRideData().getPickupLng()));
                     this.startService(new Intent(this, MyLocationRideUpdate.class));
                    SharedPreferenceManager.setrideStatus(MapsActivity.this,2);
                       startTrippop();
                }

            } else {
                //Toast.makeText(context, arrival.getMessage(), Toast.LENGTH_LONG).show();

            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.CANCEL_OPTION && response != null) {
            caneloption = (CancelOption) response.body();

            stopProgress();


            if (response.isSuccessful()) {
                if (caneloption.getStatus() > 0) {
                    parent_pop_layout.setVisibility(View.VISIBLE);
               //     adapter = new CancelledRideAdapter(caneloption, MapsActivity.this);
                    pop_recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(MapsActivity.this, caneloption.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        }
        if (_callerFunction == CommonUtility.CallerFunction.CANCEL_RIDE && response != null) {
            stopProgress();
            Cancel cancel = (Cancel) response.body();
          if (response.isSuccessful()) {
                if (cancel.getStatus() > 0) {
                   // Toast.makeText(context, cancel.getMessage(), Toast.LENGTH_LONG).show();
                    SharedPreferenceManager.setdetailFlag(this,true);
                    SharedPreferenceManager.setOnRdieStatus(this,false);
                    if (CommonUtility.isMyServiceRunning(MyLocationRideUpdate.class, this)) {
                        this.stopService(new Intent(this, MyLocationRideUpdate.class));
                    }
                    startActivity(new Intent(MapsActivity.this, HomeActivity.class));

                }

            } else {
                Toast.makeText(context, cancel.getMessage(), Toast.LENGTH_LONG).show();

            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_RATING_REVIEW && response != null) {
            stopProgress();
            Review review = (Review) response.body();

            if (response.isSuccessful()) {
                if (review.getStatus() > 0) {
                  //  Toast.makeText(context, review.getMessage(), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(MapsActivity.this, HomeActivity.class));

                }

            } else {
                Toast.makeText(context, review.getMessage(), Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();


    }

    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startLocationService() {
        if (CommonUtility.isMyServiceRunning(MyLocationService.class, MapsActivity.this)) {
            stopService(new Intent(getApplicationContext(), MyLocationService.class));
        }
        startService(new Intent(getApplicationContext(), MyLocationService.class));
       // showProgreass();
    }

    private boolean runTimePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 120);
            return true;
        }
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (receiver == null) {
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                   /* Toast.makeText(context, "latitude = "+intent.getExtras().getString("lat")+",\nlongitude = "+intent.getExtras().getString("lng"), Toast.LENGTH_LONG).show();
                   */
                    double latitude = Double.parseDouble(intent.getExtras().getString("lat"));
                    double longitude = Double.parseDouble(intent.getExtras().getString("lng"));
                    latLng = new LatLng(latitude, longitude);
                    try {
                        //rlContent.setVisibility(View.VISIBLE);
                        String url = getDirectionsUrl(latLng, pickupLatlng);
                        DownloadTask downloadTask = new DownloadTask();
                        // Start downloading json data from Google Directions API
                        downloadTask.execute(url);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
        }
        registerReceiver(receiver, new IntentFilter(getString(R.string.location_update)));
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Excption while down url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }


        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            LatLng positionNext = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result == null || result.size() < 1) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (isFirstTime) {
                    Toast.makeText(MapsActivity.this, R.string.no_points_to_route, Toast.LENGTH_SHORT).show();
                    isFirstTime = false;
                    if (pickupLatlng != null) {
                        marker = mMap.addMarker(new MarkerOptions().position(pickupLatlng).icon(BitmapDescriptorFactory.defaultMarker()).title("destination"));
                    }
                    marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker()).draggable(true).title("source"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
                }
                return;
            }
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        positionNext = new LatLng(lat, lng);
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(6);
                lineOptions.color(Color.BLUE).geodesic(true);
            }

            //tvDistanceDuration.setText("Distance:" + distance + ", Duration:" + duration);

            // Drawing polyline in the Google Map for the i-th route

            Geocoder geocoder = new Geocoder(MapsActivity.this);
            try {
                List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                String address = addressList.get(0).getLocality() + ", ";
                address += addressList.get(0).getCountryName();
                        /*if(marker!=null){
                            marker.remove();
                        }*/

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,13.2f));
                mMap.clear();
                if (pickupLatlng != null) {
                    marker = mMap.addMarker(new MarkerOptions().position(pickupLatlng).icon(BitmapDescriptorFactory.defaultMarker()).title("destination"));
                }
                marker = mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker()).draggable(true).title(address));
                float bearing = (float) bearingBetweenLocations(latLng, pickupLatlng);
                rotateMarker(marker, bearing);
                mMap.addPolyline(lineOptions);
                //mMap.setTrafficEnabled(true);
                animateMarker(marker, latLng, false);
                if (isFirstTime) {
                    isFirstTime = false;
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
                }
                //mMap.animateCamera(CameraUpdateFactory.zoomIn());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int resourceId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, resourceId);
        vectorDrawable.setBounds(0, 0, 50, 50);
        Bitmap bitmap = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private BitmapDescriptor bitmapDescriptorFromVectorDest(Context context, int resourceId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, resourceId);
        vectorDrawable.setBounds(0, 0, 200, 200);
        Bitmap bitmap = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void animateMarker(final Marker marker, final LatLng toPosition, final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
        //stopService(new Intent(getApplicationContext(),MyLocationService.class));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(googleMap!=null) {
            mMap = googleMap;
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = googleMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                MapsActivity.this, R.raw.style_json));

                if (!success) {
                    Log.e("", "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e("", "Can't find style. Error: ", e);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 120) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // iLocationPresenter.startGettingLocation();
                startLocationService();
            } else {
                runTimePermission();
            }
        }
    }


    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    private void rotateMarker(final Marker markernew, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = markernew.getRotation();
            final long duration = 2000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    float bearing = -rot > 180 ? rot / 2 : rot;

                    markernew.setRotation(bearing);

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

}