package com.techconlabs.najezdriver.activity;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Balance;
import com.techconlabs.najezdriver.model.VoucherNew;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import retrofit2.Response;

public class AddMoneyActivity extends AppCompatActivity implements RetrofitTaskListener {
    private TextInputLayout phone_inp, money_inp;
    TextInputLayout amountLayout;
    EditText et_amount;
    Button pay_btn, cupuon_btn;
    private ProgressDialog progressDialog;
    String walletMoney;
    TextView balanceTv;
    private android.support.v4.app.FragmentManager fragmentManager;
    Dialog otp_popup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_money);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(R.string.add_money);
        walletMoney = SharedPreferenceManager.getWalletBalance(AddMoneyActivity.this);

        amountLayout = (TextInputLayout) findViewById(R.id.amountLayout);
        et_amount = (EditText) findViewById(R.id.amountAddEt);
        pay_btn = (Button) findViewById(R.id.pay_btn);
        amountLayout = (TextInputLayout) findViewById(R.id.amountLayout);
        balanceTv = (TextView) findViewById(R.id.balanceTv);
        cupuon_btn = (Button) findViewById(R.id.oflinecupon_Btn);
        if (!walletMoney.equals("0")) {
            balanceTv.setText(getString(R.string.wallet_bal) + " " + walletMoney);
        }
        pay_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String addAmount = et_amount.getText().toString();
                if (!TextUtils.isEmpty(addAmount)) {
                    amountLayout.clearFocus();
                    amountLayout.setErrorEnabled(false);
                   // addMoneyWallet(addAmount);
                } else {
                    amountLayout.requestFocus();
                    amountLayout.setErrorEnabled(true);
                    amountLayout.setError(getString(R.string.enter_amt_to));

                }
            Toast.makeText(AddMoneyActivity.this,"Payment Gateway is Under Developement",Toast.LENGTH_LONG).show();

            }
        });
        cupuon_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               loadPopUP();


            }
        });

    }

    private void OfflineCoupan(String coupanCode) {
        showProgreass();
        String url = String.format(ServerConfigStage.OFFLINE_VOUCHER());
        RetrofitTask task = new RetrofitTask<VoucherNew>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.OFF_VOUCHER, url, this);
        String session = SharedPreferenceManager.getToken(this);
        task.executeOffVoucher(session, coupanCode, SharedPreferenceManager.getDriverId(this), "driver");
    }

    private void loadPopUP() {
        otp_popup = new Dialog(AddMoneyActivity.this);
        otp_popup.getWindow().getAttributes().windowAnimations = R.style.Theme_Dialog;
        otp_popup.getWindow().setGravity(Gravity.CENTER);
        otp_popup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        otp_popup.setContentView(R.layout.enter_otp_layout_new);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(otp_popup.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        otp_popup.getWindow().setAttributes(lp);


        otp_popup.setCancelable(true);
        final EditText otp_tv = (EditText) otp_popup.findViewById(R.id.otp_et);
        Button confirmBtn = (Button) otp_popup.findViewById(R.id.confirmBtn);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String otp = otp_tv.getText().toString().trim();
                if (!TextUtils.isEmpty(otp)) {
                    OfflineCoupan(otp);
                    //apiServices.callNormalOtpVerify(email_id,otp);
                } else {
                    Toast.makeText(AddMoneyActivity.this, R.string.copon_code, Toast.LENGTH_LONG).show();
                }

            }
        });

        otp_popup.show();
    }

    private void addMoneyWallet(String addAmount) {
        showProgreass();
        String url = String.format(ServerConfigStage.USER_ADD_BALANCE());
        RetrofitTask task = new RetrofitTask<Balance>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ADD_BALANCE, url, this);
        String session = SharedPreferenceManager.getToken(this);
        task.executeDriverAddBalance(session, SharedPreferenceManager.getDriverId(this), addAmount, "4564564546");
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        {
            if (_callerFunction == CommonUtility.CallerFunction.ADD_BALANCE && response != null) {
                stopProgress();
                Balance balance = (Balance) response.body();

                if (response.isSuccessful()) {
                    if (balance.getStatus() > 0) {
                        String totalBal = balance.getBalance();
                        SharedPreferenceManager.setWalletBalance(this, totalBal);
                        Toast.makeText(context, balance.getMessage().toString(), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }

                } else {
                    Toast.makeText(context, balance.getMessage(), Toast.LENGTH_LONG).show();


                }
            }
            if (_callerFunction == CommonUtility.CallerFunction.OFF_VOUCHER && response != null) {
                stopProgress();
                VoucherNew voucher = (VoucherNew) response.body();

                if (response.isSuccessful()) {
                    if (voucher.getStatus() > 0) {
                        otp_popup.dismiss();
                        String totalBal = voucher.getBalance();
                        SharedPreferenceManager.setWalletBalance(this, totalBal);
                        Toast.makeText(context, voucher.getMessage().toString(), Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    } else {
                        otp_popup.dismiss();
                        Toast.makeText(context, voucher.getMessage(), Toast.LENGTH_LONG).show();
                       // onBackPressed();


                    }
                }
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();


    }
}

