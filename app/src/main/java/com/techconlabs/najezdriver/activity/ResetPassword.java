package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Reset;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

public class ResetPassword extends AppCompatActivity implements RetrofitTaskListener<Reset> {
    private Button resetbtn;
    private EditText resetPaswd, resetcCnfrm, otpCode;
    private ProgressDialog progressDialog;
    // SessionManager session;
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;
    TextInputLayout usedpassword, newpassword, comparepassword;
    String mobile, newPassword, otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        if (getIntent().getExtras().getString("mobile") != null) {
            mobile = getIntent().getExtras().getString("mobile");
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.silver));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_up_appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(getResources().getString(R.string.reset_pwd));
        sharedPreferences = this.getSharedPreferences("NAME", 0);
        editor = sharedPreferences.edit();


        init();


        resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPassword = resetcCnfrm.getText().toString().trim();
                otp = otpCode.getText().toString().trim();
            if (vlidate()) {
                    String url = String.format(ServerConfigStage.DRIVER_RESET());
                    Log.e("url", url);

                    RetrofitTask task = new RetrofitTask<Reset>(ResetPassword.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_UPDATE_PASSWORD, url, ResetPassword.this);
                    task.executeUpdatePassword(mobile, newPassword, otp, SharedPreferenceManager.getLanguage(ResetPassword.this));

                }
            }
        });


    }

    private void init() {
        resetbtn = (Button) findViewById(R.id.resetButton);
        resetPaswd = (EditText) findViewById(R.id.passwordReset);
        resetcCnfrm = (EditText) findViewById(R.id.confirmResetPassword);
        otpCode = (EditText) findViewById(R.id.otpCode);

        //TextInput Layout
        usedpassword = (TextInputLayout) findViewById(R.id.oldpassL);
        newpassword = (TextInputLayout) findViewById(R.id.newpassL);
        comparepassword = (TextInputLayout) findViewById(R.id.confpassL);


        Typeface tf = Typeface.createFromAsset(getAssets(), "font/roboto_light.ttf");
        resetPaswd.setTypeface(tf);
        resetcCnfrm.setTypeface(tf);
        otpCode.setTypeface(tf);
        usedpassword.setTypeface(tf);
        newpassword.setTypeface(tf);
        comparepassword.setTypeface(tf);
    }

    public Boolean vlidate() {
        boolean valid = true;
        String password = resetPaswd.getText().toString().trim();
        String passcom = resetcCnfrm.getText().toString().trim();
        String oldpass = otpCode.getText().toString().trim();
        if (oldpass.isEmpty()) {
            usedpassword.setErrorEnabled(true);
            usedpassword.setError(getString(R.string.otp_error));
            usedpassword.requestFocus();
            valid = false;

        } else if (password.isEmpty() || password.length() <=7) {
            usedpassword.setErrorEnabled(false);
            usedpassword.clearFocus();
            newpassword.setErrorEnabled(true);
            newpassword.setError(getString(R.string.password_error));
            newpassword.requestFocus();
            valid = false;
        } else if (passcom.isEmpty() || !passcom.equals(password)) {
            newpassword.setErrorEnabled(false);
            newpassword.clearFocus();
            comparepassword.setError(getString(R.string.pswd_match_error));
            comparepassword.setErrorEnabled(true);
            comparepassword.requestFocus();
            valid = false;
        }

        return valid;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(ResetPassword.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();

    }


    @Override
    public void onRetrofitTaskComplete(retrofit2.Response<Reset> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(ResetPassword.this, getString(R.string.internet_connecton), Toast.LENGTH_LONG).show();


    }

}
