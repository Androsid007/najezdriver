package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.users.model.QBUser;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.chatNew.QbLogin;
import com.techconlabs.najezdriver.chatNew.QbSignInListener;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Driverdata;
import com.techconlabs.najezdriver.model.QBModel;
import com.techconlabs.najezdriver.model.VerifyOtp;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import retrofit2.Response;

public class VerifyAccountActivity extends AppCompatActivity implements RetrofitTaskListener,QbSignInListener {
    private Button confirm_btn;
    private EditText opt_et;
    private String otp_s;
    private ProgressDialog progressDialog;
    private String mobileNum, emailId, mobile;
    private EditText otpVerify;
    private String otpSubmit;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_account);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
   if (getIntent().getExtras().getString("mobile") != null && !getIntent().getExtras().getString("mobile").equals("")) {
            mobileNum = getIntent().getExtras().getString("mobile");
            //mobile = "91" + mobileNum;
            mobile = "966" + mobileNum;

   }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_new);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.tilte_bar);
        mTitle.setText(R.string.verify_otp_app);
        init();
        onclick();

    }


    private void onclick() {
        confirm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpSubmit = otpVerify.getText().toString().trim();
                toVerifyOtpToServer(mobile, otpSubmit);

            }
        });
    }

    public void toVerifyOtpToServer(String mobileNum, String otpSubmit) {
        showProgreass();
        String url = String.format(ServerConfigStage.OTP_VERIFY_TO_SERVER());
        RetrofitTask task = new RetrofitTask<VerifyOtp>(VerifyAccountActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.OTP_VERIFY_TO_SERVER, url, VerifyAccountActivity.this);
        task.executeOtpSendToServer(mobileNum, otpSubmit, "android", SharedPreferenceManager.getDeviceToken(VerifyAccountActivity.this),SharedPreferenceManager.getLanguage(VerifyAccountActivity.this));


    }


    private void init() {
        otpVerify = (EditText) findViewById(R.id.otpVerify);
        confirm_btn = (Button) findViewById(R.id.verify_btn);

    }

    private boolean validate() {
       /* otp_s = opt_et.getText().toString().trim();

        if (otp_s.isEmpty()){
            opt_et.setError(getString(R.string.plaese_enter_otp));
            return false;
        }*/

        return true;
    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(VerifyAccountActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void sendQbSenderId(QBUser qbUser) {
        showProgreass();
        String url = String.format(ServerConfigStage.QB_SENDER_ID());
        RetrofitTask task = new RetrofitTask<QBModel>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.QBID_SEND_TOSERVER, url, this);
        String token = SharedPreferenceManager.getToken(this);
        task.executeQbIdServer(token, String.valueOf(qbUser.getId()), qbUser.getEmail(), "driver");

    }


    private void afterLogin() {
        if (SharedPreferenceManager.getvechileCount(this) > 0) {
            // Toast.makeText(SignInActivity.this, R.string.register_sucess, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(VerifyAccountActivity.this, CarCategoryActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();

        } else {
            //Vechle Registation
            Intent intent = new Intent(VerifyAccountActivity.this, AddVechileInformationActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();

        }
    }
    @Override
    public void onQbSignInSuccess(QBUser qbUser) {
        if (qbUser.getId() != null) {
            sendQbSenderId(qbUser);
        } else {
            afterLogin();
        }
    }
    private void navigateToMainActivity(String emaiid,String password) {
        QBUser qbUser = new QBUser();
        qbUser.setEmail(emaiid);
        qbUser.setLogin(emaiid);
        qbUser.setPassword(password);
        QbLogin qbLogin = new QbLogin(this);
        qbLogin.signIn(qbUser,findViewById(R.id.back),VerifyAccountActivity.this);
    }



    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        if (_callerFunction == CommonUtility.CallerFunction.OTP_VERIFY_TO_SERVER && response != null) {
            VerifyOtp verifyOtp = (VerifyOtp) response.body();
            stopProgress();
            if (response.isSuccessful()) {
                if (verifyOtp.getStatus() > 0) {
                    Driverdata driverdata = verifyOtp.getDriverdata();
                    if (driverdata != null) {
                        String referCode = driverdata.getReferralcode();
                        String eMail = driverdata.getEmailid();
                        String fName = driverdata.getFname();
                        String token = verifyOtp.getToken();
                        String mobileNumber = driverdata.getMobile();
                        String city = driverdata.getCity();
                        String driverId = driverdata.getId();
                        int vechileCount = verifyOtp.getVehicleCount();
                        String canDeliver = driverdata.getCanDriverDeliver();
                        String webLock = driverdata.getWebblock();
                        int balance = 0;
                        if (verifyOtp.getBalance() != null) {
                            balance = Integer.parseInt(verifyOtp.getBalance());
                        }

                        SharedPreferenceManager.setReferalCode(context, referCode);
                        SharedPreferenceManager.setEmailId(context, eMail);
                        SharedPreferenceManager.setFirstName(context, fName);
                        SharedPreferenceManager.setFirstName(context, fName);
                        SharedPreferenceManager.setMobileNumber(context, mobileNumber);
                        SharedPreferenceManager.setCity(context, city);
                        SharedPreferenceManager.setBalance(context, balance);
                        SharedPreferenceManager.setToken(context, token);
                        SharedPreferenceManager.setvechileCount(context, vechileCount);
                        SharedPreferenceManager.setDriverId(context, driverId);
                        SharedPreferenceManager.setCanDeliver(context, canDeliver);
                        SharedPreferenceManager.setWrblockOnline(context, webLock);
                        SharedPreferenceManager.setLoggin(context, true);
                        navigateToMainActivity(SharedPreferenceManager.getEmailId(this), SharedPreferenceManager.getPassword(this));

                    } else {
                        Toast.makeText(VerifyAccountActivity.this, verifyOtp.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(VerifyAccountActivity.this, verifyOtp.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } else if (_callerFunction == CommonUtility.CallerFunction.QBID_SEND_TOSERVER && response != null) {
            stopProgress();
            if (response.body() != null) {
                QBModel qbModel = (QBModel) response.body();
                if (qbModel.getStatus() > 0) {
                    afterLogin();
                }

            } else {
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
    }
}


