package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.CardListAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.interfaces.RecyclerViewItemClickListener;
import com.techconlabs.najezdriver.model.CardModel;
import com.techconlabs.najezdriver.model.Data;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.List;

import retrofit2.Response;

public class SavedCardActivity extends AppCompatActivity implements RetrofitTaskListener, RecyclerViewItemClickListener {
    private ProgressDialog progressDialog;
    private CardListAdapter cardListAdapter;
    private RecyclerView save_cardRv;
    private CardView llAddNewCard;
    List<Data> dataList;
    private int removePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_card);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_up_appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(R.string.select_card_title);

        save_cardRv = (RecyclerView) findViewById(R.id.save_cardRv);
        llAddNewCard = (CardView) findViewById(R.id.llAddNewCard);
        save_cardRv.setHasFixedSize(false);
        LinearLayoutManager verticalmanager = new LinearLayoutManager(this);
        save_cardRv.setLayoutManager(verticalmanager);
        llAddNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SavedCardActivity.this, AddNewCardActivity.class);
                startActivity(intent);
            }
        });
    }

    private void callGetCardsApi() {
        showProgreass();
        String url = String.format(ServerConfigStage.GET_SAVED_CARD());
        RetrofitTask task = new RetrofitTask(SavedCardActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_SAVED_CARD, url, SavedCardActivity.this);
        task.execute();
    }


    private void callRemoveCardApi(String cardId) {
        showProgreass();
        CardModel cardModel = new CardModel();
        cardModel.setCardId(cardId);
        cardModel.setType("driver");
        String url = String.format(ServerConfigStage.REMOVE_SAVED_CARD());
        RetrofitTask task = new RetrofitTask(SavedCardActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.REMOVE_SAVED_CARD, url, SavedCardActivity.this, cardModel);
        task.execute();
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(SavedCardActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (_callerFunction == CommonUtility.CallerFunction.GET_SAVED_CARD) {
            if (response != null && response.body() != null) {
                CardModel cardModel = (CardModel) response.body();
                if (cardModel.getStatus() == 1) {
                    if (cardModel.getData() != null && cardModel.getData().size() > 0) {
                        dataList = cardModel.getData();
                        cardListAdapter = new CardListAdapter(SavedCardActivity.this, dataList, SavedCardActivity.this);
                        save_cardRv.setAdapter(cardListAdapter);
                    }
                } else if (cardModel.getStatus() == 2) {
                    showToast("unable to fetch cards");
                }
            }
        } else if (_callerFunction == CommonUtility.CallerFunction.REMOVE_SAVED_CARD) {
            if (response != null && response.body() != null) {
                CardModel cardModel = (CardModel) response.body();
                if (cardModel.getStatus() == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SavedCardActivity.this);
                    builder.setMessage("Are You Sure You Want To remove This Card").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    cardListAdapter.deleteItem(removePosition);
                                    callGetCardsApi();

                                }
                            }

                    ).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                }
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        showToast("server error");
    }


    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClickListener(int position) {
        removePosition = position;
        callRemoveCardApi(dataList.get(position).getCardId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        callGetCardsApi();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
