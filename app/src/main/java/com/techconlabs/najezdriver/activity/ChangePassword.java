package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.ChangePass;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;


public class ChangePassword extends AppCompatActivity implements RetrofitTaskListener<ChangePass> {
    private Button resetbtn;
    private EditText resetPaswd, resetcCnfrm, oldpassEt;
    private ProgressDialog progressDialog;
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;
    TextInputLayout newpassword, comparepassword, oldPassL;
    String mobile, newPassword, otp, oldpassword;
    String savedPass;
    String passcom;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_change_password);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_up_appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(R.string.change_password);
        if (SharedPreferenceManager.getPassword(ChangePassword.this) != null || SharedPreferenceManager.getPassword(ChangePassword.this).matches("")) {
            savedPass = SharedPreferenceManager.getPassword(ChangePassword.this);
        }
        init();

        resetbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = resetPaswd.getText().toString().trim();
                String passcom = resetcCnfrm.getText().toString().trim();
                String oldp = oldpassEt.getText().toString().trim();


                if (vlidate()) {
                    showProgreass();
                    String url = String.format(ServerConfigStage.USER_CHANGE_PASSWORD());
                    RetrofitTask task = new RetrofitTask<ChangePass>(ChangePassword.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_CHANGE_PASSWORD, url, ChangePassword.this);
                    String token = SharedPreferenceManager.getToken(ChangePassword.this);
                    task.executeChangePassword(token, oldp, passcom);

                }
            }
        });


    }

    private void init() {
        resetbtn = (Button) findViewById(R.id.resetButton);
        resetPaswd = (EditText) findViewById(R.id.passwordReset);
        resetcCnfrm = (EditText) findViewById(R.id.confirmResetPassword);
        oldpassEt = (EditText) findViewById(R.id.oldpassEt);
        //TextInput Layout
        newpassword = (TextInputLayout) findViewById(R.id.newpassL);
        comparepassword = (TextInputLayout) findViewById(R.id.confpassL);
        oldPassL = (TextInputLayout) findViewById(R.id.oldPassL);

        Typeface tf = Typeface.createFromAsset(getAssets(), "font/roboto_light.ttf");
        resetPaswd.setTypeface(tf);
        resetcCnfrm.setTypeface(tf);
        newpassword.setTypeface(tf);
        comparepassword.setTypeface(tf);
        oldPassL.setTypeface(tf);
        oldpassEt.setTypeface(tf);
    }

    public Boolean vlidate() {
        boolean valid = true;
        String password = resetPaswd.getText().toString().trim();
         passcom = resetcCnfrm.getText().toString().trim();
        String oldp = oldpassEt.getText().toString().trim();
        if (oldp.isEmpty()) {
            newpassword.setErrorEnabled(false);
            comparepassword.setErrorEnabled(false);
            oldPassL.requestFocus();
            oldPassL.setErrorEnabled(true);
            oldPassL.setError(getString(R.string.old_pwd_pass));
            return false;
        } else if (!oldp.equals(savedPass)) {
            newpassword.setErrorEnabled(false);
            comparepassword.setErrorEnabled(false);
            oldPassL.requestFocus();
            oldPassL.setErrorEnabled(true);
            oldPassL.setError(getResources().getString(R.string.password_error));
            return false;
        } else if (password.isEmpty() || password.length() <=7) {
            oldPassL.setErrorEnabled(false);
            comparepassword.setErrorEnabled(false);
            newpassword.setErrorEnabled(true);
            newpassword.setError(getString(R.string.password_error));
            newpassword.requestFocus();
            valid = false;
        } else if (passcom.isEmpty()) {
            oldPassL.setErrorEnabled(false);
            comparepassword.setErrorEnabled(false);
            newpassword.setErrorEnabled(false);
            newpassword.clearFocus();
            comparepassword.setError(getString(R.string.pass_empty));
            comparepassword.setErrorEnabled(true);
            comparepassword.requestFocus();
            valid = false;
        } else if (!passcom.equals(password)) {
            oldPassL.setErrorEnabled(false);
            // comparepassword.setErrorEnabled(false);
            newpassword.setErrorEnabled(false);
            newpassword.clearFocus();
            comparepassword.setError(getString(R.string.pswd_match_error));
            comparepassword.setErrorEnabled(true);
            comparepassword.requestFocus();
            valid = false;
        }

        return valid;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(ChangePassword.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    @Override
    public void onRetrofitTaskComplete(retrofit2.Response<ChangePass> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {

            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                    SharedPreferenceManager.setPassword(ChangePassword.this,passcom);
        } else {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }


            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(ChangePassword.this, getString(R.string.internet_connecton), Toast.LENGTH_LONG).show();


    }

    @Override
    public void onVisibleBehindCanceled() {
        super.onVisibleBehindCanceled();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }
}
