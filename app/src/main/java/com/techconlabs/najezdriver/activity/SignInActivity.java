package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.users.model.QBUser;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.chatNew.QbLogin;
import com.techconlabs.najezdriver.chatNew.QbSignInListener;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Driverdata;
import com.techconlabs.najezdriver.model.Family;
import com.techconlabs.najezdriver.model.Login;
import com.techconlabs.najezdriver.model.Luxury;
import com.techconlabs.najezdriver.model.Medium;
import com.techconlabs.najezdriver.model.Offline;
import com.techconlabs.najezdriver.model.Otp;
import com.techconlabs.najezdriver.model.QBModel;
import com.techconlabs.najezdriver.model.VehicleList;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.List;

public class SignInActivity extends AppCompatActivity implements RetrofitTaskListener, QbSignInListener {

    private EditText email, pass;
    private String emaiid, password;
    private Button LoginIn;
    private TextView frgtPswrd, sign_up_tv;
    private ProgressDialog progressDialog;
    private List<Family> familyArrayList;
    private List<Medium> mediumArrayList;
    private List<Luxury> luxuryArrayList;
    private VehicleList vehicleList;
    private TextInputLayout emailIlay, passIlay;
    private ImageView iv_arrow;
    private String mobileNo, token,model_number="";

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_in);
    init();
        frgtPswrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgetPassword.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        sign_up_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent2);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        LoginIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emaiid = email.getText().toString();
              //  mobileNo = "91" + emaiid;
                mobileNo = "966" + emaiid;


                password = pass.getText().toString();
                token = SharedPreferenceManager.getDeviceToken(SignInActivity.this);

                if (validate() && trueValid()) {
                    callSignUpService();

                }

            }
        });
    }




    private void navigateToMainActivity(String emaiid,String password) {
           QBUser qbUser = new QBUser();
        qbUser.setEmail(emaiid);
        qbUser.setLogin(emaiid);
        qbUser.setPassword(password);
        QbLogin qbLogin = new QbLogin(this);
        qbLogin.signIn(qbUser,findViewById(R.id.signUp),SignInActivity.this);
    }



    @Override
    protected void onNewIntent(Intent intent) {
        if (intent != null) {
            Bundle bundle1 = intent.getExtras();
            if (bundle1 != null && bundle1.getBoolean("activeUser")) {
                Toast.makeText(this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();
                finish();
                startActivity(new Intent(this, SignInActivity.class));

            }
            else if(bundle1 != null && bundle1.getBoolean("unblockUser")) {

                Toast.makeText(this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();
                SharedPreferenceManager.setWrblockOnline(this, "1");
                startActivity(new Intent(this, SignInActivity.class));
            }
            else if(bundle1 != null && bundle1.getBoolean("blockUser")) {
                 Toast.makeText(this, "You Are blocked By Najez TEAM", Toast.LENGTH_LONG).show();
                  startActivity(new Intent(this, SignInActivity.class));
            }
            else if(bundle1 != null && bundle1.getBoolean("deactUser")) {

                Toast.makeText(this, "You Are Active By Najez TEAM", Toast.LENGTH_LONG).show();
                  startActivity(new Intent(this, SignInActivity.class));
            }


        }
    }

    public void init() {
        email = (EditText) findViewById(R.id.emailidEt);
        pass = (EditText) findViewById(R.id.passwordEt);
        LoginIn = (Button) findViewById(R.id.buttonSignIn_login);
        frgtPswrd = (TextView) findViewById(R.id.frgtPswrdTv);
        emailIlay = (TextInputLayout) findViewById(R.id.inputlayout_email);
        passIlay = (TextInputLayout) findViewById(R.id.inputlayout_password);
        sign_up_tv = (TextView) findViewById(R.id.signup_tv);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/roboto_light.ttf");
        email.setTypeface(tf);
        pass.setTypeface(tf);
        emailIlay.setTypeface(tf);
        passIlay.setTypeface(tf);

       /* iv_arrow = (ImageView) findViewById(R.id.signin_arror_iv);*/

    }


    public boolean validate() {
        boolean valid = true;

        emaiid = email.getText().toString();
        password = pass.getText().toString();
        if (emaiid.isEmpty() || emaiid.length() < 9) {
            emailIlay.requestFocus();
            emailIlay.setErrorEnabled(true);
            emailIlay.setError(getString(R.string.enter_vld_nu));
            valid = false;
        } else if (password.isEmpty()) {
            emailIlay.clearFocus();
            emailIlay.setErrorEnabled(false);
            passIlay.requestFocus();
            passIlay.setErrorEnabled(true);
            passIlay.setError(getString(R.string.enter_pwd));
            valid = false;
        } else if (password.length() <=7) {
            emailIlay.clearFocus();
            emailIlay.setErrorEnabled(false);
            passIlay.requestFocus();
            passIlay.setErrorEnabled(true);
            passIlay.setError(getString(R.string.pws_long_error));
            valid = false;
        }

        return valid;
    }

    public boolean trueValid() {
        boolean valid = false;

        emaiid = email.getText().toString();
        password = pass.getText().toString();


        if (!emaiid.isEmpty() || emaiid.length() < 10) {
            emailIlay.clearFocus();
            emailIlay.setErrorEnabled(false);
            valid = true;
        }

        if (!password.isEmpty()) {
            passIlay.clearFocus();
            passIlay.setErrorEnabled(false);
            valid = true;

        }
        if (password.length() >=7) {
            passIlay.clearFocus();
            passIlay.setErrorEnabled(false);
            valid = true;

        }
        return valid;
    }


    public void callSignUpService() {
        showProgreass();
        String url = String.format(ServerConfigStage.DRIVER_LOGIN());
        RetrofitTask task = new RetrofitTask<Login>(SignInActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_LOGIN, url, SignInActivity.this);
        model_number = CommonUtility.getDeviceName();
        task.executeDriverLogin(mobileNo, password, "android", token,SharedPreferenceManager.getLanguage(this),model_number);

    }

    public void sendOtpCall() {
        showProgreass();
        String url = String.format(ServerConfigStage.OTP_SEND());
        RetrofitTask task = new RetrofitTask<Otp>(SignInActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_OTP_SEND, url, SignInActivity.this);
        task.executeOtpSend(mobileNo, "driver",SharedPreferenceManager.getLanguage(this));

    }
    private void setLanguageType() {
        showProgreass();
        String url = String.format(ServerConfigStage.GET_OPTION_LANGUAGE());
        RetrofitTask task = new RetrofitTask<Offline>(SignInActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LANG_TYPE, url, this);
        String session = SharedPreferenceManager.getToken(this);
        task.executeDriverLangType(session, SharedPreferenceManager.getLanguage(this), "driver");
    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(SignInActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }


    @Override
    public void onRetrofitTaskComplete(retrofit2.Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_LOGIN && response != null) {
            Login login = (Login) response.body();
            stopProgress();
            if (response.isSuccessful()) {
                if (login.getStatus() > 0) {
                    // setLanguageType();
                    SharedPreferenceManager.setPassword(this, password);
                    int verified = 0;
                    Driverdata driverdata = login.getDriverdata();
                    if (driverdata.getVerified() != null) {
                        verified = Integer.parseInt(driverdata.getVerified());
                    }
                    if (verified == 0) {
                        sendOtpCall();
                        Intent i = new Intent(SignInActivity.this, VerifyAccountActivity.class);
                        i.putExtra("mobile", emaiid);
                         startActivity(i);

                    } else {
                        driverdata = login.getDriverdata();
                        if (driverdata != null) {
                            String referCode = driverdata.getReferralcode();
                            String eMail = driverdata.getEmailid();
                            String fName = driverdata.getFname();
                            String mName = driverdata.getMname();
                            String lName = driverdata.getLname();
                            String token = login.getToken();
                            String mobileNumber = driverdata.getMobile();
                            String city = driverdata.getCity();
                            String driverId = driverdata.getId();
                            int vechileCount = login.getVehicleCount();
                            String canDeliver = driverdata.getCanDriverDeliver();
                            String webLock = driverdata.getWebblock();
                            String user_photo = driverdata.getPhoto();
                            SharedPreferenceManager.setDriverphoto(this, user_photo);


                            double balance = 0.0;
                            if (driverdata.getBalance() != null) {
                                balance = Double.valueOf(driverdata.getBalance());
                            }

                            int totalRide = 0;
                            if (driverdata.getTotalRides() != null) {
                                totalRide = Integer.parseInt(driverdata.getTotalRides());
                            }
                            float driverRating = 0;
                            if (driverdata.getRating() != null) {
                                driverRating = Float.parseFloat(driverdata.getRating());
                            }
                            int ridebal = 0;
                            if (driverdata.getTotalRides() != null) {
                                ridebal = Integer.parseInt(driverdata.getTotalRides());
                            }

                            SharedPreferenceManager.setMiddleName(context, mName);
                            SharedPreferenceManager.setLastName(context, lName);
                            SharedPreferenceManager.setReferalCode(context, referCode);
                            SharedPreferenceManager.setEmailId(context, eMail);
                            SharedPreferenceManager.setFirstName(context, fName);
                            SharedPreferenceManager.setFirstName(context, fName);
                            SharedPreferenceManager.setMobileNumber(context, mobileNumber);
                            SharedPreferenceManager.setCity(context, city);
                            SharedPreferenceManager.setPassword(context, password);
                            SharedPreferenceManager.setBalance(context, totalRide);
                            SharedPreferenceManager.setToken(context, token);
                            SharedPreferenceManager.setvechileCount(context, vechileCount);
                            SharedPreferenceManager.setDriverRating(context, driverRating);
                            SharedPreferenceManager.settotalRide(context, ridebal);
                            SharedPreferenceManager.setCanDeliver(context, canDeliver);
                            SharedPreferenceManager.setWrblockOnline(context, webLock);
                            SharedPreferenceManager.setLoggin(context, true);
                            SharedPreferenceManager.setCanDriverDeliver(context, driverdata.getCanDriverDeliver());


                            SharedPreferenceManager.setDriverId(context, driverId);

                            navigateToMainActivity(eMail, password);

                        }
                    }
                   } else {
                    Toast.makeText(SignInActivity.this, login.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
            if (_callerFunction == CommonUtility.CallerFunction.GET_OTP_SEND && response != null) {
                Otp otp = (Otp) response.body();
                stopProgress();
                if (response.isSuccessful()) {
                    if (otp.getCode() == 100) {
                        Toast.makeText(context, otp.getMessageIs(), Toast.LENGTH_LONG).show();

                    }

                } else {
                    Toast.makeText(context, otp.getMessageIs(), Toast.LENGTH_LONG).show();

                }
            } else if (_callerFunction == CommonUtility.CallerFunction.LANG_TYPE && response != null) {
                stopProgress();
                if (response.body() != null) {
                    Offline cittModel = (Offline) response.body();
                    cittModel.getMessage();
                } else {
                }
            } else if (_callerFunction == CommonUtility.CallerFunction.QBID_SEND_TOSERVER && response != null) {
                stopProgress();
                if (response.body() != null) {
                    QBModel qbModel = (QBModel) response.body();
                    if (qbModel.getStatus() > 0) {
                        afterLogin();
                    }

                } else {
                }

            }


    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        Toast.makeText(SignInActivity.this, getString(R.string.internet_error_message), Toast.LENGTH_LONG).show();

    }
    public void sendQbSenderId(QBUser qbUser) {
        showProgreass();
        String url = String.format(ServerConfigStage.QB_SENDER_ID());
        RetrofitTask task = new RetrofitTask<QBModel>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.QBID_SEND_TOSERVER, url, this);
        String token = SharedPreferenceManager.getToken(this);
        task.executeQbIdServer(token, String.valueOf(qbUser.getId()), qbUser.getEmail(), "driver");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finishAffinity();
        // overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    @Override
    public void onQbSignInSuccess(QBUser qbUser) {
        if (qbUser.getId() != null) {
            sendQbSenderId(qbUser);
        } else {
            afterLogin();
        }
    }

    private void afterLogin() {
        if (SharedPreferenceManager.getvechileCount(this) > 0) {
            // Toast.makeText(SignInActivity.this, R.string.register_sucess, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(SignInActivity.this, CarCategoryActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();

        } else {
            //Vechle Registation
            Intent intent = new Intent(SignInActivity.this, AddVechileInformationActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();

        }

    }
}
