package com.techconlabs.najezdriver.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.model.CardModel;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Response;

public class AddNewCardActivity extends AppCompatActivity implements RetrofitTaskListener<CardModel> {


    private ProgressDialog progressDialog;
    private String cardno1, cardno2, cardno3, cardno4;
    private String cardNumber;
    private String expireMonth;
    private String expireYear;
    private String cardType;
    private String cardUserName;
    private String cardCvv;
    private String type;
    private EditText et_cardno_1, et_cardno_2, et_card_no3, et_card_no_4, et_month, et_year, et_cvv, et_holdername;
    private Button save_btn;
    private int expMonth;
    int year;
    CardModel cardModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_card);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_up_appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText("Card Details");
        DateFormat df = new SimpleDateFormat("yy");
        String yer = df.format(Calendar.getInstance().getTime());
        year = Integer.parseInt(yer);
        titleText.setText(R.string.card_title);

        init();


        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stringValues();
                if (validate()) {
                    cardModel = new CardModel();
                    cardModel.setCardNumber(cardNumber);
                    cardModel.setExpireMonth(expireMonth);
                    cardModel.setExpireYear(expireYear);
                    cardModel.setCardType("card type");
                    cardModel.setCardUserName(cardUserName);
                    cardModel.setCardCvv(cardCvv);
                    cardModel.setType("driver");
                    callAddCardApi();
                } else {
                }
            }
        });
        et_cardno_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_cardno_1.getText().toString().length()==4){
                    et_cardno_2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_cardno_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_cardno_2.getText().toString().length()==4){
                    et_card_no3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_card_no3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_card_no3.getText().toString().length()==4){
                    et_card_no_4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_card_no_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_card_no_4.getText().toString().length()==4){
                    et_month.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et_month.getText().toString().length()==2){
                    et_year.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void init() {
        et_holdername = (EditText) findViewById(R.id.holder_et);
        et_cardno_1 = (EditText) findViewById(R.id.saved_card_et_1);
        et_cardno_2 = (EditText) findViewById(R.id.saved_card_et_2);
        et_card_no3 = (EditText) findViewById(R.id.saved_card_et_3);
        et_card_no_4 = (EditText) findViewById(R.id.saved_card_et_4);
        et_month = (EditText) findViewById(R.id.expirydate_et_1);
        et_year = (EditText) findViewById(R.id.expirydate_et_2);
        et_cvv = (EditText) findViewById(R.id.cvv_et);
        save_btn = (Button) findViewById(R.id.savemycardbtn);
    }

    private void stringValues() {
        cardUserName = et_holdername.getText().toString().trim();
        expireMonth = et_month.getText().toString().trim();
        expireYear = et_year.getText().toString().trim();
        cardCvv = et_cvv.getText().toString().trim();
        cardno1 = et_cardno_1.getText().toString().trim();
        cardno2 = et_cardno_2.getText().toString().trim();
        cardno3 = et_card_no3.getText().toString().trim();
        cardno4 = et_card_no_4.getText().toString().trim();
        cardNumber = cardno1 + cardno2 + cardno3 + cardno4;
        if (expireMonth.equals("") || expireMonth.isEmpty() || expireMonth == null) {

        } else {
            expMonth = Integer.parseInt(expireMonth);
        }

    }

    public boolean validate() {

        stringValues();
        int year = Calendar.getInstance().get(Calendar.YEAR) % 100;
        boolean valid = true;
        if (cardUserName.isEmpty()) {
            et_holdername.setError(getString(R.string.holder_name));
            valid = false;
        } else if (TextUtils.isEmpty(cardNumber)) {
            Toast.makeText(AddNewCardActivity.this, R.string.invalid_no, Toast.LENGTH_LONG).show();
            valid = false;
        } else if (TextUtils.isEmpty(expireMonth)) {
            et_month.setError(getString(R.string.invalid_month));
            valid = false;
        } else if (expMonth > 12) {
            et_month.setError(getString(R.string.invaldmonth));
            valid = false;
        } else if (TextUtils.isEmpty(expireYear)) {
            et_year.setError(getString(R.string.invalid_year));
            valid = false;
        }else if (Integer.parseInt(expireYear) < year) {
            et_year.setError(getString(R.string.invalid_year));
            valid = false;
        }
        return valid;
    }

    private void callAddCardApi() {
        showProgreass();
        String url = String.format(ServerConfigStage.SAVE_NEW_CARD());
        RetrofitTask task = new RetrofitTask<CardModel>(AddNewCardActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ADD_NEW_CARD, url, AddNewCardActivity.this, cardModel);
        task.execute();
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(AddNewCardActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    @Override
    public void onRetrofitTaskComplete(Response<CardModel> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response != null && response.body() != null && response.body().getStatus() == 1) {
            showSavedCardAlert();
        } else {
            Toast.makeText(AddNewCardActivity.this, R.string.unavle_erre, Toast.LENGTH_LONG).show();
        }
    }

    private void showSavedCardAlert() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setMessage(R.string.cared_saved_msg)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
