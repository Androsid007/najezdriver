package com.techconlabs.najezdriver.activity;

import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.chatNew.ChatActivity;
import com.techconlabs.najezdriver.chatNew.ChatHelper;
import com.techconlabs.najezdriver.chatNew.qb.QbDialogHolder;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.GetData;

import java.util.ArrayList;

/**
 * Created by hp1 on 12/20/2017.
 */

public class DeliveryImage extends AppCompatActivity {
    private ImageView imageView;
    private Button chatBtn;
    private EditText amountAddEt;
    private GetData rideInformation;
    private static final int REQUEST_DIALOG_ID_FOR_UPDATE = 165;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_chat);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView = (ImageView) findViewById(R.id.ivProfile);
        chatBtn = (Button) findViewById(R.id.pay_btn);
        amountAddEt = (EditText) findViewById(R.id.amountAddEt);
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("rideInformation")) {
            rideInformation = (GetData) getIntent().getExtras().getSerializable("rideInformation");

        }
        Picasso.with(DeliveryImage.this).load(rideInformation.getRideData().getDeliveryImg()).resize(1080, 720).into(imageView);
        amountAddEt.setText(rideInformation.getRideData().getDeliveryComment());
        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QBUser qbUser = new QBUser();
                qbUser.setId(Integer.parseInt(SharedPreferenceManager.getChatId(DeliveryImage.this)));
                qbUser.setPassword(SharedPreferenceManager.getPassword(DeliveryImage.this));
                qbUser.setEmail(SharedPreferenceManager.getEmailId(DeliveryImage.this));
                qbUser.setLogin(SharedPreferenceManager.getEmailId(DeliveryImage.this));
                loginToChat(qbUser);
            }
        });
    }



    @Override
    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    private void loginToChat(final QBUser user) {
        //ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                ArrayList<QBUser> selectedUsers = new ArrayList<>();

                QBUser user = new QBUser(SharedPreferenceManager.getEmailId(DeliveryImage.this), SharedPreferenceManager.getPassword(DeliveryImage.this));
                user.setEmail(SharedPreferenceManager.getEmailId(DeliveryImage.this));
                user.setId(Integer.parseInt(SharedPreferenceManager.getChatId(DeliveryImage.this)));
                user.setPassword(SharedPreferenceManager.getPassword(DeliveryImage.this));
                selectedUsers.add(user);
                QBUser qbUser = new QBUser();
                qbUser.setId(Integer.parseInt(rideInformation.getRideData().getUserdata().getChatid()));
                selectedUsers.add(qbUser);

                if (isPrivateDialogExist(selectedUsers)) {
                    ProgressDialogFragment.hide(getSupportFragmentManager());

                    selectedUsers.remove(0);
                    QBChatDialog existingPrivateDialog = QbDialogHolder.getInstance().getPrivateDialogWithUser(selectedUsers.get(0));
                   // isProcessingResultInProgress = false;
                    ChatActivity.startForResult(DeliveryImage.this, REQUEST_DIALOG_ID_FOR_UPDATE, existingPrivateDialog);
                } else {
                    // ProgressDialogFragment.show(getActivity().getSupportFragmentManager(), R.string.create_chat);
                    createDialog(selectedUsers);
                }


            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(DeliveryImage.this.getSupportFragmentManager());
                //Log.w(TAG, "Chat login onError(): " + e);
               /* showSnackbarError( vv.findViewById(R.id.layout_root), R.string.error_recreate_session, e,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loginToChat(user);
                            }
                        });*/
            }
        });
    }
    private boolean isPrivateDialogExist(ArrayList<QBUser> allSelectedUsers) {
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        selectedUsers.addAll(allSelectedUsers);
        selectedUsers.remove(0);
        return selectedUsers.size() == 1 && QbDialogHolder.getInstance().hasPrivateDialogWithUser(selectedUsers.get(0));
    }
    private void createDialog(final ArrayList<QBUser> selectedUsers) {
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {

                        ChatActivity.startForResult(DeliveryImage.this, REQUEST_DIALOG_ID_FOR_UPDATE, dialog);
                        ProgressDialogFragment.hide(DeliveryImage.this.getSupportFragmentManager());
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        ProgressDialogFragment.hide(DeliveryImage.this.getSupportFragmentManager());

                    }
                }
        );
    }


}

