package com.techconlabs.najezdriver.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.users.model.QBUser;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.CustomSignupAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.chatNew.*;
import com.techconlabs.najezdriver.common.CommonUtil;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.CityModel;
import com.techconlabs.najezdriver.model.Data;
import com.techconlabs.najezdriver.model.DriverRegister;
import com.techconlabs.najezdriver.model.Family;
import com.techconlabs.najezdriver.model.Luxury;
import com.techconlabs.najezdriver.model.Medium;
import com.techconlabs.najezdriver.model.QBModel;
import com.techconlabs.najezdriver.model.VehicleList;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageActivity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.techconlabs.najezdriver.chatNew.CommonHelper.showSnackbarError;
import static com.techconlabs.najezdriver.common.Constants.DRIVING_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.IDENTITY_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.PROFILE_PICTURE_CODE;

public class SignUpActivity extends AppCompatActivity implements RetrofitTaskListener, QbSignUpListener {

    private EditText firstNameEt, lastNameEt, emailIdEt, mobileNumberEt, passwordEt, invitationCodeEt, date_picker_et, NationalId, etIbanno, middleNameEt, inviteCodeEt;
    private TextInputLayout frstNameIlay, middleNameIlay, emailIlay, numberIlay, passIlay, inviteCodeIlay, input_layout_National, tilIbanno;
    private Button btnSignUp, sent_otp_btn;
    private Spinner countrySpinner, city_spinner;
    private List<Family> familyArrayList;
    private ImageView profileIv;
    private List<Medium> mediumArrayList;
    private List<Luxury> luxuryArrayList;
    private VehicleList vehicleList;
    private String username, photo, latlng, referralcode, firstName, lastName, emailId, number, password, middlename, mNumber;
    private ProgressDialog progressDialog;
    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;
    private TextView datePiker_tv;
    private List<Data> cityList;
    private Data data;
    private DatePickerDialog datePickerDialog;
    private static final int SELECT_PHOTO = 100;
    private static final int CAMERA = 20;
    private Spinner spBank;
    private List<String> bankList = new ArrayList<>();
    private String bank, identity, driving, interntionaBank, nationid, dob;
    private TextView residencyChooser, licenceChooser, tvDLicense, tvIdentity;
    private ImageView ivDLicense, ivIdentity;
    private int REQUEST_CODE = 121;
    String gender = "Male";
    private ScrollView srcollHolder;
    String cityListstring = "", can_deliver = "0";
    RadioButton partnerRb, deliverymanRb;
    boolean drliveryboolean = true;
    LinearLayout licenceLayout, residenyLayout;
    RadioButton maleRb, femaleRb;
    private String model_number="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.sign_up_appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(getString(R.string.app_name));

        init();
        SharedPreferenceManager.setDriverDeliveryStat(SignUpActivity.this, drliveryboolean);
        onClick();
        callCityList();
        callBankListApi();
        calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -20);
        year = calendar.get(Calendar.YEAR);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);


        showDate(year, month + 1, day);


    }

    private void callBankListApi() {
        String url = String.format(ServerConfigStage.BANK_LIST(), SharedPreferenceManager.getLanguage(SignUpActivity.this));
        RetrofitTask task = new RetrofitTask<CityModel>(this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_BANK_LIST, url, SignUpActivity.this);
        task.execute();

    }

    private void setBankSpinner(List<Data> data) {
        if (bankList != null) {
            bankList.clear();
        }
        for (int i = 0; i < data.size(); i++) {
            bankList.add(data.get(i).getBankName());
        }
        bankList.add(0, "*Bank");
        CustomSignupAdapter adapterCarType = new CustomSignupAdapter(SignUpActivity.this, R.layout.string_spinner_layout, R.id.txt, (ArrayList<String>) bankList);
        spBank.setAdapter(adapterCarType);
        spBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((LinearLayout) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                // ((TextView) parent.getChildAt(position)).setTextSize(16);
                if (position == 0) {
                    bank = "";
                } else {
                    bank = bankList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                bank = "";
            }
        });
    }

    public void init() {
        firstNameEt = (EditText) findViewById(R.id.firstNameEt);
        lastNameEt = (EditText) findViewById(R.id.lastNameEt);
        emailIdEt = (EditText) findViewById(R.id.emaiIdEt);
        mobileNumberEt = (EditText) findViewById(R.id.mobileNumberEt);
        passwordEt = (EditText) findViewById(R.id.passwordEt);
        invitationCodeEt = (EditText) findViewById(R.id.inviteCodeEt);
        btnSignUp = (Button) findViewById(R.id.buttonSignUp);
        etIbanno = (EditText) findViewById(R.id.etIbanno);
        tilIbanno = (TextInputLayout) findViewById(R.id.tilIbanno);
        inviteCodeEt = (EditText) findViewById(R.id.inviteCodeEt);

        //  countrySpinner = (Spinner) findViewById(R.id.countryCodeSpinner);
        frstNameIlay = (TextInputLayout) findViewById(R.id.input_layout_firstName);
        middleNameIlay = (TextInputLayout) findViewById(R.id.input_layout_middleName);
        emailIlay = (TextInputLayout) findViewById(R.id.input_layout_emailid);
        numberIlay = (TextInputLayout) findViewById(R.id.input_layout_number);
        passIlay = (TextInputLayout) findViewById(R.id.input_layout_password);
        inviteCodeIlay = (TextInputLayout) findViewById(R.id.input_layout_inviteCode);
        profileIv = (ImageView) findViewById(R.id.profileIv);
        residencyChooser = (TextView) findViewById(R.id.residencyChooser);
        licenceChooser = (TextView) findViewById(R.id.licenceChooser);
        srcollHolder = (ScrollView) findViewById(R.id.srcollHolder);


        date_picker_et = (EditText) findViewById(R.id.dateDrive);
        date_picker_et.setFocusable(false);
        date_picker_et.setText(getResources().getString(R.string.date));
        date_picker_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(999);

            }
        });
        city_spinner = (Spinner) findViewById(R.id.city_spinner);
        NationalId = (EditText) findViewById(R.id.NationalId);
        spBank = (Spinner) findViewById(R.id.spBank);
        ivDLicense = (ImageView) findViewById(R.id.ivDLicense);
        ivIdentity = (ImageView) findViewById(R.id.ivIdentity);
        tvDLicense = (TextView) findViewById(R.id.tvDLicense);
        tvIdentity = (TextView) findViewById(R.id.tvIdentity);
        partnerRb = (RadioButton) findViewById(R.id.partnerRb);
        deliverymanRb = (RadioButton) findViewById(R.id.deliverymanRb);
        licenceLayout = (LinearLayout) findViewById(R.id.licenceLayout);
        residenyLayout = (LinearLayout) findViewById(R.id.residenyLayout);


        //  datePiker_tv = (TextView) findViewById(R.id.datePiker_tv);
        input_layout_National = (TextInputLayout) findViewById(R.id.input_layout_National);
        middleNameEt = (EditText) findViewById(R.id.middleNameEt);
        //  sent_otp_btn = (Button) findViewById(R.id.sent_otp_btn);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/roboto_light.ttf");
        passIlay.setTypeface(tf);

        //RadioButton
        femaleRb = (RadioButton) findViewById(R.id.femaleRb);
        maleRb = (RadioButton) findViewById(R.id.maleRB);
    }

    public void onClick() {
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstName = firstNameEt.getText().toString();
                middlename = middleNameEt.getText().toString();
                emailId = emailIdEt.getText().toString();
                number = mobileNumberEt.getText().toString();
                nationid = NationalId.getText().toString().trim();
                //mNumber = "91" + number;
                mNumber = "966" + number;
         password = passwordEt.getText().toString();
                middlename = middleNameEt.getText().toString();
                lastName = lastNameEt.getText().toString();
                dob = date_picker_et.getText().toString().trim();
                referralcode = inviteCodeEt.getText().toString().trim();


                if (validate()) {
                    callSignUpService();
                }
            }
        });
        profileIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* choosePickerDialog();*/
                REQUEST_CODE = PROFILE_PICTURE_CODE;
                CropImage.activity()
                        .start(SignUpActivity.this, CropImageActivity.class);

            }
        });
        residencyChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REQUEST_CODE = IDENTITY_REQUEST_CODE;
                CropImage.activity()
                        .start(SignUpActivity.this, CropImageActivity.class);
            }
        });
        licenceChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REQUEST_CODE = DRIVING_REQUEST_CODE;
                CropImage.activity()
                        .start(SignUpActivity.this, CropImageActivity.class);
            }
        });

        partnerRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (partnerRb.isChecked()) {
                    licenceLayout.setVisibility(View.VISIBLE);
                    residenyLayout.setVisibility(View.VISIBLE);
                    drliveryboolean = true;
                    can_deliver = "0";
                    SharedPreferenceManager.setDriverDeliveryStat(SignUpActivity.this, drliveryboolean);
                    partnerRb.setChecked(true);
                    deliverymanRb.setChecked(false);
                }
            }
        });
        deliverymanRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deliverymanRb.isChecked()) {
                    licenceLayout.setVisibility(View.GONE);
                    residenyLayout.setVisibility(View.GONE);
                    drliveryboolean = false;
                    SharedPreferenceManager.setDriverDeliveryStat(SignUpActivity.this, drliveryboolean);
                    can_deliver = "1";
                    partnerRb.setChecked(false);
                    deliverymanRb.setChecked(true);
                }
            }
        });
        maleRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (maleRb.isChecked()) {
                    maleRb.setChecked(true);
                    femaleRb.setChecked(false);
                    gender = "male";

                }
            }
        });
        femaleRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (femaleRb.isChecked()) {
                    femaleRb.setChecked(true);
                    maleRb.setChecked(false);
                    gender = "female";

                }
            }
        });
    }

    public boolean validate() {
        boolean valid = true;
        firstName = firstNameEt.getText().toString();
        middlename = middleNameEt.getText().toString();
        emailId = emailIdEt.getText().toString();
        number = mobileNumberEt.getText().toString();
        password = passwordEt.getText().toString();
        interntionaBank = etIbanno.getText().toString();
        nationid = NationalId.getText().toString().trim();
        if (photo == null || photo.matches("")) {
            alertError(getString(R.string.photo_slect));
            valid = false;
        } else if (firstName.matches("")) {
            focusOnView(firstNameEt);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            tilIbanno.setErrorEnabled(false);
            passIlay.setErrorEnabled(false);
            input_layout_National.setErrorEnabled(false);
            frstNameIlay.setErrorEnabled(true);
            frstNameIlay.setError(getString(R.string.f_name_error));
            frstNameIlay.setFocusable(false);
            valid = false;
        } else if (middlename.matches("")) {
            focusOnView(middleNameEt);
            passIlay.setErrorEnabled(false);
            frstNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            input_layout_National.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            tilIbanno.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(true);
            middleNameIlay.setError(getString(R.string.lastName));
            valid = false;
        } else if (emailId.matches("")) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            focusOnView(emailIdEt);
            passIlay.setErrorEnabled(false);
            input_layout_National.setErrorEnabled(false);
            emailIlay.setErrorEnabled(true);
            emailIlay.setError(getString(R.string.email_error));
            valid = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches()) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            focusOnView(emailIdEt);
            input_layout_National.setErrorEnabled(false);
            emailIlay.setErrorEnabled(true);
            passIlay.setErrorEnabled(false);
            emailIlay.setError(getString(R.string.email_format_error));
            valid = false;
        } else if (number.matches("") || number.length() < 9) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            focusOnView(mobileNumberEt);
            input_layout_National.setErrorEnabled(false);
            numberIlay.setErrorEnabled(true);
            passIlay.setErrorEnabled(false);
            numberIlay.setError(getString(R.string.enter_vld_nu));
            valid = false;
        } else if (password.matches("")) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            input_layout_National.setErrorEnabled(false);
            focusOnView(passwordEt);
            passIlay.setErrorEnabled(true);
            passIlay.setError(getString(R.string.paswd_error));
            valid = false;
        } else if (password.length()<=7){
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            input_layout_National.setErrorEnabled(false);
            focusOnView(passwordEt);
            passIlay.setErrorEnabled(true);
            passIlay.setError(getString(R.string.pwd_length_error));
            valid = false;
        }else if (cityListstring == null || cityListstring.equalsIgnoreCase("") || cityListstring.equals("")) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            passIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            alertError(getString(R.string.pls_select_city));
            return false;
        }
        else if (nationid!=null && nationid.length() <= 9) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            focusOnView(NationalId);
            passIlay.setErrorEnabled(false);
            input_layout_National.setErrorEnabled(true);
            input_layout_National.requestFocus();
            input_layout_National.setError(getString(R.string.natioan_id));
            return false;

        } else if (bank == null || bank.equalsIgnoreCase("") || bank.equals("")) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            passIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            input_layout_National.setErrorEnabled(false);
            passIlay.setErrorEnabled(false);
            CommonUtil.showToast(SignUpActivity.this, getString(R.string.bank_select));
            return false;
        } else if (interntionaBank.isEmpty() || interntionaBank.equalsIgnoreCase("")) {
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            passIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            tilIbanno.setErrorEnabled(true);
            tilIbanno.setError(getString(R.string.bank_no));
            return false;
        }
        else if (interntionaBank!=null && interntionaBank.length() <=23){
            frstNameIlay.setErrorEnabled(false);
            middleNameIlay.setErrorEnabled(false);
            emailIlay.setErrorEnabled(false);
            numberIlay.setErrorEnabled(false);
            tilIbanno.setErrorEnabled(true);
            passIlay.setErrorEnabled(false);
            tilIbanno.setError(getString(R.string.bank_no));
            return false;
        }else if (drliveryboolean) {
            if (identity == null || identity.equalsIgnoreCase("")) {
                frstNameIlay.setErrorEnabled(false);
                middleNameIlay.setErrorEnabled(false);
                emailIlay.setErrorEnabled(false);
                numberIlay.setErrorEnabled(false);
                tilIbanno.setErrorEnabled(false);
                passIlay.setErrorEnabled(false);
                alertError(getString(R.string.vechile_2));
                return false;
            } else if (driving == null || driving.equalsIgnoreCase("")) {
                frstNameIlay.setErrorEnabled(false);
                passIlay.setErrorEnabled(false);
                middleNameIlay.setErrorEnabled(false);
                emailIlay.setErrorEnabled(false);
                numberIlay.setErrorEnabled(false);
                tilIbanno.setErrorEnabled(false);
                alertError(getString(R.string.driving_licence_proof));

                return false;
            }
        } else if (!drliveryboolean) {
            identity = "";
            driving = "";
        }



       /* if (!firstName.matches("")){
            frstNameIlay.clearFocus();
            frstNameIlay.setErrorEnabled(false);
            return true;
        }
        if (!middlename.matches("")){
            middleNameIlay.clearFocus();
            middleNameIlay.setErrorEnabled(false);
            return true;
        }
        if (!emailId.matches("")){
            emailIlay.clearFocus();
            emailIlay.setErrorEnabled(false);
            return true;

        }else if (android.util.Patterns.EMAIL_ADDRESS.matcher(emailId).matches()){
            emailIlay.clearFocus();
            emailIlay.setErrorEnabled(false);
            return true;

        }
        if (!number.matches("")){
            numberIlay.clearFocus();
            numberIlay.setErrorEnabled(false);
            return true;

        }
        if (!password.matches("")){
            passIlay.clearFocus();
            passIlay.setErrorEnabled(false);
            return true;
        }
        if (!interntionaBank.equals("")) {
            tilIbanno.clearFocus();
            tilIbanno.setErrorEnabled(false);
            return true;
        }*/

        return valid;
    }

    public boolean tureValidate() {


        return false;
    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (SignUpActivity.this != null &&
                progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    public void callSignUpService() {
        frstNameIlay.setErrorEnabled(false);
        middleNameIlay.setErrorEnabled(false);
        emailIlay.setErrorEnabled(false);
        numberIlay.setErrorEnabled(false);
        tilIbanno.setErrorEnabled(false);
        showProgreass();
        String url = String.format(ServerConfigStage.DRIVER_REGISTER());
        RetrofitTask task = new RetrofitTask<DriverRegister>(SignUpActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_REGISTER, url, SignUpActivity.this);
        model_number = CommonUtility.getDeviceName();
        if (data == null) {
            task.executeDriverRegister(emailId, firstName, lastName, middlename, password, photo, gender, mNumber, referralcode, can_deliver, "", "", "", interntionaBank, bank, dob, identity, driving, nationid, SharedPreferenceManager.getLanguage(this),model_number);
        } else {
            SharedPreferenceManager.setCity(this, data.getCityNameEn());
            task.executeDriverRegister(emailId, firstName, lastName, middlename, password, photo, gender, mNumber, referralcode, can_deliver, data.getCityNameEn(), "", "", interntionaBank, bank, dob, identity, driving, nationid, SharedPreferenceManager.getLanguage(this),model_number);
        }

    }

    public void callCityList() {
        showProgreass();
        String url = String.format(ServerConfigStage.CITY_LIST(), SharedPreferenceManager.getLanguage(SignUpActivity.this));
        RetrofitTask task = new RetrofitTask<CityModel>(SignUpActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CITY_LIST, url, SignUpActivity.this);
        task.execute();

    }

    public void sendQbSenderId(QBUser qbUser) {
        showProgreass();
        String url = String.format(ServerConfigStage.QB_SENDER_ID());
        RetrofitTask task = new RetrofitTask<QBModel>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.QBID_SEND_TOSERVER, url, this);
        String token = SharedPreferenceManager.getToken(this);
        task.executeQbIdServer(token, String.valueOf(qbUser.getId()), qbUser.getEmail(), "driver");

    }


    @Override
    public void onRetrofitTaskComplete(retrofit2.Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_REGISTER) {
            if (response.isSuccessful()) {
                DriverRegister driverRegister = (DriverRegister) response.body();
                SharedPreferenceManager.setToken(SignUpActivity.this,driverRegister.getToken());
                if (driverRegister.getStatus() > 0) {
                    SharedPreferenceManager.setPassword(SignUpActivity.this,password);
                    String can_deliver = driverRegister.getDriverdata().getCanDriverDeliver();
                    SharedPreferenceManager.setCanDriverDeliver(SignUpActivity.this, can_deliver);
                    QBUser qbUser = new QBUser();
                    qbUser.setEmail(emailId);
                    qbUser.setLogin(emailId);
                    qbUser.setPassword(password);
                    //qbUser.setFullName(preferences.getString(ConstantClass.FIRST_NAME,"")+" "+preferences.getString(ConstantClass.LAST_NAME,""));
                    QbSignup qbSignup = new QbSignup(this);
                    qbSignup.signUp(qbUser, findViewById(R.id.srcollHolder), SignUpActivity.this);
                   /* QbSignup qbSignup = new QbSignup(this);
                    qbSignup.signUp(emailId,);*/

                } else {
                    Toast.makeText(SignUpActivity.this, driverRegister.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } else if (_callerFunction == CommonUtility.CallerFunction.CITY_LIST) {
            if (response.isSuccessful()) {
                CityModel cityModel = (CityModel) response.body();
                if (cityModel.getStatus() > 0) {
                    List<String> stringList = new ArrayList<>();
                    cityList = new ArrayList<>();
                    for (int i = 0; i < cityModel.getData().size(); i++) {
                        if (cityModel.getData().get(i).getCityNameEn() != null) {
                            stringList.add(cityModel.getData().get(i).getCityNameEn());
                            cityList.add(cityModel.getData().get(i));
                        } else {
                            cityModel.getData().remove(i);
                        }
                    }
                    Data dataSelect = new Data();
                    dataSelect.setCityNameEn(getString(R.string.city_delect));
                    stringList.add(0, dataSelect.getCityNameEn());
                    cityList.add(0, dataSelect);
                    CustomSignupAdapter adapterCarType = new CustomSignupAdapter(this, R.layout.string_spinner_layout, R.id.txt, (ArrayList<String>) stringList);
                    city_spinner.setAdapter(adapterCarType);
                    city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ((LinearLayout) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                            if (position == 0) {
                                data = null;
                                cityListstring = "";
                            } else {
                                data = cityList.get(position);
                                cityListstring = String.valueOf(cityList.get(position).getCityNameEn());

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            data = null;
                        }
                    });
                } else {
                    Toast.makeText(SignUpActivity.this, cityModel.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            //city_spinner.
        } else if (_callerFunction == CommonUtility.CallerFunction.GET_BANK_LIST && response != null) {
            stopProgress();
            if (response.body() != null) {
                CityModel cittModel = (CityModel) response.body();
                setBankSpinner(cittModel.getData());
            } else {
                //  CommonUtil.showAlertDialog(SignUpActivity.this,getString(R.string.error_message),isActivityFinished);
            }
        } else if (_callerFunction == CommonUtility.CallerFunction.QBID_SEND_TOSERVER && response != null) {
            stopProgress();
            if (response.body() != null) {
                QBModel qbModel = (QBModel) response.body();
                if (qbModel.getStatus() > 0) {
                    Intent i = new Intent(SignUpActivity.this, VerifyAccountActivity.class);
                    i.putExtra("mobile", number);
                    startActivity(i);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }

            } else {
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }

   /* @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        //Toast.makeText(getApplicationContext(), "Please Select the Date", Toast.LENGTH_SHORT).show();

    }*/

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTime().getTime());
            return datePickerDialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {

        date_picker_et.setText(new StringBuilder().append(day).append("/").append(month).append("/").append(year));


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(imageReturnedIntent);
            if (resultCode == RESULT_OK) {
                switch (REQUEST_CODE) {
                    case PROFILE_PICTURE_CODE:
                        Uri selectedImage = result.getUri();
                        Bitmap yourSelectedImage = null;

                        try {
                            //decodeUri() Method Defined Below

                            yourSelectedImage = decodeUri(selectedImage);
                            photo = getByteArrayFromUri(result.getUri());

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        profileIv.setImageBitmap(yourSelectedImage);


                        break;
                    case IDENTITY_REQUEST_CODE:
                        identity = getByteArrayFromUri(result.getUri());
                        tvIdentity.setText(R.string.residency_proof);
                        ivIdentity.setVisibility(View.VISIBLE);
                        break;
                    case DRIVING_REQUEST_CODE:
                        driving = getByteArrayFromUri(result.getUri());
                        tvDLicense.setText(R.string.licence_proof);
                        ivDLicense.setVisibility(View.VISIBLE);
                        break;
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                CommonUtil.showToast(SignUpActivity.this, getString(R.string.image_loading_error));
            }
        }

    }

    //decodeUri() Method for decoding image for Out of Memory Exception
    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
    }

    private String getByteArrayFromUri(Uri selectedImage) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(SignUpActivity.this.getContentResolver(), selectedImage);
            Bitmap bitmap1 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 12, bitmap.getHeight() / 12, true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
            //Log.e("byte array", photo);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

    }

    private String getByteArrayFromBitmap(Bitmap selectedImage) {
        try {

            Bitmap bitmap1 = Bitmap.createScaledBitmap(selectedImage, selectedImage.getWidth() / 12, selectedImage.getHeight() / 12, true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
            //Log.e("byte array", photo);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void alertError(String errorMessage) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.layout_custom_dialog, null);
        TextView alertTitle;
        Button okbtn;
        alertTitle = (TextView) alertLayout.findViewById(R.id.titleTv);
        okbtn = (Button) alertLayout.findViewById(R.id.okBtn);


        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
        alertDialogBuilder.setView(alertLayout);
        alertTitle.setText(errorMessage);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                    // alertDialog=null;

                }
            }
        });

        alertDialog.show();
    }

    private void focusOnView(final EditText editText) {
        srcollHolder.post(new Runnable() {
            @Override
            public void run() {
                srcollHolder.smoothScrollTo(0, editText.getBottom());
            }
        });
    }

    @Override
    public void onQbSignUpSuccess(QBUser qbUser) {
        if (qbUser.getId() != null) {
            sendQbSenderId(qbUser);
        } else {
            Intent i = new Intent(SignUpActivity.this, VerifyAccountActivity.class);
            i.putExtra("mobile", number);
            startActivity(i);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        }
    }

    private void loginToChat(final QBUser user) {
        //ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                //user.getId();
                ProgressDialogFragment.hide(getSupportFragmentManager());
                DialogsActivity.start(SignUpActivity.this);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getSupportFragmentManager());
                //Log.w(TAG, "Chat login onError(): " + e);
                showSnackbarError( findViewById(R.id.layout_root), R.string.error_recreate_session, e,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loginToChat(user);
                            }
                        });
            }
        });
    }

}
