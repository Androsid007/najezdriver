package com.techconlabs.najezdriver.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.CancelledRideAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.customViews.Navigationfont;
import com.techconlabs.najezdriver.fragment.AboutUsFragement;
import com.techconlabs.najezdriver.fragment.AccountFragment;
import com.techconlabs.najezdriver.fragment.DriverAcceptTripFragment;
import com.techconlabs.najezdriver.fragment.EarningsFragment;
import com.techconlabs.najezdriver.fragment.FragmentFaq;
import com.techconlabs.najezdriver.fragment.HelpSupportFragment;
import com.techconlabs.najezdriver.fragment.HomeFragment;
import com.techconlabs.najezdriver.fragment.MainFragment;
import com.techconlabs.najezdriver.fragment.ReferalFragment;
import com.techconlabs.najezdriver.fragment.SettingsFragment;
import com.techconlabs.najezdriver.fragment.TripFragment;
import com.techconlabs.najezdriver.fragment.VehicleInfoFragment;
import com.techconlabs.najezdriver.location.MyLocationRideUpdate;
import com.techconlabs.najezdriver.location.MyLocationService;
import com.techconlabs.najezdriver.model.Accept;
import com.techconlabs.najezdriver.model.Cancel;
import com.techconlabs.najezdriver.model.CancelOption;
import com.techconlabs.najezdriver.model.CompleteRide;
import com.techconlabs.najezdriver.model.ConfirmArrival;
import com.techconlabs.najezdriver.model.GetData;
import com.techconlabs.najezdriver.model.Offline;
import com.techconlabs.najezdriver.model.RideData;
import com.techconlabs.najezdriver.model.RideNotification;
import com.techconlabs.najezdriver.model.Trip;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

import static com.techconlabs.najezdriver.R.anim;
import static com.techconlabs.najezdriver.R.drawable;
import static com.techconlabs.najezdriver.R.id;
import static com.techconlabs.najezdriver.R.layout;
import static com.techconlabs.najezdriver.R.string;

public class HomeActivity extends AppCompatActivity implements RetrofitTaskListener, CompoundButton.OnCheckedChangeListener, CancelledRideAdapter.RidePositionCallback {

    private Toolbar toolbar;
    boolean onLine = true;
    boolean onRide = true;
    GetData rideInformation;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private SwitchCompat switchCompat;
    private TextView tvTitle;
    private ProgressDialog progressDialog;
    private ImageView off_delivery, on_delivery;
    private FragmentManager fragmentManager;
    private String modelName, modelNum, modelYear, serialNum, colorCode = "", notification, vechileType, vechileTypeID;
    private String carImage, familyId, vechileId;
    private MediaPlayer mp;
    private LinearLayout parent_pop_layout;
    RecyclerView pop_recyclerView;
    PopupWindow pw, cancel_Pop_up;
    private CancelOption caneloption;
    CancelledRideAdapter adapter;
    private int radioPos = -1;
    private Button next_btn;
    RideNotification rideNotification;
    String option_id;
    //boolean rideAccept = true;
    RideData rideIdData;
    private Button submitButton;
    private TextView tv_totalfare, tv_clientname, tv_totaldistance, tv_timetaken;
    RatingBar ratingBar;
    Button ratingSubmit;
    String ratingbyuser;
    Dialog fareDialog, ratingDialog;
    String medium_status, family_status;
    String medium_status1;
    String vechile_status;
    Geocoder geocoder;
    List<Address> pickupLoc;
    List<Address> dropLoc;
    boolean delivertStat = true;
    BottomSheetDialog start_popup, arrivalpopup, completetrip;

    SwitchCompat deliverySwitch;
    TextView tv_delivery;
    LinearLayout delivery_layout;
    LocalBroadcastManager localBroadcastManager;
    TextView headerText;
    Boolean offlineVar = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        callString();
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("rideInformationLost")) {
            rideInformation = (GetData) getIntent().getExtras().getSerializable("rideInformationLost");
            confirmarrivalpopup(rideInformation);
        }
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        switchCompat = (SwitchCompat) findViewById(R.id.switchCompat);
        headerText = (TextView) findViewById(R.id.headerText);


        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean("vechileBlock") && SharedPreferenceManager.getLoggin(this)) {
            if (SharedPreferenceManager.getOnLineStatus(this)) {
                offlineVar = true;
                OnRideInformtoServer("0");
     }     else {  finish();
                startActivity(new Intent(this, CarCategoryActivity.class));
            }
        } else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean("deactUser") && SharedPreferenceManager.getLoggin(this)) {
            Toast.makeText(this, "You Are blocked By Admin", Toast.LENGTH_LONG).show();
            logoutRequesttoServer();
     } else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean("blockUser") && SharedPreferenceManager.getLoggin(this)) {
            Toast.makeText(this, "You Are blocked By Najez TEAM", Toast.LENGTH_LONG).show();
            logoutRequesttoServer();
        } else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean("unblockUser")) {
            if (SharedPreferenceManager.getLoggin(this)) {
                Toast.makeText(this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();
                SharedPreferenceManager.setWrblockOnline(this, "1");

            }


        } else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean("cancelRide") && SharedPreferenceManager.getLoggin(this)) {
            if(getSupportActionBar()!=null) {
                (HomeActivity.this).getSupportActionBar().show();
            }
            SharedPreferenceManager.setOnRdieStatus(this, false);
            SharedPreferenceManager.setrideStatus(this,0);
            if (CommonUtility.isMyServiceRunning(MyLocationRideUpdate.class, this)) {
                this.stopService(new Intent(this, MyLocationRideUpdate.class));
            }
            if (CommonUtility.isMyServiceRunning(MyLocationService.class, this)) {
                this.stopService(new Intent(this, MyLocationService.class));
            }

            this.startService(new Intent(this, MyLocationService.class));
            SharedPreferenceManager.setrideStatus(HomeActivity.this, 0);
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
        }
     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }
        modelName = SharedPreferenceManager.getModelName(this);
        modelNum = SharedPreferenceManager.getModelNum(this);
        modelYear = SharedPreferenceManager.getModelYear(this);
        serialNum = SharedPreferenceManager.getSerialNum(this);
        colorCode = SharedPreferenceManager.getColorCode(this);
        carImage = SharedPreferenceManager.getCarImage(this);
        familyId = SharedPreferenceManager.getfamilyId(this);
        vechileId = SharedPreferenceManager.getVechileId(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initNavigationDrawer();
        deliverySwitch = (SwitchCompat) findViewById(R.id.deliverySwitch);
        deliverySwitch.setOnCheckedChangeListener(this);
        switchCompat.setOnCheckedChangeListener(this);
        delivery_layout = (LinearLayout) findViewById(R.id.delivery_layout);
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("v_block_home") != null && !getIntent().getExtras().getString("v_block_home").equals("")) {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
            switchCompat.setVisibility(View.INVISIBLE);
            tvTitle.setVisibility(View.INVISIBLE);
    } else {

            if (SharedPreferenceManager.getOnLineStatus(this) && SharedPreferenceManager.getVechileId(this).equals(SharedPreferenceManager.getOnLineStatusModelNumber(this))) {
                switchCompat.setChecked(true);
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
                SharedPreferenceManager.setdetailFlag(this, false);

            } else {
                if (SharedPreferenceManager.getdetailFlag(this)) {
                    tvTitle.setText(string.offline);
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(id.flContent, new VehicleInfoFragment(), "on").commit();
     } else {
                    switchCompat.setChecked(false);
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
                }
            }
        }
        navigationfontfun();
   }

    private void callString() {
        if (SharedPreferenceManager.getLanguage(HomeActivity.this).equals("en")) {
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        } else if (SharedPreferenceManager.getLanguage(HomeActivity.this).equals("ar")) {
            Locale locale = new Locale("ar");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        localBroadcastManager.registerReceiver(mMessageReceiver, new IntentFilter("NazejRideDriver"));

    }

    @Override
    protected void onPause() {
        super.onPause();
        localBroadcastManager.unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {
                Bundle bundle1 = intent.getExtras();
                if (bundle1 != null && bundle1.getBoolean("unblockUser")) {
                    SharedPreferenceManager.setWrblockOnline(HomeActivity.this, "1");
                    Toast.makeText(HomeActivity.this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();

                }


            }


        }
    };

    private void deliveryRequestServer(String status) {
        showProgreass();
        String url = String.format(ServerConfigStage.DELIVERY_MEN_REQUEST());
        RetrofitTask task = new RetrofitTask<Offline>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DELIVERY_MEN_ONLINE, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriveryMenOnOff(session, status);
    }

    private void navigationfontfun() {
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent != null) {
            Bundle bundle1 = intent.getExtras();
            if (bundle1 != null && bundle1.getSerializable("value") != null) {
                rideNotification = (RideNotification) bundle1.getSerializable("value");
                if (rideNotification.getRideData() != null) {
                    rideIdData = new Gson().fromJson(rideNotification.getRideData(), RideData.class);
                    if (SharedPreferenceManager.getOnLineStatus(this)) {
                        if (rideIdData != null) {
                            if (rideIdData.getRideId() != null && !rideIdData.getRideId().equals("null") && !rideIdData.getRideId().equals("")) {
                                String rideId = rideIdData.getRideId();
                                SharedPreferenceManager.setRideId(HomeActivity.this, rideId);
                                GetRideDataApi();
                            }
                        }
                    }
                }
            } else if (bundle1 != null && bundle1.getBoolean("cancelRide") && SharedPreferenceManager.getLoggin(this)) {
                Toast.makeText(this, "Your ride has canceled by User", Toast.LENGTH_LONG).show();
                (HomeActivity.this).getSupportActionBar().show();
                SharedPreferenceManager.setOnRdieStatus(this, false);
                SharedPreferenceManager.setrideStatus(this,0);

                if (CommonUtility.isMyServiceRunning(MyLocationRideUpdate.class, this)) {
                    this.stopService(new Intent(this, MyLocationRideUpdate.class));
                }
                if (CommonUtility.isMyServiceRunning(MyLocationService.class, this)) {
                    this.stopService(new Intent(this, MyLocationService.class));
                }
                this.startService(new Intent(this, MyLocationService.class));
                SharedPreferenceManager.setrideStatus(HomeActivity.this, 0);
                try {
                    fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
                } catch (Exception e) {

                }

            } else if (bundle1 != null && bundle1.getBoolean("vechileBlock") && SharedPreferenceManager.getLoggin(this)) {
                if (SharedPreferenceManager.getOnLineStatus(this)) {
                    offlineVar = true;
                    OnRideInformtoServer("0");

                } else {
                    finish();
                    startActivity(new Intent(this, CarCategoryActivity.class));
                }
            } else if (bundle1 != null && bundle1.getBoolean("deactUser") && SharedPreferenceManager.getLoggin(this)) {
                Toast.makeText(this, "You Are blocked By Admin", Toast.LENGTH_LONG).show();
                SharedPreferenceManager.setOnLineStatus(HomeActivity.this, false);
                logoutRequesttoServer();

                // logoutUser();

            } else if (bundle1 != null && bundle1.getBoolean("blockUser") && SharedPreferenceManager.getLoggin(this)) {
                Toast.makeText(this, "You Are blocked By Najez TEAM", Toast.LENGTH_LONG).show();
                logoutRequesttoServer();

                // logoutUser();
            } else if (bundle1 != null && bundle1.getBoolean("unblockUser")) {
                if (SharedPreferenceManager.getLoggin(this)) {
                    Toast.makeText(this, "You Are Unblocked By Najez TEAM", Toast.LENGTH_LONG).show();
                    SharedPreferenceManager.setWrblockOnline(this, "1");

                }
            }

        }
    }

    private void logoutUser() {
        SharedPreferenceManager.setReferalCode(HomeActivity.this, "");
        SharedPreferenceManager.setEmailId(HomeActivity.this, "");
        SharedPreferenceManager.setFirstName(HomeActivity.this, "");
        SharedPreferenceManager.setvechileCount(HomeActivity.this, 0);
        SharedPreferenceManager.setDriverId(HomeActivity.this, "");
        SharedPreferenceManager.setMobileNumber(HomeActivity.this, "");
        SharedPreferenceManager.setCity(HomeActivity.this, "");
        SharedPreferenceManager.setPassword(HomeActivity.this, "");
        SharedPreferenceManager.setDriverRating(HomeActivity.this, 0);
        SharedPreferenceManager.setBalance(HomeActivity.this, 0);
        SharedPreferenceManager.settotalRide(HomeActivity.this, 0);
        SharedPreferenceManager.setToken(HomeActivity.this, "");
        SharedPreferenceManager.setBalance(HomeActivity.this, 0);
        SharedPreferenceManager.setLoggin(HomeActivity.this, false);
        SharedPreferenceManager.setCarListSize(HomeActivity.this, 0);
        SharedPreferenceManager.setOnLineStatus(HomeActivity.this, false);
        SharedPreferenceManager.setVechileId(HomeActivity.this, "");
        SharedPreferenceManager.setWalletBalance(HomeActivity.this, "");
        SharedPreferenceManager.setChatId(HomeActivity.this, "");
        Intent intent = new Intent(HomeActivity.this, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    ;

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case id.switchCompat:
                if (isChecked) {
                    if (!SharedPreferenceManager.getCanDriverDeliver(HomeActivity.this).equals("1")) {
                        delivery_layout.setVisibility(View.VISIBLE);
                    }
                    medium_status = SharedPreferenceManager.getVechileMediumstatus(this);
                    family_status = SharedPreferenceManager.getVechileFamilyStatus(this);


                    if (family_status.equals("") && medium_status.equals("")) {
                        vechile_status = "";
                    } else if (!family_status.equals("") && !medium_status.equals("")) {
                        vechile_status = family_status + "," + medium_status;
                    } else if (medium_status.equals("") && !family_status.equals("")) {
                        vechile_status = family_status;
                    } else if (family_status.equals("") && !medium_status.equals("")) {
                        vechile_status = medium_status;
                    } else {
                        vechile_status = "";
                    }

                    //    vechile_status = family_status + "," + medium_status;
                    tvTitle.setText(string.online);
                    if (SharedPreferenceManager.getWrblockOnline(this).equals("1")) {
                        if (!SharedPreferenceManager.getVechileId(this).equals(SharedPreferenceManager.getOnLineStatusModelNumber(this))) {
                            SharedPreferenceManager.setOnLineStatus(this, false);
                        }
                        if (!SharedPreferenceManager.getOnLineStatus(this)) {
                            if (CommonUtility.isMyServiceRunning(MyLocationService.class, this)) {
                                this.stopService(new Intent(this, MyLocationService.class));
                            }
                            this.startService(new Intent(this, MyLocationService.class));
                            fragmentManager = getSupportFragmentManager();
                            fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
                            OnRideInformtoServer("1");
                            SharedPreferenceManager.setdetailFlag(this, false);

                        }
                    } else {
                        tvTitle.setText(string.offline);
                        delivery_layout.setVisibility(View.INVISIBLE);
                        Toast.makeText(HomeActivity.this, R.string.new_block_string, Toast.LENGTH_LONG).show();
                        switchCompat.setChecked(false);


                    }
                } else if (!SharedPreferenceManager.getOnRideStatus(this)) {
                    if (SharedPreferenceManager.getdetailFlag(this)) {
                        tvTitle.setText(R.string.offline);
                        delivery_layout.setVisibility(View.INVISIBLE);
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(id.flContent, new VehicleInfoFragment(), "on").commit();

                    } else {
                        tvTitle.setText(string.offline);
                        delivery_layout.setVisibility(View.INVISIBLE);
                        fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
                        OnRideInformtoServer("0");
                        if (CommonUtility.isMyServiceRunning(MyLocationService.class, this)) {
                            this.stopService(new Intent(this, MyLocationService.class));
                        }
                    }
                } else {
                    switchCompat.setChecked(true);
                    Toast.makeText(HomeActivity.this, R.string.off_line_toast, Toast.LENGTH_LONG).show();

                }

                break;
            case R.id.deliverySwitch:
                if (isChecked) {
                    deliveryRequestServer("1");
                } else {
                    deliveryRequestServer("0");
                }
        }
    }

    private void OnRideInformtoServer(String status) {
        showProgreass();
        String url = String.format(ServerConfigStage.ON_THE_WAY());
        RetrofitTask task = new RetrofitTask<Offline>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.OFF_ONLINE, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverOnOff(session, vechileId, status, familyId, vechile_status);
    }

    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(layout.custom_tab, null);
        tabOne.setText(string.ho_me);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, drawable.home, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(layout.custom_tab, null);
        tabTwo.setText(string.earnin_gs);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, drawable.earning, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(layout.custom_tab, null);
        tabThree.setText(string.my_trip);
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, drawable.mytrip, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(layout.custom_tab, null);
        tabFour.setText(string.account);
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, drawable.account, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabFour);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), getResources().getString(string.ho_me));
        adapter.addFragment(new EarningsFragment(), getResources().getString(string.earnin_gs));
        adapter.addFragment(new TripFragment(), getResources().getString(string.my_trip));
        adapter.addFragment(new AccountFragment(), getResources().getString(string.account));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {

        if (_callerFunction == CommonUtility.CallerFunction.ACCEPT_RIDE && response != null) {
            Accept accept = (Accept) response.body();
            stopProgress();
            if (response.isSuccessful()) {
                if (accept.getStatus() > 0) {
                    confirmarrivalpopup(rideInformation);
                } else {
                }
            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.OFF_ONLINE && response != null) {
            stopProgress();
            Offline off = (Offline) response.body();
            if (response.isSuccessful()) {
                if (off.getStatus() > 0) {
                    if (SharedPreferenceManager.getOnLineStatus(this)) {
                        SharedPreferenceManager.setOnLineStatus(HomeActivity.this, false);
                        SharedPreferenceManager.setOnLineStatusModelNumber(HomeActivity.this, "");
                        SharedPreferenceManager.setVechileIdNumber(HomeActivity.this, "");

                    } else {
                        SharedPreferenceManager.setVechileIdNumber(HomeActivity.this, vechileId);
                        SharedPreferenceManager.setOnLineStatus(HomeActivity.this, true);
                        SharedPreferenceManager.setOnLineStatusModelNumber(HomeActivity.this, SharedPreferenceManager.getVechileId(this));

                    }
                }
                if (offlineVar) {
                    SharedPreferenceManager.setOnLineStatus(HomeActivity.this, false);
                    SharedPreferenceManager.setVechileIdNumber(HomeActivity.this, "");
                    finish();
                    startActivity(new Intent(this, CarCategoryActivity.class));
                }
            } else {

            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.CANCEL_OPTION && response != null) {
            caneloption = (CancelOption) response.body();
            stopProgress();
            if (response.isSuccessful()) {
                if (caneloption.getStatus() > 0) {
                    parent_pop_layout.setVisibility(View.VISIBLE);
                    //     adapter = new CancelledRideAdapter(caneloption, HomeActivity.this);
                    pop_recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(HomeActivity.this, caneloption.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        }


        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_START_TRIP && response != null) {
            stopProgress();
            Trip off = (Trip) response.body();

            if (response.isSuccessful()) {
                if (off.getStatus() > 0) {
                       completeTrippopup();
                }

            } else {

            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.CANCEL_RIDE && response != null) {
            stopProgress();
            Cancel cancel = (Cancel) response.body();

            if (response.isSuccessful()) {
                if (cancel.getStatus() > 0) {
                    Toast.makeText(context, cancel.getMessage(), Toast.LENGTH_LONG).show();
                }

            } else {
              }
        }
        if (_callerFunction == CommonUtility.CallerFunction.CONFIRM_ARRIVAL && response != null) {
            stopProgress();
            ConfirmArrival arrival = (ConfirmArrival) response.body();

            if (response.isSuccessful()) {
                if (arrival.getStatus() > 0) {
                     this.startService(new Intent(this, MyLocationRideUpdate.class));
                    startTrippop();
                }

            } else {

            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_END_TRIP && response != null) {
            stopProgress();
            CompleteRide tripComplete = (CompleteRide) response.body();
            if (response.isSuccessful()) {
                if (tripComplete.getStatus() > 0) {
                     if (CommonUtility.isMyServiceRunning(MyLocationRideUpdate.class, this)) {
                        this.stopService(new Intent(this, MyLocationRideUpdate.class));
                    }
                }

            } else {


            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.GET_RIDE_DATA && response != null) {
            stopProgress();

            if (response.isSuccessful() && response.body() != null) {
                GetData rideInformation = (GetData) response.body();
                if (rideInformation.getStatus() > 0) {
                    rideInformation.getRideData().getUserdata().getFname();
                    String ChatId = rideInformation.getRideData().getUserdata().getChatid();
                     FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", rideInformation);
                    DriverAcceptTripFragment driverAcceptTripFragment = new DriverAcceptTripFragment();
                    driverAcceptTripFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.flContent, driverAcceptTripFragment).addToBackStack(null);
                    fragmentTransaction.commit();
                }
            } else {
            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.DECLINE_RIDE && response != null) {
            stopProgress();
            Accept declineRide = (Accept) response.body();
            if (response.isSuccessful()) {
                if (declineRide.getStatus() > 0) {
                }

            } else {

            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.DELIVERY_MEN_ONLINE && response != null) {
            stopProgress();

            if (response.isSuccessful() && response.body() != null) {
                Offline rideInformation = (Offline) response.body();
                if (rideInformation.getStatus() > 0) {
                    if (!delivertStat) {
                        delivertStat = true;
                    } else {
                        delivertStat = false;
                    }
                    // Toast.makeText(context, rideInformation.getMessage(), Toast.LENGTH_LONG).show();

                } else {

                }

            } else {


            }
        }
        if (_callerFunction == CommonUtility.CallerFunction.LOGOUT_USER && response != null) {
            stopProgress();
            Offline logout = (Offline) response.body();
            if (response.isSuccessful()) {
                if (logout.getStatus() > 0) {
                    logoutUser();
                }

            } else {

            }

        }
    }

    private void GetRideDataApi() {

        showProgreass();
        String url = String.format(ServerConfigStage.GET_RIDE());
        RetrofitTask task = new RetrofitTask<GetData>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_RIDE_DATA, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverRideData(session, SharedPreferenceManager.getRideId(HomeActivity.this), "user");

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void initNavigationDrawer() {

        navigationView = (NavigationView) findViewById(id.navigation_view);
        navigationView.setItemIconTintList(null);

        View headerview = navigationView.getHeaderView(0);
       /* if (SharedPreferenceManager.getCanDeliver(this).equals("0") || SharedPreferenceManager.getCanDeliver(this) == null || SharedPreferenceManager.getCanDeliver(this).equals("")) {
            off_delivery.setVisibility(View.VISIBLE);
        }*/


        ImageView profile = (ImageView) headerview.findViewById(R.id.profile);
        if (SharedPreferenceManager.getDriverphoto(HomeActivity.this) != null && !SharedPreferenceManager.getDriverphoto(HomeActivity.this).equals("")) {
            Picasso.with(HomeActivity.this).load(SharedPreferenceManager.getDriverphoto(HomeActivity.this)).resize(1080, 720).into(profile);

        } else {

        }


        TextView email = (TextView) headerview.findViewById(id.tv_email);
        TextView tvCity = (TextView) headerview.findViewById(id.tv_city);
        String firstName = SharedPreferenceManager.getFirstName(this);
        String middleName = SharedPreferenceManager.getMiddleName(this);
        String lastName = SharedPreferenceManager.getLastName(this);
        if (middleName != null) {
            email.setText(firstName + " " + middleName + " " + lastName);
        } else {
            email.setText(firstName + " " + lastName);
        }
        tvCity.setText(SharedPreferenceManager.getCity(this));
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                switch (menuItem.getItemId()) {
                    case id.nav_home:
                        headerText.setVisibility(View.GONE);
                        if (getIntent() != null && getIntent().getExtras() != null && !getIntent().getExtras().getString("v_block_home").equals("")) {
                            fragmentManager = getSupportFragmentManager();
                            fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
                            switchCompat.setVisibility(View.INVISIBLE);
                            tvTitle.setVisibility(View.INVISIBLE);
                            drawerLayout.closeDrawers();
                        } else {
                            switchCompat.setVisibility(View.VISIBLE);
                            if (SharedPreferenceManager.getdetailFlag(HomeActivity.this)) {
                                drawerLayout.closeDrawers();
                                tvTitle.setText(getString(string.offline));
                                fragmentManager = getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(id.flContent, new VehicleInfoFragment(), "on").commit();

                            } else {
                                drawerLayout.closeDrawers();
                                tvTitle.setText(getString(string.online));
                                delivery_layout.setVisibility(View.VISIBLE);
                                fragmentManager = getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(id.flContent, new MainFragment(), "on").commit();
                            }
                        }
                        break;
                    case id.nav_get_free_ride:
                        headerText.setVisibility(View.VISIBLE);
                        tvTitle.setVisibility(View.GONE);
                        headerText.setText(string.referal_);
                        switchCompat.setVisibility(View.INVISIBLE);
                        delivery_layout.setVisibility(View.INVISIBLE);
                        drawerLayout.closeDrawers();
                        transaction.replace(id.flContent, new ReferalFragment()).commit();
                        break;
                    case id.nav_my_cards:
                        drawerLayout.closeDrawers();
                        //   popupscreen1();
                        headerText.setVisibility(View.INVISIBLE);

                        delivery_layout.setVisibility(View.INVISIBLE);
                        //transaction.replace(id.flContent, new MyCardFragment()).commit();
                        Intent intent = new Intent(HomeActivity.this, SavedCardActivity.class);
                        startActivity(intent);
                        break;
                    case id.nav_my_car:
                        delivery_layout.setVisibility(View.INVISIBLE);
                        headerText.setVisibility(View.INVISIBLE);

                        drawerLayout.closeDrawers();
                        startActivity(new Intent(HomeActivity.this, CarCategoryActivity.class));
                        break;
                    case id.nav_about:
                        delivery_layout.setVisibility(View.INVISIBLE);
                        headerText.setVisibility(View.VISIBLE);
                        headerText.setText(string.about_us);
                        tvTitle.setVisibility(View.GONE);
                        switchCompat.setVisibility(View.INVISIBLE);
                        drawerLayout.closeDrawers();
                        transaction.replace(id.flContent, new AboutUsFragement()).commit();
                        break;
                    case id.nav_faq:
                        delivery_layout.setVisibility(View.INVISIBLE);
                        headerText.setVisibility(View.VISIBLE);
                        headerText.setText(R.string.faq_);
                        tvTitle.setVisibility(View.GONE);
                        switchCompat.setVisibility(View.INVISIBLE);
                        drawerLayout.closeDrawers();
                        transaction.replace(id.flContent, new FragmentFaq()).commit();
                        break;
                    case id.nav_settings:
                        delivery_layout.setVisibility(View.INVISIBLE);
                        headerText.setVisibility(View.VISIBLE);
                        headerText.setText(string.setting);
                        tvTitle.setVisibility(View.GONE);
                        switchCompat.setVisibility(View.INVISIBLE);
                        drawerLayout.closeDrawers();
                        transaction.replace(id.flContent, new SettingsFragment()).commit();
                        break;
                    case id.nav_hepl_support:
                        delivery_layout.setVisibility(View.INVISIBLE);
                        headerText.setVisibility(View.VISIBLE);
                        headerText.setText(string.help_and_support);
                        tvTitle.setVisibility(View.GONE);
                        switchCompat.setVisibility(View.INVISIBLE);
                        drawerLayout.closeDrawers();
                        transaction.replace(id.flContent, new HelpSupportFragment()).commit();


                        //transaction.replace(R.id.flContent,new HelpSupportFragment()).commit();
                        break;

                    case id.nav_logout:
                        delivery_layout.setVisibility(View.INVISIBLE);
                        drawerLayout.closeDrawers();
                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                        builder.setMessage(string.logout_msg).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        logoutRequesttoServer();
                                    }
                                }

                        ).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alert = builder.create();
                        alert.show();
                        break;


                }

                return true;
            }
        });
        RatingBar ratingBar = (RatingBar) headerview.findViewById(id.ratingBar);
        ratingBar.setRating(Float.parseFloat(String.valueOf(SharedPreferenceManager.getDriverRating(HomeActivity.this))));
        drawerLayout = (DrawerLayout) findViewById(id.drawer_layout);

    }


    private void confirmarrivalpopup(final GetData rideInformation) {
        arrivalpopup = new BottomSheetDialog(HomeActivity.this);
        View popupView = getLayoutInflater().inflate(R.layout.confirm_arrival_popup, null);
        arrivalpopup.setContentView(popupView);
        arrivalpopup.show();
        arrivalpopup.setCanceledOnTouchOutside(false);
        TextView textViewNamePickUp = (TextView) popupView.findViewById(R.id.textViewNamePickUp);
        textViewNamePickUp.setText(rideInformation.getRideData().getUserdata().getFname());
        RelativeLayout confirmbtn = (RelativeLayout) popupView.findViewById(R.id.confiem_arrival);
        confirmbtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                arrivalpopup.dismiss();
                if (rideInformation != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("rideInformation", rideInformation);
                    Intent intent = new Intent(HomeActivity.this, MapsActivity.class);
                    startActivity(intent.putExtras(bundle));
                }


            }
        });
        arrivalpopup.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

                return true;

            }
        });


    }

    private void confirmArrivalApi() {
        showProgreass();
        String url = String.format(ServerConfigStage.CONFIRM_DRIVER_ARRIVAL());
        RetrofitTask task = new RetrofitTask<ConfirmArrival>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CONFIRM_ARRIVAL, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverConfirmArrival(session, SharedPreferenceManager.getRideId(HomeActivity.this));

    }

    private void logoutRequesttoServer() {
        showProgreass();
        String url = String.format(ServerConfigStage.LOGOUT_REQUEST());
        RetrofitTask task = new RetrofitTask<Offline>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LOGOUT_USER, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeLogout(session, "driver");
    }


    private void cancelridepopup() {
        LayoutInflater inflater = (LayoutInflater) HomeActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.cancel_ride_pop_up, null);
        layout.setAnimation(AnimationUtils.loadAnimation(this, anim.slide_up_dialog));
        cancel_Pop_up = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        cancel_Pop_up.showAtLocation(layout, Gravity.CENTER, 0, 0);
        cancel_Pop_up.setOutsideTouchable(true);
        cancel_Pop_up.setFocusable(false);
        cancel_Pop_up.update();


        pop_recyclerView = (RecyclerView) layout.findViewById(R.id.radio_recyclerView);
        parent_pop_layout = (LinearLayout) layout.findViewById(R.id.parent_pop_layout);
        pop_recyclerView.setHasFixedSize(false);
        LinearLayoutManager verticalmanager = new LinearLayoutManager(HomeActivity.this);
        pop_recyclerView.setLayoutManager(verticalmanager);
        next_btn = (Button) layout.findViewById(R.id.pop_up_btn);
        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_Pop_up.dismiss();
                if (radioPos != -1) {
                    option_id = caneloption.getOptions().get(radioPos).getId();
                    afterOptionCancelRide();
                    start_popup.dismiss();
                } else {

                }
            }
        });


    }

    private void afterOptionCancelRide() {
        showProgreass();
        String url = String.format(ServerConfigStage.CANCEL_RRIDE());
        RetrofitTask task = new RetrofitTask<Trip>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CANCEL_RIDE, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverCancelRide(session, SharedPreferenceManager.getRideId(HomeActivity.this), "driver", option_id);
    }


    private void acceptRide(String status) {
        mp.stop();
        showProgreass();
        String url = String.format(ServerConfigStage.ACCEPT_RIDE());
        RetrofitTask task = new RetrofitTask<Accept>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ACCEPT_RIDE, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverRideAccept(session, SharedPreferenceManager.getDriverId(HomeActivity.this), familyId, SharedPreferenceManager.getRideId(HomeActivity.this), vechileId, status);
    }

    private void declineRide(String status) {
        mp.stop();
        showProgreass();
        String url = String.format(ServerConfigStage.ACCEPT_RIDE());
        RetrofitTask task = new RetrofitTask<Accept>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DECLINE_RIDE, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverRideAccept(session, SharedPreferenceManager.getDriverId(HomeActivity.this), familyId, vechileId, SharedPreferenceManager.getRideId(HomeActivity.this), status);
    }

    private void StartTrip() {
        showProgreass();
        String url = String.format(ServerConfigStage.START_TRIP());
        RetrofitTask task = new RetrofitTask<Trip>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_START_TRIP, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverStartTrip(session, SharedPreferenceManager.getRideId(HomeActivity.this));

    }


    private void cancelRide() {
        showProgreass();
        String url = String.format(ServerConfigStage.CANCEL_RIDE());
        RetrofitTask task = new RetrofitTask<CancelOption>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.CANCEL_OPTION, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeCancelRide(session);
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(HomeActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    private void startTrippop() {

        start_popup = new BottomSheetDialog(HomeActivity.this);
        View popupView = getLayoutInflater().inflate(R.layout.start_trip_popup, null);
        start_popup.setContentView(popupView);
        start_popup.show();
        start_popup.setCanceledOnTouchOutside(false);

        RelativeLayout startTrip_layout = (RelativeLayout) start_popup.findViewById(id.startTrip_layout);
        final RelativeLayout cancleTrip_layout = (RelativeLayout) start_popup.findViewById(R.id.cancleTrip_layput);
        start_popup.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        cancleTrip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelRide();
                cancelridepopup();
            }
        });
        startTrip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start_popup.dismiss();
                StartTrip();


            }
        });
        if (start_popup.isShowing() && start_popup != null) {
            new CountDownTimer(10000, 1000) {
                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    cancleTrip_layout.setVisibility(View.VISIBLE);
                }
            }.start();
        } else {

        }

    }


    private void completeTrippopup() {
        completetrip = new BottomSheetDialog(HomeActivity.this);
        View popupView = getLayoutInflater().inflate(R.layout.complete_trip_popup, null);
        completetrip.setContentView(popupView);
        completetrip.show();
        completetrip.setCanceledOnTouchOutside(false);
        completetrip.setCancelable(false);


        RelativeLayout complete_trip_layout = (RelativeLayout) completetrip.findViewById(id.complete_trip_layout);
        complete_trip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completetrip.dismiss();
                rideCompleteApi();
            }
        });
        completetrip.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

    }

    private void rideCompleteApi() {
        showProgreass();
        String url = String.format(ServerConfigStage.RIDE_COMPLETE());
        RetrofitTask task = new RetrofitTask<Trip>(HomeActivity.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_END_TRIP, url, HomeActivity.this);
        String session = SharedPreferenceManager.getToken(HomeActivity.this);
        task.executeDriverRideComplete(session, SharedPreferenceManager.getRideId(HomeActivity.this));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case id.drawer:

                drawerLayout.openDrawer(GravityCompat.END);  // OPEN DRAWER
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(id.drawer).setEnabled(true);


        return super.onPrepareOptionsMenu(menu);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "font/roboto_light.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new Navigationfont("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void RidePos(int pos) {
        radioPos = pos;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (CommonUtility.isMyServiceRunning(MyLocationService.class, this)) {
            this.stopService(new Intent(this, MyLocationService.class));
        }
    }

}
