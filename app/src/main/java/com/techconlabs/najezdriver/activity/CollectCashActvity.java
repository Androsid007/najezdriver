package com.techconlabs.najezdriver.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.CollectCashAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.location.MyLocationService;
import com.techconlabs.najezdriver.model.CollectCashModel;
import com.techconlabs.najezdriver.model.FareDetail;
import com.techconlabs.najezdriver.model.PartialPaymentModel;
import com.techconlabs.najezdriver.model.Review;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.ArrayList;

import retrofit2.Response;

public class CollectCashActvity extends AppCompatActivity implements RetrofitTaskListener{
    TextView tv_totalCash,tv_totalKms,tv_minutes,tv_totalBal,tv_walletBal,tv_received,tv_customerPaid;
    EditText et_amount;
    RecyclerView recyclerView;
    TextView tv_done,tv_no_payment;
    private ProgressDialog progressDialog;
    CollectCashAdapter nCashAdapter;
    ArrayList<CollectCashModel>  cashModelList = new ArrayList<>();
    ArrayList<FareDetail> fareDetails;
    ImageView iv_drop_down;
    int count =1 ;
    String totalBal;
    String bal;
    Button doneBtn;
    TextView sumbitTv;
    RatingBar ratingBar;
    Button ratingSubmit;
    String ratingbyuser;
    Dialog fareDialog;
    private EditText et_review;
    String driverReview;
    String userName;
    RelativeLayout relativeLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_cash_actvity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        TextView titleText = (TextView) toolbar.findViewById(R.id.appbartextV);
        titleText.setText(R.string.cash_collect);
        init();
        onClick();
        callFareDetail();
        if(SharedPreferenceManager.getCollectedCash(CollectCashActvity.this)!=null&&!SharedPreferenceManager.getCollectedCash(CollectCashActvity.this).equals("")) {
            et_amount.setText(SharedPreferenceManager.getCollectedCash(CollectCashActvity.this));
        }
        if(!SharedPreferenceManager.getwalletUpdate(CollectCashActvity.this))
        {

            et_amount.setFocusable(false);
            sumbitTv.setClickable(false);
            sumbitTv.setBackground(getResources().getDrawable(R.drawable.disablebutton_bg));
            sumbitTv.setTextColor(getResources().getColor(R.color.white));
            doneBtn.setClickable(true);
            doneBtn.setBackground(getResources().getDrawable(R.drawable.new_button_layout));
            doneBtn.setTextColor(getResources().getColor(R.color.white));
        }

        if (getIntent() != null && getIntent().getExtras() != null &&!getIntent().getExtras().getString("username").equals("")&& getIntent().getExtras().getString("username")!= null){
            userName = getIntent().getExtras().getString("username");
        }else {
            userName = "";
        }
    }

    private void callFareDetail() {
        showProgreass();
        String url = String.format(ServerConfigStage.DRIVER_BREAK_DOWN());
        RetrofitTask task = new RetrofitTask<CollectCashModel>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_BREAK_DOWN, url, this);
        String session = SharedPreferenceManager.getToken(this);
        task.executeDriverBreakDown(session,SharedPreferenceManager.getRideId(CollectCashActvity.this));

    }

    private void callPartialPayment(EditText editText) {
        showProgreass();
        String url = String.format(ServerConfigStage.PARTIAL_PAYMENT());
        RetrofitTask task = new RetrofitTask<PartialPaymentModel>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.PARTIAL_PAYMENT, url, this);
        String session = SharedPreferenceManager.getToken(this);
        task.executePartialPayment(session,SharedPreferenceManager.getRideId(CollectCashActvity.this),editText.getText().toString());

    }

    private void init() {
        //TextView
        tv_totalCash = (TextView) findViewById(R.id.balaceTv);
        tv_totalKms= (TextView) findViewById(R.id.totalKms);
        tv_minutes = (TextView) findViewById(R.id.timeMinutes);
        tv_totalBal = (TextView) findViewById(R.id.totalTv);
        tv_received = (TextView) findViewById(R.id.receivedTv);
        tv_customerPaid = (TextView) findViewById(R.id.customerPasidTv);
        tv_done = (TextView) findViewById(R.id.doneTv);
        sumbitTv = (TextView) findViewById(R.id.sumbitTv);
        doneBtn = (Button) findViewById(R.id.doneBtn);
        relativeLayout = (RelativeLayout) findViewById(R.id.mainHolder);

         et_amount = (EditText) findViewById(R.id.amoutEt);

        //RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.collectcashRv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(CollectCashActvity.this,LinearLayoutManager.VERTICAL,false));
    }

    private void onClick() {

        et_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!et_amount.getText().toString().equals("") && et_amount.getText().toString() != null) {
                    if(bal==null || bal.equals("")|| bal.equals("null"))
                    {
                      bal="0";
                    }
                    if (Integer.parseInt(et_amount.getText().toString()) < Integer.parseInt(bal)) {
                        sumbitTv.setClickable(false);
                        sumbitTv.setBackground(getResources().getDrawable(R.drawable.disablebutton_bg));
                        sumbitTv.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        sumbitTv.setClickable(true);
                        sumbitTv.setBackground(getResources().getDrawable(R.drawable.new_button_layout));
                        sumbitTv.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        sumbitTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!et_amount.getText().toString().isEmpty()&&SharedPreferenceManager.getwalletUpdate(CollectCashActvity.this)){
                    if(et_amount.getText().toString()!=null && et_amount.getText().toString().equals("")) {
                        SharedPreferenceManager.setCollectedCash(CollectCashActvity.this, String.valueOf(et_amount.getText().toString()));
                    }
                    callPartialPayment(et_amount);
                }
            }
        });
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count ==1){
                    count =0;
                     recyclerView.setVisibility(View.VISIBLE);

                }else {
                    count =1;
                    recyclerView.setVisibility(View.GONE);
                }

            }
        });
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingDialog();
            }
        });
    }


    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_BREAK_DOWN) {
            if (response.isSuccessful()) {
                 if (response.body() != null) {
                    CollectCashModel cashModel = (CollectCashModel) response.body();
                    if (cashModel.getStatus() > 0) {
                        relativeLayout.setVisibility(View.VISIBLE);
                        fareDetails = cashModel.getFareDetails();
                        tv_totalCash.setText("SAR" + " " + cashModel.getRideBreakdown().getTotalFare());
                        tv_totalKms.setText(cashModel.getRideBreakdown().getDistance() + "KMS");
                        tv_minutes.setText(cashModel.getRideBreakdown().getDuration());
                        bal = cashModel.getRideBreakdown().getAmountCollected();
                        if(cashModel.getRideBreakdown().getTotalFare()!=null && !cashModel.getRideBreakdown().getTotalFare().equals("") ) {
                            tv_totalBal.setText("SAR" + " " + cashModel.getRideBreakdown().getTotalFare());
                        }
                        else{
                            tv_totalBal.setText("SAR" + " " + 0);
                        }
                        if(cashModel.getRideBreakdown().getReceivedFromWallet()!=null && !cashModel.getRideBreakdown().getReceivedFromWallet().equals("")) {
                            tv_received.setText("SAR" + " " + cashModel.getRideBreakdown().getReceivedFromWallet());
                        }else{
                            tv_received.setText("SAR" + " " + 0);
                        }
                        if(cashModel.getRideBreakdown().getCashcollected()!=null && !cashModel.getRideBreakdown().getCashcollected().equals("") ) {
                            tv_customerPaid.setText("SAR" + " " + cashModel.getRideBreakdown().getCashcollected());
                        }
                        else{
                            tv_customerPaid.setText("SAR" + " " + 0);
                        }
                        nCashAdapter = new CollectCashAdapter(CollectCashActvity.this, fareDetails);
                        recyclerView.setAdapter(nCashAdapter);
                        nCashAdapter.notifyDataSetChanged();

                    }
                }
            }
        }else if (_callerFunction ==CommonUtility.CallerFunction.PARTIAL_PAYMENT ){
            if (response.isSuccessful()){
                if (response.body() !=null){
                    PartialPaymentModel paymentModel = (PartialPaymentModel) response.body();
                    if (paymentModel.getStatus()>0){


                      et_amount.setFocusable(false);
                        sumbitTv.setClickable(false);
                        sumbitTv.setBackground(getResources().getDrawable(R.drawable.disablebutton_bg));
                        sumbitTv.setTextColor(getResources().getColor(R.color.white));
                        SharedPreferenceManager.setwalletUpdate(CollectCashActvity.this,false);
                        doneBtn.setClickable(true);
                        doneBtn.setBackground(getResources().getDrawable(R.drawable.new_button_layout));
                        doneBtn.setTextColor(getResources().getColor(R.color.white));
                        Intent intent = new Intent(this, CollectCashActvity.class);
                        intent.putExtra("username", userName);
                        this.startActivity(intent);
                    }else {
                        Toast.makeText(CollectCashActvity.this,paymentModel.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }

        }else if (_callerFunction == CommonUtility.CallerFunction.DRIVER_RATING_REVIEW && response != null) {
           stopProgress();
            Review review = (Review) response.body();
            if (response.isSuccessful()) {
                if (review.getStatus() > 0) {
                   // Toast.makeText(context, review.getMessage(), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(CollectCashActvity.this, HomeActivity.class));
                    SharedPreferenceManager.setCollectedCash(CollectCashActvity.this,"");
                    SharedPreferenceManager.setOnRdieStatus(this, false);
                    SharedPreferenceManager.setrideStatus(this,0);
                    SharedPreferenceManager.setwalletUpdate(CollectCashActvity.this,true);
                    if (CommonUtility.isMyServiceRunning(MyLocationService.class, this)) {
                        this.stopService(new Intent(this, MyLocationService.class));
                    }
                    this.startService(new Intent(this, MyLocationService.class));
                  finish();

                }
            } else {
               // Toast.makeText(context, review.getMessage(), Toast.LENGTH_LONG).show();
                //finish();
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
    private void DriverReview(String review) {
        showProgreass();
        String url = String.format(ServerConfigStage.Driver_Review());
        RetrofitTask task = new RetrofitTask<Review>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_RATING_REVIEW, url, CollectCashActvity.this);
        String session = SharedPreferenceManager.getToken(CollectCashActvity.this);
        task.executeDriverReview(session, SharedPreferenceManager.getRideId(CollectCashActvity.this), ratingbyuser, review);
    }

    private void ratingDialog() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CollectCashActvity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.rating_screen, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        dialogBuilder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        TextView textView = (TextView) dialogView.findViewById(R.id.userRatingTextView);
        textView.setText("How Was Your Trip With " + userName + "?");
        ratingBar = (RatingBar) dialogView.findViewById(R.id.ratingBar);
        ratingSubmit = (Button) dialogView.findViewById(R.id.ratingSubmit);
        et_review = (EditText) dialogView.findViewById(R.id.rdivereeviewEt);
        Drawable progress = ratingBar.getProgressDrawable();
        //  DrawableCompat.setTint(progress, getActivity().getResources().getColor(R.color.fab_color));
        /***************Button Click*******************/
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingbyuser = String.valueOf(rating);
            }
        });
        ratingSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onClick(View v) {
                driverReview = "";
                DriverReview(driverReview);
                dialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }
        });
        dialogBuilder.show();

    }

    @Override
    public void onBackPressed() {

    }
}
