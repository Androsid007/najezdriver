package com.techconlabs.najezdriver.chatNew;

import com.quickblox.users.model.QBUser;

/**
 * Created by Dell on 10/24/2017.
 */

public interface QbSignInListener {
    void onQbSignInSuccess(QBUser qbUser);
}
