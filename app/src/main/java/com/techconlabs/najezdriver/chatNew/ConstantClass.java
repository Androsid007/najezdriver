package com.techconlabs.najezdriver.chatNew;

/**
 * Created by X-cellent Technology on 13-06-2017.
 */

public class ConstantClass {

    public static final String LATITUDE_VALUE = "lat" ;
    public static final String LONGITUDE_VALUE = "long" ;
    public static final String ADDRESS = "add" ;
    public static final String NAJEZTAXI_PREF = "najez_taxi" ;
    public static final String DROP_LAT = "drop_lat" ;
    public static final String DROP_LONG = "drop_long" ;
    public static final String DROP_ADD = "drop_add" ;
    public static final String PICK_LAT = "pick_lat" ;
    public static final String PICK_LONG = "pick_long" ;
    public static final String PICK_ADD = "pick_add" ;
    public static final String SEARCH_HISTORY = "search_history" ;
    public static final String CURRENT_RIDE_PAGE = "current_page" ;
    public static final String SCHEDULE_PAGE = "schedule_page" ;
    public static final String HISTORY_PAGE = "history_page" ;
    public static final String ISUSERLOGGEDIN= "is_user_logged_in";
    public static final String USER_ID = "user_id";
    public static final String MOVE_PIN_MARKER = "move_pin_marker" ;
    public static final String HOME_ACTIVITY_BACK = "home_activity" ;
    public static final String PUSH_NOTIFICATION = "pushNotification" ;
    public static final String MESSAGE = "message" ;
    public static final String REGISTRATION_COMPLETE = "registrationComplete" ;
    public static final String USER_CURRENT_LOC = "user_current_loc" ;
    public static final String USER_OUTSTATION_LOC = "user_outside_location" ;
    public static final String CAR_NAME = "car_name" ;
    public static final String ESTIMATED_FARE = "estimate_fare" ;
    public static final String CAR_TYPE = "car_type" ;
    public static final String RIDE_ID = "ride_id" ;
    public static final String CALCULATED_PRICE = "calculated_price" ;
    public static final String FORGOT_PASSWORD_EMAILID = "forgot_email_id" ;
    public static final String TOKEN_KEY = "token_key";
    public static final String DROP_ADD_WITH = "drop_add_value" ;
    public static final String FIRST_NAME = "first_name" ;
    public static final String LAST_NAME = "last_name" ;
    public static final String REFERAL_CODE = "referal_code" ;
    public static final String EMAIL_ID = "EMAIL_ID" ;
    public static final String SOCIAL_LOGIN_USER_ID = "social_login_id" ;
    public static final String Full_name = "full_name";
    public static final String Mobile = "mobile";
    public static final String User_photo = "user_photo";
    public static final String CHOOSED_LANGUAGE = "ChoosedLanguage";
    public static final String VEHICLE_TYPE = "vehicle_type" ;
    public static final String TRIP_TYPE = "trip_type" ;
    public static final String PICK_UP_LOCATION = "pick_up_location";
    public static final String NOTIFICATION_TYPE = "notificationType";
    public static final String DROP_LOCATION = "drop_location";
    public static final String RIDE_DATA ="rideData" ;
    public static final String QB_ID = "qb_id";
    public static final String QB_PASSWORD = "qb_password";


    public class HISTORYDATA{
        public static final String TRAVEL_DATE = "" ;
        public static final String TRAVEL_TIME = "" ;
        public static final String AMOUNT_PAY = "" ;
        public static final String CAR_TYPE = "" ;
        public static final String PAY_TYPE = "" ;
    }

    public class DRIVERDETAILS {
        public static final String DRIVER_ID = "driver_id" ;
        public static final String DRIVER_PROFILE_IMAGE = "driver_profile_image" ;
        public static final String DRIVER_MOBILE = "driver_mobile_no" ;
        public static final String DRIVER_LATITUDE = "driver_lat" ;
        public static final String DRIVER_LONG = "driver_lng" ;
        public static final String DRIVER_FIRSTNAME = "driver_first_name" ;
        public static final String DRIVER_MIDDLENAME = "driver_middlename" ;
        public static final String DRIVER_LASTNAME = "driver_lastname" ;
        public static final String CAR_NUMBER = "car_no" ;
        public static final String CAR_NAME = "car_name" ;
        public static final String VEHICLE_TYPE = "vehicle_type" ;
        public static final String DRIVER_CITY = "driver_city" ;

    }

    /*public class SOCIALLOGIN {
        public static final String EMAIL_ID = "email_id" ;
        public static final String FULL_NAME = "full_name" ;
        public static final String LOGIN_TYPE = "login_type" ;
    }*/
}
