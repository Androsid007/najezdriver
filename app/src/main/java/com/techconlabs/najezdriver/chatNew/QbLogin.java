package com.techconlabs.najezdriver.chatNew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;


/**
 * Created by Dell on 10/24/2017.
 */

public class QbLogin {
    private Context context;
    private ProgressDialog progressDialog;
    private QbSignInListener qbSignInListener;
    private SharedPreferences preferences;
    public QbLogin(Context context){
        this.context = context;
        preferences = context.getSharedPreferences(ConstantClass.NAJEZTAXI_PREF, context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Login to quickblox. Please Wait...");
        progressDialog.setCancelable(false);
    }
    public void signIn(final QBUser qbUser, final View rootView, final QbSignInListener qbSignInListener) {
        if(qbSignInListener!=null){
            this.qbSignInListener = qbSignInListener;
        }
        progressDialog.show();
        QBUsers.signIn(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                progressDialog.dismiss();
                if(qbUser.getId()!=null) {
                   // progressDialog.dismiss();

                    SharedPreferenceManager.setChatId(context,String.valueOf(qbUser.getId()));
                }
                if(qbSignInListener!=null){
                    //progressDialog.dismiss();
           CommonHelper.showToast(context,"qb login success");
                    qbSignInListener.onQbSignInSuccess(qbUser);
                }
            }

            @Override
            public void onError(QBResponseException errors) {
                CommonHelper.showToast(context,"qb login fail");
                progressDialog.dismiss();
                if(qbSignInListener!=null){
                    qbSignInListener.onQbSignInSuccess(qbUser);
                }

            }
        });
    }
}
