package com.techconlabs.najezdriver.chatNew;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.techconlabs.najezdriver.R;

import static com.techconlabs.najezdriver.chatNew.CommonHelper.showSnackbarError;


public class MainActivity extends AppCompatActivity {

    QBUser currentUser ;
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;

    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;

    String[] permissionsRequired = new String[]{
            android.Manifest.permission.CAMERA,

            android.Manifest.permission.READ_EXTERNAL_STORAGE,
           };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        storagePermission();

        QBSettings.getInstance().init(getApplicationContext(), "64432", "JBgzQHgs7-PdyDB", "WxwdttLCRLnZzh-");
        QBSettings.getInstance().setAccountKey("mW1gyE5sNEBmDyxysmaM");
         currentUser = getUserFromSession();
        login(currentUser);

    }
     private void storagePermission() {
        if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, permissionsRequired[1]) != PackageManager.PERMISSION_GRANTED
             ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])

            ||ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])) {
                //Show Information about why you need the permission
                ActivityCompat.requestPermissions(MainActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);

            } else if (permissionStatus.getBoolean(permissionsRequired[0], false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                sentToSettings = true;
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);

            } else {
                //just request the permission
                ActivityCompat.requestPermissions(this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();


        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {

            } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0])
                      || ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])
                    ) {
                ActivityCompat.requestPermissions(MainActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);

            } else {
                Toast.makeText(getBaseContext(), "No Permission", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void login(final QBUser currentUser){
        QBUsers.signIn(currentUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                if(qbUser.getId()!=null) {
                  loginToChat(currentUser);
                }

            }

            @Override
            public void onError(QBResponseException errors) {
                CommonHelper.showToast(MainActivity.this,"qb login fail");
            }
        });
    }
    private QBUser getUserFromSession(){
        //QBUser user = SharedPrefsHelper.getInstance().getQbUser();
        /*user.setId(QBSessionManager.getInstance().getSessionParameters().getUserId());*/
        QBUser user = new QBUser("ram@gmail.com", "1111111111");
        user.setEmail("ram@gmail.com");
        user.setId(36839844);
       // user.setFullName("SID MIS");
        return user;
    }

    /*private void login(final QBUser user) {
        ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_login);
        ChatHelper.getInstance().login(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                SharedPrefsHelper.getInstance().saveQbUser(user);
                DialogsActivity.start(MainActivity.this);
                finish();

                ProgressDialogFragment.hide(getSupportFragmentManager());
            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getSupportFragmentManager());

            }
        });
    }*/

    private void loginToChat(final QBUser user) {
        //ProgressDialogFragment.show(getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                //user.getId();
                ProgressDialogFragment.hide(getSupportFragmentManager());
                DialogsActivity.start(MainActivity.this);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getSupportFragmentManager());
                //Log.w(TAG, "Chat login onError(): " + e);
                showSnackbarError( findViewById(R.id.layout_root), R.string.error_recreate_session, e,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loginToChat(user);
                            }
                        });
            }
        });
    }
}
