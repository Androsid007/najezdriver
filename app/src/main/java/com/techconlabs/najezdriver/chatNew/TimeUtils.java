package com.techconlabs.najezdriver.chatNew;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

    private TimeUtils() {
    }

    public static String getTime(long milliseconds) {
             SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            return dateFormat.format(new Date(milliseconds));



    }

    public static String getDate(long milliseconds) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd", Locale.getDefault());

            return dateFormat.format(new Date(milliseconds));
        }
        catch (Exception e)
        {
            return  null;
        }
    }

    public static long getDateAsHeaderId(long milliseconds) {

            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
            return Long.parseLong(dateFormat.format(new Date(milliseconds)));


    }
}
