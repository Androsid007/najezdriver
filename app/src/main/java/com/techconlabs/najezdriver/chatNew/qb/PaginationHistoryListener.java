package com.techconlabs.najezdriver.chatNew.qb;

public interface PaginationHistoryListener {
    void downloadMore();
}
