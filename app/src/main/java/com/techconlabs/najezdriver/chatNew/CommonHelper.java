package com.techconlabs.najezdriver.chatNew;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.quickblox.core.exception.QBResponseException;
import com.techconlabs.najezdriver.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by X-cellent Technology on 06-06-2017.
 */

public class CommonHelper {

    public static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE= 2;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA= 1;
    public static final int IMAGE_CAPTURE = 1;
    public static final int IMAGE_GALLERY = 2;

    public static boolean validateEmail(CharSequence email_id){
        if (TextUtils.isEmpty(email_id)){
            return false ;
        }else {
            return Patterns.EMAIL_ADDRESS.matcher(email_id).matches() ;
        }
    }

    public static Boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    public static boolean isNotNullEmpty(Object object) {

        if (object == null)
            return false;
        else if (object instanceof Collection<?>)
            return !((Collection<?>) object).isEmpty();
        else if (object instanceof String) {
            return !(isEmpty((String) object));
        }
        return true;
    }

    public static boolean isEmpty(String s) {

        if (s == null)
            return true;
        else return s.equals("");

    }

    /*public static Bitmap getMarkerBitmapFromView(@DrawableRes int resId, Context context) {

        View customMarkerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
       // ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
      //  markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }*/

    public static ProgressDialog showProgressDialog(Context context){
        ProgressDialog progressDialog = new ProgressDialog(context) ;
        progressDialog.setMessage("Please wait.. Getting your Location");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        return progressDialog ;
    }

    public static String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public static boolean isUserLoggedIn(SharedPreferences pref) {
        return pref.getBoolean(ConstantClass.ISUSERLOGGEDIN, false) && pref.getInt(ConstantClass.USER_ID, 0) > 0;
    }

 /*   public static LatLng midPoint(double lat1, double long1, double lat2, double long2)
    {
        return new LatLng((lat1+lat2)/2, (long1+long2)/2);
    }*/

    public static float angleBteweenCoordinate(double lat1, double long1, double lat2,
                                                double long2) {

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        brng = 360 - brng;

        return (float) brng;
    }

    public static void hideKeyboard(Context context, Activity activity){
        InputMethodManager imm = (InputMethodManager) context.
        getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm!=null) {
            if (activity == null)
                return;
            if (activity.getCurrentFocus() == null)
                return;
            if (activity.getCurrentFocus().getWindowToken() == null)
                return;
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
/*
    public static void fadeanimationMap(final Marker marker){
        ValueAnimator ani = ValueAnimator.ofFloat(1, 0);
        ani.setDuration(2000);
        ani.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                marker.setAlpha((float) animation.getAnimatedValue());
            }
        });
        ani.start();
    }

    public static void fadeinanimationMap(final Marker marker){
        ValueAnimator ani = ValueAnimator.ofFloat(0, 1);
        ani.setDuration(2000);
        ani.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                marker.setAlpha((float) animation.getAnimatedValue());
            }
        });
        ani.start();
    }*/

    private void hideKeyboardFunction(Context context, Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static boolean isCameraPermissionGranted(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {


                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }


    }

    public static boolean isStoragePermissionGranted(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {


                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }


    }


    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;

    }

    public static void copy(String srcDir, String dstDir) throws IOException {
        File src = new File(srcDir);
        File dst = new File(dstDir);

        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static String scaleImage(String current_file) {
        int required_size = 1;
        FileOutputStream fos = null;


        File selected_file = new File(current_file);
        Float filesize = selected_file.length() / (float) (1024 * 1000);
        //CommonHelpers.print_log("File Size", filesize+"");

        File path = CommonHelper.Create_MY_IMAGES_DIR();
        OutputStream outFile = null;


        if (filesize > required_size) {
            if (CommonHelper.isNotNullEmpty(path)) {
                File file = new File(path, String.valueOf(System
                        .currentTimeMillis()) + ".jpg");
                int reduce_size = (int) ((required_size / filesize) * 100);
                //CommonHelpers.print_log("Reduce Size", reduce_size + "");
                try {

                    fos = new FileOutputStream(file);
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmapOptions.inSampleSize = 1;
                    Bitmap scaledBitmap = BitmapFactory.decodeFile(current_file);
                    scaledBitmap.compress(Bitmap.CompressFormat.JPEG, reduce_size, fos);
                    fos.flush();
                    fos.close();
                    scaledBitmap.recycle();
                    return file.getAbsolutePath();
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }

        return current_file;

    }

    public static File Create_MY_IMAGES_DIR() {
        File MY_IMG_DIR = null;
        try {
            MY_IMG_DIR = new File(Environment.getExternalStorageDirectory(),
                    File.separator + "com.najez.app" + File.separator
                            + "Images");
            if (!MY_IMG_DIR.exists()) {
                MY_IMG_DIR.mkdirs();
            }
        } catch (Exception e) {
            // TODO: handle exception

        }
        return MY_IMG_DIR;
    }

    public static void showProgress(ProgressDialog progressDialog){
        if(progressDialog!=null && !progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public static void hideProgress(ProgressDialog progressDialog){
        progressDialog.dismiss();
    }

    public static String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    public static void showAlertMessage(Context context,String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setCancelable(true);
        builder.setTitle("1")
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

  /*  public static FinalCarModel loadData(JSONObject jsonObject) {
        FinalCarModel final_model = new FinalCarModel();
        try {
            final_model.setCity_id(jsonObject.getString("city"));
            final_model.setId(jsonObject.getString("id"));
            final_model.setType(jsonObject.getString("type"));
            final_model.setPassenger(jsonObject.getString("passengers"));
            final_model.is_selected=false;
        }catch (JSONException e){
            e.printStackTrace();
        }
        return final_model ;
    }*/

    public static void showToast( Context context, String s) {
        Toast.makeText(context,s,Toast.LENGTH_LONG).show();
    }

    public static void showSnackbarError(View rootLayout, @StringRes int resId, QBResponseException e, View.OnClickListener clickListener) {
     showSnackbar(rootLayout, resId, e, R.string.dlg_retry, clickListener);
    }

    public static Snackbar showSnackbar(View view, @StringRes int errorMessage, Exception e,
                                        @StringRes int actionLabel, View.OnClickListener clickListener) {
        final String NO_CONNECTION_ERROR = "Connection failed. Please check your internet connection.";
        final String NO_RESPONSE_TIMEOUT = "No response received within reply timeout.";
        String error = (e == null) ? "" : e.getMessage();
        boolean noConnection = NO_CONNECTION_ERROR.equals(error);
        boolean timeout = error.startsWith(NO_RESPONSE_TIMEOUT);
        if (noConnection || timeout) {
            return showSnackbar(view, R.string.no_internet_connection, actionLabel, clickListener);
        } else if (errorMessage == 0) {
            return showSnackbar(view, error, actionLabel, clickListener);
        } else if (error.equals("")) {
            return showSnackbar(view, errorMessage, "No Internet", actionLabel, clickListener);
        } else {
            return showSnackbar(view, errorMessage, error, actionLabel, clickListener);
        }
    }

    public static Snackbar showSnackbar(View view, @StringRes int message,
                                        @StringRes int actionLabel,
                                        View.OnClickListener clickListener) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(actionLabel, clickListener);
        snackbar.show();
        return snackbar;
    }
    private static Snackbar showSnackbar(View view, String message,
                                         @StringRes int actionLabel,
                                         View.OnClickListener clickListener) {
        Snackbar snackbar = Snackbar.make(view, message.trim(), Snackbar.LENGTH_INDEFINITE);
        if (clickListener != null) {
            snackbar.setAction(actionLabel, clickListener);
        }
        snackbar.show();
        return snackbar;
    }

    public static Snackbar showSnackbar(View view, @StringRes int errorMessage, String error,
                                        @StringRes int actionLabel, View.OnClickListener clickListener) {
       // String errorMessageString = NazejCustApplication.getInstance().getString(errorMessage);
        String message = String.format("%s: %s", "not Valid", error);
        return showSnackbar(view, message, actionLabel, clickListener);
    }

}
