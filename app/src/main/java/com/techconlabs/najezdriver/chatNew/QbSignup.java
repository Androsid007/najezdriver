package com.techconlabs.najezdriver.chatNew;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;


/**
 * Created by Dell on 10/24/2017.
 */

public class QbSignup {
    private Context context;
    private ProgressDialog progressDialog;
    private QbSignUpListener qbSignUpListener;
    private SharedPreferences preferences;
    public QbSignup(Context context){
        this.context = context;
        preferences = context.getSharedPreferences(ConstantClass.NAJEZTAXI_PREF, context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Registering to quickblox. Please Wait...");
        progressDialog.setCancelable(false);
    }
    public void signUp(final QBUser qbUser, final View rootView, final QbSignUpListener qbSignUpListener) {
        if(qbSignUpListener!=null){
            this.qbSignUpListener = qbSignUpListener;
        }
        progressDialog.show();
        QBUsers.signUpSignInTask(qbUser).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                progressDialog.dismiss();
                //CommonHelper.showToast(context,"qb signup success");
                if(qbUser.getId()!=null) {
                    SharedPreferenceManager.setChatId(context,String.valueOf(qbUser.getId()));
                }
                if(qbSignUpListener!=null){
                    qbSignUpListener.onQbSignUpSuccess(qbUser);
                }
            }
            @Override
            public void onError(QBResponseException error) {
                progressDialog.dismiss();
                CommonHelper.showToast(context,"qb signup failed");
                if(qbSignUpListener!=null){
                    qbSignUpListener.onQbSignUpSuccess(qbUser);
                }
               /* CommonHelper.showSnackbarError(rootView, R.string.errors, error, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        signUp(qbUser,rootView,qbSignUpListener);
                    }
                });*/
            }
        });
    }
}
