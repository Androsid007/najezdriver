package com.techconlabs.najezdriver.api;

public class ServerConfigStage {



     public static String PRODUCTION_URL = "http://13.126.37.98/najezdev/api/driver/";
     public static String NODE_URL = "http://13.126.37.98:3000/api/rest_api/";
     public static String COMMON_URL = "http://13.126.37.98/najezdev/api/common/";


/*
// Testing URL
    public static String PRODUCTION_URL = "http://35.196.171.153/najeztesting/api/driver/";
    public static String NODE_URL = "http://35.196.171.153:3002/api/rest_api/";
    public static String COMMON_URL = "http://35.196.171.153/najeztesting/api/common/";



*/

    public static String DRIVER_REGISTER() {
        return get_Production_URL() + "driverregister";

    }

    public static String DRIVER_LOGIN() {
        return get_Production_URL() + "driverlogin";
    }

    public static String DRIVER_RESET() {

        return get_Production_URL() + "driver_password_reset";
    }

    public static String DRIVER_ACCOUNT_UPDATE() {

        return get_Production_URL() + "driverupdate";

    }

    public static String CAR_LISTING() {
        return get_Production_URL() + "registerd_car";
    }


    public static String RATE_CARD() {
        return get_Production_URL() + "onlineratecard";
    }

    public static String VEHICLE_REGISTER() {

        return get_Production_URL() + "vehicleregister";
    }


    public static String CAR_TYPE_LIST() {
        return get_Production_URL() + "drivervehicle";

    }


    public static String OTP_VERIFY_TO_SERVER() {
        return get_Production_URL() + "driver_register_step2";

    }

    public static String ACCEPT_RIDE() {
        return get_Production_Node() + "accept_ignore_ride";

    }

    public static String ON_THE_WAY() {
        return get_Production_URL() + "drivershift";
    }

    public static String VOUCHER_COUPAN() {
        return get_Production_URL() + "getratecard";

    }


    public static String HELP_SUPPORT_TYPE() {
        return get_Common_URL() + "supportoption";

    }

    public static String CANCEL_RIDE() {
        return get_Production_URL() + "drivercancel_option";

    }

    public static String USER_FORGET_PASSWORD() {
        return get_Common_URL() + "send_otp";
    }

    public static String BANK_LIST() {
        return get_Common_URL() + "bank_list/?lang=%s";
    }

    public static String OTP_SEND() {
        return get_Common_URL() + "send_otp";
    }

    public static String CITY_LIST() {
        return get_Common_URL() + "city_list/?lang=%s";

    }

    public static String ABOUT_NAZEZ() {
        return get_Common_URL() + "getpage";
    }

    public static String HELP_SUPPORT() {
        return get_Common_URL() + "support";
    }


    public static String SERVER_TOKEN() {
        return get_Production_URL() + "driverdevice";
    }

    public static String START_TRIP() {
        return get_Production_Node() + "driverstartride";
    }

    public static String RIDE_HISTORY() {
        return get_Production_URL() + "ride_history";
    }

    public static String CANCEL_RRIDE() {
        return get_Production_Node() + "ride_cancel";
    }

    public static String get_Production_URL() {
        String URL = null;
      /*if (MainApplication.getApplicationConfigureEntity() != null)
         URL = MainApplication.getApplicationConfigureEntity().getApi();
      else*/
        URL = PRODUCTION_URL;
        return URL;
    }

    public static String get_Production_Node() {
        String URL = null;
      /*if (MainApplication.getApplicationConfigureEntity() != null)
         URL = MainApplication.getApplicationConfigureEntity().getApi();
      else*/
        URL = NODE_URL;
        return URL;
    }

    public static String get_Common_URL() {
        String URL = null;
      /*if (MainApplication.getApplicationConfigureEntity() != null)
         URL = MainApplication.getApplicationConfigureEntity().getApi();
      else*/
        URL = COMMON_URL;
        return URL;
    }

    public static String SAVE_NEW_CARD() {
        return get_Common_URL() + "saveusercard";
    }

    public static String GET_SAVED_CARD() {
        return get_Common_URL() + "get_user_card/";
    }

    public static String REMOVE_SAVED_CARD() {
        return get_Common_URL() + "remove_savedcard";
    }

    public static String CONFIRM_DRIVER_ARRIVAL() {
        return get_Production_Node() + "driverarrival";
    }

    public static String LAST_LOCATION() {
        return get_Production_Node() + "last_location";

    }
    public static String RIDE_COMPLETE() {
        return get_Production_Node() + "ride_completion";
    }

    public static String GET_RIDE() {
        return get_Common_URL() + "getridedata";


    }

    public static String USER_CHANGE_PASSWORD() {
        return get_Production_URL() + "changepassword";

    }

    public static String Driver_Review() {
        return get_Production_URL() + "driverreview";
    }

    public static String USER_CHECK_BALANCE() {
        return get_Production_Node() + "transaction/driver_check_balance";

    }

    public static String USER_ADD_BALANCE() {
        return get_Production_Node() + "transaction/driver_add_money";

    }

    public static String USER_SEND_BALANCE() {
        return get_Production_Node() + "transaction/driver_transfer_money";

    }
    public static String DRIVER_TRANSCATION_DETAILS() {
        return get_Production_Node() + "transaction/driver_transfer_detail";

    }

    public static String Transatction_Detail() {
        return get_Production_Node() + "transaction/driver_transaction_details";
    }

    public static String OFFLINE_VOUCHER() {
        return get_Production_Node() + "transaction/offline_voucher_transaction";
    }

    public static String DELIVERY_MEN_REQUEST() {
        return get_Production_URL() + "optdelivery";
    }
    public static String GET_OPTION_LANGUAGE() {
        return get_Common_URL() + "opt_lang";
       }

    public static String GET_LONG_CITY_LIST() {
        return "http://13.126.37.98/najezdev/api/driver/getLongTripCityList";
    }
    public static String ADD_lONG_CITY() {
        return "http://13.126.37.98/najezdev/api/driver/optlongtrip";

    }
    public static String GET_COLOR(){
        return "http://13.126.37.98/najezdev/api/common/colorlist";
    }
    public static String DRIVER_BREAK_DOWN(){
        return get_Production_Node() + "transaction/driver_ride_break_down";
    }
    public static String PARTIAL_PAYMENT(){
        return get_Production_Node() + "transaction/partial_payment";
    }

    public static String QB_SENDER_ID(){
        return get_Common_URL() + "chatid";
    }

    public static String LOGOUT_REQUEST() {
        return get_Production_Node() + "logout";
    }


}