package com.techconlabs.najezdriver.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


/**
 * Created by X-cellent Technology on 05-06-2017.
 */

public class CustomEdit extends android.support.v7.widget.AppCompatEditText {


    public CustomEdit(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEdit(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/roboto_regular.ttf");
        setTypeface(tf);
    }
}

