package com.techconlabs.najezdriver.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;


/**
 * Created by X-cellent Technology on 05-06-2017.
 */

public class CustomButton extends Button {

    Context context;
    AttributeSet attributeSet;
    int defStyle;
    public CustomButton(Context context) {
        super(context);
        this.context= context;
        setcustomfont(this, context, null);
    }

    public CustomButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.context= context;
        this.attributeSet= attributeSet;
        setcustomfont(this, context, attributeSet);
    }

    public CustomButton(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        this.context= context;
        this.attributeSet= attributeSet;
        this.defStyle= defStyle;
        setcustomfont(this, context, attributeSet);
    }


    private void setcustomfont(TextView textview, Context context, AttributeSet attributeSet) {
        TypedArray a = context.obtainStyledAttributes(attributeSet, R.styleable.CustomTextView);
        String font = a.getString(R.styleable.CustomTextView_font);
        setCustomFont(textview, font, context);
        a.recycle();
    }

    private void setCustomFont(TextView textview, String font, Context context) {
        if(font == null) {
            return;
        }
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/" + "Roboto_Regular.ttf");
        if(tf != null) {
            textview.setTypeface(tf);
        }
    }
}
