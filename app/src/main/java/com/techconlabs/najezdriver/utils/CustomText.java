package com.techconlabs.najezdriver.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


/**
 * Created by X-cellent Technology on 05-06-2017.
 */

public class CustomText extends android.support.v7.widget.AppCompatTextView {

    public CustomText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomText(Context context) {
        super(context);
        init();
    }

    public void init() {
       Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/roboto_regular.ttf");
        setTypeface(tf, 1);
    }
}
