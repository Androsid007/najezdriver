package com.techconlabs.najezdriver.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.TripTwoAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Hisory;
import com.techconlabs.najezdriver.model.TodayTrip;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RejectedTripFragment extends Fragment implements RetrofitTaskListener<Hisory> {

    private ArrayList<TodayTrip> todayTripArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TripTwoAdapter adapter;
    private ProgressDialog progressDialog;
    private Hisory historyDetail;
    TextView tripSize;
    TextView nodata;

    public RejectedTripFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rejected_trip, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.triprecyclerview);
        tripSize = (TextView) view.findViewById(R.id.tripSize);
        nodata = (TextView) view.findViewById(R.id.nodata);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // historyApiData();

        //prepareTripData();
        historyApiData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void historyApiData() {
        showProgreass();
        String url = String.format(ServerConfigStage.RIDE_HISTORY());
        RetrofitTask task = new RetrofitTask<Hisory>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.HISTORY_TRIP_API, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());

        task.executeHistoryToken(session, "driver", "4");

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    @Override
    public void onRetrofitTaskComplete(Response<Hisory> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body() != null) {
                historyDetail = response.body();
                if (historyDetail.getHistory().size() != 0) {
                    tripSize.setText(String.valueOf(historyDetail.getHistory().size()));
                    if (historyDetail.getStatus() > 0) {
                            adapter = new TripTwoAdapter(historyDetail, getActivity());
                            recyclerView.setAdapter(adapter);

                        adapter.notifyDataSetChanged();
                    } else {
                        tripSize.setText("0");
                        nodata.setVisibility(View.VISIBLE);
                        nodata.setText("NO Trip Data Avilable");
                    }

                } else {

                    Toast.makeText(context, historyDetail.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

        }

    }


    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }
}
