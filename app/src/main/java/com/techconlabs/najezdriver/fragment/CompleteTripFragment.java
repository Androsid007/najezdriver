package com.techconlabs.najezdriver.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.activity.CollectCashActvity;
import com.techconlabs.najezdriver.activity.HomeActivity;
import com.techconlabs.najezdriver.activity.MapsActivity;
import com.techconlabs.najezdriver.adapter.CancelledRideAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.DirectionJsonParsor;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.location.MyLocationRideUpdate;
import com.techconlabs.najezdriver.model.CancelOption;
import com.techconlabs.najezdriver.model.CompleteRide;
import com.techconlabs.najezdriver.model.GetData;
import com.techconlabs.najezdriver.model.Review;
import com.techconlabs.najezdriver.model.RideValue;
import com.techconlabs.najezdriver.model.Trip;
import com.techconlabs.najezdriver.model.UserRideData;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

/**
 * Created by X-cellent Technology on 02-11-2017.
 */

public class CompleteTripFragment extends Fragment implements RetrofitTaskListener {

    TextView loctaionnameTv;
    ImageView iv_nav_loc;
    private GoogleMap mMap;
    BroadcastReceiver receiver;
    private Marker marker;
    ProgressDialog progressDialog;

    private boolean isFirstTime = true;
    private LatLng latLng;
    private boolean isMarkerRotating = false;


    private TextView tv_totalfare, tv_clientname, tv_totaldistance, tv_timetaken;
    RatingBar ratingBar;
    Button ratingSubmit;
    String ratingbyuser;
    Dialog fareDialog;
    private EditText et_review;
    String driverReview;


    PopupWindow cancel_Pop_up;
    private CancelOption caneloption;
    private LinearLayout parent_pop_layout;
    RecyclerView pop_recyclerView;
    private Button next_btn;
    private int radioPos = -1;
    String option_id;
    private CancelledRideAdapter adapter;
    BottomSheetDialog start_popup, completetrip;
    private Button submitButton;
    GetData rideInformation;
    RideValue rideData;
    UserRideData userRideData;
    String userName = "";
    View vv;
    TextView nameSTextView;
    RelativeLayout startTripLayout;
    RelativeLayout cancleTripLayout;
    Button buttonStartTrip;
    Button cancelButton;
    LatLng pickupLatlng;
    Button btn_call, btn_chat;
    String phone;
    LinearLayout startTrip_layout;
    double drop_lat, drop_log, pick_lat, pick_long;
    String drop_location_name;
    RelativeLayout nav_layout;
    LinearLayout ride_info_layout;
    LatLng mLastKnowLatLng;
    LatLng dropLatlng;
    GoogleMap mGoogleMap;
    ArrayList markerPoints;
    SupportMapFragment mMapFragment;
    String pick_name= "",drop_name ="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vv = inflater.inflate(R.layout.complete_trip_fragment, container, false);
        markerPoints = new ArrayList();
        iv_nav_loc = (ImageView) vv.findViewById(R.id.navgatonIV);
        loctaionnameTv = (TextView) vv.findViewById(R.id.loctaionnameTv);
        nameSTextView = (TextView) vv.findViewById(R.id.textViewName);
        nav_layout = (RelativeLayout) vv.findViewById(R.id.nav_layout);
        ride_info_layout = (LinearLayout) vv.findViewById(R.id.ride_info_layout);
        mMapFragment = SupportMapFragment.newInstance();

        getChildFragmentManager().beginTransaction().replace(R.id.gooleMapContainer, mMapFragment).commit();
        Bundle args = getArguments();
        rideInformation = (GetData) args.getSerializable("data");
        if (rideInformation != null) {
            rideData = rideInformation.getRideData();
            userRideData = rideData.getUserdata();
            userName = userRideData.getFname();
            nameSTextView.setText(userName);
            pick_lat = Double.parseDouble(rideInformation.getRideData().getPickupLat());
            pick_long = Double.parseDouble(rideInformation.getRideData().getPickupLng());
            drop_lat = Double.parseDouble(rideInformation.getRideData().getDropLat());
            drop_log = Double.parseDouble(rideInformation.getRideData().getDropLng());
            loctaionnameTv.setText(rideInformation.getRideData().getDropLocation());
            mLastKnowLatLng = new LatLng(Double.parseDouble(rideInformation.getRideData().getPickupLat()), Double.parseDouble(rideInformation.getRideData().getPickupLng()));
            dropLatlng = new LatLng(Double.parseDouble(rideInformation.getRideData().getDropLat()), Double.parseDouble(rideInformation.getRideData().getDropLng()));


        }

        iv_nav_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rideInformation != null) {
                    pick_lat = Double.parseDouble(rideInformation.getRideData().getPickupLat());
                    pick_long = Double.parseDouble(rideInformation.getRideData().getPickupLng());
                    drop_lat = Double.parseDouble(rideInformation.getRideData().getDropLat());
                    drop_log = Double.parseDouble(rideInformation.getRideData().getDropLng());
                    String uri = "http://maps.google.com/maps?saddr=" + drop_lat + "," + drop_log + "&daddr=" + pick_lat + "," + pick_long;
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), "Location Not Avilable", Toast.LENGTH_LONG).show();
                }

            }
        });
        TextView textViewName = (TextView) vv.findViewById(R.id.textViewName);
        textViewName.setText(userName);
        RelativeLayout complete_trip_layout = (RelativeLayout) vv.findViewById(R.id.complete_trip_layout);
        complete_trip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rideCompleteApi();
            }
        });

        initializMap();
        return vv;
    }

    private void initializMap() {
        final View mapView = mMapFragment.getView();
        mMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                initializeMap(mapView, googleMap);
                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    getActivity(), R.raw.style_json));
                    if (!success) {
                        Log.e("", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("", "Can't find style. Error: ", e);
                }
            }
        });
    }

    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if(addresses!=null &&addresses.size()>0) {

                Address obj = addresses.get(0);
                String add = obj.getAddressLine(0);
                add = add + "\n" + obj.getCountryName();
                add = add + "\n" + obj.getCountryCode();
                add = add + "\n" + obj.getAdminArea();
                add = add + "\n" + obj.getPostalCode();
                add = add + "\n" + obj.getSubAdminArea();
                add = add + "\n" + obj.getLocality();
                add = add + "\n" + obj.getSubThoroughfare();
                pick_name = obj.getSubAdminArea();
                drop_name = obj.getSubAdminArea();


                Log.v("IGA", "Address" + add);
            }
            // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();

            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    private void initializeMap(View mapView, GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        if (mLastKnowLatLng != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            markerPoints.add(mLastKnowLatLng);
            if (dropLatlng != null) {
                markerPoints.add(dropLatlng);
            }

            MarkerOptions options = new MarkerOptions();

            options.position(mLastKnowLatLng);
            if (dropLatlng != null) {
                options.position(dropLatlng);
            }
            if (markerPoints.size() == 1) {
                getAddress(pick_lat,pick_long);
                options.title(pick_name);
                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.pinlocation));
            } else if (markerPoints.size() == 2) {
                getAddress(drop_lat,drop_log);
                options.title(drop_name);
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }

            mGoogleMap.addMarker(options);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(mLastKnowLatLng));
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
            mGoogleMap.setMyLocationEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(mLastKnowLatLng).zoom(14.0f).build();
            mGoogleMap.moveCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            if (markerPoints.size() >= 2) {
                LatLng origin = (LatLng) markerPoints.get(0);
                LatLng dest = (LatLng) markerPoints.get(1);


                String url = getDirectionsUrl(origin, dest);

                CompleteTripFragment.DownloadTask downloadTask = new CompleteTripFragment.DownloadTask();

                downloadTask.execute(url);


            }
        }

    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

// Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

// Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

// Sensor enabled
        String sensor = "sensor=false";

// Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

// Output format
        String output = "json";

// Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

// Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

// Connecting to url
            urlConnection.connect();

// Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //    Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

// For storing data from web service
            String data = "";

            try {
// Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
// doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            CompleteTripFragment.ParserTask parserTask = new CompleteTripFragment.ParserTask();

// Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    class ParserTask extends AsyncTask<String, Integer, java.util.List<java.util.List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected java.util.List<java.util.List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            java.util.List<java.util.List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionJsonParsor parser = new DirectionJsonParsor();

// Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(java.util.List<java.util.List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

// Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

// Fetching i-th route
                java.util.List<HashMap<String, String>> path = result.get(i);

// Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

// Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                lineOptions.color(Color.BLUE);

            }

// Drawing polyline in the Google Map for the i-th route
            if (mGoogleMap != null && lineOptions != null) {
                mGoogleMap.addPolyline(lineOptions);
            }
        }
    }

    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        if (_callerFunction == CommonUtility.CallerFunction.DRIVER_END_TRIP && response != null) {
            ((MapsActivity) getActivity()).stopProgress();
            CompleteRide tripComplete = (CompleteRide) response.body();
            if (response.isSuccessful()) {
                if (tripComplete.getStatus() > 0) {
                    Toast.makeText(context, tripComplete.getTotalAmount(), Toast.LENGTH_LONG).show();
                    if (CommonUtility.isMyServiceRunning(MyLocationRideUpdate.class, getActivity())) {
                        getActivity().stopService(new Intent(getActivity(), MyLocationRideUpdate.class));
                    }

                    ride_info_layout.setVisibility(View.GONE);
                    nav_layout.setVisibility(View.GONE);
                    SharedPreferenceManager.setrideStatus(getActivity(), 4);
                    Intent intent = new Intent(getActivity(), CollectCashActvity.class);
                    intent.putExtra("username", userName);
                    getActivity().startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                    getActivity().finish();
                }

            } else {
                //Toast.makeText(context, tripComplete.getMessage(), Toast.LENGTH_LONG).show();
            }
        } else if (_callerFunction == CommonUtility.CallerFunction.DRIVER_RATING_REVIEW && response != null) {
            ((MapsActivity) getActivity()).stopProgress();
            Review review = (Review) response.body();
            if (response.isSuccessful()) {
                if (review.getStatus() > 0) {
                    Toast.makeText(context, review.getMessage(), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getActivity(), HomeActivity.class));
                    getActivity().finish();
                }
            } else {
                Toast.makeText(context, review.getMessage(), Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        Toast.makeText(getActivity(), String.valueOf(t), Toast.LENGTH_LONG).show();

    }

    private void totalfare(CompleteRide tripComplete) {
        fareDialog = new Dialog(getActivity());
        fareDialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        fareDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        fareDialog.getWindow().setGravity(Gravity.CENTER);
        fareDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fareDialog.setContentView(R.layout.fragment_collect_cash);
        fareDialog.setCanceledOnTouchOutside(false);
        fareDialog.setCancelable(true);
        fareDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });
        /***************INIT*****************/
        tv_totalfare = (TextView) fareDialog.findViewById(R.id.totalcashTv);
        tv_clientname = (TextView) fareDialog.findViewById(R.id.clientnameTv);
        tv_clientname.setText(userName);
        tv_totaldistance = (TextView) fareDialog.findViewById(R.id.distanceTv);
        tv_timetaken = (TextView) fareDialog.findViewById(R.id.timeTv);
        tv_timetaken.setText(tripComplete.getMinutes() + " Minutes");
        submitButton = (Button) fareDialog.findViewById(R.id.done_btn);
        tv_totalfare.setText(tripComplete.getTotalAmount());
        tv_totaldistance.setText(tripComplete.getKilometers());


        /***************Button Click*******************/
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fareDialog.dismiss();
                ratingDialog();

            }
        });

        fareDialog.show();
    }

    private void rideCompleteApi() {
        ((MapsActivity) getActivity()).showProgreass();
        String url = String.format(ServerConfigStage.RIDE_COMPLETE());
        RetrofitTask task = new RetrofitTask<Trip>(CompleteTripFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_END_TRIP, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeDriverRideComplete(session, SharedPreferenceManager.getRideId(getActivity()));
    }

    private void DriverReview(String review) {
        ((MapsActivity) getActivity()).showProgreass();
        String url = String.format(ServerConfigStage.Driver_Review());
        RetrofitTask task = new RetrofitTask<Review>(CompleteTripFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_RATING_REVIEW, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeDriverReview(session, SharedPreferenceManager.getRideId(getActivity()), ratingbyuser, review);
    }

    private void ratingDialog() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.rating_screen, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        dialogBuilder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return true;
            }
        });

        TextView textView = (TextView) dialogView.findViewById(R.id.userRatingTextView);
        textView.setText("How Was Your Trip With " + userName + "?");
        ratingBar = (RatingBar) dialogView.findViewById(R.id.ratingBar);
        ratingSubmit = (Button) dialogView.findViewById(R.id.ratingSubmit);
        et_review = (EditText) dialogView.findViewById(R.id.rdivereeviewEt);
        Drawable progress = ratingBar.getProgressDrawable();
        //  DrawableCompat.setTint(progress, getActivity().getResources().getColor(R.color.fab_color));
        /***************Button Click*******************/
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingbyuser = String.valueOf(rating);
            }
        });
        ratingSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driverReview = "";
                DriverReview(driverReview);
                dialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }
        });
        dialogBuilder.show();

    }
}
