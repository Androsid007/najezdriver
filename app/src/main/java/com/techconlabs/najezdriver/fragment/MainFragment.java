
package com.techconlabs.najezdriver.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 8/11/2017.
 */

public class MainFragment extends SuperFragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String[] titles;
    private TextView tvTab;
    private int count;
    private String balance = null;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_main, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);

        titles = new String[]{getActivity().getResources().getString(R.string.home), getActivity().getResources().getString(R.string.earnin_gs), getActivity().getResources().getString(R.string.my_trip), getActivity().getResources().getString(R.string.account)};
        setupViewPager(viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);


        count = SharedPreferenceManager.getBalance(getActivity());
        setupTabIcons();
        return view;

    }

    private void setupTabIcons() {
        for (int i = 0; i < titles.length; i++) {
            tvTab = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            if (i == 0) {
                tvTab.setText(titles[i]);
                tvTab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.home_tab_selector, 0, 0);
                tabLayout.getTabAt(i).setCustomView(tvTab);
            } else if (i == 1) {
                tvTab.setText(titles[i]);
                tvTab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.earning_tab_selector, 0, 0);
                tabLayout.getTabAt(i).setCustomView(tvTab);
            } else if (i == 2) {
                tvTab.setText(titles[i]);
                tvTab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.my_trip_tab_selector, 0, 0);
                tabLayout.getTabAt(i).setCustomView(tvTab);
            } else if (i == 3) {
                tvTab.setText(titles[i]);
                tvTab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.account_tab_selector, 0, 0);
                tabLayout.getTabAt(i).setCustomView(tvTab);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new HomeFragment(), getString(R.string.home));
        adapter.addFragment(new WalletFrgamnet(), getString(R.string.earnin_gs));
        adapter.addFragment(new TripFragment(), getString(R.string.my_trip));
        adapter.addFragment(new AccountFragment(), getString(R.string.account));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<android.support.v4.app.Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}