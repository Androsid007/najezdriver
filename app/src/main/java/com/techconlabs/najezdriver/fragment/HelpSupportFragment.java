package com.techconlabs.najezdriver.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Suport;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by hp1 on 8/13/2017.
 */

public class HelpSupportFragment extends Fragment implements RetrofitTaskListener<Suport> {
    private EditText emailId, enterName, typeYourComment;
    private String email, eName, ourComment;
    private ProgressDialog progressDialog;
    private Button submitButton;
    private TextInputLayout lv_name, lv_email, lv_comment;
    private ImageView uploadimgIv;
    String imageUpload;

    private static final int SELECT_PHOTO = 100;
    Spinner spinner;
    private String supportTypeId;

    public HelpSupportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.help_support, container, false);
        emailId = (EditText) view.findViewById(R.id.emailId);
        enterName = (EditText) view.findViewById(R.id.enterName);
        typeYourComment = (EditText) view.findViewById(R.id.typeYourComment);
        submitButton = (Button) view.findViewById(R.id.submitButton);
        lv_name = (TextInputLayout) view.findViewById(R.id.name_layout);
        lv_comment = (TextInputLayout) view.findViewById(R.id.comment_layout);
        lv_email = (TextInputLayout) view.findViewById(R.id.email_layout);
        uploadimgIv = (ImageView) view.findViewById(R.id.imageuploadIv);
        // spinner = (Spinner) view.findViewById(R.id.spinerValue);

        email = SharedPreferenceManager.getEmailId(getActivity());
        eName = SharedPreferenceManager.getFirstName(getActivity());
        if (!email.isEmpty()) {
            emailId.setText(email);
            emailId.setFocusable(false);

        }
        if (!eName.isEmpty()) {
            enterName.setText(eName);
            enterName.setFocusable(false);
        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setValue();
                if (Vlaidate()) {
                    callToService(email, eName, ourComment);
                } else {

                }

            }
        });

        uploadimgIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPicker = new Intent(Intent.ACTION_PICK);
                photoPicker.setType("image/*");
                startActivityForResult(photoPicker, SELECT_PHOTO);
            }
        });

        // callToGetSupporttype();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    imageUpload = getByteArrayFromUri(selectedImage);

                    Bitmap yourSelectedImage = null;
                    try {
                        //decodeUri() Method Defined Below
                        yourSelectedImage = decodeUri(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    uploadimgIv.setImageBitmap(yourSelectedImage);
                }
        }
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o2);

    }

    public void setValue() {
        email = emailId.getText().toString().trim();
        eName = enterName.getText().toString().trim();
        ourComment = typeYourComment.getText().toString().trim();
    }

    public boolean Vlaidate() {
        boolean valid = true;
        if (email.isEmpty()) {
            lv_email.requestFocus();
            lv_email.setErrorEnabled(true);
            lv_email.setError(getString(R.string.email_error));
            valid = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            lv_email.requestFocus();
            lv_email.setErrorEnabled(true);
            lv_email.setError(getString(R.string.email_format_error));
            valid = false;
        }
        if (eName.isEmpty()) {
            lv_name.requestFocus();
            lv_name.setErrorEnabled(true);
            lv_name.setError(getString(R.string.name_text));
            valid = false;
        }
        if (ourComment.isEmpty()) {
            lv_comment.requestFocus();
            lv_comment.setErrorEnabled(true);
            lv_comment.setError(getString(R.string.comment_error));
            valid = false;
        }
        return valid;
    }


    public void callToGetSupporttype() {
        showProgreass();
        String url = String.format(ServerConfigStage.HELP_SUPPORT_TYPE());
        RetrofitTask task = new RetrofitTask<Suport>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.SUPPORT_TYPE, url, getActivity());
        task.execute();
        typeYourComment.setText("");
    }

    public void callToService(String email, String eName, String ourComment) {
        showProgreass();
        String url = String.format(ServerConfigStage.HELP_SUPPORT());
        RetrofitTask task = new RetrofitTask<Suport>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.HELP_SUPPORT, url, getActivity());
        String token = SharedPreferenceManager.getToken(getActivity());
        task.executeHelpSupport(token, email, eName, ourComment, imageUpload, "driver");
        typeYourComment.setText("");
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRetrofitTaskComplete(Response<Suport> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body().getStatus() > 0) {
                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                getActivity().finish();

    }
        }
    }


    //When Support type Enable
        /*{
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body().getStatus() > 0) {
                if(_callerFunction == CommonUtility.CallerFunction.SUPPORT_TYPE){

                    final Suport suportType = response.body();
                    List<String> categories = new ArrayList<String>();
                    for(int i=0;i<suportType.getOptions().size();i++){
                        categories.add(suportType.getOptions().get(i).getText());
                    }
        /*            *//**//*//**//*//* Spinner Drop down elements
                    List<String> categories = new ArrayList<String>();
                    categories.add("Aug,18");
                    categories.add("Aug,19");
                    categories.add("Aug,20");
                    categories.add("Aug,21");
                    categories.add("Aug,22");
                    categories.add("Aug,23");*//**//*


                    CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), (ArrayList<String>) categories);
                    spinner.setAdapter(customSpinnerAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            supportTypeId = suportType.getOptions().get(position).getId();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            supportTypeId ="";
                        }
                    });
                }
                else{
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                }


            }
        }
    }*/

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }

    private String getByteArrayFromUri(Uri selectedImage) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
            Bitmap bitmap1 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 12, bitmap.getHeight() / 12, true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
            //Log.e("byte array", photo);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

    }
}

