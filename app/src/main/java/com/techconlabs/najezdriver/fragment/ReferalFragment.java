package com.techconlabs.najezdriver.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by hp1 on 8/13/2017.
 */

public class ReferalFragment extends Fragment {
    private TextView referCode, copyCode,disc_tv;
    private String referal;
    private Button shareBtn;

    public ReferalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.refreal_screen, container, false);
        referCode = (TextView) view.findViewById(R.id.referCode);
        copyCode = (TextView) view.findViewById(R.id.inputCopy);
        shareBtn = (Button) view.findViewById(R.id.addInvite);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "NAJEZ");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, " ");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        referal = SharedPreferenceManager.getReferalCode(getActivity());
        referCode.setText(referal);
        copyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(getString(R.string.copied), referal);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), getString(R.string.copied), Toast.LENGTH_LONG).show();

            }


        });
        disc_tv = (TextView) view.findViewById(R.id.disc_tv);
        disc_tv.setText(Html.fromHtml(getString(R.string.new_referral_string)+"\n"+getString(R.string.get_10)+" " +getString(R.string.there_ride)));
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

}
