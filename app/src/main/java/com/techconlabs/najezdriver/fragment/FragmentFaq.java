package com.techconlabs.najezdriver.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.About;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import retrofit2.Response;

/**
 * Created by Dell on 8/23/2017.
 */

public class FragmentFaq extends Fragment implements RetrofitTaskListener<About> {
    private WebView descText;
    private ProgressDialog progressDialog;
    private LinearLayout aboutVis;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        descText = (WebView) view.findViewById(R.id.descText);
        descText.getSettings().setJavaScriptEnabled(true);
         aboutVis = (LinearLayout) view.findViewById(R.id.aboutVis);
        callToService();
        return view;
    }

    public void onResume() {
        super.onResume();
    }

    public void callToService()
   {
        showProgreass();
        String url = String.format(ServerConfigStage.ABOUT_NAZEZ());
        RetrofitTask task = new RetrofitTask<About>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ABOUT_US, url, getActivity());
       String session = SharedPreferenceManager.getToken(getActivity());
       task.executeAboutUs(session,"driver-faq",SharedPreferenceManager.getLanguage(getActivity()));
   }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    @Override
    public void onRetrofitTaskComplete(Response<About> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body().getStatus() > 0) {
                  About about = (About) response.body();
                aboutVis.setVisibility(View.VISIBLE);
                descText.loadData(about.getData(), "text/html", null);
     }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();


    }
}
