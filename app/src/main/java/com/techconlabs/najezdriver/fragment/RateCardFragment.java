package com.techconlabs.najezdriver.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.RateCardAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Datum;
import com.techconlabs.najezdriver.model.Rate;
import com.techconlabs.najezdriver.model.Voucher;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * Created by hp1 on 8/13/2017.
 */


public class RateCardFragment extends Fragment implements RetrofitTaskListener {
    private RecyclerView recyclerView;
    private ArrayList<Datum> rateCardList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private RateCardAdapter mAdapter;
    private Button entercoupnbtn;


    public RateCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.purchase_recycleview, container, false);
        init(view);
        entercoupnbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupWindow coupnWindow;
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View complete_layout = inflater.inflate(R.layout.purchase_ride_layout, null);
                complete_layout.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.popanim));
                coupnWindow = new PopupWindow(complete_layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                coupnWindow.showAtLocation(complete_layout, Gravity.CENTER, 0, 0);


                Button coupnBtn = (Button) complete_layout.findViewById(R.id.coupnBtn);
                final EditText coupnEt = (EditText) complete_layout.findViewById(R.id.coupnEt);
                coupnEt.requestFocus();
                coupnBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String coupanCode = coupnEt.getText().toString();

                        if (TextUtils.isEmpty(coupanCode)) {
                            // coupnEt.setError(getString(R.string.coupn_string));
                            Toast.makeText(getActivity(), getString(R.string.coupn_string), Toast.LENGTH_LONG).show();

                        } else {
                            OfflineCoupan(coupanCode);
                            coupnWindow.dismiss();
                        }
                        //  complete_layout.setAnimation(AnimationUtils.loadAnimation(HomeActivity.this, anim.slide_out_down,null));

                    }
                });
                coupnWindow.setOutsideTouchable(false);
                coupnWindow.setFocusable(true);
                coupnWindow.update();
                coupnWindow.setBackgroundDrawable(new BitmapDrawable());
            }
        });
        apiCallService();
        return view;

    }

    private void OfflineCoupan(String coupanCode) {
        showProgreass();
        String url = String.format(ServerConfigStage.VOUCHER_COUPAN());
        RetrofitTask task = new RetrofitTask<Voucher>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.VOUCHER_APPLY, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeVoucher(session, coupanCode);
    }

    public void apiCallService() {
        showProgreass();
        String url = String.format(ServerConfigStage.RATE_CARD());
        RetrofitTask task = new RetrofitTask<Rate>(this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.RATE_CARD, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeRateCard(session);

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }


    public void init(View view)

    {
        entercoupnbtn = (Button) view.findViewById(R.id.couponCodeBtn);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerPurchase);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        if (_callerFunction == CommonUtility.CallerFunction.RATE_CARD && response != null) {
            Rate rateCard = (Rate) response.body();
            stopProgress();
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (rateCard.getStatus() > 0) {
                        rateCardList = rateCard.getData();
                        mAdapter = new RateCardAdapter(getActivity(), rateCardList);
                        recyclerView.setAdapter(mAdapter);
                    } else {
                        Toast.makeText(context, rateCard.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }
            }
        }

        if (_callerFunction == CommonUtility.CallerFunction.VOUCHER_APPLY && response != null) {
            stopProgress();
            Voucher voucher = (Voucher) response.body();
            if (response.isSuccessful()) {
                if (voucher.getStatus() > 0) {
                    int ridebal = 0;
                    if (voucher.getData().getTotalRides() != null) {
                        ridebal = Integer.parseInt(voucher.getData().getTotalRides());
                    }
                    SharedPreferenceManager.settotalRide(getActivity(), ridebal);

                    Toast.makeText(context, voucher.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, voucher.getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }

}
