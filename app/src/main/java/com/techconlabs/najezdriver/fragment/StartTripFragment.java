package com.techconlabs.najezdriver.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.core.ui.dialog.ProgressDialogFragment;
import com.quickblox.users.model.QBUser;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.activity.DeliveryImage;
import com.techconlabs.najezdriver.activity.MapsActivity;
import com.techconlabs.najezdriver.chatNew.ChatActivity;
import com.techconlabs.najezdriver.chatNew.ChatHelper;
import com.techconlabs.najezdriver.chatNew.qb.QbDialogHolder;
import com.techconlabs.najezdriver.common.DirectionJsonParsor;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.GetData;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class StartTripFragment extends Fragment {
    View vv;
    GetData rideInformation;
    TextView textViewNamePickUp;
    RelativeLayout confirmbtn;
    SupportMapFragment mMapFragment;
    LatLng mLastKnowLatLng;
    LatLng dropLatlng;
    GoogleMap mGoogleMap;
    ArrayList markerPoints;
    Button btn_call, btn_chat;
    private static final int REQUEST_DIALOG_ID_FOR_UPDATE = 165;

    String phone;
    ImageView go_loc;
    private Button delivery_view;

    double drop_lat, drop_log, pick_lat, pick_long;
    String pick_name = "", drop_name = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

                  vv = inflater.inflate(R.layout.fragment_start_trip, container, false);

                markerPoints = new ArrayList();
                Bundle args = getArguments();
                btn_call = (Button) vv.findViewById(R.id.callBtn);
                btn_chat = (Button) vv.findViewById(R.id.chat_btn);
                go_loc = (ImageView) vv.findViewById(R.id.go_loc);
                delivery_view = (Button) vv.findViewById(R.id.delivery_view);


                phone = SharedPreferenceManager.getMobileNumber(getActivity());
                btn_call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                        startActivity(intent);
                    }
                });
                rideInformation = (GetData) args.getSerializable("data");
                mMapFragment = SupportMapFragment.newInstance();
                //  dropLatlng = new LatLng(Double.parseDouble(rideInformation.getRideData().getDropLat()), Double.parseDouble(rideInformation.getRideData().getDropLng()));
                getChildFragmentManager().beginTransaction().replace(R.id.gooleMapContainer, mMapFragment).commit();
                mLastKnowLatLng = new LatLng(Double.parseDouble(rideInformation.getRideData().getPickupLat()), Double.parseDouble(rideInformation.getRideData().getPickupLng()));
                dropLatlng = new LatLng(Double.parseDouble(rideInformation.getRideData().getDropLat()), Double.parseDouble(rideInformation.getRideData().getDropLng()));
                textViewNamePickUp = (TextView) vv.findViewById(R.id.textViewNamePickUp);
                pick_lat = Double.parseDouble(rideInformation.getRideData().getPickupLat());
                pick_long = Double.parseDouble(rideInformation.getRideData().getPickupLng());
                drop_lat = Double.parseDouble(rideInformation.getRideData().getDropLat());
                drop_log = Double.parseDouble(rideInformation.getRideData().getDropLng());
                textViewNamePickUp.setText(rideInformation.getRideData().getUserdata().getFname());
                confirmbtn = (RelativeLayout) vv.findViewById(R.id.confiem_arrival);
                confirmbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (rideInformation != null) {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("rideInformation", rideInformation);
                            Intent intent = new Intent(getActivity(), MapsActivity.class);
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                            startActivity(intent.putExtras(bundle));
                            getActivity().finish();
                        }

                    }
                });

                initializMap();
                if (rideInformation != null && rideInformation.getRideData() != null && rideInformation.getRideData().getIsDelivery() == 1) {
                    delivery_view.setVisibility(View.VISIBLE);

                }
                if (SharedPreferenceManager.getChatId(getActivity()).equals("") && SharedPreferenceManager.getChatId(getActivity()) == null) {
                    btn_chat.setBackgroundColor(getActivity().getResources().getColor(R.color.gold));
                    btn_chat.setEnabled(false);
                }
                btn_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        QBUser qbUser = new QBUser();
                        qbUser.setId(Integer.parseInt(SharedPreferenceManager.getChatId(getActivity())));
                        qbUser.setPassword(SharedPreferenceManager.getPassword(getActivity()));
                        qbUser.setEmail(SharedPreferenceManager.getEmailId(getActivity()));
                        qbUser.setLogin(SharedPreferenceManager.getEmailId(getActivity()));
                        loginToChat(qbUser);


                    }
                });
                delivery_view.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {

                                                         if (!SharedPreferenceManager.getChatId(getActivity()).equals("null") && !rideInformation.getRideData().getDeliveryImg().equals("") && !rideInformation.getRideData().getDeliveryImg().equals("null")) {
                                                             Bundle bundle = new Bundle();
                                                             bundle.putSerializable("rideInformation", rideInformation);
                                                             Intent intent = new Intent(getActivity(), DeliveryImage.class);
                                                             startActivity(intent.putExtras(bundle));

                                                         } else {
                                                             AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                                                             builder1.setMessage(rideInformation.getRideData().getDeliveryComment());
                                                             builder1.setCancelable(true);

                                                             builder1.setPositiveButton(
                                                                     "Ok",
                                                                     new DialogInterface.OnClickListener() {
                                                                         public void onClick(DialogInterface dialog, int id) {
                                                                             dialog.cancel();
                                                                         }
                                                                     });

                                                             builder1.show();
                                                         }

                                                     }
                                                 }
                );


                go_loc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rideInformation != null) {
                            pick_lat = Double.parseDouble(rideInformation.getRideData().getPickupLat());
                            pick_long = Double.parseDouble(rideInformation.getRideData().getPickupLng());
                            drop_lat = Double.parseDouble(rideInformation.getRideData().getDropLat());
                            drop_log = Double.parseDouble(rideInformation.getRideData().getDropLng());
                            String uri = "http://maps.google.com/maps?saddr=" + drop_lat + "," + drop_log + "&daddr=" + pick_lat + "," + pick_long;
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                            startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "Location Not Avilable", Toast.LENGTH_LONG).show();
                        }
 }
                });




        return vv;

        }



    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if(addresses!=null &&addresses.size()>0) {

                Address obj = addresses.get(0);
                String add = obj.getAddressLine(0);
                add = add + "\n" + obj.getCountryName();
                add = add + "\n" + obj.getCountryCode();
                add = add + "\n" + obj.getAdminArea();
                add = add + "\n" + obj.getPostalCode();
                add = add + "\n" + obj.getSubAdminArea();
                add = add + "\n" + obj.getLocality();
                add = add + "\n" + obj.getSubThoroughfare();
                pick_name = obj.getSubAdminArea();
                drop_name = obj.getSubAdminArea();
                Log.v("IGA", "Address" + add);

            }


              // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();

            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void initializMap() {
        final View mapView = mMapFragment.getView();
        mMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                initializeMap(mapView, googleMap);
                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    getActivity(), R.raw.style_json));

                    if (!success) {
                        Log.e("", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("", "Can't find style. Error: ", e);
                }
            }
        });


    }

    private void initializeMap(View mapView, GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        if (mLastKnowLatLng != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            markerPoints.add(mLastKnowLatLng);
            if (dropLatlng != null) {
                markerPoints.add(dropLatlng);
            }

            MarkerOptions options = new MarkerOptions();

            options.position(mLastKnowLatLng);
            if (dropLatlng != null) {
                options.position(dropLatlng);
            }
            if (markerPoints.size() == 1) {
                getAddress(pick_lat,pick_long);
                options.title(pick_name);
                options.icon(BitmapDescriptorFactory.fromResource(R.drawable.pinlocation));
            } else if (markerPoints.size() == 2) {
                getAddress(drop_lat,drop_log);
                options.title(drop_name);
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }

            mGoogleMap.addMarker(options);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(mLastKnowLatLng));
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(mLastKnowLatLng).zoom(14.0f).build();
            mGoogleMap.moveCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            if (markerPoints.size() >= 2) {
                LatLng origin = (LatLng) markerPoints.get(0);
                LatLng dest = (LatLng) markerPoints.get(1);


                String url = getDirectionsUrl(origin, dest);

                DownloadTask downloadTask = new DownloadTask();

                downloadTask.execute(url);


            }
        }

    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

// Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

// Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

// Sensor enabled
        String sensor = "sensor=false";

// Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

// Output format
        String output = "json";

// Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

// Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

// Connecting to url
            urlConnection.connect();

// Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //    Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

// For storing data from web service
            String data = "";

            try {
// Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
// doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

// Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionJsonParsor parser = new DirectionJsonParsor();

// Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

// Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

// Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

// Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

// Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                lineOptions.color(Color.BLUE);

            }

// Drawing polyline in the Google Map for the i-th route
            if(mGoogleMap!=null && lineOptions!=null) {
                mGoogleMap.addPolyline(lineOptions);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }
    private void loginToChat(final QBUser user) {
       // ProgressDialogFragment.show(getActivity().getSupportFragmentManager(), R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                ArrayList<QBUser> selectedUsers = new ArrayList<>();

                QBUser user = new QBUser(SharedPreferenceManager.getEmailId(getActivity()), SharedPreferenceManager.getPassword(getActivity()));
                user.setEmail(SharedPreferenceManager.getEmailId(getActivity()));
                user.setId(Integer.parseInt(SharedPreferenceManager.getChatId(getActivity())));
                user.setPassword(SharedPreferenceManager.getPassword(getActivity()));
                selectedUsers.add(user);
                QBUser qbUser = new QBUser();
                qbUser.setId(Integer.parseInt(rideInformation.getRideData().getUserdata().getChatid()));
                selectedUsers.add(qbUser);

                if (isPrivateDialogExist(selectedUsers)) {
                    ProgressDialogFragment.hide(getActivity().getSupportFragmentManager());
                     selectedUsers.remove(0);
                    QBChatDialog existingPrivateDialog = QbDialogHolder.getInstance().getPrivateDialogWithUser(selectedUsers.get(0));
                     ChatActivity.startForResult(getActivity(), REQUEST_DIALOG_ID_FOR_UPDATE, existingPrivateDialog);
                } else {
                   // ProgressDialogFragment.show(getActivity().getSupportFragmentManager(), R.string.create_chat);
                    createDialog(selectedUsers);
                }


            }

            @Override
            public void onError(QBResponseException e) {
                ProgressDialogFragment.hide(getActivity().getSupportFragmentManager());

            }
        });
    }
    private boolean isPrivateDialogExist(ArrayList<QBUser> allSelectedUsers) {
        ArrayList<QBUser> selectedUsers = new ArrayList<>();
        selectedUsers.addAll(allSelectedUsers);
        selectedUsers.remove(0);
        return selectedUsers.size() == 1 && QbDialogHolder.getInstance().hasPrivateDialogWithUser(selectedUsers.get(0));
    }
    private void createDialog(final ArrayList<QBUser> selectedUsers) {
        ChatHelper.getInstance().createDialogWithSelectedUsers(selectedUsers,
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle args) {

                        ChatActivity.startForResult(getActivity(), REQUEST_DIALOG_ID_FOR_UPDATE, dialog);
                        ProgressDialogFragment.hide(getActivity().getSupportFragmentManager());
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        ProgressDialogFragment.hide(getActivity().getSupportFragmentManager());

                    }
                }
        );
    }


}
