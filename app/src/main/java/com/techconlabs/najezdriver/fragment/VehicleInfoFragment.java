package com.techconlabs.najezdriver.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.VehcileTypeAdapter;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.interfaces.OnCheckedListner;
import com.techconlabs.najezdriver.model.OptVehicleFamily;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehicleInfoFragment extends Fragment implements OnCheckedListner {
    private String modelName, modelNum, modelYear, serialNum, colorCode = "";
    private TextView mName, mNumber, mYear, sNumer, mColor;
    private String carImage, familyId, vechildeType, vechildeTypeId;
    private ImageView ivCar;
    RecyclerView recyclerView;
    VehcileTypeAdapter adapter;
    List<OptVehicleFamily> vehicleFamilies = null;
    boolean checked_status = false;


    public VehicleInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vechile_information, container, false);
        modelName = SharedPreferenceManager.getModelName(getActivity());
        modelNum = SharedPreferenceManager.getModelNum(getActivity());
        modelYear = SharedPreferenceManager.getModelYear(getActivity());
        serialNum = SharedPreferenceManager.getSerialNum(getActivity());
        colorCode = SharedPreferenceManager.getColorCode(getActivity());
        carImage = SharedPreferenceManager.getCarImage(getActivity());
        familyId = SharedPreferenceManager.getfamilyId(getActivity());
        if (SharedPreferenceManager.getVechileList(getActivity()) != null) {
            vehicleFamilies = SharedPreferenceManager.getVechileList(getActivity());
        }
        mName = (TextView) view.findViewById(R.id.tvCarName);
        mNumber = (TextView) view.findViewById(R.id.tvCarNumber);
        mYear = (TextView) view.findViewById(R.id.mYear);
        sNumer = (TextView) view.findViewById(R.id.sNumer);
        mColor = (TextView) view.findViewById(R.id.mColor);
        ivCar = (ImageView) view.findViewById(R.id.ivCar);
        SharedPreferenceManager.setVechileMediumstatus(getActivity(), "");
        SharedPreferenceManager.setVechileFamilyStatus(getActivity(), "");
        if (modelNum.equals("")) {
            mNumber.setVisibility(View.GONE);
        } else {
            mNumber.setText(modelNum);
        }
        mName.setText(modelName);
        mYear.setText(modelYear);
        sNumer.setText(serialNum);
        mColor.setText(colorCode);
        if (carImage != null && !carImage.equals("")) {
            Picasso.with(getActivity()).load(carImage).resize(1080, 720).centerInside().into(ivCar);
        } else {
            ivCar.setImageResource(R.drawable.car_info_car);
        }
        SharedPreferenceManager.setfamilyId(getActivity(), familyId);
        recyclerView = (RecyclerView) view.findViewById(R.id.cartypeRv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        if (vehicleFamilies != null) {
            adapter = new VehcileTypeAdapter(getActivity(), vehicleFamilies, VehicleInfoFragment.this);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void OnChage(int position, boolean change) {
        checked_status = vehicleFamilies.get(position).setSelected(change);

        if (checked_status) {
            if (vehicleFamilies.get(position).getFamily().equals("Medium")) {
                SharedPreferenceManager.setVechileMediumstatus(getActivity(), vehicleFamilies.get(position).getId());
            } else if (vehicleFamilies.get(position).getFamily().equals("Family")) {
                SharedPreferenceManager.setVechileFamilyStatus(getActivity(), vehicleFamilies.get(position).getId());
            }
        } else {
            SharedPreferenceManager.setVechileMediumstatus(getActivity(), "");
            SharedPreferenceManager.setVechileFamilyStatus(getActivity(), "");
        }


    }
}


