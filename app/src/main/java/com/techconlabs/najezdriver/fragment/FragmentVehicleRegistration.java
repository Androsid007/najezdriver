package com.techconlabs.najezdriver.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.activity.CarCategoryActivity;
import com.techconlabs.najezdriver.adapter.CustomModelSpinnerAdapter;
import com.techconlabs.najezdriver.adapter.StringSpinnerAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtil;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.CarType;
import com.techconlabs.najezdriver.model.CityModel;
import com.techconlabs.najezdriver.model.ColorList;
import com.techconlabs.najezdriver.model.Data;
import com.techconlabs.najezdriver.model.VehicleList;
import com.techconlabs.najezdriver.model.VehicleRegister;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.techconlabs.najezdriver.common.Constants.AUTH_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.DRIVING_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.IDENTITY_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.INSURENCE_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.PHOTO_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.REGISTRATION_REQUEST_CODE;
import static com.techconlabs.najezdriver.common.Constants.TAF_REQUEST_CODE;

/**
 * Created by Dell on 8/12/2017.
 */

public class FragmentVehicleRegistration extends Fragment implements RetrofitTaskListener, View.OnClickListener {
    private int REQUEST_CODE = 121;
    private String photo = "", identity = "", driving = "", insurance = "", auth = "", registration = "", tafweeth = "", tafweethExp = "";
    private TextView tvPhotoChoser, residencyChooser, licenceChooser, tvAuthorizationImgChoser, tvVechileInsuranceChoser, tvVehicleRegistration,
            tvTafweethChooser, tvTafWeeth, tvAuth, tvVehicleInsurence, tvVehicleRegis, tvDLicense, tvIdentity, tvPhotograph;
    private ProgressDialog progressDialog;
    private Spinner spBank, spCarManufaturingYear,
            spCarModelSpinner, spVehicleTypeSpinner;
    private EditText etIbanno, etCarColor, etVehiclePlateName, etVehicleSn, etvechilenumber;
    private TextInputLayout tilIbanno, tilCarColor, tilVehiclePlateName, tilVehicleSn, vechilenumber;
    private Button buttonSignUp;
    private ScrollView srcollHolder;
    private String carType, carModel, yearOfManuFacturing,colorS, bank;
    private ImageView ivTafWeeth, ivAuth, ivVehicleInsurence, ivVehicleRegis, ivDLicense, ivIdentity, ivPhotograph;
    private List<VehicleList> carTypeList = new ArrayList<>();
    private List<com.techconlabs.najezdriver.model.List> carTypeListNew = new ArrayList<>();
    private List<String> carModelList = new ArrayList<>();
    private List<String> carManufacturingYearList = new ArrayList<>();
    private List<String> bankList = new ArrayList<>();
    private List<String> emptycolorList = new ArrayList<>();
    private boolean isActivityFinished = true;
    private String vehcilePlaetNo,plateChar;
    private boolean driverstat;
    private Spinner spColor;
    LinearLayout serial_holder, photo_holder;
    RelativeLayout spColorAdd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        if (view == null) {
            view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_vehicle_registration, container, false);
        }
        init(view);

        driverstat = SharedPreferenceManager.getDriverDeliveryStat(getActivity());
        if (!driverstat) {
            serial_holder.setVisibility(View.GONE);
            photo_holder.setVisibility(View.GONE);
            spColorAdd.setVisibility(View.GONE);

        } else {
            serial_holder.setVisibility(View.VISIBLE);
            photo_holder.setVisibility(View.VISIBLE);
            spColorAdd.setVisibility(View.VISIBLE);
  }
        callCarTypeApi();
        callColorList();

        //setCarModelSpinner(new ArrayList<Luxury>());
        //set Car Type Spinner
        setCarManufacturingSpinner();
        tvPhotoChoser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilCarColor.clearFocus();
                tilCarColor.setErrorEnabled(false);
                REQUEST_CODE = PHOTO_REQUEST_CODE;
                CropImage.activity().start(getContext(), FragmentVehicleRegistration.this);
            }
        });
        residencyChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilCarColor.clearFocus();
                tilCarColor.setErrorEnabled(false);
                REQUEST_CODE = IDENTITY_REQUEST_CODE;
                CropImage.activity()
                        .start(getContext(), FragmentVehicleRegistration.this);
            }
        });
        licenceChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tilCarColor.clearFocus();
                tilCarColor.setErrorEnabled(false);
                REQUEST_CODE = DRIVING_REQUEST_CODE;
                CropImage.activity()
                        .start(getContext(), FragmentVehicleRegistration.this);
            }
        });
        tvAuthorizationImgChoser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REQUEST_CODE = AUTH_REQUEST_CODE;
                CropImage.activity()
                        .start(getContext(), FragmentVehicleRegistration.this);
            }
        });
        tvVechileInsuranceChoser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REQUEST_CODE = INSURENCE_REQUEST_CODE;
                CropImage.activity()
                        .start(getContext(), FragmentVehicleRegistration.this);
            }
        });
        tvVehicleRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REQUEST_CODE = REGISTRATION_REQUEST_CODE;
                CropImage.activity()
                        .start(getContext(), FragmentVehicleRegistration.this);
            }
        });
        tvTafweethChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                REQUEST_CODE = TAF_REQUEST_CODE;
                CropImage.activity()
                        .start(getContext(), FragmentVehicleRegistration.this);
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isActivityFinished = false;
                registerVehicle();
            }
        });
        return view;
    }

    private void callColorList() {
        stopProgress();
        String url = String.format(ServerConfigStage.GET_COLOR());
        RetrofitTask task = new RetrofitTask<ColorList>(this,CommonUtility.HTTP_REQUEST_TYPE.GET,CommonUtility.CallerFunction.GET_COLOR_LIST,url,getActivity());
        task.execute();
    }

    private void callBankListApi() {
        String url = String.format(ServerConfigStage.BANK_LIST(),SharedPreferenceManager.getLanguage(getActivity()));
        RetrofitTask task = new RetrofitTask<CityModel>(this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_BANK_LIST, url, getActivity());
        task.execute();
    }

    private void callCarTypeApi() {
        showProgreass();
        String url = String.format(ServerConfigStage.CAR_TYPE_LIST());
        String token = SharedPreferenceManager.getToken(getActivity());
        RetrofitTask task = new RetrofitTask<CarType>(this, CommonUtility.HTTP_REQUEST_TYPE.GET, CommonUtility.CallerFunction.GET_CAR_TYPE_LIST, url, getActivity());
        task.execute();
    }



    private void setCarTypeSpinner(final List<com.techconlabs.najezdriver.model.List> vehicleList) {
        Collections.sort(vehicleList, new CustomComparator());
          if (vehicleList != null) {
            com.techconlabs.najezdriver.model.List firstList = new com.techconlabs.najezdriver.model.List();
            firstList.setId("0");
            firstList.setType("0");
            firstList.setCarBrand("*CAR");
            firstList.setCarModel(" Model");
            vehicleList.add(0, firstList);
            CustomModelSpinnerAdapter adapterCarType = new CustomModelSpinnerAdapter(getActivity(), R.layout.string_spinner_layout, R.id.txt, vehicleList);
              spVehicleTypeSpinner.setAdapter(adapterCarType);
            spVehicleTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((LinearLayout) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                    if (carTypeList != null) {
                        carTypeList.clear();
                    }
                    if (position == 0) {
                        carModel = "";
                    } else {
                       // carModel = vehicleList.get(position).getCarBrand() + " " + vehicleList.get(position).getCarModel();
                        carModel = vehicleList.get(position).getId();

                        for (int i = 0; i < vehicleList.size(); i++) {
                            if (vehicleList.get(i).getCarModel().equalsIgnoreCase(vehicleList.get(position).getCarModel())) {
                                VehicleList vehicleListNew = new VehicleList();
                                vehicleListNew.setId(vehicleList.get(i).getId());
                                vehicleListNew.setType(vehicleList.get(i).getType());
                                carTypeList.add(vehicleListNew);
                            }
                        }
                    }
                    setCarModelSpinner(carTypeList);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    carModel = "";
                }
            });
        }
    }



    public class CustomComparator implements Comparator<com.techconlabs.najezdriver.model.List> {
        public int compare(com.techconlabs.najezdriver.model.List object1, com.techconlabs.najezdriver.model.List object2) {
            return object1.getCarBrand().compareToIgnoreCase(object2.getCarBrand());
        }
    }

    private void setCarModelSpinner(List<VehicleList> carModels) {

        if (carModelList != null) {
            carModelList.clear();
        }

        for (int i = 0; i < carModels.size(); i++) {
            carModelList.add(carModels.get(i).getType());
        }
         carModelList.add(0, "*TYPE OF VEHICLE");
        StringSpinnerAdapter adapterCarType = new StringSpinnerAdapter(getActivity(), R.layout.string_spinner_layout, R.id.txt, (ArrayList<String>) carModelList);
        spCarModelSpinner.setAdapter(adapterCarType);
        spCarModelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((LinearLayout) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                if (position == 0) {
                    carType = "";
                } else {
                    carType = carModelList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                carType = "";
            }
        });
    }

    private void setCarManufacturingSpinner() {
        carManufacturingYearList.add(0, "*YEAR OF THE MANUFACTURE");
        carManufacturingYearList.add(1, "2010");
        carManufacturingYearList.add(2, "2011");
        carManufacturingYearList.add(3, "2012");
        carManufacturingYearList.add(4, "2013");
        carManufacturingYearList.add(5, "2014");
        carManufacturingYearList.add(6, "2015");
        carManufacturingYearList.add(7, "2016");
        carManufacturingYearList.add(8, "2017");

        StringSpinnerAdapter adapterCarType = new StringSpinnerAdapter(getActivity(), R.layout.string_spinner_layout, R.id.txt, (ArrayList<String>) carManufacturingYearList);
        spCarManufaturingYear.setAdapter(adapterCarType);
        spCarManufaturingYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((LinearLayout) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                if (position == 0) {
                    yearOfManuFacturing = "";
                } else {
                    yearOfManuFacturing = carManufacturingYearList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                yearOfManuFacturing = "";
            }
        });

    }

    private void setBankSpinner(List<Data> data) {
        if (bankList != null) {
            bankList.clear();
        }
        for (int i = 0; i < data.size(); i++) {
            bankList.add(data.get(i).getBankName());
        }
        bankList.add(0, getString(R.string.bank_spinner));
        StringSpinnerAdapter adapterCarType = new StringSpinnerAdapter(getActivity(), R.layout.string_spinner_layout, R.id.txt, (ArrayList<String>) bankList);
        spBank.setAdapter(adapterCarType);
        spBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((LinearLayout) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                if (position == 0) {
                    bank = "";
                } else {
                    bank = bankList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                bank = "";
            }
        });
    }
    private void setColorList(ArrayList<String> data) {
        if (emptycolorList != null) {
            emptycolorList.clear();
        }
        emptycolorList.addAll(data);
        emptycolorList.add(0, "*COLOR");
        StringSpinnerAdapter adapterCarType = new StringSpinnerAdapter(getActivity(), R.layout.string_spinner_layout, R.id.txt, (ArrayList<String>) emptycolorList);
        spColor.setAdapter(adapterCarType);
        spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((LinearLayout) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                if (position == 0) {
                    colorS = "";
                } else {
                    colorS = emptycolorList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                colorS = "";
            }
        });
    }


    private void callPhotoChooser(int requestCode) {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, requestCode);
    }

    private void init(View view) {
        spColorAdd=(RelativeLayout)view.findViewById(R.id.spColorAdd);
        tvPhotoChoser = (TextView) view.findViewById(R.id.tvPhotoChoser);
        residencyChooser = (TextView) view.findViewById(R.id.residencyChooser);
        licenceChooser = (TextView) view.findViewById(R.id.licenceChooser);
        tvAuthorizationImgChoser = (TextView) view.findViewById(R.id.tvAuthorizationImgChoser);
        tvVechileInsuranceChoser = (TextView) view.findViewById(R.id.tvVechileInsuranceChoser);
        tvVehicleRegistration = (TextView) view.findViewById(R.id.tvVehicleRegistration);
        tvTafweethChooser = (TextView) view.findViewById(R.id.tvTafweethChooser);


        tvTafWeeth = (TextView) view.findViewById(R.id.tvTafWeeth);
        tvAuth = (TextView) view.findViewById(R.id.tvAuth);
        tvVehicleInsurence = (TextView) view.findViewById(R.id.tvVehicleInsurence);
        tvVehicleRegis = (TextView) view.findViewById(R.id.tvVehicleRegis);
        tvDLicense = (TextView) view.findViewById(R.id.tvDLicense);
        tvIdentity = (TextView) view.findViewById(R.id.tvIdentity);
        tvPhotograph = (TextView) view.findViewById(R.id.tvPhotograph);
        spColor = (Spinner) view.findViewById(R.id.spColor);
        ivTafWeeth = (ImageView) view.findViewById(R.id.ivTafWeeth);
        ivAuth = (ImageView) view.findViewById(R.id.ivAuth);
        ivVehicleInsurence = (ImageView) view.findViewById(R.id.ivVehicleInsurence);
        ivVehicleRegis = (ImageView) view.findViewById(R.id.ivVehicleRegis);
        ivDLicense = (ImageView) view.findViewById(R.id.ivDLicense);
        ivIdentity = (ImageView) view.findViewById(R.id.ivIdentity);
        ivPhotograph = (ImageView) view.findViewById(R.id.ivPhotograph);
        ivPhotograph.setColorFilter(getActivity().getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);
        ivIdentity.setColorFilter(getActivity().getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);
        ivDLicense.setColorFilter(getActivity().getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);
        ivVehicleRegis.setColorFilter(getActivity().getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);
        ivVehicleInsurence.setColorFilter(getActivity().getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);
        ivAuth.setColorFilter(getActivity().getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);
        ivTafWeeth.setColorFilter(getActivity().getResources().getColor(R.color.black), PorterDuff.Mode.MULTIPLY);

        spBank = (Spinner) view.findViewById(R.id.spBank);
        spCarManufaturingYear = (Spinner) view.findViewById(R.id.spCarManufaturingYear);
        spCarModelSpinner = (Spinner) view.findViewById(R.id.spCarModelSpinner);
        spVehicleTypeSpinner = (Spinner) view.findViewById(R.id.spVehicleTypeSpinner);

        etIbanno = (EditText) view.findViewById(R.id.etIbanno);
        etVehiclePlateName = (EditText) view.findViewById(R.id.etVehiclePlateName);
        etVehicleSn = (EditText) view.findViewById(R.id.etVehicleSn);

        tilIbanno = (TextInputLayout) view.findViewById(R.id.tilIbanno);
        tilVehiclePlateName = (TextInputLayout) view.findViewById(R.id.tilVehiclePlateName);
        tilVehicleSn = (TextInputLayout) view.findViewById(R.id.tilVehicleSn);
        vechilenumber = (TextInputLayout) view.findViewById(R.id.vechilenumber);
        etvechilenumber = (EditText) view.findViewById(R.id.etvechilenumber);
        buttonSignUp = (Button) view.findViewById(R.id.buttonSignUp);
        ivPhotograph.setOnClickListener(FragmentVehicleRegistration.this);
        ivIdentity.setOnClickListener(FragmentVehicleRegistration.this);
        ivDLicense.setOnClickListener(FragmentVehicleRegistration.this);
        ivVehicleRegis.setOnClickListener(FragmentVehicleRegistration.this);
        ivVehicleInsurence.setOnClickListener(FragmentVehicleRegistration.this);
        ivAuth.setOnClickListener(FragmentVehicleRegistration.this);
        ivTafWeeth.setOnClickListener(FragmentVehicleRegistration.this);

        srcollHolder = (ScrollView) view.findViewById(R.id.srcollHolder);

        serial_holder = (LinearLayout) view.findViewById(R.id.serial_holder);
        photo_holder = (LinearLayout) view.findViewById(R.id.photo_holder);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                switch (REQUEST_CODE) {
                    /*case PHOTO_REQUEST_CODE:
                        photo = getByteArrayFromUri(result.getUri());
                        tvPhotograph.setText(R.string.photo_jpg);
                        ivPhotograph.setVisibility(View.VISIBLE);
                        break;*/
                    case IDENTITY_REQUEST_CODE:
                        identity = getByteArrayFromUri(result.getUri());
                        tvIdentity.setText(R.string.residency_proof);
                        ivIdentity.setVisibility(View.VISIBLE);
                        break;
                    case DRIVING_REQUEST_CODE:
                        driving = getByteArrayFromUri(result.getUri());
                        tvDLicense.setText(R.string.licence_proof);
                        ivDLicense.setVisibility(View.VISIBLE);
                        break;
                    case AUTH_REQUEST_CODE:
                        auth = getByteArrayFromUri(result.getUri());
                        tvAuth.setText(R.string.auth_proof);
                        ivAuth.setVisibility(View.VISIBLE);
                        break;
                    case REGISTRATION_REQUEST_CODE:
                        registration = getByteArrayFromUri(result.getUri());
                        tvVehicleRegis.setText(R.string.registration_proof);
                        ivVehicleRegis.setVisibility(View.VISIBLE);
                        break;
                    case INSURENCE_REQUEST_CODE:
                        insurance = getByteArrayFromUri(result.getUri());
                        tvVehicleInsurence.setText(R.string.insurance_proof);
                        ivVehicleInsurence.setVisibility(View.VISIBLE);
                        break;
                    case TAF_REQUEST_CODE:
                        tafweeth = getByteArrayFromUri(result.getUri());
                        tvTafWeeth.setText(R.string.tafweeth_proof);
                        ivTafWeeth.setVisibility(View.VISIBLE);
                        break;
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                CommonUtil.showToast(getActivity(), getString(R.string.image_loading_error));
            }
        }

    }

    private String getByteArrayFromUri(Uri selectedImage) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
            Bitmap bitmap1 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 12, bitmap.getHeight() / 12, true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
            //Log.e("byte array", photo);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

    }

    private void registerVehicle() {
        if (driverstat) {
            if (doValidation()) {
                showProgreass();
                vehcilePlaetNo = etvechilenumber.getText().toString().trim();
               plateChar =etVehiclePlateName.getText().toString().trim();
                  VehicleRegister vehicleRegister = new VehicleRegister();
                vehicleRegister.setVehicleSno(etVehicleSn.getText().toString());
                vehicleRegister.setVehiclePlateNum(vehcilePlaetNo);
                vehicleRegister.setVehiclePlateChar(plateChar);
                vehicleRegister.setVehicleType(carType);
                vehicleRegister.setCarModel(carModel);
                vehicleRegister.setVehicleYear(yearOfManuFacturing);
                vehicleRegister.setVehicleColor(colorS);
                vehicleRegister.setPhotograph(photo);
                vehicleRegister.setVehicleReg(registration);
                vehicleRegister.setVehicleInsur(insurance);
                vehicleRegister.setAuthImg(auth);
                if (tafweeth != null && !tafweeth.equals("")) {
                    vehicleRegister.setTafweethImg(tafweeth);
                } else {
                    vehicleRegister.setTafweethImg("");
                }
                if (tafweethExp != null && !tafweethExp.equals("")) {
                    vehicleRegister.setTafweethExpire(tafweethExp);
                } else {
                    vehicleRegister.setTafweethExpire("");
                }

                String url = String.format(ServerConfigStage.VEHICLE_REGISTER());
                RetrofitTask task = new RetrofitTask<VehicleRegister>(FragmentVehicleRegistration.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.VEHICLE_REGISTRATION, url, getActivity(), vehicleRegister);
                task.execute();
            }
        } else if (!driverstat) {
            if (deliveryValidate()) {
                showProgreass();
                vehcilePlaetNo = etvechilenumber.getText().toString().trim();
                plateChar =etVehiclePlateName.getText().toString().trim();
                VehicleRegister vehicleRegister = new VehicleRegister();
                vehicleRegister.setVehicleSno(etVehicleSn.getText().toString());
                vehicleRegister.setVehiclePlateNum(vehcilePlaetNo);
                vehicleRegister.setVehiclePlateChar(plateChar);
                vehicleRegister.setVehicleType(carType);
                vehicleRegister.setCarModel(carModel);
                vehicleRegister.setVehicleYear(yearOfManuFacturing);
                vehicleRegister.setVehicleColor("");
                vehicleRegister.setPhotograph(photo);
                vehicleRegister.setVehicleReg(registration);
                vehicleRegister.setVehicleInsur(insurance);
                vehicleRegister.setAuthImg(auth);
                if (tafweeth != null && !tafweeth.equals("")) {
                    vehicleRegister.setTafweethImg(tafweeth);
                } else {
                    vehicleRegister.setTafweethImg("");
                }
                if (tafweethExp != null && !tafweethExp.equals("")) {
                    vehicleRegister.setTafweethExpire(tafweethExp);
                } else {
                    vehicleRegister.setTafweethExpire("");
                }

                String url = String.format(ServerConfigStage.VEHICLE_REGISTER());
                RetrofitTask task = new RetrofitTask<VehicleRegister>(FragmentVehicleRegistration.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.VEHICLE_REGISTRATION, url, getActivity(), vehicleRegister);
                task.execute();
            }
        }

    }

    private boolean doValidation() {
        if (etVehicleSn.getText() == null || etVehicleSn.getText().toString().equalsIgnoreCase("")) {
            tilVehicleSn.requestFocus();
            tilVehicleSn.setErrorEnabled(true);
            focusOnView(etVehicleSn);
            tilVehicleSn.setError(getString(R.string.vechile_serial));
            return false;
        } else if (etVehiclePlateName.getText() == null || etVehiclePlateName.getText().toString().equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.requestFocus();
            focusOnView(etVehiclePlateName);
            tilVehiclePlateName.setErrorEnabled(true);
            tilVehiclePlateName.setError(getString(R.string.plate_no));
            return false;
        } else if (etvechilenumber.getText() == null || etvechilenumber.getText().toString().equalsIgnoreCase("")) {
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            vechilenumber.requestFocus();
            focusOnView(etvechilenumber);
            vechilenumber.setErrorEnabled(true);
            vechilenumber.setError(getString(R.string.plate_no));
            return false;
        } else if (carModel == null || carModel.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(), getString(R.string.vehcile_model));
            return false;
        } else if (carType == null || carType.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(), getString(R.string.vechile_typ));
            return false;
        } else if (yearOfManuFacturing == null || yearOfManuFacturing.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(), getString(R.string.manufacture_type));
            return false;
        } else if (colorS == null || colorS.equals("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(), getString(R.string.vechile_color));
            return false;

        } /*else if (photo == null || photo.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            tilCarColor.clearFocus();
            tilCarColor.setErrorEnabled(false);
            alertError(getString(R.string.vehvile_photo));
            //   CommonUtil.showToast(getActivity(),getString(R.string.vehvile_photo));
            return false;
        } */else if (registration == null || registration.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            alertError(getString(R.string.vechile_resid_proof));
            // CommonUtil.showToast(getActivity(),getString(R.string.vechile_resid_proof));
            return false;
        } else if (insurance == null || insurance.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            alertError(getString(R.string.insurance_proof_vehcile));
            // CommonUtil.showToast(getActivity(),getString(R.string.insurance_proof_vehcile));
            return false;
        } else if (auth == null || auth.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            alertError(getString(R.string.auth_image));
            //CommonUtil.showToast(getActivity(),getString(R.string.auth_image));
            return false;
        }

        /*else if(etIbanno.getText()==null || etIbanno.getText().toString().equalsIgnoreCase("")){
            tilIbanno.requestFocus();
            tilIbanno.setErrorEnabled(true);
            tilIbanno.setError("*Enter vehicle iban number.");
            return false;
        }*/
     /*   else if(bank==null || bank.equalsIgnoreCase("")){
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            tilCarColor.clearFocus();
            tilCarColor.setErrorEnabled(false);
            tilIbanno.clearFocus();
            tilIbanno.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(),getString(R.string.bank_select));
            return false;
        }*/
        tilVehicleSn.clearFocus();
        tilVehicleSn.setErrorEnabled(false);
        tilVehiclePlateName.clearFocus();
        tilVehiclePlateName.setErrorEnabled(false);
        tilIbanno.clearFocus();
        tilIbanno.setErrorEnabled(false);
        return true;
    }

    public boolean deliveryValidate() {
        if (carModel == null || carModel.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(), getString(R.string.vehcile_model));
            return false;
        } else if (carType == null || carType.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(), getString(R.string.vechile_typ));
            return false;
        } else if (yearOfManuFacturing == null || yearOfManuFacturing.equalsIgnoreCase("")) {
            tilVehicleSn.clearFocus();
            tilVehicleSn.setErrorEnabled(false);
            tilVehiclePlateName.clearFocus();
            tilVehiclePlateName.setErrorEnabled(false);
            CommonUtil.showToast(getActivity(), getString(R.string.manufacture_type));
            return false;
        } else if (!driverstat) {
            photo = "";
            auth = "";
            insurance = "";
            registration = "";
            return true;
        }


        return true;

    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {

        if (_callerFunction == CommonUtility.CallerFunction.VEHICLE_REGISTRATION && response != null) {
            stopProgress();
            if (response.body() != null) {
                VehicleRegister vehicleRegister = (VehicleRegister) response.body();
                CommonUtil.showToast(context, vehicleRegister.getMessage());
                String driverId = SharedPreferenceManager.getDriverId(getActivity());
                Intent intent = new Intent(getActivity(), CarCategoryActivity.class);
                int i = SharedPreferenceManager.getvechileCount(getActivity());
                SharedPreferenceManager.setvechileCount(getActivity(), ++i);

                intent.putExtra("driverId", driverId);
                startActivity(intent);
                getActivity().finish();
            } else {
                CommonUtil.showAlertDialog(getActivity(), getString(R.string.error_message), isActivityFinished);
            }
        } else if (_callerFunction == CommonUtility.CallerFunction.GET_CAR_TYPE_LIST && response != null) {
            if (response.body() != null) {
                CarType carType = (CarType) response.body();
                if (carType.getVehicleList() != null && carType.getVehicleList().size() > 0) {
                    for (int i = 0; i < carType.getVehicleList().size(); i++) {
                        if (carType.getVehicleList().get(i).getList() != null && carType.getVehicleList().get(i).getList().size() > 0) {

                            for (int j = 0; j < carType.getVehicleList().get(i).getList().size(); j++) {
                                carType.getVehicleList().get(i).getList().get(j).setId(carType.getVehicleList().get(i).getList().get(j).getId());
                                carType.getVehicleList().get(i).getList().get(j).setType(carType.getVehicleList().get(i).getType());
                                carTypeListNew.add(carType.getVehicleList().get(i).getList().get(j));
                            }
                        } else {

                        }
                    }

                    setCarTypeSpinner(carTypeListNew);
                    callBankListApi();
                }
            } else {
                CommonUtil.showAlertDialog(getActivity(), getString(R.string.error_message), isActivityFinished);
            }
        } else if (_callerFunction == CommonUtility.CallerFunction.GET_BANK_LIST && response != null) {
            stopProgress();
            if (response.body() != null) {
                CityModel cittModel = (CityModel) response.body();
                setBankSpinner(cittModel.getData());
            } else {
                CommonUtil.showAlertDialog(getActivity(), getString(R.string.error_message), isActivityFinished);
            }
        }else if (_callerFunction == CommonUtility.CallerFunction.GET_COLOR_LIST && response !=null){
            stopProgress();
            if (response.body() != null){
                ColorList colorList = (ColorList) response.body();
                if (colorList.getStatus()>0){
                    setColorList(colorList.getColorlist());
                }else {
                    CommonUtil.showAlertDialog(getActivity(), getString(R.string.error_message), isActivityFinished);
                }
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
        CommonUtil.showAlertDialog(getActivity(), getString(R.string.error_message), isActivityFinished);
        // CommonUtil.showToast(getActivity(),"Please check your Intenet Connection.");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivPhotograph:
                photo = "";
                tvPhotograph.setText(getActivity().getResources().getString(R.string.photograph));
                ivPhotograph.setVisibility(View.GONE);
                break;
            case R.id.ivIdentity:
                identity = "";
                tvIdentity.setText(getActivity().getResources().getString(R.string.residency_identity));
                ivIdentity.setVisibility(View.GONE);
                break;
            case R.id.ivDLicense:
                driving = "";
                tvDLicense.setText(getActivity().getResources().getString(R.string.driving_licence));
                ivDLicense.setVisibility(View.GONE);
                break;
            case R.id.ivVehicleRegis:
                registration = "";
                tvVehicleRegis.setText(getActivity().getResources().getString(R.string.vechile_registration));
                ivVehicleRegis.setVisibility(View.GONE);
                break;
            case R.id.ivVehicleInsurence:
                insurance = "";
                tvVehicleInsurence.setText(getActivity().getResources().getString(R.string.insurance_of_vehicles));
                ivVehicleInsurence.setVisibility(View.GONE);
                break;
            case R.id.ivAuth:
                auth = "";
                tvAuth.setText(getActivity().getResources().getString(R.string.authorization_image));
                ivAuth.setVisibility(View.GONE);
                break;
            case R.id.ivTafWeeth:
                tafweeth = "";
                tvTafWeeth.setText(getActivity().getResources().getString(R.string.tafweeth));
                ivTafWeeth.setVisibility(View.GONE);
                break;
        }
    }

    public void alertError(String errorMessage) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.layout_custom_dialog, null);
        TextView alertTitle;
        Button okbtn;
        alertTitle = (TextView) alertLayout.findViewById(R.id.titleTv);
        okbtn = (Button) alertLayout.findViewById(R.id.okBtn);


        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setView(alertLayout);
        alertTitle.setText(errorMessage);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null && alertDialog.isShowing()) {
                    alertDialog.dismiss();
                    // alertDialog=null;

                }
            }
        });

        alertDialog.show();
    }

    private void focusOnView(final EditText editText) {
        srcollHolder.post(new Runnable() {
            @Override
            public void run() {
                srcollHolder.smoothScrollTo(0, editText.getBottom());
            }
        });
    }


}
