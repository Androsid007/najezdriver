package com.techconlabs.najezdriver.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techconlabs.najezdriver.R;


public class MyCardFragment extends Fragment {


    public MyCardFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.mycard_layout, container, false);
        init(view);
        return view;


    }

    private void init(View v) {
    }
}
