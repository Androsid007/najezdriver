package com.techconlabs.najezdriver.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.UpdateProfile;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment implements RetrofitTaskListener<UpdateProfile> {
    private EditText firstNameEt, lastNameEt, emailIdEt, mobileNumberEt, passwordEt, middleNameEt;
    private TextInputLayout frstNameIlay, lastNameIlay, emailIlay, numberIlay, passIlay, input_layout_middleName;
    private Button btnSignUp;
    private String username, photo, gender, latlng, referralcode, firstName, lastName, emailId, number, password, middlename;
    private ProgressDialog progressDialog;
    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;
    private TextView datePiker_tv;
    Uri selectedImage;
    LinearLayout frgmentholder;
    private ImageView profile_iv;
    private static final int SELECT_PHOTO = 100;
    private static final int CAMERA = 20;


    private String email, fName, mName, lName, mNumber, mPass, cityName;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        init(view);
        email = SharedPreferenceManager.getEmailId(getActivity());
        fName = SharedPreferenceManager.getFirstName(getActivity());
        mName = SharedPreferenceManager.getMiddleName(getActivity());
        lName = SharedPreferenceManager.getLastName(getActivity());
        mNumber = SharedPreferenceManager.getMobileNumber(getActivity());
        mPass = SharedPreferenceManager.getPassword(getActivity());
        cityName = SharedPreferenceManager.getCity(getActivity());
        //     datePiker_tv.setPaintFlags(datePiker_tv.getPaintFlags()|;);

        mobileNumberEt.setText(mNumber);
        emailIdEt.setText(email);
        firstNameEt.setText(fName);
        middleNameEt.setText(mName);
        lastNameEt.setText(lName);
        mobileNumberEt.setFocusable(false);
        emailIdEt.setFocusable(false);
        firstNameEt.setFocusable(false);
        middleNameEt.setFocusable(false);
        lastNameEt.setFocusable(false);


        if (SharedPreferenceManager.getDriverphoto(getActivity()) != null &&
                !SharedPreferenceManager.getDriverphoto(getActivity()).equals("")) {
           Picasso.with(getActivity()).load(SharedPreferenceManager.getDriverphoto(getActivity())).resize(1080, 720).into(profile_iv);
        } else {
        }


        passwordEt.setText(mPass);
        return view;

    }

    public void init(View view) {
        firstNameEt = (EditText) view.findViewById(R.id.firstNameEt);
        lastNameEt = (EditText) view.findViewById(R.id.lastNameEt);
        emailIdEt = (EditText) view.findViewById(R.id.emaiIdEt);
        mobileNumberEt = (EditText) view.findViewById(R.id.mobileNumberEt);
        passwordEt = (EditText) view.findViewById(R.id.passwordEt);
        btnSignUp = (Button) view.findViewById(R.id.buttonSignUp);
        frstNameIlay = (TextInputLayout) view.findViewById(R.id.input_layout_firstName);
        lastNameIlay = (TextInputLayout) view.findViewById(R.id.input_layout_lastName);
        emailIlay = (TextInputLayout) view.findViewById(R.id.input_layout_emailid);
        numberIlay = (TextInputLayout) view.findViewById(R.id.input_layout_number);
        passIlay = (TextInputLayout) view.findViewById(R.id.input_layout_password);
        profile_iv = (ImageView) view.findViewById(R.id.ivProfile);
        input_layout_middleName = (TextInputLayout) view.findViewById(R.id.input_layout_middleName);
        frgmentholder = (LinearLayout) view.findViewById(R.id.frgmentholder);

        middleNameEt = (EditText) view.findViewById(R.id.middleNameEt);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstName = firstNameEt.getText().toString();
                middlename = middleNameEt.getText().toString();
                emailId = emailIdEt.getText().toString();
                number = mobileNumberEt.getText().toString();
                password = passwordEt.getText().toString();
                lastName = lastNameEt.getText().toString();


                if (validate()) {
                    callSignUpService();
                }
            }
        });
        profile_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePickerDialog();

            }
        });
    }

    public boolean validate() {
        boolean valid = true;
        if (photo == null || photo.matches("")) {
            Toast.makeText(getContext(), R.string.choose_profile_toats, Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }

    public void callSignUpService() {
        showProgreass();
        String url = String.format(ServerConfigStage.DRIVER_ACCOUNT_UPDATE());
        RetrofitTask task = new RetrofitTask<UpdateProfile>(AccountFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DRIVER_PROFILE_UPDATE, url, getActivity());
        String token = SharedPreferenceManager.getToken(getActivity());
        task.executeDriverUpdate(token, emailId, firstName, middlename, lastName, gender, number, photo);

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

    @Override
    public void onRetrofitTaskComplete(retrofit2.Response<UpdateProfile> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body().getStatus() > 0) {
                UpdateProfile updateDriverData = response.body();
                //UpdateDriverData driverdata = updateDriverData.getDriverdata();
                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                SharedPreferenceManager.setFirstName(getActivity(), updateDriverData.getDriverdata().getFname());
                SharedPreferenceManager.setModelNum(getActivity(), updateDriverData.getDriverdata().getMobile());
                SharedPreferenceManager.setMiddleName(getActivity(), updateDriverData.getDriverdata().getMname());
                SharedPreferenceManager.setLastName(getActivity(), updateDriverData.getDriverdata().getLname());
                SharedPreferenceManager.setEmailId(getActivity(), updateDriverData.getDriverdata().getEmailid());
                SharedPreferenceManager.setDriverphoto(getActivity(), updateDriverData.getDriverdata().getPhoto());
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     /*familyArrayList = new ArrayList<>();

                mediumArrayList = new ArrayList<>();
                luxuryArrayList = new ArrayList<>();

                familyArrayList = response.body().getVehicleList().getFamily();
                mediumArrayList = response.body().getVehicleList().getMedium();
                luxuryArrayList = response.body().getVehicleList().getLuxury();

                vehicleList = new VehicleList();
                vehicleList = response.body().getVehicleList();
                Toast.makeText(SignUpActivity.this, "Registration Succesfull", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                //intent.putExtra("vehicleList", vehicleList);

                startActivity(intent);*/
            } else {
                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();

                    Bitmap yourSelectedImage = null;
                    try {
                        //decodeUri() Method Defined Below

                        yourSelectedImage = decodeUri(selectedImage);
                        photo = getByteArrayFromUri(selectedImage);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    profile_iv.setImageBitmap(yourSelectedImage);
                    SharedPreferenceManager.setDriverphoto(getActivity(), photo);

                }
                break;
            case CAMERA:
                if (resultCode == RESULT_OK) {
                    Bitmap bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    photo = getByteArrayFromBitmap(bitmap);
                    profile_iv.setImageBitmap(bitmap);
                    SharedPreferenceManager.setDriverphoto(getActivity(), photo);
                }
                break;
        }
    }

    //decodeUri() Method for decoding image for Out of Memory Exception
    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 140;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage), null, o2);

    }

    private String getByteArrayFromUri(Uri selectedImage) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
            Bitmap bitmap1 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 12, bitmap.getHeight() / 12, true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
            //Log.e("byte array", photo);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

    }

    private void choosePickerDialog() {

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getActivity());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from Gallery",
                "Capture photo from Camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    private String getByteArrayFromBitmap(Bitmap selectedImage) {
        try {

            Bitmap bitmap1 = Bitmap.createScaledBitmap(selectedImage, selectedImage.getWidth() / 12, selectedImage.getHeight() / 12, true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
            //Log.e("byte array", photo);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
