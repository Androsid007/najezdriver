package com.techconlabs.najezdriver.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.activity.AddMoneyActivity;
import com.techconlabs.najezdriver.activity.SendMoneyActvity;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Balance;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WalletFrgamnet extends Fragment implements RetrofitTaskListener<Balance> {
    private TabLayout tabLayout;
    private ViewPager walletPager;
    private TextView tv_senMoney, tv_addMoney, tv_walletMoney;
    private ProgressDialog progressDialog;

    private String totalBal = "";


    public WalletFrgamnet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wallet_frgamnet, container, false);
        init(view);
        walletPager = (ViewPager) view.findViewById(R.id.walletPager);
        setupViewPager(walletPager);
        walletPager.setClipToPadding(false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(walletPager);
        checkBalnce();
        tv_senMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SendMoneyActvity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        tv_addMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddMoneyActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });

        if (!totalBal.equals("") && totalBal != null) {
            totalBal = SharedPreferenceManager.getWalletBalance(getActivity());
           tv_walletMoney.setText(getString(R.string.sar)+"  "+ totalBal);
    }


        return view;
    }

    private void checkBalnce() {
        showProgreass();
        String url = String.format(ServerConfigStage.USER_CHECK_BALANCE());
        RetrofitTask task = new RetrofitTask<Balance>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CHECK_BALANCE, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeDriverBalance(session, SharedPreferenceManager.getDriverId(getActivity()));
    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }


    private void init(View view) {
        tv_senMoney = (TextView) view.findViewById(R.id.sendmoneyTv);
        tv_addMoney = (TextView) view.findViewById(R.id.addmoneyTv);
        tv_walletMoney = (TextView) view.findViewById(R.id.walletBalTv);

    }

    @Override
    public void onStart() {
        super.onStart();

        totalBal = "" + SharedPreferenceManager.getWalletBalance(getActivity());

         //  checkBalnce();

    }

    @Override
    public void onResume() {
        super.onResume();
        // checkBalnce();

        totalBal = "" + SharedPreferenceManager.getWalletBalance(getActivity());
        tv_walletMoney.setText(getString(R.string.sar)+"  "+ totalBal);
        setupViewPager(walletPager);
    }

    private void setupViewPager(ViewPager walletPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new AllWaletFrgament(), getString(R.string.history));
        adapter.addFragment(new CreditWalletFragment(), getString(R.string.ctredit));
        adapter.addFragment(new DebitWalletFragment(), getString(R.string.debit));

        walletPager.setAdapter(adapter);
    }

    @Override
    public void onRetrofitTaskComplete(Response<Balance> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    try {
                        if (response.body().getBalance() != null && !response.body().getBalance().equals("")) {
                            SharedPreferenceManager.setWalletBalance(getActivity(), response.body().getBalance());
                        } else {
                            SharedPreferenceManager.setWalletBalance(getActivity(), "0");

                        }
                    }
                    catch (Exception e)
                    {

                    }

                   // Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();

                } else {
                    //Toast.makeText(context, response.body().getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();


    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
