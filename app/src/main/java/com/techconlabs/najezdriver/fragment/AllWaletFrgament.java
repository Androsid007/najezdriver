package com.techconlabs.najezdriver.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.AllWalletAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Transaction;
import com.techconlabs.najezdriver.model.TransactionDetail;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllWaletFrgament extends Fragment implements RetrofitTaskListener<TransactionDetail> {
    private RecyclerView recyclerView;
    List<Transaction> transactions = new ArrayList<>();
    AllWalletAdapter walletAdapter;
    private ProgressDialog progressDialog;
    String totalBalance;
    String driver_id, transaction_id;
    TextView tv_nodata;

    public AllWaletFrgament() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_walet_frgament, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.allwalletRv);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        driver_id = SharedPreferenceManager.getDriverId(getActivity());
        tv_nodata = (TextView) view.findViewById(R.id.response_tv) ;
        callTransactionApi();

        return view;
    }

    private void callTransactionApi() {
        showProgreass();
        String url = String.format(ServerConfigStage.Transatction_Detail());
        RetrofitTask task = new RetrofitTask<TransactionDetail>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.TRASACTION_DETAIL, url, getActivity());
        String token = SharedPreferenceManager.getToken(getActivity());
        task.executeTransactionDetail(token, driver_id, "all");
    }

    @Override
    public void onResume() {
        super.onResume();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }


    @Override
    public void onRetrofitTaskComplete(Response<TransactionDetail> response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (response.isSuccessful()) {
            if (response.body() != null) {
                if (response.body().getStatus() > 0) {
                    totalBalance = response.body().getBalance();
                    try {
                        if (getActivity()!=null &&totalBalance != null && !totalBalance.equals("")) {
                            SharedPreferenceManager.setWalletBalance(getActivity(), totalBalance);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                    transactions = response.body().getTransactions();
                    tv_nodata.setVisibility(View.GONE);
                    if (transactions != null && transactions.size() > 0) {
                        walletAdapter = new AllWalletAdapter(getActivity(), transactions);
                        recyclerView.setAdapter(walletAdapter);

                    }
                } else {
                    tv_nodata.setVisibility(View.VISIBLE);
                    tv_nodata.setText(response.body().getMessage());
                }
            }
        } else {
           // Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();


    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (getActivity()!=null && progressDialog != null && progressDialog.isShowing())
             progressDialog.dismiss();
    }

}
