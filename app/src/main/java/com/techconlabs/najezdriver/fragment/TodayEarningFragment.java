package com.techconlabs.najezdriver.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.adapter.TripAdapter;
import com.techconlabs.najezdriver.model.TodayTrip;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TodayEarningFragment extends Fragment {

    private ArrayList<TodayTrip> todayTripArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TripAdapter mAdapter;


    public TodayEarningFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today_earning, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.triprecyclerview);

        mAdapter = new TripAdapter(todayTripArrayList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareTripData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter = new TripAdapter(todayTripArrayList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareTripData();
    }

    private void prepareTripData() {
        TodayTrip todayTrip = new TodayTrip("Rahul Kumar", "9:00 PM", "AED 30.50", "Cash");
        todayTripArrayList.add(todayTrip);

        todayTrip = new TodayTrip("Praveen Kumar", "8:45 PM", "AED 20.10", "Credit Card");
        todayTripArrayList.add(todayTrip);

        todayTrip = new TodayTrip("Kisan", "7:30 PM", "AED 32.50", "Credit Card");
        todayTripArrayList.add(todayTrip);

        todayTrip = new TodayTrip("Vishwajeet", "5:00 PM", "AED 3.50", "Cash");
        todayTripArrayList.add(todayTrip);

        todayTrip = new TodayTrip("Siddharth", "3:00 PM", "AED 23", "Wallet");
        todayTripArrayList.add(todayTrip);

        todayTrip = new TodayTrip("Shafiq Anjum", "1:00 PM", "AED 39.50", "Cash");
        todayTripArrayList.add(todayTrip);

        todayTrip = new TodayTrip("Kapil", "11:00 AM", "AED 30.50", "Credit Card");
        todayTripArrayList.add(todayTrip);


        mAdapter.notifyDataSetChanged();
    }

}
