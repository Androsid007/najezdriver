package com.techconlabs.najezdriver.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.model.Accept;
import com.techconlabs.najezdriver.model.GetData;
import com.techconlabs.najezdriver.model.RideValue;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Response;

/**
 * Created by X-cellent Technology on 14-10-2017.
 */

public class DriverAcceptTripFragment extends Fragment implements RetrofitTaskListener {
    LatLng mLastKnowLatLng;
    private MediaPlayer mp;
    GetData rideInformation;
    ProgressDialog progressDialog;
    View layout;
    private String carImage, familyId, vechileId;
    private GoogleMap mGoogleMap;
    TextView userratingTv,driverType;
    TextView clientnameTv;
    Button acceptBtn;
    RelativeLayout acceptTrip;
    private SupportMapFragment mMapFragment;
    //    TextView timTextView;
    Timer timer;
    private RatingBar ratingBar;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragmentdriveraccept, container, false);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                declineRide("0");
            }
        }, 10000);
        ratingBar = (RatingBar) layout.findViewById(R.id.ratingBar);
        Drawable progress = ratingBar.getProgressDrawable();
        DrawableCompat.setTint(progress, getActivity().getResources().getColor(R.color.fab_color));

        mMapFragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.DriverContainer, mMapFragment).commit();
        familyId = SharedPreferenceManager.getfamilyId(getActivity());
        vechileId = SharedPreferenceManager.getVechileId(getActivity());
        if(getArguments()!=null) {
            Bundle args = getArguments();
            rideInformation = (GetData) args.getSerializable("data");



        }

        if (rideInformation!=null&&rideInformation.getRideData()!=null && rideInformation.getRideData().getUserdata().getRating() != null && !rideInformation.getRideData().getUserdata().getRating().equals("")) {
            ratingBar.setRating(Float.parseFloat(rideInformation.getRideData().getUserdata().getRating()));
        } else {
            ratingBar.setRating((float) 0.0);
        }
        mp = MediaPlayer.create(getActivity(), R.raw.newnotification);
        mp.start();
        mp.setLooping(true);
        acceptBtn = (Button) layout.findViewById(R.id.actButton);
        acceptTrip = (RelativeLayout) layout.findViewById(R.id.acceptTrip);
        userratingTv = (TextView) layout.findViewById(R.id.userratingTextView);
        driverType = (TextView) layout.findViewById(R.id.driverType);
        RideValue rideData = rideInformation.getRideData();
        String picklat = rideData.getPickupLat();
        String picklng = rideData.getPickupLng();
        mLastKnowLatLng = new LatLng(Double.parseDouble(picklat), Double.parseDouble(picklng));
        clientnameTv = (TextView) layout.findViewById(R.id.clientnameTextView);
        clientnameTv.setText(rideInformation.getRideData().getUserdata().getFname());
        acceptTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.setOnRdieStatus(getActivity(), true);
                mp.stop();
                timer.cancel();
                acceptRide("1");

            }
        });
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.setOnRdieStatus(getActivity(), true);
                mp.stop();
                timer.cancel();
                acceptRide("1");

            }
        });

        if(rideInformation!=null && rideInformation.getRideData()!=null && rideInformation.getRideData().getIsDelivery()==1)
        {
            driverType.setText(R.string.delivery_option);
        }

        initializMap();
        return layout;
    }


    private void acceptRide(String status) {
        mp.stop();
       /* Runnable runnable = new Runnable() {
            public void run () {*/
        showProgreass();
        // Do your stuff here  -- show your progress bar
            /*}
        };
        Handler handler = new Handler();
        handler.postDelayed(runnable, 10000);*/
        String url = String.format(ServerConfigStage.ACCEPT_RIDE());
        RetrofitTask task = new RetrofitTask<Accept>(DriverAcceptTripFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ACCEPT_RIDE, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeDriverRideAccept(session, SharedPreferenceManager.getDriverId(getActivity()), familyId, SharedPreferenceManager.getRideId(getActivity()), vechileId, status);
    }

    private void declineRide(String status) {
       try {
           mp.stop();
       }catch (Exception e)
       {

       }
        //    showProgreass();
        String url = String.format(ServerConfigStage.ACCEPT_RIDE());
        RetrofitTask task = new RetrofitTask<Accept>(DriverAcceptTripFragment.this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.DECLINE_RIDE, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeDriverRideAccept(session, SharedPreferenceManager.getDriverId(getActivity()), familyId, vechileId, SharedPreferenceManager.getRideId(getActivity()), status);
    }


    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }


    @Override
    public void onRetrofitTaskComplete(final Response response, Context context, CommonUtility.CallerFunction _callerFunction) {

        if (_callerFunction == CommonUtility.CallerFunction.ACCEPT_RIDE && response != null) {
            final Accept accept = (Accept) response.body();
            Runnable progressRunnable = new Runnable() {
                @Override
                public void run() {
                    stopProgress();
                    if (response.isSuccessful()) {
                        if (accept.getStatus() > 0) {
                            SharedPreferenceManager.setrideStatus(getActivity(), 1);
                            confirmarrivalpopup(rideInformation);
                        } else {
                        }
                    }
                }

            };

            Handler pdCanceller = new Handler();
            pdCanceller.postDelayed(progressRunnable, 15000);


        }
        if (_callerFunction == CommonUtility.CallerFunction.DECLINE_RIDE && response != null) {
            stopProgress();
            Accept declineRide = (Accept) response.body();
            if (response.isSuccessful()) {
                if (declineRide.getStatus() > 0) {
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                }

            } else {

            }
        }


    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();
    }


    private void confirmarrivalpopup(final GetData rideInformation) {
        try {
            Bundle bundle = new Bundle();
            bundle.putSerializable("data", rideInformation);
            StartTripFragment startTripFragment = new StartTripFragment();
            startTripFragment.setArguments(bundle);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, startTripFragment).commit();
        }
        catch(Exception e){

        }
    }

    private void initializMap() {
        final View mapView = mMapFragment.getView();
        mMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                initializeMap(mapView, googleMap);
                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    getActivity(), R.raw.style_json));

                    if (!success) {
                        Log.e("", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("", "Can't find style. Error: ", e);
                }
            }
        });
    }

    private void initializeMap(View mapView, GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        if (mLastKnowLatLng != null) {
            googleMap.addMarker(new MarkerOptions().position(mLastKnowLatLng)
                    .title("Pickup Location"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(mLastKnowLatLng));
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mGoogleMap.setMyLocationEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(mLastKnowLatLng).zoom(14.0f).build();
            mGoogleMap.moveCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            //      setCameraPosition(mLastKnowLatLng);
        }
    }

    private void setCameraPosition(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng).zoom(14.0f).build();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
            /*mGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));*/
        mGoogleMap.moveCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        // Clears all the existing markers
        mGoogleMap.clear();
        mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
    }
}