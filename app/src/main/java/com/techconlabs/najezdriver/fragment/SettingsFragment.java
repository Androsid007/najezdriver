package com.techconlabs.najezdriver.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.activity.ChangePassword;
import com.techconlabs.najezdriver.activity.HomeActivity;
import com.techconlabs.najezdriver.adapter.LongCityListAdapter;
import com.techconlabs.najezdriver.adapter.StringSpinnerAdapter;
import com.techconlabs.najezdriver.api.ServerConfigStage;
import com.techconlabs.najezdriver.common.CommonUtility;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;
import com.techconlabs.najezdriver.customViews.CustomToggle;
import com.techconlabs.najezdriver.interfaces.OnCheckedListner;
import com.techconlabs.najezdriver.model.CityModel;
import com.techconlabs.najezdriver.model.Data;
import com.techconlabs.najezdriver.model.LongCityList;
import com.techconlabs.najezdriver.model.LongCityModel;
import com.techconlabs.najezdriver.model.Offline;
import com.techconlabs.najezdriver.model.SelectedCityList;
import com.techconlabs.najezdriver.networkservice.RetrofitTask;
import com.techconlabs.najezdriver.networkservice.RetrofitTaskListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements RetrofitTaskListener,OnCheckedListner {
    private CustomToggle notification_switich;
    private RadioButton englishRadio_btn, arabic_radibtn;
    private LinearLayout changeLayout;
    private LinearLayout tohomeBtn,setLongtrip;
    Button doneBtn;
    Spinner spinner;
    private Data data;
    private List<Data> cityList;
    private ProgressDialog progressDialog;
    String cityListString = null;
    List<Address> addresses;
    List<LatLng> savesLatlng;
    double latitude;
    double longitude;
    BottomSheetDialog bottomSheetDialog,logcityDialog;
     RecyclerView recyclerView;
    TextView btn_done,btn_cancel;
    List<LongCityList> longCityListList;
    List<SelectedCityList> longCitySelectedListList;
    LongCityListAdapter adapter;
    String longcityId ="";
    List<LongCityList> longCityListList_filtered;
    String  languageType = "en";


    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        init(view);
         onClick();
        if(SharedPreferenceManager.getLanguage(getActivity()).equals("ar"))
        {
            englishRadio_btn.setChecked(false);
            arabic_radibtn.setChecked(true);


        }
        else{
            englishRadio_btn.setChecked(true);
            arabic_radibtn.setChecked(false);

        }



        return view;
    }


    private void init(View v) {
        notification_switich = (CustomToggle) v.findViewById(R.id.notification_switich);
        englishRadio_btn = (RadioButton) v.findViewById(R.id.english_rbt);
        arabic_radibtn = (RadioButton) v.findViewById(R.id.arabic_rtn);
        changeLayout = (LinearLayout) v.findViewById(R.id.changeLayout);
        tohomeBtn = (LinearLayout) v.findViewById(R.id.tohomeBtn);
        setLongtrip = (LinearLayout) v.findViewById(R.id.setLongtrip);



    }
    public void calladdLongCity(){
        longCityListList_filtered = getSizeFilteredLsit(longCityListList);
   if (longCityListList_filtered != null && longCityListList_filtered.size() > 0) {
            for (int i = 0; i < longCityListList_filtered.size(); i++) {
                if (i == 0) {
                    longcityId = longCityListList_filtered.get(i).getId() + "";
                } else {
                    longcityId = longcityId + "," + longCityListList_filtered.get(i).getId();
                }
            }
        }
        if(longcityId!=null && !longcityId.equals("")) {
            showProgreass();

            String url = String.format(ServerConfigStage.ADD_lONG_CITY());
            RetrofitTask task = new RetrofitTask<Offline>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.ADD_lONG_CITY, url, getActivity());
            String token = SharedPreferenceManager.getToken(getActivity());
            task.executeAddLongCity(token, longcityId);
        }
        else{

            Toast.makeText(getActivity(), "Please Select City", Toast.LENGTH_LONG).show();
        }
    }

    private List<LongCityList> getSizeFilteredLsit(List<LongCityList> longCityListList) {
        ArrayList<LongCityList> lists = new ArrayList<>();
        if (longCityListList != null) {
            for (int i = 0; i < longCityListList.size(); i++) {
                if (longCityListList.get(i).isSelceted()) {
                    lists.add(longCityListList.get(i));
                }
            }
        }
        return lists;
    }

    private void onClick() {
      englishRadio_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (englishRadio_btn.isChecked()) {
                    englishRadio_btn.setChecked(true);
                    arabic_radibtn.setChecked(false);
                    languageType = "en";
                    SharedPreferenceManager.setLanguage(getActivity(), languageType);
                    Locale locale = new Locale("en");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                    SharedPreferenceManager.setLanguage(getActivity(),languageType);
                    setLanguageType();
                }
            }
        });
        arabic_radibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (arabic_radibtn.isChecked()) {
                    arabic_radibtn.setChecked(true);
                    englishRadio_btn.setChecked(false);
                    languageType = "ar";
                    Locale locale = new Locale("ar");
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics());
                    SharedPreferenceManager.setLanguage(getActivity(),languageType);
                    setLanguageType();
             }
            }
        });

        notification_switich.setOnCheckedChangeListner(new CustomToggle.OnCheckedChangeListner() {
            @Override
            public void onChecked(boolean isChecked) {
                if (isChecked) {
                    Toast.makeText(getActivity(), R.string.checked, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), R.string.un_checked, Toast.LENGTH_LONG).show();
                }
            }
        });
        changeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ChangePassword.class));
            }
        });
        tohomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup();
            }
        });

        // notification_switich.setOnCheckedChangeListener(this);
        setLongtrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                longCitypopup();
            }
        });
    }

    private void longCitypopup() {
        callLongCity();
        logcityDialog = new BottomSheetDialog(getActivity());
        View cityView = getActivity().getLayoutInflater().inflate(R.layout.long_city_trip, null);
        logcityDialog.setContentView(cityView);
        logcityDialog.show();
        btn_done = (TextView) cityView.findViewById(R.id.addbutton);
        btn_cancel = (TextView) cityView.findViewById(R.id.cancelButton);
        recyclerView = (RecyclerView) cityView.findViewById(R.id.longcityRv);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calladdLongCity();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logcityDialog.dismiss();
            }
        });
    }

    private void callLongCity() {
        showProgreass();
        String url = String.format(ServerConfigStage.GET_LONG_CITY_LIST(), SharedPreferenceManager.getLanguage(getActivity()));
        RetrofitTask task = new RetrofitTask<LongCityModel>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.GET_LONG_CITY_LIST, url, getActivity());
        String token =  SharedPreferenceManager.getToken(getActivity());
        task.executeLongCity(token);
    }

    private void popup() {
        callCityList();
        bottomSheetDialog = new BottomSheetDialog(getActivity());
        View popupView = getActivity().getLayoutInflater().inflate(R.layout.select_city_home, null);
        bottomSheetDialog.setContentView(popupView);
        bottomSheetDialog.show();

        doneBtn = (Button) popupView.findViewById(R.id.doneBtn);
        spinner = (Spinner) popupView.findViewById(R.id.city_spinner);
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    // getlatlng(data.getCityNameEn());
                   if(data!=null&&data.getCityNameEn()!=null)
                    {
                        callLastLocation();
                    }
                }
            }
        });
    }


    public void callCityList() {
        showProgreass();
        String url = String.format(ServerConfigStage.CITY_LIST(), SharedPreferenceManager.getLanguage(getActivity()));
        RetrofitTask task = new RetrofitTask<CityModel>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.CITY_LIST, url, getActivity());
        task.execute();

    }

    public void callLastLocation() {
        showProgreass();
        String url = String.format(ServerConfigStage.LAST_LOCATION());
        RetrofitTask task = new RetrofitTask<Offline>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LAST_LOCATION, url, getActivity());
        String token = SharedPreferenceManager.getToken(getActivity());
        task.executeLastLoc(token, data.getCityNameEn());

    }

    private void setLanguageType() {
        showProgreass();
        String url = String.format(ServerConfigStage.GET_OPTION_LANGUAGE());
        RetrofitTask task = new RetrofitTask<Offline>(this, CommonUtility.HTTP_REQUEST_TYPE.POST, CommonUtility.CallerFunction.LANG_TYPE, url, getActivity());
        String session = SharedPreferenceManager.getToken(getActivity());
        task.executeDriverLangType(session, SharedPreferenceManager.getLanguage(getActivity()), "driver");
    }


    public boolean validate() {
        if (cityListString == null || cityListString.equalsIgnoreCase("") || cityListString.equals("")) {
            Toast.makeText(getActivity(), R.string.selcet_city, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    public void onRetrofitTaskComplete(Response response, Context context, CommonUtility.CallerFunction _callerFunction) {
        stopProgress();
        if (_callerFunction == CommonUtility.CallerFunction.CITY_LIST) {
            if (response.isSuccessful()) {
                CityModel cityModel = (CityModel) response.body();
                if (cityModel.getStatus() > 0) {
                    List<String> stringList = new ArrayList<>();
                    cityList = new ArrayList<>();
                    for (int i = 0; i < cityModel.getData().size(); i++) {
                        if (cityModel.getData().get(i).getCityNameEn() != null) {
                            stringList.add(cityModel.getData().get(i).getCityNameEn());
                            cityList.add(cityModel.getData().get(i));
                        } else {
                            cityModel.getData().remove(i);
                        }
                    }
                    Data dataSelect = new Data();
                    dataSelect.setCityNameEn(getString(R.string.pls_select_city));
                    stringList.add(0, dataSelect.getCityNameEn());
                    cityList.add(0, dataSelect);
                    StringSpinnerAdapter adapterCarType = new StringSpinnerAdapter(getActivity(), R.layout.string_spinner_layout, R.id.txt, (ArrayList<String>) stringList);
                    spinner.setAdapter(adapterCarType);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if (position == 0) {
                                data = null;
                            } else {
                                data = cityList.get(position);
                                cityListString = String.valueOf(data);

                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            data = null;
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), cityModel.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
            //city_spinner.
        }else if (_callerFunction == CommonUtility.CallerFunction.LAST_LOCATION) {
            if (response.isSuccessful()) {
                Offline offline = (Offline) response.body();
                if (offline.getStatus() > 0) {
                    Toast.makeText(getActivity(), String.valueOf(offline.getMessage()), Toast.LENGTH_LONG).show();
                    bottomSheetDialog.dismiss();
                } else {
                    Toast.makeText(getActivity(), String.valueOf(offline.getMessage()), Toast.LENGTH_LONG).show();
                    bottomSheetDialog.dismiss();
                }
            } else {
                Toast.makeText(getActivity(), "Technical Issue", Toast.LENGTH_LONG).show();
                bottomSheetDialog.dismiss();
            }

        } else if (_callerFunction == CommonUtility.CallerFunction.LANG_TYPE) {
            if (response.isSuccessful()) {
                Offline language = (Offline) response.body();
                if (language.getStatus() > 0) {
                    startActivity(new Intent(getActivity(),HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    getActivity().finish();
                    //Toast.makeText(getActivity(), language.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), language.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }else if (_callerFunction == CommonUtility.CallerFunction.GET_LONG_CITY_LIST){
            if (response.isSuccessful()){

                LongCityModel cityList = (LongCityModel) response.body();

                if (cityList.getStatus()>0){

                    longCityListList = cityList.getCityList();
                    longCitySelectedListList= cityList.getSelectedCityList();
                    if(longCitySelectedListList.size()>0)
                    {

                        for(int j=0;j<longCitySelectedListList.size();j++)
                        {
                      for(int i =0;i<longCityListList.size();i++)

                    {
                        if(longCitySelectedListList.get(j).getCityid().equals(longCityListList.get(i).getId())){
                                    longCityListList.get(i).setSelceted(true);
                                }
                            }}

                    }
                    adapter =  new LongCityListAdapter(cityList.getCityList(),getActivity(),this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }

            }
        }else if (_callerFunction == CommonUtility.CallerFunction.ADD_lONG_CITY){
            if (response.isSuccessful()){
                Offline addcity = (Offline) response.body();
                if (addcity.getStatus()>0){
                    Toast.makeText(getActivity(),addcity.getMessage(),Toast.LENGTH_LONG).show();
                    logcityDialog.dismiss();
                }else {
                    Toast.makeText(getActivity(),addcity.getMessage(),Toast.LENGTH_LONG).show();
                    logcityDialog.dismiss();
                }
            }
        }
    }

    @Override
    public void onRetrofitTaskFailure(Throwable t) {
        stopProgress();

    }

    public void showProgreass() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.plz_wait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void stopProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void getlatlng(String citylist) {
        if (Geocoder.isPresent()) {
            try {
                String location = citylist;
                Geocoder gc = new Geocoder(getActivity());
                addresses = gc.getFromLocationName(location, 5); // get the found Address Objects
                savesLatlng = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available
                for (Address a : addresses) {
                    if (a.hasLatitude() && a.hasLongitude()) {
                        savesLatlng.add(new LatLng(a.getLatitude(), a.getLongitude()));
                        latitude = savesLatlng.get(0).latitude;
                        longitude = savesLatlng.get(0).longitude;
                        if(data!=null&&data.getCityNameEn()!=null) {

                            callLastLocation();
                        }
                    }
                }
            } catch (IOException e) {
            }
            Toast.makeText(getActivity(), String.valueOf(latitude) + " ," + String.valueOf(longitude), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void OnChage(int position, boolean change) {
        longCityListList.get(position).setSelceted(change);

    }


}