package com.techconlabs.najezdriver.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techconlabs.najezdriver.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CollectCashFragment extends Fragment {


    public CollectCashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_collect_cash, container, false);
        init(view);


        return view;
    }

    private void init(View v) {

    }

}
