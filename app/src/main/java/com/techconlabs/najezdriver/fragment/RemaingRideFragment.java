package com.techconlabs.najezdriver.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.techconlabs.najezdriver.R;
import com.techconlabs.najezdriver.common.SharedPreferenceManager;

/**
 * Created by hp1 on 8/13/2017.
 */

public class RemaingRideFragment extends Fragment {
    private Button purchaseRide, entercoupnbtn;
    private TextView ride_bal_tv;

    public RemaingRideFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ride_balance_layout, container, false);

        init(view);

        purchaseRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.flContent, new RateCardFragment()).commit();

            }
        });

        return view;

    }

    public void init(View view)

    {
        ride_bal_tv = (TextView) view.findViewById(R.id.ride_bal_tv);
        purchaseRide = (Button) view.findViewById(R.id.purchaseRide);

        ride_bal_tv.setText(getString(R.string.ride_bal) + " " + SharedPreferenceManager.gettotalRide(getActivity()));


    }

}
