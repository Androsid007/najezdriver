package com.techconlabs.najezdriver.common;

/**
 * Created by Dell on 8/17/2017.
 */

public class Constants {

    public static final int PHOTO_REQUEST_CODE =121;
    public static final int IDENTITY_REQUEST_CODE =122;
    public static final int DRIVING_REQUEST_CODE =123;
    public static final int REGISTRATION_REQUEST_CODE =124;
    public static final int INSURENCE_REQUEST_CODE =125;
    public static final int AUTH_REQUEST_CODE =126;
    public static final int TAF_REQUEST_CODE =127;
    public static final int PROFILE_PICTURE_CODE =128;
}
