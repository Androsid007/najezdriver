package com.techconlabs.najezdriver.common;

import android.content.Context;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techconlabs.najezdriver.model.OptVehicleFamily;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp1 on 6/19/2017.
 */

public class SharedPreferenceManager {

    public static void setReferalCode(Context context, String refeCode){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("refeCode", refeCode).commit();
    }
    public static String getReferalCode(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("refeCode", "");
    }
    public static void setEmailId(Context context, String eMail){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("eMail", eMail).commit();
    }
    public static String getEmailId(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("eMail", "");
    }
    public static void setFirstName(Context context, String fName){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("fName", fName).commit();
    }
    public static String getFirstName(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("fName", "");
    }
    public static void setMiddleName(Context context, String mName){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("mName", mName).commit();
    }
    public static String getMiddleName(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("mName", "");
    }
    public static void setLastName(Context context, String lName){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("lName", lName).commit();
    }
    public static String getLastName(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("lName", "");
    }
    public static void setToken(Context context, String token){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("token", token).commit();
    }
    public static String getToken(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("token", "");
    }
    public static void setvechileCount(Context context, int vechileCount){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("vechileCount", vechileCount).commit();
    }
    public static int getvechileCount(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("vechileCount",0);
    }
    public static void setDriverId(Context context, String driverId){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("driverId", driverId).commit();
    }
    public static String getDriverId(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("driverId", "");
    }
    public static void setDriverRating(Context context, float driverRating){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putFloat("driverRating", driverRating).commit();
    }
    public static float getDriverRating(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat("driverRating",0);
    }


    public static void setCity(Context context, String cityNameEn) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("city", cityNameEn).commit();
    }
    public static String getCity(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("city", "");
    }
    public static void setMobileNumber(Context context, String mobileNumber){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("mobileNumber", mobileNumber).commit();
    }
    public static String getMobileNumber(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("mobileNumber", "");
    }

    public static void setPassword(Context context, String password) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("password", password).commit();
    }
    public static String getPassword(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("password", "");
    }

    public static void setBalance(Context context, int balance){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("balance", balance).commit();
    }
    public static int getBalance(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("balance",0);
    }
    public static void settotalRide(Context context, int ridebal){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("ridebal", ridebal).commit();
    }
    public static int gettotalRide(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("ridebal",0);
    }
    public static void setDeviceToken(Context context, String deviceToken) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("deviceToken", deviceToken).commit();
    }
    public static String getDeviceToken(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("deviceToken", "");
    }
    public static void setfamilyId(Context context, String familyId){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("familyId", familyId).commit();
    }
    public static String getfamilyId(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("familyId", "");
    }
    public static void setModelName(Context context, String modelName){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("modelName", modelName).commit();
    }
    public static String getModelName(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("modelName", "");
    }
    public static void setModelNum(Context context, String modelNum){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("modelNum", modelNum).commit();
    }
    public static String getModelNum(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("modelNum", "");
    }
    public static void setModelYear(Context context, String modelYear){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("modelYear", modelYear).commit();
    }
    public static String getModelYear(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("modelYear", "");
    }
    public static void setSerialNum(Context context, String serialNum){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("serialNum", serialNum).commit();
    }
    public static String getSerialNum(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("serialNum", "");
    }
    public static void setColorCode(Context context, String colorCode){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("colorCode", colorCode).commit();
    }
    public static String getColorCode(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("colorCode", "");
    }
    public static void setVechileId(Context context, String vechileId){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("vechileId", vechileId).commit();
    }
    public static String getVechileId(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("vechileId", "");
    }
    public static void setCarImage(Context context, String carImage){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("carImage", carImage).commit();
    }
    public static String getCarImage(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("carImage", "");
    }
    public static void setOnLineStatus(Context context, Boolean offlineStatus){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("offlineStatus", offlineStatus).commit();
    }
    public static boolean getOnLineStatus(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("offlineStatus", false);
    }
    public static void setRideId(Context context, String rideId){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("rideId", rideId).commit();
    }
    public static String getRideId(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("rideId", "");
    }

    public static void setOnRdieStatus(Context context, Boolean onRide){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("onRide", onRide).commit();
    }
    public static boolean getOnRideStatus(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("onRide", false);
    }
    public static void setVechilePosition(Context context, int vechilePosition){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("vechilePosition", vechilePosition).commit();
    }
    public static int getVechilePosition(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("vechilePosition",0);
    }

    public static void setWalletBalance(Context context, String walletbalance){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("walletbalance", walletbalance).commit();
    }
    public static String getWalletBalance(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("walletbalance","");
    }




    public static void setOnLineStatusModelNumber(Context context, String modelNum){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("onlineModelNum", modelNum).commit();
    }
    public static String getOnLineStatusModelNumber(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("onlineModelNum", "");
    }
    public static void setVechileMediumstatus(Context context, String mediumStat){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("mediumStat", mediumStat).commit();
    }
    public static String getVechileMediumstatus(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("mediumStat", "");
    }
    public static void setVechileFamilyStatus(Context context, String familyStaus){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("familyStaus", familyStaus).commit();
    }
    public static String getVechileFamilyStatus(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("familyStaus", "");
    }
    public static void setVechileList(Context context, List<OptVehicleFamily> vehcileList){
        Gson gson= new Gson();
        String data=gson.toJson(vehcileList);

        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("vehcileList", data).commit();
    }
   /* public static ArrayList<OptVehicleFamily> getVechileList_new(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)/
    }*/
    public static List<OptVehicleFamily> getVechileList(Context context) {
        if (PreferenceManager.getDefaultSharedPreferences(context).getString("vehcileList", "") != null) {
            String myData = PreferenceManager.getDefaultSharedPreferences(context).getString("vehcileList", "");
            Gson gson = new Gson();
            List<OptVehicleFamily> productFromShared = new ArrayList<>();
//        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
//        String jsonPreferences = sharedPref.getString(PRODUCT_TAG, "");
            Type type = new TypeToken<List<OptVehicleFamily>>() {
            }.getType();
            productFromShared = gson.fromJson(myData, type);

            return productFromShared;
        }
        return  null;
    }


    public static void setWrblockOnline(Context context, String webLock){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("webLock", webLock).commit();
    }
    public static String getWrblockOnline(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("webLock", "");
    }
    public static void setCanDeliver(Context context, String canDeliver){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("canDeliver", canDeliver).commit();
    }
    public static String getCanDeliver(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("canDeliver", "");
    }
    public static void setActiveDiver(Context context, String activeDriver){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("activeDriver", activeDriver).commit();
    }
    public static String getActiveDiver(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("activeDriver", "");
    }


    public static void setDriverOnline(Context context, int driverOnline){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("activeDriver", driverOnline).commit();
    }
    public static int getDriverOnline(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("driverOnline", 0);
    }

    public static void setDriverDeliveryStat(Context context, Boolean sriverDelivery){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("sriverDelivery", sriverDelivery).commit();
    }
    public static boolean getDriverDeliveryStat(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("sriverDelivery", true);
    }

    public static void setDriverphoto(Context context, String photo){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("photo", photo).commit();
    }
    public static String getDriverphoto(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("photo", "");
    }

    public static void setLanguage(Context context, String language){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("language", language).commit();
    }
    public static String getLanguage(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("language", "en");
    }

    public static void setdetailFlag(Context context, Boolean detailFlag){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("detailFlag", detailFlag).commit();
    }
    public static boolean getdetailFlag(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("detailFlag", true);
    }

    public static void setLoggin(Context context, Boolean loginStatus){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("loginStatus", loginStatus).commit();
    }
    public static boolean getLoggin(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("loginStatus", false);
    }
    public static void setCarListSize(Context context, int carSize){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("carSize", carSize).commit();
    }
    public static int getCarListSize(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("carSize",0);
    }

    public static void setCanDriverDeliver(Context context, String candeliver){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("candeliver", candeliver).commit();
    }
    public static String getCanDriverDeliver(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("candeliver","");
    }

    public static void setrideStatus(Context context, int rideStaus){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("rideStaus", rideStaus).commit();
    }
    public static int getrideStatus(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("rideStaus",0);
    }

    public static void setVechileIdNumber(Context context, String vecNo){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("vecNo", vecNo).commit();
    }
    public static String getVechileIdNumber(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("vecNo", "");
    }

    public static void setwalletUpdate(Context context, Boolean walletSubmit){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("walletSubmit", walletSubmit).commit();
    }
    public static boolean getwalletUpdate(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("walletSubmit", true);
    }


    public static void setChatId(Context context, String chatId){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("chatId", chatId).commit();
    }
    public static String getChatId(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("chatId","");
    }


    public static void setCollectedCash(Context context, String collectedCash){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("collectedCash", collectedCash).commit();
    }
    public static String getCollectedCash(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context).getString("collectedCash","");
    }


}
