package com.techconlabs.najezdriver.common;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.widget.Toast;

/**
 * Created by Dell on 8/9/2017.
 */

public class CommonUtil {
public static void replaceFragment(Context context, int containerId, Fragment fragment){
        Activity activity = (Activity) context;
        FragmentManager fragmentManager = activity.getFragmentManager();
        fragmentManager.beginTransaction().replace(containerId,fragment).commit();
    }

    public static void showToast(Context context, String s) {
        Toast.makeText(context,s,Toast.LENGTH_LONG).show();
    }

    public static void showAlertDialog(Activity activity,String message,boolean isActivityFinished){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(activity);
        }
        builder.setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }

}
