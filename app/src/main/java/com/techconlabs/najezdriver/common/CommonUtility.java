package com.techconlabs.najezdriver.common;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class CommonUtility {
    public enum CallerFunction {
        DRIVER_REGISTER,DRIVER_LOGIN,GET_FORGET_PASSWORD,GET_UPDATE_PASSWORD,CAR_LISTING,ABOUT_US,HELP_SUPPORT,RATE_CARD,VEHICLE_REGISTRATION,CITY_LIST,GET_CAR_TYPE_LIST,GET_BANK_LIST,GET_OTP_SEND,OTP_VERIFY_TO_SERVER,ACCEPT_RIDE,OFF_ONLINE,VOUCHER_APPLY,SUPPORT_TYPE,DEVICE_TOKEN,DRIVER_START_TRIP,HISTORY_TRIP_API,CANCEL_OPTION,CANCEL_RIDE,ADD_NEW_CARD,CONFIRM_ARRIVAL,DRIVER_END_TRIP,GET_RIDE_DATA,DECLINE_RIDE,DRIVER_RATING_REVIEW,CHECK_BALANCE,ADD_BALANCE,SEND_BALANCE,TRASACTION_DETAIL,OFF_VOUCHER,DELIVERY_MEN_ONLINE,LOGOUT_USER
        ,GET_SAVED_CARD,REMOVE_SAVED_CARD,GET_CHANGE_PASSWORD,DRIVER_PROFILE_UPDATE,LANG_TYPE,LAST_LOCATION,TRANSACTION_DETAIL,GET_LONG_CITY_LIST,ADD_lONG_CITY,GET_COLOR_LIST,DRIVER_BREAK_DOWN,PARTIAL_PAYMENT,QBID_SEND_TOSERVER
   }
    public enum HTTP_REQUEST_TYPE{
        GET,POST
    }

    public static final int RETROFIT_TIMEOUT=30000;
    public static final int RETROFIT_LONG_TIMEOUT=60000;

    public static boolean isNetworkAvailable(Context _context) {
        ConnectivityManager cm = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean isMyServiceRunning(Class<?> serviceClass,Context _context) {
        ActivityManager manager = (ActivityManager) _context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public static String getDateCurrentTimeZone(long timestamp) {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        }catch (Exception e) {
        }
        return "";
    }

    public static String getFormattedDate(String bookingTime){
        String dateText="";
        try {
            long val = Long.parseLong(bookingTime);
            Date date = new Date(val);
            SimpleDateFormat df2 = new SimpleDateFormat("dd EEEE', 'KK:mm a");
            dateText = df2.format(date);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return dateText;
    }


    public static String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    public static String getDeviceName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            if (model.startsWith(manufacturer)) {
                return capitalize(model);
            }
            return capitalize(manufacturer) + " " + model;
        }
        catch(Exception e){

        }
        return "";
    }
    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }

}
