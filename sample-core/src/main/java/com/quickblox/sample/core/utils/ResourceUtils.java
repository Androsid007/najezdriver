package com.quickblox.sample.core.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.quickblox.sample.core.CoreApp;

public class ResourceUtils {

    public static String getString(@StringRes int stringId) {
        return CoreApp.getInstance().getString(stringId);
    }

    public static Drawable getDrawable(@DrawableRes int drawableId) {
        return CoreApp.getInstance().getResources().getDrawable(drawableId);
    }

    public static int getColor(@ColorRes int colorId) {
        try {
            if (CoreApp.getInstance() != null) {
                return CoreApp.getInstance().getResources().getColor(colorId);
            }
            return 0;
        }
        catch (Exception e){
            return 0;
        }
    }

    public static int getDimen(@DimenRes int dimenId) {
        try {
            return (int) CoreApp.getInstance().getResources().getDimension(dimenId);
        }
        catch (Exception e) {
            return 0;
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

}
